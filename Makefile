# makefile for gensched (linux only  at present, windows build planned)

# current version
VERSION=0.7
export VERSION

APP_SHORT=gensched
APP=$(APP_SHORT)$(APPEXT)
APP_TEST=testapp

MAKEFLAGS += --no-builtin-rules

.PHONY: demo-html demo-pdf dox p_build_doc

# this target is actually a file, but declaring it as PHONY target ensures it gets rebuilded each time
.PHONY: VERSION.txt

# don't remove secondary files
.SECONDARY:

# local folders
CONFIG_SRC_DIR=config
BUILD_DIR=build
BUILD_DIR_DEMO=$(BUILD_DIR)$(PATHSEP)html$(PATHSEP)demo
SRC_DIR=src
SRC_DIR_TEST=src/test
SRC_DIR_APP=src/main
SRC_DIR_DOC=doc
INCLUDE_DIR=include
OBJ_DIR=$(BUILD_DIR)/obj
OBJ_DIR_TEST=$(BUILD_DIR)/obj/test
OBJ_DIR_APP=$(BUILD_DIR)/obj/app

ARCHIVE_SRC_NAME=$(APP_SHORT)-src_$(VERSION)_$(NOW2).$(ARCHIVE_EXT)
ARCHIVE_BIN_NAME=$(APP_SHORT)-bin_$(OS)_$(VERSION)_$(NOW2).$(ARCHIVE_EXT)
ARCHIVE_BIN_FULL_NAME=$(APP_SHORT)-bin-full_$(OS)_$(VERSION)_$(NOW2).$(ARCHIVE_EXT)


ifndef ComSpec
#--------------------------------------------------------------------
# Linux specific stuff
NOW=$(shell date +%Y%m%d_%H%M)
NOW2=$(shell date +%Y%m%d)
export NOW2

CMDSEP=;
CMDPREF=./
CMDEXT=sh
APPEXT=
PATHSEP2=/
RM=rm
CP=cp
MV=mv
MKDIR=mkdir -p
RMDIR=rmdir
NULL=/dev/null
PREFIX=./
CONFIG_DIR=/etc/$(APP)
INSTALL_DIR=/usr/local/bin
OS=$(shell uname -s)_$(shell uname -p)
LDFLAGS+=-L/usr/lib

ARCHIVE_PROG=tar
ARCHIVE_OPT=cfz
ARCHIVE_EXT=tar.gz

TEMP=/tmp

else
include misc/makefile_Windows
endif

#--------------------------------------------------------------------
LDFLAGS += -lboost_filesystem -lboost_system

# to use this as path separator, we need to remove the trailing spaces
PATHSEP3=$(strip $(PATHSEP2))
PATHSEP:=$(PATHSEP3)

ifdef ComSpec
PATHSEP=$(subst \\,\,$(PATHSEP3))
endif

# additional scripts
INSTALL_SCRIPT=install.$(CMDEXT)
UNINSTALL_SCRIPT=uninstall.$(CMDEXT)

# general compiler flags
#CFLAGS = -std=c++0x -W -Wall -Wextra -s -O2 -Iinclude -Isrc/other -DGENSCHED_VERSION="\"$(VERSION)\"" -DBUILD_DATE=$(NOW2) -DSVN_REV="\"$(SVN_REV)\""
CFLAGS = -std=c++11 -W -Wall -Wextra -s -O2 -Iinclude -Isrc/other -DGENSCHED_VERSION="\"$(VERSION)\"" -DBUILD_DATE=$(NOW2)

# if test mode, define symbol for compilation
test: CFLAGS+=-DTESTMODE
test2: CFLAGS+=-DTESTMODE

# ==========================================================================
# COMMAND-LINE OPTIONS: START

# To enable debug builds, pass DEBUG=yes on makefile command-line
ifeq "$(DEBUG)" ""
	DEBUG=no
else
	DEBUG=yes
endif

ifeq "$(DEBUG)" "yes"
	CFLAGS += -g -DDEBUG
	DEMO_FLAGS += -d
endif

ifeq "$(LOG)" ""
	LOG=no
else
	LOG=yes
endif
ifeq "$(LOG)" "no"
	L=@
endif

# Demo build: add Debug flag
ifeq "$(DD)" ""
	DD=no
else
	DD=yes
endif
ifeq "$(DD)" "yes"
	DEMO_FLAGS += -d
endif

# Demo build: add Verbose flag
ifeq "$(DV)" ""
	DV=no
else
	DV=yes
endif
ifeq "$(DV)" "yes"
	DEMO_FLAGS += -v
endif

# Demo build: add "French" flag
ifeq "$(DF)" ""
	DF=no
else
	DF=yes
endif
ifeq "$(DF)" "yes"
	DEMO_FLAGS += -l fr
endif


# ==========================================================================

# if Windows, add Boost path (unneeded on Linux)
ifdef ComSpec
	CFLAGS += -I$(BOOST_WIN32_HEADERS)
	LDFLAGS += -L$(BOOST_WIN32_LIBS)
endif

# generated variables
SRC_FILES      = $(wildcard $(SRC_DIR)/*.cpp)
SRC_FILES_APP  = $(wildcard $(SRC_DIR_APP)/*.cpp)
SRC_FILES_TEST = $(wildcard $(SRC_DIR_TEST)/*.cpp)
HEAD_FILES     = $(wildcard $(INCLUDE_DIR)/*.h)
DITAA_IN_FILES = $(wildcard src/img/*.dit)
DEMO_IN_FILES  = $(wildcard demo/demoA*.csv)
DEMO_IN_FILES  += $(wildcard demo/demoB*.csv)
DEMO_IN_FILES  += $(wildcard demo/demoC*.csv)
DEMO_IN_FILES  += $(wildcard demo/demoE*.csv)
CONFIG_FILES   = $(wildcard $(CONFIG_SRC_DIR)/*.ini)
CONFIG_FILES  += $(wildcard $(CONFIG_SRC_DIR)/*.css)
CONFIG_FILES  += $(wildcard $(CONFIG_SRC_DIR)/*.js)
SRC_DOC_FILES  = $(wildcard doc/*.dox)

DEMO_CONFIG_FILES = $(wildcard demo/*.ini)

OBJ_FILES       = $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))
OBJ_FILES_TEST  = $(patsubst $(SRC_DIR_TEST)/%.cpp,$(OBJ_DIR_TEST)/%.o,$(SRC_FILES_TEST))
OBJ_FILES_APP   = $(patsubst $(SRC_DIR_APP)/%.cpp,$(OBJ_DIR_APP)/%.o,$(SRC_FILES_APP))

EXE_FILES       = $(patsubst $(SRC_DIR_APP)/%.cpp,$(BUILD_DIR)/%$(APPEXT),$(SRC_FILES_APP))

DITAA_OUT_FILES = $(patsubst src/img/%.dit,$(BUILD_DIR)/img/%.png,$(DITAA_IN_FILES))

DEMO_OUT_FILES  = $(patsubst demo/%.csv,$(BUILD_DIR_DEMO)/%/index.html,$(DEMO_IN_FILES))
DEMO_OUT_PDF_FILES  = $(patsubst demo/%.csv,$(BUILD_DIR_DEMO)/%/sched.pdf,$(DEMO_IN_FILES))

# we manually add demoD to the output files, because no pdf produced for full-1
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoD_full-00/index.html
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoD_full-01/index.html
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoD_full-10/index.html
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoD_full-11/index.html
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoD2/index.html
DEMO_OUT_FILES  += $(BUILD_DIR_DEMO)/demoE_2/index.html


SCREENSH_FILES  = $(patsubst demo/%.csv,$(BUILD_DIR)/img/%_cropped.png,$(DEMO_IN_FILES))

DEMO_FOLDERS = $(patsubst $(BUILD_DIR)/html/demo/%/index.html,%,$(DEMO_OUT_FILES))
TIDY_FILES_IN = $(foreach dir,$(DEMO_FOLDERS),$(wildcard $(BUILD_DIR)/html/demo/$(dir)/*.html))
TIDY_FILES_OUT = $(patsubst $(BUILD_DIR)/html/demo/%.html,$(BUILD_DIR)/tidy/%.stderr,$(TIDY_FILES_IN))


all:  $(EXE_FILES)
	@echo "-target '$(@F)': done."

#$(EXE_FILES): build_folders
#all: $(BUILD_DIR)$(PATHSEP)$(APP)
#	@echo "-target '$(@F)': done."


line="----------------------------------------------------"
# this target is only for debugging purposes
show:
	@echo "-EXE_FILES=$(EXE_FILES)"
	@echo $(line)
	@echo "-SRC_FILES=$(SRC_FILES)"
	@echo "-SRC_FILES_APP=$(SRC_FILES_APP)"
	@echo "-SRC_FILES_TEST=$(SRC_FILES_TEST)"
	@echo $(line)
	@echo "-OBJ_FILES=$(OBJ_FILES)"
	@echo "-OBJ_FILES_APP=$(OBJ_FILES_APP)"
	@echo "-OBJ_FILES_TEST=$(OBJ_FILES_TEST)"
	@echo $(line)
	@echo "-OBJ_DIR=$(OBJ_DIR)"
	@echo "-OBJ_DIR_APP=$(OBJ_DIR_APP)"
	@echo "-OBJ_DIR_TEST=$(OBJ_DIR_TEST)"
	@echo $(line)
	@echo "-DITAA_OUT_FILES=$(DITAA_OUT_FILES)"
	@echo "-DITAA_IN_FILES=$(DITAA_IN_FILES)"
	@echo "-DEMO_OUT_FILES=$(DEMO_OUT_FILES)"
	@echo "-DEMO_OUT_PDF_FILES=$(DEMO_OUT_PDF_FILES)"
	@echo "-DEMO_IN_FILES=$(DEMO_IN_FILES)"
	@echo "-CONFIG_FILES=$(CONFIG_FILES)"
	@echo "-SCREENSH_FILES=$(SCREENSH_FILES)"
	@echo "-DEMO_CONFIG_FILES=$(DEMO_CONFIG_FILES)"
	@echo "-CFLAGS=$(CFLAGS)"
	@echo "-LDFLAGS=$(LDFLAGS)"
	@echo "-ARCHIVE_SRC_NAME=$(ARCHIVE_SRC_NAME)"
	@echo "-ARCHIVE_BIN_NAME=$(ARCHIVE_BIN_NAME)"
	@echo "-ARCHIVE_BIN_FULL_NAME=$(ARCHIVE_BIN_FULL_NAME)"
	@echo "-DIST_BIN_CONTENT=$(DIST_BIN_CONTENT)"
	@echo "-DEMO_FOLDERS=$(DEMO_FOLDERS)"
#	@echo "-TIDY_FILES_IN=$(TIDY_FILES_IN)"
#	@echo "-TIDY_FILES_OUT=$(TIDY_FILES_OUT)"
#	@echo "-TIDY_FILES_OUT2=$(TIDY_FILES_OUT2)"

help:
	@echo " available targets:"
	@echo " - all: builds app (default target)"
	@echo " - cleanobj: erases obj files"
	@echo " - cleanbin: erases binary files"
	@echo " - clean: erases some crap files"
	@echo " - cleandoc: erases doc files"
	@echo " - cleandemo: erases demo files"
	@echo " - cleanall: all of the above cleanxxx targets"
	@echo ""
	@echo " - check-output: processes all the generated demo html files through tidy and detect any html violation"
	@echo " - doc: generates user documentation: doxygen + producing demo sample files"
	@echo " - dox: generates only doxygen generated files"
	@echo " - doc-dev: generates full doxygen generated files: user manual and source documentation"
	@echo ""
	@echo " - install: install on local machine"
	@echo " - install-bin: install on local machine (only binaries)"
	@echo " - uninstall:"
	@echo ""
	@echo "Options:"
	@echo " - DEBUG={yes|no}: if yes, builds debug version"
	@echo " - LOG={yes|no}: if yes, prints out build steps"
	@echo " - DD={yes|no}: if yes, adds the "debug" flag to demo build"
	@echo " - DV={yes|no}: if yes, adds the "verbose" flag to demo build"


#-------------------------------------------------------------
check:
	cppcheck --enable=all src -I include 2>cppcheck.txt
	@xdg-open cppcheck.txt

#-------------------------------------------------------------
# erase object files (and binary)
cleanobj:
	$(L)find $(OBJ_DIR)/* -type f -name "*.o" -delete

cleanbin:
	$(L)-$(RM) $(BUILD_DIR)/$(APP)

# erases produced html doc
cleandoc:
	$(L)-$(RM) -r $(BUILD_DIR)/html/* $(BUILD_DIR)/*.stderr

# erases unnecessary files generated during some steps of build process
clean:
	$(L)-$(RM) $(BUILD_DIR)/*_std*.txt
	$(L)find . -type f -name '*~' -delete
	@echo "-target '$(@F)': done."

# cleans all generated demos (but not the demo sources!)
cleandemo:
	$(L)-$(RM) -r $(BUILD_DIR_DEMO)/* 2> $(NULL)
	$(L)-$(RM) -r $(BUILD_DIR)/demo*.stderr 2> $(NULL)
	@echo "-target '$(@F)': done."

# all of the previous 'clean*' targets, also erases program
cleanall: clean cleandoc cleandemo cleanobj cleanbin
	$(L)-$(RM) $(BUILD_DIR)/$(APP)
	@echo "-target '$(@F)': done."

#-------------------------------------------------------------
# generates developper docs
doc-dev: DOX_FILE=doxyfile_dev.dox
doc-dev: build_doc
	@echo "-target '$(@F)': done."

# generates full user documentation (with screenshots)
doc: $(SCREENSH_FILES) dox
	@echo "-target '$(@F)': done."

# generates doxygen-generated pages only, an copies all necessary files into the 'html' folder:
# demos and images, but not the (scripted) screenshots
dox: DOX_FILE=doxyfile.dox
dox: build_folders build_doc_0
	@echo "-target '$(@F)': done."

# only doxygen run, with misc generated files
build_doc_0: doc_misc $(DITAA_OUT_FILES) $(BUILD_DIR)/html/index.html $(BUILD_DIR)/html/gpl.txt
#	xdg-open build/html/index.html
	@echo "-target '$(@F)': done."

# same as build_doc_0: but also builds demo files
build_doc_1: build_doc_0 demo-html
	@echo "-target '$(@F)': done."

# same as build_doc_1: but also builds demo pdf files
build_doc_2: build_doc_1 demo-pdf
	@echo "-target '$(@F)': done."


# copy gpl licence into doc output folder
$(BUILD_DIR)/html/gpl.txt: gpl.txt
	$(L)$(CP) $< $@

# build the programs default values to be copied in user doc
doc_misc: $(BUILD_DIR)/sample_option_f.txt $(BUILD_DIR)/sample_option_h.txt

$(BUILD_DIR)/sample_option_f.txt: all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) -f > $(BUILD_DIR)/sample_option_f.txt
$(BUILD_DIR)/sample_option_h.txt: all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) -h > $(BUILD_DIR)/sample_option_h.txt

# generation of html documentation with doxygen
$(BUILD_DIR)/html/index.html: $(SRC_DOC_FILES) $(HEAD_FILES) $(SRC_FILES) misc/mystylesheet.css build_folders $(DOX_FILE)
	@echo "-starting doxygen"
	$(L)doxygen $(DOX_FILE) 1>$(BUILD_DIR)/doxygen_stdout.txt 2>$(BUILD_DIR)/doxygen_stderr.txt

build_folders:
	$(L)-$(MKDIR) $(BUILD_DIR_DEMO)
	$(L)-$(MKDIR) $(BUILD_DIR)/img
	$(L)-$(MKDIR) $(OBJ_DIR)
	$(L)-$(MKDIR) $(OBJ_DIR_TEST)
	$(L)-$(MKDIR) $(OBJ_DIR_APP)

#-------------------------------------------------------------
install: all doc
	$(PREFIX)$(INSTALL_SCRIPT)
	@echo "-target '$(@F)': done."

install-bin: all
	$(PREFIX)$(INSTALL_SCRIPT)
	@echo "-target '$(@F)': done."

uninstall:
	$(PREFIX)$(UNINSTALL_SCRIPT)
	@echo "-target '$(@F)': done."


#-------------------------------------------------------------
dist: dist-bin dist-bin-full dist-src

# this is the list of stuff that is passed to the archiver tool. If "full" distrib required, then we add the html pages
DIST_BIN_CONTENT=$(BUILD_DIR)$(PATHSEP)$(APP) $(CONFIG_SRC_DIR) $(INSTALL_SCRIPT) $(UNINSTALL_SCRIPT) README.txt VERSION.txt gpl.txt demo/*
dist-bin-full: DIST_BIN_CONTENT+=$(BUILD_DIR)/html

VERSION.txt:
	$(L)@echo "This file is automatically generated by makefile at build time" > VERSION.txt
	$(L)@echo "$(APP) software:" >> VERSION.txt
	$(L)@echo " -version: $(VERSION)" >> VERSION.txt
	$(L)@echo " -build date: $(NOW2)" >> VERSION.txt
	$(L)@echo " -build platform: $(OS)" >> VERSION.txt

# builds source archive, from svn repository
dist-src: VERSION.txt clean
	$(L)-$(RM) -r $(TEMP)$(PATHSEP)$(APP)$(PATHSEP)*
	$(L)-$(RMDIR) $(TEMP)$(PATHSEP)$(APP)
	# export from repository
	$(L)svn export . $(TEMP)$(PATHSEP)$(APP) >/dev/null
	# add version info to archive
	$(L)$(CP) VERSION.txt $(TEMP)$(PATHSEP)$(APP)
	# build archive
	$(L)cd $(TEMP) $(CMDSEP) $(ARCHIVE_PROG) $(ARCHIVE_OPT) $(ARCHIVE_SRC_NAME) $(APP)
	$(L)-$(CP) $(TEMP)$(PATHSEP)$(ARCHIVE_SRC_NAME) .
	@echo "-target '$(@F)': done."

# builds binary release, holds binaries and install script. No manual
dist-bin: VERSION.txt $(INSTALL_SCRIPT) $(UNINSTALL_SCRIPT) all
	$(L)$(ARCHIVE_PROG) $(ARCHIVE_OPT) $(ARCHIVE_BIN_NAME) $(DIST_BIN_CONTENT)
	@echo "-target '$(@F)': done."

# build full binary release, included documentation
dist-bin-full: VERSION.txt all doc
	$(L)$(ARCHIVE_PROG) $(ARCHIVE_OPT) $(ARCHIVE_BIN_FULL_NAME) $(DIST_BIN_CONTENT)
	@echo "-target '$(@F)': done."

#-------------------------------------------------------------
# builds demos
#demo: build_folders $(DEMO_OUT_FILES) tidy_samples
demo-html: build_folders $(DEMO_OUT_FILES) $(BUILD_DIR)/sdiff.txt
	@echo "-target '$(@F)': done."

demo-pdf: demo-html $(DEMO_OUT_PDF_FILES)
	@echo "-target '$(@F)': done."

check-output: $(TIDY_FILES_OUT)
	@echo "-target $@: done."

# tidy_process, related to target 'check-output'
# stops build only on error (return value=2), see http://stackoverflow.com/questions/26849036/
$(BUILD_DIR)/tidy/%.stderr: $(BUILD_DIR)/html/demo/%.html
	@mkdir -p $(@D)
	@tidy -e -q < $< 1>/dev/null 2>$@; [ $$? -ne 2 ]

#---------------------------------
# these are ditaa generated drawings. Thumbs up for ditaa !
# matches $(DITAA_OUT_FILES)
$(BUILD_DIR)/img/%.png: src/img/%.dit build_folders
	$(L)ditaa -r -o $< $@ 1>$(BUILD_DIR)/ditaa_stdout.txt 2>$(BUILD_DIR)/ditaa_stderr.txt
	@echo "- Done ditaa on $<"


# copy demo files into the 'build' folder
$(BUILD_DIR_DEMO)/demo%.csv: demo/demo%.csv build_folders
	$(L)$(CP) $< $@

$(BUILD_DIR_DEMO)/%.ref: demo/%.ref build_folders
	$(L)$(CP) $< $@

$(BUILD_DIR_DEMO)/%.ini: demo/%.ini build_folders
	$(L)$(CP) $< $@

# creates the four identical input files for "full" demo from the same source file
$(BUILD_DIR_DEMO)/demoD_full-%.csv: demo/demoD_full.csv build_folders
	$(L)$(CP) $< $@

$(BUILD_DIR_DEMO)/demoD2.csv: demo/demoD_full.csv build_folders
	$(L)$(CP) $< $@

$(BUILD_DIR_DEMO)/demoE_2.csv: demo/demoE_1.csv build_folders
	$(L)$(CP) $< $@

# copying the groups files for "full" demo
$(BUILD_DIR_DEMO)/groups_demoD_full.ini: demo/groups_demoD_full.ini build_folders
	$(L)$(CP) $< $@

#	@echo "-target $@: done."


# -------------------------------------------------------------
# HERE, FIVE RULES FOR BUILDING HTML DEMO FILES

# demoA: single file samples
$(BUILD_DIR_DEMO)$(PATHSEP)demoA_%$(PATHSEP)index.html: $(BUILD_DIR_DEMO)$(PATHSEP)demoA_%.csv all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) $< 2>$(BUILD_DIR)$(PATHSEP)demoA_$*.stderr
	@echo "-target $@: done."

# demoB: groups file provided
$(BUILD_DIR_DEMO)/demoB_%/index.html: $(BUILD_DIR_DEMO)/demoB_%.csv $(BUILD_DIR_DEMO)/groups_demoB_%.ini all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) -g $(BUILD_DIR_DEMO)/groups_demoB_$*.ini $< 2>$(BUILD_DIR)/demoB_$*.stderr
	@echo "-target $@: done."

# demoC: configuration file provided, but no groups file
$(BUILD_DIR_DEMO)/demoC_%/index.html: $(BUILD_DIR_DEMO)/demoC_%.csv $(BUILD_DIR_DEMO)/demoC_%.ini all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) -p $(BUILD_DIR_DEMO)/demoC_$*.ini $< 2>$(BUILD_DIR)/demoC_$*.stderr
	@echo "-target $@: done."

# generating html full sample, with configuration and groups file
$(BUILD_DIR_DEMO)/demoD_%/index.html: $(BUILD_DIR_DEMO)/demoD_%.csv $(BUILD_DIR_DEMO)/demoD_%_params.ini $(BUILD_DIR_DEMO)/groups_demoD_full.ini all $(BUILD_DIR_DEMO)/demoD_full.ref all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) -p $(BUILD_DIR_DEMO)/demoD_$*_params.ini -g $(BUILD_DIR_DEMO)/groups_demoD_full.ini -c $< 2>$(BUILD_DIR)/demoD_$*.stderr
	@echo "-target $@: done."

# demoE: special demo showing the year leap
$(BUILD_DIR_DEMO)/demoE_1/index.html: $(BUILD_DIR_DEMO)/demoE_1.csv all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) -w "50,51,2-4" $(DEMO_FLAGS) $< 2>$(BUILD_DIR)/demoE_1.stderr
	@echo "-target $@: done."
$(BUILD_DIR_DEMO)/demoE_2/index.html: $(BUILD_DIR_DEMO)/demoE_2.csv all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) $< 2>$(BUILD_DIR)/demoE_2.stderr
	@echo "-target $@: done."

# generating a sample run for sdiff
$(BUILD_DIR)/sdiff.txt: demo/demoD_full.csv demo/demoD_full_edit.csv
	$(L)$(BUILD_DIR)$(PATHSEP)sdiff demo/demoD_full.csv demo/demoD_full_edit.csv 1>build/sdiff.txt 2>build/sdfiff_stderr.txt

# demoD2: generating a sample run with weeks given on command-line. Input data is the same as demoD
# generating html full sample, with configuration and groups file
$(BUILD_DIR_DEMO)/demoD2/index.html: $(BUILD_DIR_DEMO)/demoD2.csv $(BUILD_DIR_DEMO)/demoD_full-00_params.ini $(BUILD_DIR_DEMO)/groups_demoD_full.ini all $(BUILD_DIR_DEMO)/demoD_full.ref all
	$(L)$(BUILD_DIR)$(PATHSEP)$(APP) $(DEMO_FLAGS) -w "40-41" -p $(BUILD_DIR_DEMO)/demoD_full-00_params.ini -g $(BUILD_DIR_DEMO)/groups_demoD_full.ini -c $< 2>$(BUILD_DIR)/demoD2_stderr.txt
	@echo "-target $@: done."


# -------------------------------------------------------------
# generating pdf files of calendar view
$(BUILD_DIR)/html/demo/demo%/sched.pdf: $(BUILD_DIR)/html/demo/demo%/sched.html
	$(L)wkhtmltopdf --quiet --orientation Landscape $< $@
	$(L)pdftk $@ cat 2-end output $(BUILD_DIR)/html/demo/demo$*/temp.pdf
	$(L)mv $(BUILD_DIR)/html/demo/demo$*/temp.pdf $(BUILD_DIR)/html/demo/demo$*/sched.pdf
	@echo "-target $@: done."

$(BUILD_DIR)/html/demo/demo%/sched.html: $(BUILD_DIR)/html/demo/demo%/index.html
	@echo "-target $@: done."

# generic rule for building screenshots
# matches $(SCREENSH_FILES)
$(BUILD_DIR)/img/%_cropped.png: $(BUILD_DIR_DEMO)/%.csv all
#	@echo "- Running screenshots scripts"
	$(L)xdg-open $(BUILD_DIR_DEMO)/$*/sched.html
	$(L)scrot $(BUILD_DIR)/img/$*.png -d 1 -u
	$(L)convert $(BUILD_DIR)/img/$*.png -crop 950x400+0+250 -resize 70% $(BUILD_DIR)/img/$*_cropped.png
	@echo "-target $@: done."

#-------------------------------------------------------------
# compiling source files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(HEAD_FILES)
	@echo " - compiling file $@"
	$(L)$(CXX) -o $@ -c $< $(CFLAGS)

# compiling apps (main functions)
$(OBJ_DIR_APP)$(PATHSEP)%.o: $(SRC_DIR_APP)/%.cpp $(HEAD_FILES)
	@echo " - compiling file $@"
	$(L)$(CXX) -o$@ -c $<  $(CFLAGS)

# compiling test source files
$(OBJ_DIR_TEST)$(PATHSEP)%.o: $(SRC_DIR_TEST)/%.cpp $(HEAD_FILES)
	@echo " - compiling file $@"
	$(L)$(CXX) -o $@ -c $< $(CFLAGS)

# linking app files
$(BUILD_DIR)$(PATHSEP)%$(APPEXT): $(OBJ_DIR_APP)/%.o $(OBJ_FILES) Makefile
	@echo " - linking file $@"
	$(L)$(CXX) -o $@ -s  $< $(OBJ_FILES) $(LDFLAGS)
	@echo "-target $@: done."

# linking test app
$(BUILD_DIR)$(PATHSEP)$(APP_TEST): $(OBJ_FILES_TEST) $(OBJ_FILES) Makefile
	@echo " - linking file $(APP_TEST)"
	$(L)$(CXX) -o $@ -s $(OBJ_FILES_TEST) $(OBJ_FILES) $(LDFLAGS)
	@echo "-target $@: done."

#-------------------------------------------------------------
diff:
	git diff | colordiff | aha > diff.html
	xdg-open diff.html

test: build_folders $(BUILD_DIR)/$(APP_TEST)
	@echo "- Running tests"
	$(BUILD_DIR)/$(APP_TEST)
	@echo "-target $@: done."

test2: build/testfile_catch
	@echo "- Running catch tests"
	build/testfile_catch
	@echo "-target $@: done."

# link catch test
build/testfile_catch: build/testfile_catch.o $(OBJ_FILES)
	$(L)$(CXX) -o $@ -s build/testfile_catch.o $(OBJ_FILES) $(LDFLAGS)
	@echo "-target $@: done."

# compile catch test
build/testfile_catch.o: src/test_catch/testfile_catch.cpp
	$(L)$(CXX) -o $@ -c $< $(CFLAGS)
	@echo "-target $@: done."

