/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// params.cpp

#include "globals.h"
#include "params.h"
#include "helper_functions.h"
#include "cal_data.h"
#include "htag.h"
#include "error.h"
#include "strings.h"

#include <iomanip>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

namespace boost_pt = boost::property_tree;

void AssignDefaults_TS( std::vector<TimeSlot>& v_ts );
void AssignDefaults_WD( std::vector<WeekDay>&  v_wd );
void ReadVectorFromIniFile( boost_pt::ptree& pt, size_t nb, std::vector<PairCodeValue>& vect, const std::string& prefix );

/// Read a \c float from property tree \c pt, only if found
#define READ_INI_FLOAT( a, b ) \
	{ \
		std::string s = pt.get( b, "" ); \
		if( s.size() != 0 ) \
			a =  STRING2FLOAT( s ); \
	}

/// Read an \c int from property tree \c pt, only if found
#define READ_INI_INT( a, b ) \
	{ \
		std::string s = pt.get( b, "" ); \
		if( s.size() != 0 ) \
			a =  STRING2INT( s ); \
	}

#define READ_INI_BOOL( a, b ) \
	{ \
		std::string s = pt.get( b, "" ); \
		if( s.size() != 0 ) \
			a =  static_cast<bool>(STRING2INT( s )); \
	}


//-----------------------------------------------------------------------------------
/// Constructor, assigns default values
Params::Params() :
	CSV_delim( ',' )
	, atomic_duration( 1.0f )
	, MaxDuration( 5 )
	, NbTimeSlotsAM( 3 )
{
	v_index_name =
	{
		"week",
		"day",
		"timeslot",
		"group",
		"type",
		"subject",
		"room",
		"instructor",
		"duration",
		"comment"
	};


// input file indexes default values
	for( size_t i=0; i<IT_MAXVALUE_INPUT_INDEX; i++ )
	{
		input_pos.push_back(i);
		m_input_to_names[ input_pos[i] ] = i;
	}

	nb_tokens = *std::max_element( input_pos.begin(), input_pos.end() ) +1; // +1 because if highest index if, say, 9, then it means we have 10 tokens

	NbCSSColors     = 22; // this is related to the number of colors in provided CSS file

	unknown_subject_code    = "S???";
	unknown_instructor_code = "M. X";
	unknown_room_code       = "RRR";

// assign default values to days and weekdays
	AssignDefaults_TS( _v_timeslots );
	AssignDefaults_WD( _v_weekdays );

	_v_gtypes.push_back( std::make_pair( "AG",   GT_ATOMIC  ) ); // Atomic Group
	_v_gtypes.push_back( std::make_pair( "CG",   GT_CLASS  ) );  // Class Group
	_v_gtypes.push_back( std::make_pair( "DG",   GT_DIV  ) );    // Division Group
	_v_gtypes.push_back( std::make_pair( "N/A",  GT_NONE  ) );    // None

	_v_etypes.push_back( std::make_pair( "*OFF", ET_DAY_OFF ) );    // Day off (special event)
	_v_etypes.push_back( std::make_pair( "*SPE", ET_SPECIAL ) );    // special event

// assigning default values to boolean parameters
	for( size_t i=0; i<BS_DUMMY; ++i )
		bswitch[ (EN_BOOL_SWITCH)i ] = true;
	bswitch[ BS_ONE_PAGE_PER_DIV ] = false;
	bswitch[ BS_ONE_PAGE_PER_WEEK ] = false;
}

//-----------------------------------------------------------------------------------
/// computes the event type and group from two string taken from input file
/**
- with default parameters, type can be "AG,CG,DG,*OFF,*SPE"

- if \c groupType = AG,CG or DG, then \c eventType will be set to "teaching" (\c ET_TEACH)
- else, the group type is undetermined, and will be set later
*/
void Params::ComputeType( std::string type, EN_EVENT_TYPE& eventType, EN_GROUP_TYPE& groupType )
{
	bool found = false;
	for( const auto& gt: _v_gtypes )
		if( gt.first == type )
		{
			groupType = gt.second;
			eventType = ET_TEACH;
			found = true;
			break;
		}

	if( !found ) // not a teaching event
	{
		for( const auto& et: _v_etypes )
			if( et.first == type )
			{
				eventType = et.second;
				found = true;
				break;
			}
	}
	if( !found )
		throw ERROR( "invalid type string", type );
}

//-----------------------------------------------------------------------------------
bool
Params::InputFieldsCheck()
{
	for( size_t i=0; i<IT_MAXVALUE_INPUT_INDEX; i++ )
		for( size_t j=0; j<IT_MAXVALUE_INPUT_INDEX; j++ )
			if( i != j )
				if( input_pos[i] == input_pos[j] )
				{
					CERR << "Error in field indexes: field " << i << " and field " << j << " have same value: " << input_pos[i] << ENDL;
					return false;
				}

	return true;
}
//-----------------------------------------------------------------------------------
/// Initialisation of global parameters
bool
Params::ReadParamsFromFile( const std::string& fname )
{
	DEBUG_IN;
    boost_pt::ptree pt;
    try
    {
    	boost_pt::read_ini( fname, pt );
    }
    catch(...)
    {
    	CERR << "exception in reading parameters ini file " << fname << ENDL;
		DEBUG_OUT;
    	return false;
    }

	unknown_subject_code    = pt.get( "unknown_data.unknown_subject_code",    unknown_subject_code  );
	unknown_instructor_code = pt.get( "unknown_data.unknown_instructor_code", unknown_instructor_code  );
	unknown_room_code       = pt.get( "unknown_data.unknown_room_code",       unknown_room_code );

	input_pos[IT_WEEK]       = pt.get( "InputFileFields.week",       input_pos[IT_WEEK] );
	input_pos[IT_DAY]        = pt.get( "InputFileFields.day",        input_pos[IT_DAY] );
	input_pos[IT_TIMESLOT]   = pt.get( "InputFileFields.timeslot",   input_pos[IT_TIMESLOT] );
	input_pos[IT_GROUP]      = pt.get( "InputFileFields.group",      input_pos[IT_GROUP] );
	input_pos[IT_TYPE]       = pt.get( "InputFileFields.type",       input_pos[IT_TYPE] );
	input_pos[IT_SUBJECT]    = pt.get( "InputFileFields.subject",    input_pos[IT_SUBJECT] );
	input_pos[IT_INSTRUCTOR] = pt.get( "InputFileFields.instructor", input_pos[IT_INSTRUCTOR] );
	input_pos[IT_ROOM]       = pt.get( "InputFileFields.room",       input_pos[IT_ROOM] );
	input_pos[IT_DURATION]   = pt.get( "InputFileFields.duration",   input_pos[IT_DURATION] );
	input_pos[IT_COMMENT]    = pt.get( "InputFileFields.comment",    input_pos[IT_COMMENT] );

	for( size_t i=0; i<IT_MAXVALUE_INPUT_INDEX; i++ )
		m_input_to_names[ input_pos[i] ] = i;

	nb_tokens = *std::max_element( input_pos.begin(), input_pos.end() ) +1; // +1 because if highest index if, say, 9, then it means we have 10 tokens

	if( false == InputFieldsCheck() )
	{
		CERR << "Error, inconsistent field indexes\n";
		DEBUG_OUT;
    	return false;
	}

	GroupsFileName = pt.get( "config.GroupsFileName", GroupsFileName );

	ReferenceFileName = pt.get( "config.ReferenceFileName", "" );
	if( ReferenceFileName.size() != 0 )
		HasReferenceFile = true;

	LanguageFileName =  pt.get( "config.LanguageFileName", LanguageFileName );

	READ_INI_FLOAT( atomic_duration,  "general.atomic_duration"  );
	READ_INI_INT(   NbTimeSlotsAM,    "general.NbTimeSlotsAM"    );

// reading of boolean parameters
	for( size_t i=0; i<BS_DUMMY; ++i )
	{
		std::string key_name = std::string("general.") + GetBoolSwitchString( (EN_BOOL_SWITCH)i );
		READ_INI_BOOL( bswitch[(EN_BOOL_SWITCH)i], key_name );
	}

// make an attempt to read days (code and value) from configuration file
	std::string n1 = pt.get( "general.NbDays", "" );
	if( n1.size() )
	{
		int NbDays;
		TRY_STRING2INT( n1, NbDays, "NbDays" );
		try
		{
			ReadVectorFromIniFile( pt, NbDays, _v_weekdays, "day" );
		}
		catch( const ErrorClass& err )
		{
			std::cerr << "Caught error in " << __FUNCTION__ << "(), general.NbDays=" << n1 << '\n';
			throw err;
		}
	}
	std::string n2 = pt.get( "general.NbTimeSlots", "" );
	if( n2.size() )
	{
		int NbTimeSlots;
		TRY_STRING2INT( n2, NbTimeSlots, "NbTimeSlot" );
		try
		{
			ReadVectorFromIniFile( pt, NbTimeSlots, _v_timeslots, "TS" );
		}
		catch( const ErrorClass& err )
		{
			std::cerr << "Caught error in " << __FUNCTION__ << "(), general.NbTimeSlots=" << n2 << '\n';
			throw err;
		}
	}

	NbCSSColors =  pt.get( "general.NbColors", NbCSSColors );

// read sectionning codes
	std::string sa = pt.get( "sectioning.atomic_name", "" );
	if( sa.size() )
	{
		_v_gtypes.clear();  // remove defaults
		_v_gtypes.push_back( std::make_pair( sa, GT_ATOMIC  ) );

		std::string sc = pt.get( "sectioning.class_name", "C" );
		if( sc.size() )
		{
			_v_gtypes.push_back( std::make_pair( sc, GT_CLASS  ) );

			std::string sd = pt.get( "sectioning.division_name", "D" );
			if( sd.size() )
				_v_gtypes.push_back( std::make_pair( sd, GT_DIV  ) );
		}
	}

	std::string sdo = pt.get( "sectioning.day_off", "*OFF" );
	if( !sdo.empty() )
	{
		_v_etypes.clear();
		_v_etypes.push_back( std::make_pair( sdo, ET_DAY_OFF  ) );

		std::string spe = pt.get( "sectioning.special", "*SPE" );
		if( spe.empty() )
		{
			throw ERROR( "missing event type code", "special event" );
		}
		_v_etypes.push_back( std::make_pair( spe, ET_SPECIAL  ) );
	}

	std::string str_year = pt.get( "calendar.year", "" );
	if( str_year.size() )
	{
		TRY_STRING2INT( str_year, g_caldata._current_year, "year string" );
		if( g_caldata._current_year > 2100 || g_caldata._current_year < 2000 ) // arbitrary...
		{
			std::cerr << "Invalid year string: \"" << str_year << "\" in configuration file " << fname << ", field \"calendar.year\"\n";
			std::cout << "Error, check stderr for details\n";
			throw ERROR( "Invalid year string", str_year );
		}
	}

	graphVolumePerWeek_width =  pt.get( "general.graphVolumePerWeek_width", graphVolumePerWeek_width );

	DEBUG_OUT;
	return true;
}

//-----------------------------------------------------------------------------------
/// Assign defaul Time Slot codes
void
AssignDefaults_TS( std::vector<TimeSlot>& v_ts )
{
	v_ts.push_back( TimeSlot( "M1", "9AM" ) );
	v_ts.push_back( TimeSlot( "M2", "10AM"  ) );
	v_ts.push_back( TimeSlot( "M3", "11AM"  ) );
	v_ts.push_back( TimeSlot( "A1", "2PM"  ) );
	v_ts.push_back( TimeSlot( "A2", "3PM") );
	v_ts.push_back( TimeSlot( "A3", "4PM"  ) );
	v_ts.push_back( TimeSlot( "A4", "5PM") );
}
//-----------------------------------------------------------------------------------
/// Assign default Week Day codes
void
AssignDefaults_WD( std::vector<WeekDay>& v_wd )
{
	v_wd.push_back( WeekDay( "M",  "Monday"   ) );
	v_wd.push_back( WeekDay( "Tu", "Tuesday"  ) );
	v_wd.push_back( WeekDay( "W",  "Wenesday" ) );
	v_wd.push_back( WeekDay( "Th", "Thursday" ) );
	v_wd.push_back( WeekDay( "F",  "Friday"   ) );
}
//-----------------------------------------------------------------------------------
void
Params::PrintParams( std::ostream& f ) const
{
	f << " - Input file fields positions: (starting from 1)\n";
	for( size_t i=0; i<IT_MAXVALUE_INPUT_INDEX; i++ )
		f << "  -index " << v_index_name[i] << " = " << input_pos[i]+1 << g_endl;

	f << " -unknown_subject_code=" << unknown_subject_code << g_endl;
	f << " -unknown_instructor_code=" << unknown_instructor_code << g_endl;
	f << " -unknown_room_code=" << unknown_room_code << g_endl;

	f << " -CSV_delim=" << CSV_delim << g_endl;
	f << " -nb_tokens=" << nb_tokens << g_endl;

	f << "Section: general:"
		<< "\n   -atomic_duration=" << atomic_duration
		<< "\n   -MaxDuration="     << (int)MaxDuration
		<< "\n   -NbTimeSlots="     << GetNbTimeSlots()
		<< "\n   - NbTimeSlotsAM="  << NbTimeSlotsAM
		<< "\n   - NbDays="         << GetNbDays()
		<< "\n   - NbCSSColors="    << NbCSSColors
		<< "\n   - OverrideYear="   << OverrideYear
		<< "\n   - WeekClString="   << WeekClString
		<< "\n   - DebugMode="      << DebugMode
		<< "\n   - VerboseMode="    << VerboseMode
		<< "\n   - graphVolumePerWeek_width=" << graphVolumePerWeek_width
		<< "\n";

	f << "- Group type codes: " << _v_gtypes.size() << g_endl;
	for( const auto& e: _v_gtypes )
		f << e.first << "-" << e.second << g_endl;

	f << "- Event type codes: " << _v_etypes.size() << g_endl;
	for( const auto& e: _v_etypes )
		f << e.first << "-" << e.second << g_endl;

	f << "- Week days codes/values:\n";
	for( const auto& e: _v_weekdays )
		f << "  - " << e.code << " : " << e.value << g_endl;

	f << "- Time slots codes/values:\n";
	for( const auto& e: _v_timeslots )
		f << "  - " << e.code << " : " << e.value << g_endl;

	f << "- Misc. switches\n";
	for( size_t i=0; i<BS_DUMMY; ++i )
		f << " - " << GetBoolSwitchString( (EN_BOOL_SWITCH)i ) << ": " << std::boolalpha << bswitch.at( (EN_BOOL_SWITCH)i ) << g_endl;
	f << g_endl;
}

//-----------------------------------------------------------------------------------
void
Params::Dump( std::ostream& f ) const
{
	f << "-------------------------------------\n";
	f << "Params:\n";

	PrintParams( f );

	f << " -OutputDirRootName=" << OutputDirRootName << ENDL;
	f << " -OutputDirFullName=" << OutputDirFullName << ENDL;
	f << " -GroupsFileName="    << GroupsFileName    << ENDL;
	f << " -HasReferenceFile="  << HasReferenceFile  << ENDL;
	f << " -ReferenceFileName=" << ReferenceFileName << ENDL;
	f << " -LanguageFileName="  << LanguageFileName  << ENDL;
	f << " -OverrideYear="      << OverrideYear      << ENDL;
	f << " -WeekClString="      << WeekClString      << ENDL;
	f << " -DebugMode="         << DebugMode         << ENDL;
	f << " -VerboseMode="       << VerboseMode       << ENDL;

	f << ENDL;
}

//-----------------------------------------------------------------------------------
std::string
Params::GetLinkOnWeekSchedule( int week_no, int div_idx, EN_LINK_TEXT lt, std::string txt ) const
{
	std::stringstream oss;
	oss << "<a href=\"sched";

	if( true == bswitch.at(BS_ONE_PAGE_PER_DIV) )
		oss << "_d" << (div_idx==-1 ? 1 : div_idx+1); // if "no division", then just show the first one

	if( true == bswitch.at(BS_ONE_PAGE_PER_WEEK) )
		oss << "_w" << week_no;

	oss << ".html";

	if( false == bswitch.at(BS_ONE_PAGE_PER_WEEK) )
		oss << "#s" << week_no;

	oss << "\">";
	switch( lt )
	{
		case LT_WEEK: oss /*<< "w"*/ << week_no; break;
		case LT_DIV:  oss << "d" << div_idx+1; break;
		case LT_PROVIDED:  oss << txt; break;
		default: assert(0);
	}
	oss << "</a>";

	return oss.str();
}

//-----------------------------------------------------------------------------------
std::string
Params::GetCodeFromEventType( EN_EVENT_TYPE type ) const
{
	for( const auto& et: _v_etypes )
		if( et.second == type )
			return et.first;
	if( type == ET_TEACH )
		return "-";

	CERR << "Error: in " << __FUNCTION__ << "(): invalid event typecode: " << (size_t)type << ENDL;
	throw ERROR( "invalid event type code", (size_t)type );
}

//-----------------------------------------------------------------------------------
std::string
Params::GetCodeFromGroupType( EN_GROUP_TYPE type ) const
{
	for( const auto& gt: _v_gtypes )
		if( gt.second == type )
			return gt.first;
	if( type == GT_NONE )
		return "(No Group)";

	CERR << "Error: in " << __FUNCTION__ << "(): invalid group typecode: " << (size_t)type << ENDL;
	throw ERROR( "invalid group type code", (size_t)type );
}


//-----------------------------------------------------------------------------------
uchar
Params::GetTimeSlotIndexFromCode( const std::string& slot_code ) const
{
	for( size_t i=0; i<_v_timeslots.size(); i++ )
		if( _v_timeslots[i].code == slot_code )
			return static_cast<uchar>(i);
	CERR << "Error: invalid time slot code: " << slot_code << ENDL;
	throw ERROR( "invalid time slot code", slot_code );
}

//-----------------------------------------------------------------------------------
size_t
Params::GetDayIndexFromCode( const std::string& day_code ) const
{
	DEBUG_IN;
	for( size_t i=0; i<_v_weekdays.size(); i++ )
		if( _v_weekdays[i].code == day_code )
		{
			DEBUG_OUT;
			return i;
		}
	CERR << "Error: invalid week day code: " << day_code << ENDL;
	DEBUG_OUT;
	throw ERROR( "invalid week day code", day_code );
}

//-----------------------------------------------------------------------------------
void
Params::SetInputFileName( const char* n )
{
	StringVector v_out;
	Tokenize( n, v_out, '.' );
	if( v_out.size() < 2 )
		throw ERROR( "filename has no extension", std::string(n) );

	if( v_out.size() > 2 )
		throw ERROR( "input filename parsing error", std::string(n) );

	if( v_out[1] != "csv" )
		throw ERROR( "file format not handled", n );

	OutputDirRootName = v_out[0];

	std::ostringstream oss;
	oss << OutputDirRootName;
//	cerr << "oss=" << oss.str() << endl;

	if( true == bswitch[BS_OUTPUT_TIME_STAMP] )
		oss << "_" << g_caldata.GetNow();

	OutputDirFullName = oss.str();
	OutputFile::s_OutputFolder = OutputDirFullName;
}

//-----------------------------------------------------------------------------------
/// Helper function for Params::ReadFromFile(), used to fetch weekdays code-values and timeslots code-values
void
ReadVectorFromIniFile( boost_pt::ptree& pt, size_t nb, std::vector<PairCodeValue>& vect, const std::string& prefix )
{
	std::ostringstream oss1_code, oss1_name;
	oss1_code << prefix << "_codes.";
	oss1_name << prefix << "_names.";

//	CERR << "Reading code:" << oss1_code.str() << " name:"<< oss1_name.str() << ENDL;
	vect.clear();
	for( size_t i=0; i<nb; i++ )
	{
		std::ostringstream oss2_code, oss2_name;
		oss2_code << oss1_code.str() << prefix << i;
		oss2_name << oss1_name.str() << prefix << i;

		std::string code = pt.get( oss2_code.str(), "" );
		std::string name = pt.get( oss2_name.str(), "" );
		if( !code.size() )
		{
			CERR << " Error: in " << __FUNCTION__ << ": unable to read string " << oss2_code.str() << " in ini file\n";
			throw ERROR( "unable to read string 'code' in ini file", oss2_code.str() );
		}
		if( !name.size() )
		{
			CERR << " Error: in " << __FUNCTION__ << ": unable to read string " << oss2_name.str() << " in ini file\n";
			throw ERROR( "unable to read string 'name' in ini file", oss2_name.str() );
		}
		vect.push_back( PairCodeValue( code, name ) );
	}
}

//-----------------------------------------------------------------------------------
void
Params::PrintToHtml( FileHTML& f ) const
{
	{
		HTAG p1( f, HT_P );
		p1.OpenTag();
		f.fout << "Configuration file: <tt>" << ConfigFileName << "</tt>";
		f.fout << ", read status: " << ( ConfigFileRead ? "Yes" : "no, using default values" ) << ".\n";
	}
	{
		HTAG ul( f, HT_OL );
		ul.OpenTag();

		HTAG li( f, HT_LI );
		li.OpenTag();
		f.fout << "Valid group codes/values:" << g_endl;
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			f.fout << g_lio << g_strings.GetTxt( S_GR_DIV ) << ": " << GetCodeFromGroupType(    GT_DIV )    << g_lic;
			f.fout << g_lio << g_strings.GetTxt( S_GR_CLASS ) << ": " << GetCodeFromGroupType(  GT_CLASS )  << g_lic;
			f.fout << g_lio << g_strings.GetTxt( S_GR_ATOMIC ) << ": " << GetCodeFromGroupType( GT_ATOMIC ) << g_lic;
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Valid week days codes/values:" << g_endl;
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();

			for( size_t i=0; i<GetNbDays(); i++ )
			{
				const auto& e = GetWeekday(i);
				HTAG li( f, HT_LI );
				li.OpenTag();
				f.fout << e.code << ": " << e.value;
			}
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Valid time slots codes/values:";
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();

			for( size_t i=0; i<GetNbTimeSlots(); i++ )
			{
				const auto& e = GetTimeSlot(i);
				HTAG li( f, HT_LI );
				li.OpenTag();
				f.fout << e.code << ": " << e.value;
			}
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Input file fields order:";
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			for( size_t i=0; i<IT_MAXVALUE_INPUT_INDEX; i++ )
				f.fout << g_lio << v_index_name[i] << " field position: " << input_pos[i]+1 << g_lic << g_endl;
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Unknown special field values:";
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			f.fout << g_lio << "Unknown instructor: \"" << unknown_instructor_code << '"' << g_lic;
			f.fout << g_lio << "Unknown room: \""       << unknown_room_code       << '"' << g_lic;
			f.fout << g_lio << "Unknown subject: \""    << unknown_subject_code    << '"' << g_lic;
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Misc. switches:";
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			f.fout << g_lio << "Atomic duration: " << atomic_duration << g_lic;
			f.fout << g_lio << "Number of morning time slots: " << NbTimeSlotsAM << g_lic;
			f.fout << g_lio << "csv delim character: " << CSV_delim << " (ASCII 0x" << std::hex << (int)CSV_delim << ')' << std::dec << g_lic;
			f.fout << g_lio << "requested language filename: " << (LanguageFileName.empty() ? "(none)" : LanguageFileName) << g_lic;
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << "Boolean switches:";
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			for( size_t i=0; i<BS_DUMMY; ++i )
				f.fout << g_lio << GetBoolSwitchString( (EN_BOOL_SWITCH)i ) << ": " << std::boolalpha << bswitch.at( (EN_BOOL_SWITCH)i ) << g_lic;
		}
	}
}

//-----------------------------------------------------------------------------------
/// Parsing of command-line arguments, \b except for configuration file, that is read before
void
Params::CommandLineAnalysis( int argc, const char** argv )
{
	VerboseMode = HasSingleOption( "-v", argc, argv );
	DebugMode   = HasSingleOption( "-d", argc, argv );

	bswitch[BS_OUTPUT_TIME_STAMP]  =  HasSingleOption( "-t", argc, argv );
	bswitch[BS_COLOR_PER_SUBJECT]  = !HasSingleOption( "-i", argc, argv );
	bswitch[BS_PRINT_EVENT_TYPE]   = !HasSingleOption( "-i", argc, argv );
	bswitch[BS_COPY_STYLE_SHEETS]  = !HasSingleOption( "-n", argc, argv );

	HasStringOption( "-w", argc, argv, WeekClString );
	HasStringOption( "-g", argc, argv, GroupsFileName );
//	HasStringOption( "-p", argc, argv, ConfigFileName );

	std::string cl_LangCode;
	if( HasStringOption( "-l", argc, argv, cl_LangCode ) )
		if( cl_LangCode != "en" )  // because english is the default, so no need to load anything
		{
			std::ostringstream oss;
			oss << "LF_" << cl_LangCode << ".ini"; // build Language File filename
			LanguageFileName = oss.str();
		}

	std::string year;
	if( HasStringOption( "-y", argc, argv, year ) )
		TRY_STRING2INT( year, OverrideYear, "year" );

	{
		bool override_1 = false;
		if( HasSingleOption( "-pwy", argc, argv ) )  // Page per Week: Yes
		{
			bswitch[BS_ONE_PAGE_PER_DIV] = true;
			override_1 = true;
		}
		if( HasSingleOption( "-pwn", argc, argv ) )  // Page per Week: No
		{
			bswitch[BS_ONE_PAGE_PER_DIV] = false;
			override_1 = true;
		}
		if( override_1 )
			COUT << " - overriding option 'OnePagePerWeek': " << bswitch[BS_ONE_PAGE_PER_DIV] << g_endl;
	}

	SetInputFileName( argv[argc-1] );
}

//-----------------------------------------------------------------------------------
/// Get a meaningful string from an enum EN_BOOL_SWITCH, used to fetch values in configuration file
std::string
GetBoolSwitchString( EN_BOOL_SWITCH bs )
{
	std::string n;
	switch( bs )
	{
		case BS_ONE_PAGE_PER_DIV:   n = "OnePagePerDiv";    break;
		case BS_ONE_PAGE_PER_WEEK:  n = "OnePagePerWeek";   break;
		case BS_COLOR_PER_SUBJECT:  n = "ColorPerSubject";  break;
		case BS_PRINT_DAY_DATE:     n = "PrintDayDate";     break;
		case BS_PRINT_UNKNOWN:      n = "PrintUnknowItem";  break;
		case BS_OUTPUT_TIME_STAMP:  n = "OutputTimeStamp";  break;
		case BS_PRINT_EVENT_TYPE:   n = "PrintEventType";   break;
		case BS_PRINT_NOON_LINE:    n = "PrintNoonSepLine"; break;
		case BS_COPY_STYLE_SHEETS:  n = "CopyStyleSheets";  break;
		case BS_COMMENT_FORMATTING: n = "CommentFormatting"; break;
		case BS_SHOW_SUM_RATIO:     n = "Show Sum Reference Ratio"; break;

		default: assert(0);
	}
	return n;
}

//-----------------------------------------------------------------------------------


