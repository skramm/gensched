/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// stats.cpp

#include "stats.h"
#include "globals.h"
#include "htag.h"
#include "user_data.h"
#include "strings.h"
#include "params.h"
#include "groups.h"
#include "helper_functions.h"

//extern Params        g_params;

//-----------------------------------------------------------------------------------
/// helper function for Stats::PrintStatsHtml()
/**
Second argument is a vector of tuples, holding invalid events (see \ref InvalidEvent)
*/
void
PrintInvalidEvents(
	FileHTML&                        f,       ///< where
	const Params&                    params,  ///< parameters
	const std::vector<InvalidEvent>& vec      ///< reference on a vector of tuples
)
{
	DEBUG_IN;
	assert( vec.size() > 0 );

	HTAG div( f, HT_DIV, AT_ID, "invalid" );
	div.OpenTag();

	f.fout << HTAG( HT_H3, g_strings.GetTxt(S_NB_EVENTS_INVALID) );

	HTAG table( f, HT_TABLE );
	table.OpenTag();
	{                                     // first header line
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		f.fout << HTAG( HT_TH, "Field", AT_STYLE, "text-align:right;" );
		const auto& li = vec[0];
		for( size_t i=0; i<std::get<1>(li).size(); i++ ) // just to print the correct nb of columns
			f.fout << HTAG( HT_TH, (int)i+1 );
		f.fout <<  HTAG( HT_TH ) << g_endl;
	}
	{                                     // second header line
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_CW_LINE) );
		const auto& li = vec[0];
		CERR << "li.size()=" << std::get<1>(li).size() << " nb elems in map=" << params.m_input_to_names.size() << ENDL;
		for( size_t i=0; i<std::get<1>(li).size(); i++ )
		{
			CERR << "i=" << i << ENDL;
			const auto& it = std::find( params.input_pos.begin(), params.input_pos.end(), i );
			if( it == params.input_pos.end() )
				f << HTAG( HT_TH );
			else
			{
				CERR << "FOUND! *it=" << *it << ENDL;
				f << HTAG( HT_TH, params.v_index_name.at( params.m_input_to_names.at(*it) ) );
			}
		}
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_ERRORS) ) << g_endl;
	}

	CERR << "Done step 1\n";

	for( const auto& li: vec )         // print content into table
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		f.fout << HTAG( HT_TH, (int)std::get<0>(li) );      // print line number
		for( const auto& col: std::get<1>(li) )
			f.fout << HTAG( HT_TD, col );
		f.fout << HTAG( HT_TD, std::get<2>(li) ) << g_endl; // print error message
	}
	table.CloseTag();
	div.CloseTag();

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// helper function for Stats::PrintStatsHtml()
void
PrintTableOfEvents(
	FileHTML&                 f,
	const Params&             params,
	const std::vector<Event>& vec,
	const UserData&           data,
	std::string               txt
)
{
	DEBUG_IN;

	HTAG div( f, HT_DIV ); //, AT_ID, "dupe" );
	div.OpenTag();
	{
		f.fout << HTAG( HT_H3, txt + ": " + std::to_string( vec.size() ) );
		HTAG table( f, HT_TABLE );
		table.OpenTag();
		{
			HTAG tr( f, HT_TR );                                     // table header
			tr.OpenTag();
			f.fout << HTAG( HT_TH );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_WEEK) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_DAY) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_TIME_SLOT) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_EVENT_TYPE) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_GROUP_TYPE) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_Subj) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_Instr) );
			f.fout << HTAG( HT_TH, g_strings.GetTxt(S_Room) );
		}

		int counter = 0;
		for( const auto& e: vec )
		{
			HTAG tr( f, HT_TR );
			tr.OpenTag();
			f.fout << HTAG( HT_TD, ++counter );
			f.fout << HTAG( HT_TD, params.GetLinkOnWeekSchedule( e.GetIndex( IT_WEEK ), e.GetIndex( IT_DIVISION ), LT_WEEK ) );
			f.fout << HTAG( HT_TD, params.GetWeekday(  e.GetIndex( IT_DAY ) ).value );
			f.fout << HTAG( HT_TD, params.GetTimeSlot( e.GetIndex( IT_TIMESLOT) ).value );
			f.fout << HTAG( HT_TD, params.GetCodeFromEventType( e.GetEventType() ) );
			f.fout << HTAG( HT_TD, params.GetCodeFromGroupType( e.GetGroupType() ) );
			f.fout << HTAG( HT_TD, data.GetItem( IT_SUBJECT,    e.GetIndex(IT_SUBJECT)    ) );
			f.fout << HTAG( HT_TD, data.GetItem( IT_INSTRUCTOR, e.GetIndex(IT_INSTRUCTOR) ) );
			f.fout << HTAG( HT_TD, data.GetItem( IT_ROOM,       e.GetIndex(IT_ROOM)       ) );
		}
	}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
void
Stats::AddInvalidEvent( const LineContent& line, const char* msg )
{
	std::cerr << "- Adding new invalid event: " << msg << ", line: " << line.second << ", " << _v_invalid.size()+1 << " invalid events\n";
	int c=0;
	for( const auto& e: line.first )
		CERR << ++c << ":" << e << ENDL;

	_v_invalid.push_back( std::make_tuple( line.second, line.first, std::string(msg) ) );
}

//-----------------------------------------------------------------------------------
void
Stats::AddEvent2Stats( const Event& event, EN_EVENT_CAT et )
{
	switch( et )
	{
		case EC_SPECIAL: _v_special.push_back( event ); break;
		case EC_DUPE:    _v_dupe.push_back( event );    break;

		default: assert(0);
	}
}

//-----------------------------------------------------------------------------------
/// Generates the "stats" page, showing some general information on the processed data
void
Stats::PrintStatsHtml( const UserData& data, const Params& params, const GroupSets& groups ) const
{
	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( OF_STATS );

	FileHTML& f = *fp;
	f.Open();
	f.fout << HTAG( HT_H2, g_strings.GetTxt(S_EVENT) ) <<  g_endl;
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();

		HTAG li( f, HT_LI );
		li.OpenTag();
		f.fout << g_strings.GetTxt(S_NB_EVENTS_TOTAL)   << ": " << GetNbEvents( EC_ALL ) << g_endl;
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_INVALID) << ": " << GetNbEvents( EC_INVALID ) << g_lic << g_endl;
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_DUPE)    << ": " << GetNbEvents( EC_DUPE )    << g_lic << g_endl;
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_VALID)   << ": " << GetNbEvents( EC_VALID )   << g_lic << g_endl;
		}
		li.CloseTag();

		li.OpenTag();
		f.fout << g_strings.GetTxt(S_NB_EVENTS_VALID) << g_endl;
		{
			HTAG ul2( f, HT_UL );
			ul2.OpenTag();
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_TEACH)   << ": " << GetNbEvents( EC_VALID ) - GetNbEvents( EC_SPECIAL ) << g_lic << g_endl;
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_SPECIAL) << ": " << GetNbEvents( EC_SPECIAL ) << g_lic << g_endl;
		}
		li.CloseTag();

		if( false == params.WeekClString.empty() ) // if query is only on part of the file
		{
			li.OpenTag();
			f.fout << g_strings.GetTxt(S_SELECTED_WEEKS) << ": (" << params.WeekClString << ")\n";
			HTAG ul3( f, HT_UL );
			ul3.OpenTag();
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_CONSIDER) << ": " << GetNbEvents( EC_VALID ) - GetNbEvents( EC_IGNORED );
			f.fout << g_lio << g_strings.GetTxt(S_NB_EVENTS_IGNORED)  << ": " << GetNbEvents( EC_IGNORED ) << g_endl;
			if( GetNbEvents( EC_IGNORED ) )
			{
				f.fout << " (" << _v_IgnoredWeeks.size() << ' ' << g_strings.GetTxt(S_IGNORED_WEEKS) << ": ";
				for( const auto& w: _v_IgnoredWeeks )
					f.fout << w << ' ';
				f.fout << ')';
			}
			f.fout << g_lic << g_endl;
		}
	}

	if( GetNbEvents( EC_INVALID ) )
		PrintInvalidEvents( f, params, _v_invalid );

	if( GetNbEvents( EC_SPECIAL ) )
		PrintTableOfEvents( f, params, _v_special, data, g_strings.GetTxt(S_NB_EVENTS_SPECIAL) );

	if( GetNbEvents( EC_DUPE ) )
		PrintTableOfEvents( f, params, _v_dupe, data, g_strings.GetTxt(S_NB_EVENTS_DUPE) );

	if( _v_commented.size() )
	{
		{
			HTAG h3( f, HT_H3 );
			h3 << g_strings.GetTxt(S_NB_EVENTS_COMMENTED)  << ": " << _v_commented.size();
			f << h3;
		}
		HTAG table( f, HT_TABLE );
		table.OpenTag();
		{
			HTAG tr( f, HT_TR );
			tr.OpenTag();
			f.fout << HTAG( HT_TH );
			f.fout << HTAG( HT_TH, "Week" );
			f.fout << HTAG( HT_TH, "Day" );
			f.fout << HTAG( HT_TH, "Instructor" );
			f.fout << HTAG( HT_TH, "Comment" );
		}
		int c=0;
		for( const auto& e: _v_commented )
		{
			HTAG tr( f, HT_TR );
			tr.OpenTag();
			f << HTAG( HT_TH, ++c );
			e.PrintAsHtmlLine( f, data );
		}
	}

	{
		HTAG h3( f, HT_H3 );
		h3 << g_strings.GetTxt(S_LoC) << ": " << GetTotNbConflicts();
		f << h3;
	}
	if( GetTotNbConflicts() )
	{
		f << HTAG( HT_P, "(<a href=\"conflicts.html\">" + g_strings.GetTxt( S_FN_CONFLICTS ) + "</a>)\n" );
		HTAG ul( f, HT_UL );
		ul.OpenTag();
		f.fout << g_lio << g_strings.GetTxt(S_C_GTS)      << ": " << _v_conflicts.at(CT_GTS)      << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_C_ITS)      << ": " << _v_conflicts.at(CT_ITS)      << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_C_RTS)      << ": " << _v_conflicts.at(CT_RTS)      << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_C_DAY_OFF)  << ": " << _v_conflicts.at(CT_DAY_OFF)  << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_C_TOO_LONG) << ": " << _v_conflicts.at(CT_TOO_LONG) << g_lic << g_endl;
	}

	f.fout << HTAG( HT_H3, g_strings.GetTxt(S_MI) + " :" ) << g_endl;
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();
		f.fout << g_lio << g_strings.GetTxt(S_MI_NI) << ": " << _m_MissingInfo.at(MI_NOINSTR) << ' ' << g_strings.GetTxt(S_EVENT) << " (" << 100.0*_m_MissingInfo.at(MI_NOINSTR)/NbEvents_valid << "%)" << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_MI_NR) << ": " << _m_MissingInfo.at(MI_NOROOM)  << ' ' << g_strings.GetTxt(S_EVENT) << " (" << 100.0*_m_MissingInfo.at(MI_NOROOM) /NbEvents_valid << "%)" << g_lic << g_endl;
		f.fout << g_lio << g_strings.GetTxt(S_MI_NS) << ": " << _m_MissingInfo.at(MI_NOSUBJ)  << ' ' << g_strings.GetTxt(S_EVENT) << " (" << 100.0*_m_MissingInfo.at(MI_NOSUBJ) /NbEvents_valid << "%)" << g_lic << g_endl;
	}

	f.fout << HTAG( HT_H3, g_strings.GetTxt(S_STATS_PER_GROUP) );
	HTAG table( f, HT_TABLE );
	table.OpenTag();
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		f.fout << HTAG( HT_TH, "Group" ) << g_endl;
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_DIV),       AT_TITLE, "Division Groups volume" ) << g_endl;
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_CLASS),     AT_TITLE, "Class Groups volume"    ) << g_endl;
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_ATOMIC),    AT_TITLE, "Atomic Groups volume"   ) << g_endl;
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_SUM),                AT_TITLE, "Sum"                    ) << g_endl;
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_EMPTY_SLOTS_ABBREV), AT_TITLE, g_strings.GetTxt(S_EMPTY_SLOTS)   ) << g_endl;
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_NB_DAYS)  ) << g_endl;
	}

	for( size_t i=0; i<groups.GetNbGroups(GT_ATOMIC); i++ )
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		const std::tuple<Triplet,size_t,size_t>& tup = TotalPerGroup.at(i);
		f << HTAG( HT_TD, groups.GetGroup( GT_ATOMIC, i ).GetName() );
		f << HTAG( HT_TD, std::get<0>(tup).Get( GT_DIV    ) );
		f << HTAG( HT_TD, std::get<0>(tup).Get( GT_CLASS  ) );
		f << HTAG( HT_TD, std::get<0>(tup).Get( GT_ATOMIC ) );
		f << HTAG( HT_TD, std::get<0>(tup).Sum() );
		f << HTAG( HT_TD, std::get<1>(tup) );
		f << HTAG( HT_TD, std::get<2>(tup) );
	}
	table.CloseTag();

	f.Close();

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
Stats::Stats()
{
	_v_conflicts.resize( CT_DUMMY, 0 );

	_m_MissingInfo[MI_NOINSTR] = 0;
	_m_MissingInfo[MI_NOROOM] = 0;
	_m_MissingInfo[MI_NOSUBJ] = 0;
}

//-----------------------------------------------------------------------------------
size_t
Stats::GetNbEvents( EN_EVENT_CAT cat ) const
{
	switch( cat )
	{
		case EC_DUPE    : return _v_dupe.size();    break;
		case EC_INVALID : return _v_invalid.size(); break;
		case EC_SPECIAL : return _v_special.size(); break;
		case EC_VALID   : return NbEvents_valid;    break;
		case EC_IGNORED : return NbIgnoredEvents;   break;
		case EC_ALL:
			return _v_dupe.size() + _v_invalid.size() + NbEvents_valid;
		break;
		default: assert(0);
	}
	assert(0);
}

//-----------------------------------------------------------------------------------
size_t
Stats::GetTotNbConflicts() const
{
	size_t compt = 0;
	for( const auto& i: _v_conflicts )
		compt += i;
	return compt;
}

//-----------------------------------------------------------------------------------
void
Stats::AddCommentedEvent( const Event& e )
{
	_v_commented.push_back( e );
}
//-----------------------------------------------------------------------------------
void
Stats::AddIgnoredEvent( const Event& event )
{
	NbIgnoredEvents++;
	int week_no = event.GetIndex( IT_WEEK );
#if 1
	push_back_ifnotpresent( _v_IgnoredWeeks, week_no );
#else
	if( std::find( _v_IgnoredWeeks.begin(), _v_IgnoredWeeks.end(), week_no ) == _v_IgnoredWeeks.end() )
		_v_IgnoredWeeks.push_back( week_no );
#endif
}
//-----------------------------------------------------------------------------------
