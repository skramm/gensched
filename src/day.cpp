/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file day.cpp
*/

#include <algorithm>
#include <set>

#include "helper_functions.h"
#include "day.h"
#include "stats.h"
#include "params.h"
#include "conflict.h"
#include "globals.h"

/// Allocation of static variable
Params* DayEvents::sp_params_d = 0;

//-----------------------------------------------------------------------------------
/// If day is not off, return 0, else, returns a pointer on the corresponding event
const
Event* DayEvents::IsOff() const
{
    if( !_is_off )
        return 0;

    for( const auto& e: _v_events )
		if( e.GetEventType() == ET_DAY_OFF )
            return &e;

    assert(0);
    return 0; // to avoid a warning
}

//-----------------------------------------------------------------------------------
/// Parses the events of day and adds to \c v_out all the requested (\c it) index of the events of the day
void
DayEvents::AddToIndexList( EN_INFOTYPE it, std::vector<size_t>& v_out ) const
{
	for( const auto& e: _v_events )
	{
		if( false == e.IsSpecialEvent() )
		{
			int item = e.GetIndex( it );
			assert( item >= 0 );
			push_back_ifnotpresent( v_out, static_cast<size_t>(item) );
		}
	}
}

//-----------------------------------------------------------------------------------
void
DayEvents::DumpDay( std::ostream& f ) const
{
	if( _v_events.empty() )
		return;

	f << "*** Dump of day " << (int)_day_index << g_endl;
	f << " -NbSpecialEvents=" << _nbSpecialEvents << g_endl;
	f << " -is_off=" << _is_off;
	if( _is_off )
		f << ", text=*" << _se_text << "*";
	f << g_endl;

	f << " -nb events=" << _v_events.size() << g_endl;
    for( const auto& e: _v_events )
		f << e;
}

//-----------------------------------------------------------------------------------
/// Parses all events of the day and returns the volume that is related to atomic group \c ag_idx
Triplet
DayEvents::GetVolumeT_d1( size_t ag_idx ) const
{
    DEBUG_IN;
    Triplet t;
    for( const auto& e: _v_events )
    {
        if( e.HoldsGroup( GT_ATOMIC, ag_idx ) )
            t += e.GetEventVolume();
    }
    DEBUG_OUT;
    return t;
}

//-----------------------------------------------------------------------------------
/// Adds the event duration to \c sum, into the correct field
void
AddEventDuration( Triplet& sum, const Event& e, float atomic_duration )
{
	auto dur = e.GetIndex( IT_DURATION ) * atomic_duration;
	sum.Add( e.GetEventType()==ET_SPECIAL ? GT_NONE : e.GetGroupType(), dur );
}

//-----------------------------------------------------------------------------------
/// Returns volume associated with information of type \c it that has index \c idx
Triplet
DayEvents::GetVolumeT_d2( EN_INFOTYPE it, size_t idx ) const
{
    DEBUG_IN;
    Triplet sum;
    for( const auto& e: _v_events )
    {
        int eidx = e.GetIndex( it );
        assert( eidx >= 0 );              /// \todo maybe replace this by some error handling ? see after testing
        if( static_cast<size_t>(eidx) == idx )
			AddEventDuration( sum, e, sp_params_d->atomic_duration );
    }
    DEBUG_OUT;
    return sum;
}

//-----------------------------------------------------------------------------------
/// Returns total duration (volume) of all the events of the day that :
/**
-# involves either instructor, room or subject (depending on \c it) with index \c idx,
-# and that involve the student atomic group \c at_group.
*/
Triplet
DayEvents::GetVolume2Cond_d( EN_INFOTYPE it, size_t idx, size_t ag_idx ) const
{
    DEBUG_IN;
    Triplet sum;
    for( const auto& e: _v_events )
    {
        size_t n = e.GetIndex( it );
        if( n == idx )
        {
            if( e.HoldsGroup( GT_ATOMIC, ag_idx ) )
				AddEventDuration( sum, e, sp_params_d->atomic_duration );
        }
    }
    DEBUG_OUT;
    return sum;
}
//-----------------------------------------------------------------------------------
Triplet
DayEvents::GetVolPerDiv_d( int div_idx, EN_INFOTYPE infotype, size_t idx ) const
{
    DEBUG_IN;
    Triplet sum;
    for( const auto& e: _v_events )
    {
        if( (int)idx == e.GetIndex( infotype ) )
            if( div_idx == -1 || div_idx == e.GetIndex( IT_DIVISION ) )
				AddEventDuration( sum, e, sp_params_d->atomic_duration );
    }
    DEBUG_OUT;
    return sum;
}
//-----------------------------------------------------------------------------------
/// Parses the events of the day and adds them to the set \c v_out if
/**
- they belong to group \c group_idx,
- AND they are of type \c gt,
- AND they are about field  \c idx of \c it,
- OR \c it is IT_IGNORE
*/
void
DayEvents::GetEventList_d5(
	EN_INFOTYPE    it,
	size_t         idx,
	size_t         group_idx,
	EN_GROUP_TYPE  gt,
	std::vector<const Event*>& v_out
) const
{
    DEBUG_IN;
    for( auto& e: _v_events )
    {
        if( e.GetGroupType() == gt )
        {
            if( e.GetIndex( IT_GROUP ) == static_cast<int>(group_idx) )
            {
                bool AddIt = true;
                if( it != IT_IGNORE )
                    if( e.GetIndex( it ) != static_cast<int>(idx) )
                        AddIt = false;

                if( AddIt )
                    v_out.push_back( &e );
            }
        }
    }
    DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void
DayEvents::GetEventList_d3( EN_INFOTYPE it, size_t idx, std::vector<const Event*>& v_out ) const
{
    DEBUG_IN;
    for( const auto& e: _v_events )
    {
        if( e.GetIndex( it ) == static_cast<int>(idx) )
            v_out.push_back( &e );
    }
    DEBUG_OUT;
}
//-----------------------------------------------------------------------------------

#if 0
void PrintEventVector( std::vector<const Event*> ve, std::ostream& s )
{
    s << "Vector of events, size=" << ve.size() << ENDL;
    for( const auto& e: ve )
    {
        s << *e;
    }
    s << "--------------------------------------\n";
}
#endif

//-----------------------------------------------------------------------------------
/// Counts in the current day the number of empty slots for \c ag_idx
size_t
DayEvents::GetNbEmptySlots_d( size_t ag_idx ) const
{
    size_t nb = 0;

// step 1: build a set of (sorted) events for that group
    std::vector<const Event*> ve;
    for( const auto& e: _v_events )
        if( e.HoldsGroup( GT_ATOMIC, ag_idx ) )
            ve.push_back( &e );

// step 2: sort this set by time slot
    std::sort( ve.begin(), ve.end(), PointerCompare() );

// step 3: parse this set, and check for empty slots
    if( !ve.empty() )
    {
        for( size_t i=0; i<ve.size()-1; i++ )
        {
            const Event* pe1 = ve[i];
            const Event* pe2 = ve[i+1];

            if( !pe1->IsConflicted() || !pe2->IsConflicted() ) // only do this counting only if none of the two events are conflicted
            {

                int diff = 	pe2->GetIndex( IT_TIMESLOT ) - pe1->GetIndex( IT_TIMESLOT ) - pe1->GetIndex( IT_DURATION );
#if 0
                if( diff < 0 )
                {
                    PrintEventVector( ve, std::cerr );
                    CERR << "pe1:" << *pe1;
                    CERR << "pe2:" << *pe2;
                    CERR << ENDL << "!!!!!!!!!!!!! diff=" << diff << ENDL;
                    CERR << " - pe1->GetIndex( IT_TIMESLOT ) =" << pe1->GetIndex( IT_TIMESLOT ) << ENDL;
                    CERR << " - pe2->GetIndex( IT_TIMESLOT ) =" << pe2->GetIndex( IT_TIMESLOT ) << ENDL;
                    CERR << " - pe1->GetIndex( IT_DURATION ) =" << pe1->GetIndex( IT_DURATION ) << ENDL;
                    CERR << " - pe2->GetIndex( IT_DURATION ) =" << pe2->GetIndex( IT_DURATION ) << ENDL << ENDL;
                }
#endif
                assert( diff >= 0 );
                nb += diff;
            }
        }
    }
    return nb;
}

//-----------------------------------------------------------------------------------
const Event* DayEvents::GetEventFromId_d( size_t Id ) const
{
    for( size_t i=0; i<_v_events.size(); i++ )
        if( _v_events[i].GetId() == Id )
        {
            return &_v_events[i];
        }
    return 0; // the event is not in this week
}

//-----------------------------------------------------------------------------------
/// Searches the week for events happening at \c time_slot of \c day
std::vector<const Event*> DayEvents::GetEventsOnTS( size_t time_slot_idx ) const
{
    std::vector<const Event*> v_out;
    for( size_t i=0; i<_v_events.size(); i++ )
    {
        const Event* event = &_v_events.at(i);
        if( event->GetIndex( IT_TIMESLOT ) == static_cast<int>(time_slot_idx) )
            v_out.push_back( event );
    }
    return v_out;
}

//-----------------------------------------------------------------------------------
/// Returns true if events have their time slots that overlap
bool EventsOverlap( const Event& ea, const Event& eb )
{
	assert( gp_params );
    size_t ts_a = ea.GetIndex(IT_TIMESLOT);                 // check timeslots: do they overlap ?
    size_t ts_b = eb.GetIndex(IT_TIMESLOT);

    if( ts_b == ts_a )
        return true;

    for( decltype(gp_params->MaxDuration) i=1; i<gp_params->MaxDuration; i++ )
    {
        if ( ea.GetIndex(IT_DURATION) == i+1 && ts_b == ts_a+i )
            return true;
        if ( eb.GetIndex(IT_DURATION) == i+1 && ts_a == ts_b+i )
            return true;
    }

    return false;
}

//-----------------------------------------------------------------------------------
/// checking for duplicate events, local function. Returns true if event \e is a dupe
bool CheckForDuplicate( const Event& e, const std::vector<Event>& v_events )
{
    bool is_dupe = false;
    for( auto& i: v_events )
        if( i == e )      // first, check if event e is a duplicate of some previously stored event
        {
            CERR << " * ALERT: duplicate events, ignored event: " << e << ENDL;
            CERR << " * INITIAL Event: " << i << ENDL;
            is_dupe = true;
            g_stats.AddEvent2Stats( e, EC_DUPE );
            break; // no need to continue loop !
        }
    return is_dupe;
}

//-----------------------------------------------------------------------------------
/// Adds the given event to the day, also does some checking and counting
/**
This is where the potential conflicts are checked for.

Returns true if event is to be added to the summary (not a duplicate and not a "day off" event)

- First, checks if the event is a dupe. If yes, then add it to the dupe list and get out

*/
bool
DayEvents::AddEventToDay(
    Event& ev,                                ///< input event. Not const because in case of conflict, this event will be tagged
    std::vector<Conflict>& v_week_conflicts   ///< input and output. If any, conflicts will be added to this vector
)
{
    DEBUG_IN;

//	CERR << "Adding to day the event: " << ev;

    if( CheckForDuplicate( ev, _v_events ) ) // if duplicate, don't add it
    {
        CERR << " - duplicate, no add to day\n";
        DEBUG_OUT;
        return false;
    }

    if( ev.IsSpecialEvent() )
		_nbSpecialEvents++;
    else
    {
        if( ev.GetIndex( IT_INSTRUCTOR ) == 0 )                   // check if event holds some missing information
            g_stats.AddMissingInformation( MI_NOINSTR );
        if( ev.GetIndex( IT_ROOM ) == 0 )
            g_stats.AddMissingInformation( MI_NOROOM );
        if( ev.GetIndex( IT_SUBJECT ) == 0 )
            g_stats.AddMissingInformation( MI_NOSUBJ );
    }

    std::vector<Conflict> v_conflict;

    if( ev.IsConflicted() )    // if event came over here already conflicted, then create the new conflict and register it
    {
        Conflict c( ev.GetConflictType(), ev );
        v_conflict.push_back( c );
    }

    if( ev.GetEventType() == ET_DAY_OFF )
    {
		CERR << "Is DAY_OFF\n";
        _is_off = true;
        _se_text = ev.GetComment();
    }

// checking for conflicts with all the other events already registered on that day
    for( auto& i: _v_events )
    {
        if( ev.GetEventType() == ET_DAY_OFF || i.GetEventType() == ET_DAY_OFF ) // if one of the events is a "day off" event,
        {
            // then they both conflict
            CERR << "* ALERT: day off conflict\n";
            Conflict c( CT_DAY_OFF, i, ev );
            v_conflict.push_back( c );
            break;
        }
        else
        {
            if( EventsOverlap( ev, i ) )			 // check timeslots: do they overlap ?
            {
                if( i.GetIndex(IT_INSTRUCTOR) == ev.GetIndex(IT_INSTRUCTOR) && i.GetIndex(IT_INSTRUCTOR) != 0 )
                {
                    CERR << "* ALERT: Instructor and time slot identical:\n";
                    Conflict c( CT_ITS, i, ev );
                    v_conflict.push_back( c );
                }
                if( i.GetIndex(IT_ROOM)  == ev.GetIndex(IT_ROOM) && i.GetIndex(IT_ROOM) != 0 )
                {
                    CERR << "* ALERT: room and time slot identical:\n";
                    Conflict c( CT_RTS, i, ev );
                    v_conflict.push_back( c );
                }

/// \todo simplify group overlap detection code
                bool GroupProblem = false;
                if( i.GetGroupType() == GT_ATOMIC && ev.HoldsGroup( GT_ATOMIC, i.GetIndex( IT_GROUP ) ) )
                    GroupProblem = true;
                if( ev.GetGroupType() == GT_ATOMIC && i.HoldsGroup( GT_ATOMIC, ev.GetIndex( IT_GROUP ) ) )
                    GroupProblem = true;
                if( i.GetGroupType() == ev.GetGroupType() )
                    if( i.GetIndex( IT_GROUP ) == ev.GetIndex( IT_GROUP ) )
                        GroupProblem = true;

                if( i.GetGroupType() == GT_CLASS && ev.HoldsGroup( i.GetGroupType(), i.GetIndex( IT_GROUP ) ) )
                    GroupProblem = true;
                if( ev.GetGroupType() == GT_CLASS && i.HoldsGroup( ev.GetGroupType(), ev.GetIndex( IT_GROUP ) ) )
                    GroupProblem = true;

                if( GroupProblem )
                {
                    CERR << "* ALERT: Group discrepancy:\n";
                    Conflict c( CT_GTS, i, ev );
                    v_conflict.push_back( c );
                }
            }
        }
    }

// add conflicts to the week vector conflict
    v_week_conflicts.insert( v_week_conflicts.end(), v_conflict.begin(), v_conflict.end() );

    if( !v_conflict.empty() )                                     // if any conflicts were detected, then printout some message
        CERR << " - CONFLICT DETECTED WITH Event : "<< ev;

	_v_events.push_back(ev);
	g_stats.NbEvents_valid++;
	if( ev.IsSpecialEvent() )
		g_stats.AddEvent2Stats( ev, EC_SPECIAL );

    DEBUG_OUT;
    return ev.GetEventType() != ET_DAY_OFF;
}

//-----------------------------------------------------------------------------------

