/**
\file testfile_catch.cpp
\brief uses the catch testing framework
*/

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "helper_functions.h"

TEST_CASE( "test1", "[t1]" )
{
	std::cout << "Running tests with catch " << CATCH_VERSION_MAJOR << '.' << CATCH_VERSION_MINOR << '.' << CATCH_VERSION_PATCH << '\n';

	SECTION( "date" )
	{
		std::string format( "%Y%m%d" );

		CHECK( GetDate( 2, 0, 2014, format ) == "20140106" ); // week 2, monday, 2014 is 6/01

		CHECK( GetDate( 2, 0, 2018, format ) == "20180108" ); // week 2, monday, 2018 is 8/01
		CHECK( GetDate( 2, 0, 2019, format ) == "20190107" ); // week 2, monday, 2019 is 7/01
		CHECK( GetDate( 3, 0, 2019, format ) == "20190114" ); // week 3, monday, 2019 is 14/01
		CHECK( GetDate( 2, 0, 2020, format ) == "20200106" ); // week 2, monday, 2019 is 6/01

	}
}
