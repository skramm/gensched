/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// user_data.cpp

#include <ctime>

#include "globals.h"

#include "user_data.h"
#include "helper_functions.h"
#include "htag.h"
#include "strings.h"
#include "groups.h"
#include "stats.h"
#include "params.h"
#include "cal_data.h"
#include "error.h"


/// Allocation of static variable
Params* UserData::sp_params = 0;

/// overload of operator + for std::pair, see http://stackoverflow.com/a/21956181/193789
template <typename T,typename U>
std::pair<T,U> operator+(const std::pair<T,U> & l,const std::pair<T,U> & r)
{
    return {l.first+r.first,l.second+r.second};
}

//-----------------------------------------------------------------------------------
#if 0
bool INSTRUCTOR_AVAILABILITY::InstructorIsAvailable( const Event& e ) const
{
	cerr << __FUNCTION__ << endl;
	string instr = g_data.GetItem( IT_INSTRUCTOR, e.GetIndex( IT_INSTRUCTOR ) );
	for( const auto& u: _v_unav )
	{
		cerr << " * considering instr " << u._instr << endl;
		if( u._instr == instr )
		{
			size_t day = e.GetIndex( IT_DAY );
			for( const auto& a1: u._v_days_unav )
			{
				if( a1 == day )
				{
					cerr << "conflict: instructor " << instr << " unavailable on day " << a1  << endl;
					return false;
				}
			}
			for( const auto& a2: u._v_single_days )
			{
				if( std::get<0>(a2) == day && std::get<1>(a2) == e.GetIndex( IT_WEEK ) )
				{
					cerr << "conflict: instructor " << instr << " unavailable on day " << std::get<0>(a2)  << " on week " << std::get<1>(a2) << endl;
					return false;
				}
			}
			break;
		}
	}
	return true;
}
#endif

//-----------------------------------------------------------------------------------
/// local helper function
void
PrintVolumeDetails( FileHTML& f, const Triplet& t, const Params& params )
{
	if( t.GetNbValues() > 1 )
	{
		f.fout << " (";
		if( t.Get(GT_DIV) )
			f.fout << params.GetCodeFromGroupType( GT_DIV    ) << ": " << t.Get(GT_DIV) << "h., ";
		if( t.Get(GT_CLASS) )
			f.fout << params.GetCodeFromGroupType( GT_CLASS  ) << ": " << t.Get(GT_CLASS) << "h.";
		if( t.Get(GT_ATOMIC) )
			f.fout << ", " << params.GetCodeFromGroupType( GT_ATOMIC ) << ": " << t.Get(GT_ATOMIC) << "h." << g_endl;
		if( t.Get(GT_NONE) )
			f.fout << ", misc.: " << t.Get(GT_NONE) << "h." << g_endl;
		f.fout << ")\n";
	}
}
//-----------------------------------------------------------------------------------
std::vector<bool>
UserData::FindDataWithVolume( EN_INFOTYPE it ) const
{
	std::vector<bool> columns( GetNbItems( it ), false );
	CERR << "Items : col size=" << getString( it ) << ": " << columns.size() << "\n"; // " _v_subjects size=" << _v_subjects.size() <<  "\n";
	for( size_t i=0; i<GetNbItems( it ); i++ )
	{
		for( size_t ag_idx=0; ag_idx<g_groups.GetNbGroups(GT_ATOMIC); ag_idx++ )  // table lines
		{
			Triplet t = GetVolume2Cond( it, i, ag_idx );
			if( t.Sum() > 0.0f )
				columns[i] = true;
		}
//		CERR << " - item " << i << ": " << _v_subjects.at( i ) << "=> col has volume=" << columns.at(i) << "\n";
	}
	CERR << "Done\n";
	return columns;
}
//-----------------------------------------------------------------------------------
/// Find data with Special Event
std::vector<bool>
UserData::FindDataWithSE( EN_INFOTYPE it ) const
{
	std::vector<bool> columns( GetNbItems( it ), false );
	for( size_t i=0; i<GetNbItems( it ); i++ )
	{
		for( size_t ag_idx=0; ag_idx<g_groups.GetNbGroups(GT_ATOMIC); ag_idx++ )  // table lines
		{
			Triplet t = GetVolume2Cond( it, i, ag_idx );
			if( t.Get(GT_NONE) > 0.0f )
				columns[i] = true;
		}
	}
	return columns;
}
//-----------------------------------------------------------------------------------
/// Generates page holding sorted list of events for all instructors
void
UserData::PrintPage_InstrDiary() const
{
	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( OF_ID );

	FileHTML& f = *fp;
//	f.SetTitle( "Raw instructor calendars" );
	f.Open();

	EventPrintDetails what;
	what.instructor = false;
	what.week_no = false;

	{                          // 1 - print list of instructors with links
		HTAG ul( f,  HT_UL );
		ul.OpenTag();
		for( size_t i=1; i<_v_instructors.size(); i++ )
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			f.fout << "<a href=\"#instr_" << i << "\">" << g_strings.GetTxt(S_Instr) << ": " << _v_instructors[i] << "</a>";
		}
	}

	{                          // 2 - print sorted list of events per instructors
		for( size_t i=1; i<_v_instructors.size(); i++ )
		{
			CERR << "i=" << i << ENDL;

			f.fout << "<h4 id=\"instr_" << i << "\">" << g_strings.GetTxt(S_Instr) << ": " << _v_instructors[i] << "</h4>\n";

			std::vector<const Event*> v_all = GetEventList_2( IT_INSTRUCTOR, i );
			Triplet tvol = GetVolume( v_all );

			f.fout << "<p> -" << v_all.size() << ' ' << g_strings.GetTxt(S_EVENT) << ", volume: " << tvol.Sum() << "h.";
			PrintVolumeDetails( f, tvol, *sp_params );

//			f.fout << "h. (CM=" << tvol.Get(GT_DIV) << "h.,TD=" << tvol.Get(GT_CLASS) << "h., TP=" << tvol.Get(GT_ATOMIC) << "h.)</p>\n";
			f.fout << "</p>\n";

			{
				HTAG ol( f, HT_OL );
				ol.OpenTag();

				int c = 1;
				for( size_t j=0; j<GetNbWeeks(); j++ )
				{
					const WeekEvents& week = GetWeek( j );
					std::vector<const Event*> v_week = GetEventListHoldingWeek( v_all, week.GetWeekNum() );
					if( !v_week.empty() )
					{
						HTAG li2( f, HT_LI );
						li2.OpenTag();

						std::ostringstream oss;
						oss << g_strings.GetTxt( S_WEEK ) << ' ' << week.GetWeekNum();

						f.fout << sp_params->GetLinkOnWeekSchedule( week.GetWeekNum(), -1, LT_PROVIDED, oss.str() );

						PrintEventList( f, v_week, what, c );
					}
					c += v_week.size();
				}
			}
		}
	}
	f.Close();
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
void
UserData::GenerateCalendars() const
{
	OutputFile_SP fp1 = g_files.GetFile( OF_ICAL );

	FileHTML& f = *fp1;
	f.SetTitle( "Calendar files" );
	f.Open();
	f.fout << HTAG( HT_P, g_strings.GetTxt( S_ICAL_TXT ) ) << g_endl;

	HTAG table( f, HT_TABLE, AT_ID, "ical" );
	table.OpenTag();
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		{
			HTAG td( f, HT_TD );
			td.OpenTag();
			f.fout << HTAG( HT_H3, g_strings.GetTxt( S_ICAL_INSTRUCTORS ) );
			td.CloseTag();
			td.OpenTag();
			f.fout << HTAG( HT_H3, g_strings.GetTxt( S_ICAL_TRAINEES ) );
		}
		tr.CloseTag();

		tr.OpenTag();
		{
			HTAG td( f, HT_TD );
			td.OpenTag();
			SaveCalendar( f, CAL_INSTRUCTOR );
		}
		{
			HTAG td( f, HT_TD );
			td.OpenTag();
			SaveCalendar( f, CAL_TRAINEES );
		}
	}
	table.CloseTag();

	f.Close();
}
//-----------------------------------------------------------------------------------
/// helper function for UserData::SaveCalendar(), adds to output page the link on the ical file
void
GenIcalLink( FileHTML& f, const std::string& name, const std::string& text )
{
	{
		HTAG li( f, HT_LI );
		li.OpenTag();
		{
			HTAG a( f, HT_A, AT_HREF, "ical/" + name + ".ics" );
			a.AddAttrib( AT_DOWNLOAD, "ical/" + name + ".ics" );
			a.PrintWithContent( text );
		}
	}
	f.fout << g_endl;
}
//-----------------------------------------------------------------------------------
void
UserData::SaveCalendar( FileHTML& f, EN_CALENDAR_TYPE agt ) const
{
	DEBUG_IN;
	assert( _v_instructors.size() > 1 );

	HTAG ul( f, HT_UL );
	ul.OpenTag();
	if( agt == CAL_INSTRUCTOR )
	{
		for( size_t i=1; i<_v_instructors.size(); i++ )
		{
			std::vector<const Event*> v_out = GetEventList_2( IT_INSTRUCTOR, i );

			std::string name = "instructor_" + ReplaceSpaces(_v_instructors[i]);
			GenerateIcal( name, v_out, agt );
			GenIcalLink( f, name, _v_instructors[i] );
		}
	}
	else
		for( size_t i=0; i<g_groups.GetNbGroups( GT_ATOMIC ); i++ )
		{
			std::vector<const Event*> v_out = GetEventList_5( IT_IGNORE, 0, i, GT_ATOMIC );

			std::string name = "group_" + g_groups.GetGroup( GT_ATOMIC, i ).GetName();
			GenerateIcal( name, v_out, agt );
			GenIcalLink( f, name, "Group " + g_groups.GetGroup( GT_ATOMIC, i ).GetName() );
		}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Generates an ical file
/**
\bug At present, the generated Ical files use the "local time" spec of the RFC
http://tools.ietf.org/search/rfc5545

Google calendar does not seem to be compliant with the RFC and treats these as UTC time,
thus a problem if you are not in a UTC time zone.
*/
void
UserData::GenerateIcal(
	const std::string&               name,   ///< name of file to be created
	const std::vector<const Event*>& v_out,  ///< vector holding pointers on evente
	EN_CALENDAR_TYPE                 agt     ///< either instructor or group of students
) const
{
	DEBUG_IN;
	FileICAL f( name );
	f.Open();
	f.fout << "BEGIN:VCALENDAR" << FileICAL::CRLF;
	f.fout << "VERSION:2.0" << FileICAL::CRLF;

	f.fout << "PRODID:gensched-v" << GENSCHED_VERSION << FileICAL::CRLF;
	for( const auto& pe: v_out ) // type: pointer on event
	{
		pe->PrintAsIcal( f, agt, *this );
	}
	f.fout << "END:VCALENDAR" << FileICAL::CRLF;
//	f.Close();  unneeded, gets closed automatically
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
size_t
UserData::GetNbItems( EN_INFOTYPE it ) const
{
	switch( it )
	{
		case  IT_SUBJECT    : return _v_subjects.size();    break;
		case  IT_ROOM       : return _v_rooms.size();       break;
		case  IT_INSTRUCTOR : return _v_instructors.size(); break;
		default: assert(0);
	}
}
//-----------------------------------------------------------------------------------
/// Returns the index of week \c week_Id
size_t
UserData::GetWeekIndex( int week_Id ) const
{
	for( size_t i=0; i<_v_weeks.size(); i++ )
	if( _v_weeks[i].GetWeekNum() == week_Id )
			return i;
	throw ERROR( "Cannot find given week", std::to_string(week_Id) );
}
//-----------------------------------------------------------------------------------
/// Assigns the week numbers we want to extract, and checks for the year leap
void
UserData::SetWeeks( const std::vector<size_t>& v_num_weeks )
{
	_v_weeks.resize( v_num_weeks.size() ); // create the weeks using empty constructor

	bool IncYear=false;
	for( size_t i=0; i<_v_weeks.size(); i++ )
	{
		_week_indexes[v_num_weeks.at(i)] = i;
		_v_weeks.at(i).SetWeekNumber( v_num_weeks.at(i) );
		if( i>0 )                                         // first week has to be in the current year
			if( v_num_weeks.at(i) < v_num_weeks.at(i-1) ) // this means there is a year leap
				IncYear = true;
		CERR << "- week " << i << " nb=" << v_num_weeks.at(i) << " incyear=" << IncYear << ENDL;
		_v_weeks.at(i)._yearLeap = IncYear;
		_v_weeks.at(i).AllocateForSubjects( g_groups.GetNbGroups( GT_DIV ), _v_subjects.size() );
	}
}
//-----------------------------------------------------------------------------------
bool
UserData::HoldsWeek( int week_no ) const
{
	if( _week_indexes.find( week_no ) == _week_indexes.cend() )
		return false;
	return true;
}
//-----------------------------------------------------------------------------------
Triplet
UserData::GetVolume2Cond( EN_INFOTYPE it, size_t idx, size_t ag_idx ) const
{
	Triplet sum;
	for( const auto& w: _v_weeks )
		sum += w.GetVolume2Cond_w( it, idx, ag_idx );
	return sum;
}
//-----------------------------------------------------------------------------------
std::vector<const Event*>
UserData::GetEventList_5( EN_INFOTYPE it, size_t idx, size_t group_idx, EN_GROUP_TYPE gt ) const
{
	DEBUG_IN;
	std::vector<const Event*> v_out;

	for( const auto& w: _v_weeks )
		w.GetEventList_w5( it, idx, group_idx, gt, v_out );

	DEBUG_OUT;
	return v_out;
}
//-----------------------------------------------------------------------------------
std::vector<const Event*>
UserData::GetEventList_2( EN_INFOTYPE it, size_t idx ) const
{
	DEBUG_IN;
	std::vector<const Event*> v_out;

	for( const auto& w: _v_weeks )
		w.GetEventList_w3( it, idx, v_out );

	DEBUG_OUT;
	return v_out;
}
//-----------------------------------------------------------------------------------
std::vector<const Event*>
UserData::GetEventListHoldingWeek( std::vector<const Event*> v_in, int week_num ) const
{
	DEBUG_IN;
	std::vector<const Event*> v_out;

	for( const auto& pe: v_in )
		if( pe->GetIndex( IT_WEEK ) == week_num )
			v_out.push_back( pe );

	DEBUG_OUT;
	return v_out;
}
//-----------------------------------------------------------------------------------
/// private, prints line of quick links at top of file
void
UserData::PrintHeaderWeekLinks( FileHTML& f, int current_week, EN_OUTPUTFILE_ID filetype, bool onePagePerWeek, int div_idx ) const
{
	HTAG div( f, HT_DIV, AT_CLASS, "head-week-links" );
	div.OpenTag();
	{
		HTAG table( f, HT_TABLE );
		table.OpenTag();
		{
			HTAG tr( f, HT_TR );
			tr.OpenTag();
			f.fout << "<td>" << g_strings.GetTxt( S_WEEK ) << ":</td>";
			for( const auto& week: _v_weeks )
			{
				f.fout << "<td";
				if( current_week == week.GetWeekNum() )
					f.fout << " class='currentw'";
				f.fout << "><a href=\"";
				f.fout << GenerateOutputFileName(filetype, div_idx, onePagePerWeek?week.GetWeekNum():-1 ) << ".html";
				if( !onePagePerWeek )
					f.fout << "#s"<< week.GetWeekNum();
				f.fout << "\">" << week.GetWeekNum()  << "</a></td>";
			}
		}
	}
}
//-----------------------------------------------------------------------------------
void
UserData::DumpData( std::ostream& f ) const
{
	f << "-Dump of data:\n";
	for( const auto& week: _v_weeks )
		week.DumpWeek( f );
}
//-----------------------------------------------------------------------------------
/// Returns the number of days
size_t
UserData::GetNbDays( EN_INFOTYPE it, size_t idx ) const
{
	size_t nb = 0;
	for( const auto& week: _v_weeks )
		nb += week.GetNbDays_w2( it, idx );
	return nb;
}
//-----------------------------------------------------------------------------------
/// return the volume associated with index \c idx of data, identified by \c it
Triplet
UserData::GetVolumeT( EN_INFOTYPE it, size_t idx ) const
{
	Triplet sum;
	for( const auto& week: _v_weeks )
		sum += week.GetVolumeT_w2( it, idx );
	return sum;
}
//-----------------------------------------------------------------------------------
const Event*
UserData::GetEventFromId( size_t Id ) const
{
	for( const auto& w: _v_weeks )
	{
		const Event* e = w.GetEventFromId_w( Id );
		if( e )
			return e;
	}
	std::cerr << "Error: unknown event Id " << Id << ENDL;
	throw ERROR( "unknown event", Id );
}

//-----------------------------------------------------------------------------------
/// Generates a data file of the volume per group per week, generates a gnuplot script, an runs it
/**
The output png file is shown in OF_GW page
*/
void
UserData::generateVolPerWeekPlot() const
{
	FileCSV f_csv( "data_per_week" );
	f_csv.Open();
	size_t Nbag = g_groups.GetNbGroups( GT_ATOMIC );
	f_csv.fout << "#week volume_per_group_(" << Nbag << ")\n";

	for( size_t w=0; w<GetNbWeeks(); w++ )   // for each week
	{
		const WeekEvents& week = _v_weeks[w];

		f_csv.fout << week.GetWeekNum() << ' ';
		for( size_t i=0; i<Nbag; i++ )   // for each atomic group
		{
			Triplet sum = week.GetVolumeT_w1( i );
			f_csv.fout << sum.Sum();
			if( i != Nbag-1 )
				f_csv.fout << ' ';
			else
				f_csv.fout << '\n';
		}
	}
	f_csv.fout << "# (eof)\n";
	f_csv.Close();

	std::ostringstream oss;
	oss << sp_params->OutputDirFullName << "/week_volume_gen.plt";
	std::ofstream fout_plot( oss.str() );
	assert( fout_plot.is_open() );
	fout_plot
//		<< "#!/usr/bin/env gnuplot\n"
		<< "set terminal pngcairo size " << sp_params->graphVolumePerWeek_width << ",500\n"
		<< "print \"start gnuplot script\"\n"
		<< "set output '" << sp_params->OutputDirFullName << '/' << "vol_per_week.png'\n"
		<< "set title 'Volume per Group per Week'\n"
		<< "set xlabel 'Week'\n"
		<< "set ylabel 'Volume (h.)'\n"
		<< "set key outside\n"
		<< "set style data histogram\n"
		<< "set style histogram cluster gap 1\n"
		<< "set style fill solid border -1\n"
		<< "set boxwidth 0.9\n"
		<< "plot \\\n";

	for( size_t i=0; i<Nbag; i++ )
	{
		fout_plot << "  '" << sp_params->OutputDirFullName << '/' << "data_per_week.csv'"
			<< " using "  << i+2 << ":xtic(1)"
			<< " ti 'gr. " << g_groups.GetGroup(GT_ATOMIC, i).GetName() << "'";
		if( i != Nbag-1 )
			fout_plot << ", \\";
		fout_plot << "\n";
	}
	fout_plot << "print 'done!'\n";
	fout_plot.close();

//	std::system( "pwd" );

	std::string str( std::string("gnuplot ") + oss.str() );
//	std::cout << str << g_endl;
	int j = std::system( str.c_str() );
	if( j )
		std::cout << "Error: unable to start gnuplot\n";
//	else
//		std::cout << "gnuplot script file: started\n";

}
//-----------------------------------------------------------------------------------
/// Print the main output file(s), either the schedule or the week summary
/**
May generate several pages, if the option \c OnePagePerWeek is active
*/
void
UserData::PrintPage_Week( const Params& params, EN_OUTPUTFILE_ID ofid ) const
{
	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( ofid==OID_SCHED?OF_SCHED:OF_WS );

	if( ofid == OID_SUMMARY )
		generateVolPerWeekPlot();

	if( false == params.bswitch.at(BS_ONE_PAGE_PER_DIV ) || g_groups.GetNbGroups( GT_DIV ) == 0 )
		GenerateWeek( fp, ofid, params );
	else
		for( int div_idx=0; (size_t)div_idx<g_groups.GetNbGroups( GT_DIV ); div_idx++ )
		{
			fp->SetFileName( GenerateOutputFileName( ofid, div_idx ) );
			GenerateWeek( fp, ofid, params, div_idx );
		}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Helper function for void UserData::PrintPage_Schedule(). Generates the table for a week schedule
void
UserData::GenerateWeek( OutputFile_SP& f, EN_OUTPUTFILE_ID ofid, const Params& params, int div_idx ) const
{
	DEBUG_IN;

	bool onePagePerWeek = params.bswitch.at(BS_ONE_PAGE_PER_WEEK);
	if( !onePagePerWeek )
		f->Open();

	for( size_t i=0; i<GetNbWeeks(); i++ )
	{
		const WeekEvents& week = _v_weeks[i];
		if( onePagePerWeek )
		{
			f->SetFileName( GenerateOutputFileName( ofid, div_idx, week.GetWeekNum() ) );
			f->Open();
		}
		PrintHeader( *f, i, ofid, week._yearLeap, div_idx );
		PrintHeaderWeekLinks( *f, week.GetWeekNum(), ofid, onePagePerWeek, div_idx );

		if( ofid == OID_SUMMARY )
			week.PrintSummary( *f, *this, div_idx );
		else
		{
			week.PrintCalendar( *f, *this, params.bswitch.at(BS_COLOR_PER_SUBJECT), div_idx );

			if( week.GetNbConflicts() )
				PrintErrorMessage( *f, week.GetNbConflicts() );
		}

		if( onePagePerWeek )
			f->Close();
	}

	if( !onePagePerWeek )
		f->Close();

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void
UserData::PrintPage_Conflicts() const
{
	OutputFile_SP fp = g_files.GetFile( OF_CONFLICTS );

	FileHTML& f = *fp;
	f.Open();

	size_t count_c = 0;
	for( size_t i=0; i<GetNbWeeks(); i++ )
		count_c += _v_weeks[i].GetNbConflicts();

	f.fout << "<p>" << g_strings.GetTxt(S_NB_CONFLICTS) << ": " << count_c << "</p>" << g_endl;

	HTAG ul( f, HT_UL );
	ul.OpenTag();
	for( size_t i=0; i<GetNbWeeks(); i++ )
	{
		const WeekEvents& week = _v_weeks[i];
		if( week.GetNbConflicts() )
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			{
				HTAG h4( f, HT_H4 );
				h4.OpenTag();
				if( true == sp_params->bswitch[BS_ONE_PAGE_PER_DIV] )    // if one page per division, we print all the links on the different divisions
				{
					f.fout << g_strings.GetTxt(S_WEEK) << " " << week.GetWeekNum() << " (";
					for( size_t i=0; i<g_groups.GetNbGroups( GT_DIV ); i++ )
					{
						f.fout << sp_params->GetLinkOnWeekSchedule( week.GetWeekNum(), i, LT_DIV );
						if( i+1 != g_groups.GetNbGroups( GT_DIV ) )
							f.fout << " - ";
					}
					f.fout << ")";
				}
				else
					f.fout << sp_params->GetLinkOnWeekSchedule( week.GetWeekNum(), -1, LT_PROVIDED, g_strings.GetTxt(S_WEEK) + " " + std::to_string(week.GetWeekNum()) );

				f.fout << g_endl << ": " << week.GetNbConflicts() << " " << g_strings.GetTxt(S_CONFLICTS);
			}
			week.PrintWeekConflicts( f, *this );
			li.CloseTag(true);
		}
	}
	ul.CloseTag();

	f.Close();
}
//-----------------------------------------------------------------------------------
/// Helper function for printing next/previous links in UserData::PrintHeader()
void
GenPrevNextLink(
	FileHTML&        f,
	bool             onePagePerWeek,
	EN_OUTPUTFILE_ID out_id,
	int              NextPrevWeek,
	EN_STRING_ID     strid,
	std::string      img,
	int              div_idx
)
{
	if( NextPrevWeek == -1 ) // no link
		return;

	HTAG ta( f, HT_A );
	std::ostringstream oss;
	if( !onePagePerWeek )
		oss << "#s" << NextPrevWeek;
	else
		oss << GenerateOutputFileName( out_id, div_idx, NextPrevWeek ) << ".html";
	ta.AddAttrib( AT_HREF, oss.str() );
	ta.AddAttrib( AT_TITLE, g_strings.GetTxt(strid) );
	ta.OpenTag();
	f.fout << "<img src=\"" << img << ".png\" alt=\"" << g_strings.GetTxt(strid) << "\">";
}

//-----------------------------------------------------------------------------------
/// Prints header of the week (schedule and week summary page)
void
UserData::PrintHeader(
	FileHTML&        f,                 ///< output file
	size_t           widx,              ///< week index (NOT the week number). Passed so we can add the link on previous and following week
	EN_OUTPUTFILE_ID out_id,            ///< What is calling this function (=OID_SCHED or OID_SUMMARY)
	bool             IsNextYear,        ///< true if we stepped over january
	int              div_idx            ///< division index (-1 if all divisions on same page)
) const
{
	DEBUG_IN;

	int wnum = _v_weeks[widx].GetWeekNum();

	bool onePagePerWeek = sp_params->bswitch[BS_ONE_PAGE_PER_WEEK];

	CERR << "div_idx=" << div_idx << ENDL;
	size_t Year = g_caldata._current_year;
	if( IsNextYear )
		Year++;

	size_t LastDayIdx = sp_params->GetNbDays()-1;
	f.fout << "\n<hr>\n";

	{                                                       // week number and day names
		HTAG table( f, HT_TABLE, AT_CLASS, "week_header" );
		std::ostringstream oss;
		oss << 's' << wnum;
		table.AddAttrib( AT_ID, oss.str() );
		table.OpenTag();
		{
			HTAG tr( f, HT_TR );
			tr.OpenTag();
			{
				HTAG td( f, HT_TD );
				td.OpenTag();
				{
					HTAG h3(HT_H3);
					h3 << g_strings.GetTxt(S_WEEK) << " " << wnum;
					f << h3;
				}
				td.CloseTag();

				td.OpenTag();
				f.fout << sp_params->GetWeekday(0).value << " " << GetDate( wnum, 0, Year ) << " -> ";
				f.fout << sp_params->GetWeekday(LastDayIdx).value << " " << GetDate( wnum, LastDayIdx, Year );
				td.CloseTag();

				if( div_idx >= 0 && g_groups.GetNbGroups( GT_DIV ) )
				{
					f.fout << HTAG( HT_TD, "Division: " + g_groups.GetGroup( GT_DIV, static_cast<size_t>(div_idx) ).GetName() );
				}
				f.fout << HTAG( HT_TD, g_strings.GetTxt( S_GenOn ) + " " + g_caldata.GetNow() );
			}
		}
	}
	{                                                // navigation links (next, previous,...)
		HTAG div( f, HT_DIV, AT_CLASS, "navbar" );
		div.OpenTag();

		int PreviousWeek = -1;
		int NextWeek = -1;
		if( widx )
			PreviousWeek = _v_weeks[widx-1].GetWeekNum();
		if( widx != _v_weeks.size()-1 )
			NextWeek = _v_weeks[widx+1].GetWeekNum();

		{
			HTAG ta( f, HT_A, AT_HREF, "index.html" );
			ta.AddAttrib( AT_TITLE, g_strings.GetTxt(S_LINKS_INDEX) );
			ta.PrintWithContent( "<img src=\"bH.png\" alt=\"index\">" );
		}

		GenPrevNextLink( f, onePagePerWeek, out_id, PreviousWeek, S_LINKS_PREV, "bL", div_idx );

// print "top" link only if single page for all weeks
		if( !onePagePerWeek )
			f.fout << "<a href=\"#top\" title=\"" << g_strings.GetTxt(S_LINKS_TOP) << "\">"
				<< "<img src=\"bU.png\" alt=\"top\"></a>" << g_endl;

		GenPrevNextLink( f, onePagePerWeek, out_id, NextWeek, S_LINKS_NEXT, "bR", div_idx );

		{                                     // generate link to toggle from schedule / summary page
			std::ostringstream oss;
			oss << GenerateOutputFileName( !out_id, div_idx, onePagePerWeek?wnum:-1 ) << ".html";
			if( !onePagePerWeek )
				oss << "#s" << wnum;

			HTAG a( f, HT_A, AT_HREF, oss.str() );
			a.PrintWithContent( "<img src=\"bS.png\" alt=\"switch\" title=\"switch\">" );
		}
	}

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// private, used to get the correct pointer on the correct vector of data, depending on what is requested.
const StringVector*
UserData::GetPointer( EN_INFOTYPE it ) const
{
	const StringVector* p=0;
	switch( it )
	{
		case IT_SUBJECT    : p = &_v_subjects;    break;
		case IT_INSTRUCTOR : p = &_v_instructors; break;
		case IT_ROOM       : p = &_v_rooms;       break;
		default: assert(0);
	}
	return p;
}

//-----------------------------------------------------------------------------------
/// Returns the string item with index \c idx from the user database
std::string
UserData::GetItem( EN_INFOTYPE it, size_t idx ) const
{
	const StringVector* p = GetPointer( it );
	if( p->size() < idx )
	{
		std::cerr << "Error: invalid index: " << idx << " with infotype=" << (int)it << ", vector size=" << p->size() << ENDL;
		throw ERROR( "invalid index", idx );
	}
	return p->at(idx);
}

//-----------------------------------------------------------------------------------
size_t
UserData::GetIndexFromString( EN_INFOTYPE it, const std::string& value ) const
{
	const StringVector* p = GetPointer( it );

	size_t ret_val = 0;
	const auto& a = std::find( p->begin(), p->end(), value );

#if 1
	if( a != p->end() )
		ret_val = a - p->begin();
#endif
//	assert( a != p->end() );
	return ret_val;
}

//-----------------------------------------------------------------------------------
/// A helper function used by UserData::PrintLists()
void
PrintList( std::string title, const StringVector& list )
{
	std::cerr << "------------------------------------------------\n";
	std::cerr << "* List of all " << title << " (" << list.size() << " elements)\n";
	size_t count=0;
	for( const auto& i : list )
		std::cerr << ++count << " : -" << i << "-\n";
}

//-----------------------------------------------------------------------------------
void
UserData::PrintLists() const
{
	PrintList( "instructors", _v_instructors );
	PrintList( "subjects",    _v_subjects );
	PrintList( "rooms",       _v_rooms );
	std::cerr << "---------------------------------------\n";
}

//-----------------------------------------------------------------------------------
/// checks volume reference data, and adds to the list of subjects the ones that are not already included.
/**
	This is useful in case of badly spelled subject in the reference file. A warning is issued and the subject is added to the list,
	so they will appear in different columns in the output file.

	Returns true if any subjects have been added to the list.
*/
bool
UserData::AddSubjectsIfNeeded()
{
	DEBUG_IN;

	bool added = false;
	for( auto it = _volref._m_trip.begin(); it != _volref._m_trip.end(); it++ )
	{
		added = push_back_ifnotpresent( _v_subjects, it->first );
		if( added )
			std::cerr << "-WARNING: found subject \"" << it->first << "\" in volume reference file that was not referenced in input data file\n";
	}

	if( added && gp_params->DebugMode )
		PrintList( "subjects", _v_subjects );

	DEBUG_OUT;
	return added;
}

//-----------------------------------------------------------------------------------
void
UserData::Init()
{
	_m_sub_instr.clear();

	_v_subjects.clear();
	_v_instructors.clear();
	_v_rooms.clear();

	_week_indexes.clear();
	_v_weeks.clear();

	_v_subjects.push_back(    sp_params->unknown_subject_code );
	_v_instructors.push_back( sp_params->unknown_instructor_code );
	_v_rooms.push_back(       sp_params->unknown_room_code );
}

//-----------------------------------------------------------------------------------
/// Creates the set of instructors, subjects, rooms (and groups if needed) from the input data
void
UserData::BuildLists( const std::vector<LineContent>& in_table )
{
	DEBUG_IN;
	GenerateStringVector( in_table, _v_subjects,    sp_params->input_pos[IT_SUBJECT],    sp_params->unknown_subject_code );
	GenerateStringVector( in_table, _v_instructors, sp_params->input_pos[IT_INSTRUCTOR], sp_params->unknown_instructor_code );
	GenerateStringVector( in_table, _v_rooms,       sp_params->input_pos[IT_ROOM],       sp_params->unknown_room_code );

	if( !sp_params->HasGroups ) // if no groups given, then generate the group list from the input data
	{
		StringVector v_atomic_groups;
		GenerateStringVector( in_table, v_atomic_groups, sp_params->input_pos[IT_GROUP], "" );
		std::sort( v_atomic_groups.begin(), v_atomic_groups.end() );
		g_groups.CreateAtomicGroups( v_atomic_groups );
		CERR << "Generating atomic groups from input data, " << v_atomic_groups.size() << " groups found\n";
	}
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// fills map \c _m_sub_instr with instructors involved in subject
void
UserData::AddToSI_Map( const Event& e )
{
	const std::string& subject = GetItem( IT_SUBJECT,    e.GetIndex( IT_SUBJECT ) );
	const std::string& instr   = GetItem( IT_INSTRUCTOR, e.GetIndex( IT_INSTRUCTOR ) );

	if( _m_sub_instr.find( subject ) == _m_sub_instr.end() )   // if subject not in map,
		_m_sub_instr[subject].push_back( instr );              // then add instructor to that subject
	else
	{
//		const std::vector<std::string>& v = _m_sub_instr[subject];  // add instructor only if
		push_back_ifnotpresent( _m_sub_instr[subject], instr );     // add instructor only if not already present in map
//		if( std::find( v.begin(), v.end(), instr ) == v.end() )
//			_m_sub_instr[subject].push_back( instr );
	}
}

//-----------------------------------------------------------------------------------
/// Helper function, builds a set of string items from index (column) \c idx
/**
of input data \c vv_intable and stores them (sorted) in \c v_outset
*/
void
UserData::GenerateStringVector(
	const std::vector<LineContent>& vv_intable,   ///< the input data
	StringVector&                   v_outset,     ///< output vector of strings, holding the set of extracted items
	size_t                          idx,          ///< index on input data column
	const std::string&              unknown )     ///< if an item is equal to this value, then it won't be stored in the output set
{
	DEBUG_IN;

	for( size_t k=0; k<vv_intable.size(); k++ )
	{
		const StringVector& v_string = vv_intable[k].first;
		const std::string& item = v_string.at(idx);
		if( item != unknown && !item.empty())
		{
			std::string type_str = v_string.at(sp_params->input_pos[IT_TYPE]);
			if( type_str != sp_params->GetCodeFromEventType( ET_DAY_OFF ) ) // && type_str != g_params.GetCodeFromEventType( ET_SPECIAL ) )
				push_back_ifnotpresent( v_outset, item );
		}
	}
	if( v_outset.size() > 1 )                               // else, nothing to sort
		std::sort( v_outset.begin()+(unknown.size()?1:0), v_outset.end() ); // +1, so the unknown code always stays at position 0

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Helper function for UserData::PrintPage_GroupWeek(), prints first line of the table header
void
UserData::PrintWeekUsage_line1( FileHTML& f ) const
{
	HTAG tr1( f, HT_TR );
	tr1.OpenTag();

	{
		HTAG ht_first( HT_TH, "", AT_CLASS, "firstcol_gw" );
		f.fout << ht_first << g_endl;
	}

	size_t NbColsPerWeek=2;
	if( g_groups.HasClassGroups() )
	{
		NbColsPerWeek += 2;                 // one for the class groups, one for the "sum" column
		if( g_groups.HasDivisions() )
			NbColsPerWeek += 1;
	}
// first line of the table (the week numbers)
	for( const auto& week: _v_weeks )
	{
		HTAG th( f, HT_TH );
		th.AddAttrib( AT_COLSPAN, NbColsPerWeek );
		th.AddAttrib( AT_CLASS, "blw" );

		if( sp_params->bswitch[BS_ONE_PAGE_PER_DIV] )           // if one page per division, then no links
		{
			th.OpenTag();
			f.fout << g_strings.GetTxt(S_WEEK_SHORT) << week.GetWeekNum();
			th.CloseTag(true);
		}
		else
		{

			std::ostringstream oss;
			oss << "<a href=\""
				<< GenerateOutputFilePath(OID_SCHED, -1, week.GetWeekNum(), *sp_params )
				<< "\">"
				<< g_strings.GetTxt(S_WEEK_SHORT) << week.GetWeekNum() << "</a>\n";
			th.PrintWithContent( oss.str() );
		}
	}
	{
		HTAG th( f, HT_TH );
		th.AddAttrib( AT_COLSPAN, NbColsPerWeek );
		th.AddAttrib( AT_CLASS, "blw" );
		th.PrintWithContent( g_strings.GetTxt(S_SUMS) );
	}
}

//-----------------------------------------------------------------------------------
/// Helper function for UserData::PrintPage_GroupWeek(), prints second line of the table header
void
UserData::PrintWeekUsage_line2( FileHTML& f ) const
{
	HTAG tr2( f, HT_TR, AT_CLASS, "small" );
	tr2.OpenTag();

	f.fout << HTAG( HT_TH, "Gr.", AT_CLASS, "brw" ) << g_endl;
	for( size_t i=0; i<GetNbWeeks(); i++ )
	{
		if( g_groups.HasDivisions() )
		{
			HTAG th( f, HT_TH, AT_TITLE, "Division Groups volume" );
			th.OpenTag();
			f.fout << sp_params->GetCodeFromGroupType(GT_DIV);
		}
		if( g_groups.HasClassGroups() )
		{
			f.fout << HTAG( HT_TH, sp_params->GetCodeFromGroupType(GT_CLASS),  AT_TITLE, "Class Groups volume"  ) << g_endl;
			f.fout << HTAG( HT_TH, sp_params->GetCodeFromGroupType(GT_ATOMIC), AT_TITLE, "Atomic Groups volume" ) << g_endl;
		}
		else
			f.fout << HTAG( HT_TH, "Vol.", AT_CLASS, "blw" );	// no need to print the type of group, there is none

		if( g_groups.HasClassGroups() )
			f.fout << HTAG( HT_TH, g_sum ) << g_endl;

		HTAG th( f, HT_TH, AT_TITLE, g_strings.GetTxt(S_EMPTY_SLOTS) );
		th.AddAttrib( AT_CLASS, "brw" );
		th.PrintWithContent( g_strings.GetTxt(S_EMPTY_SLOTS_ABBREV) );       // Empty Slots column
	}

	if( g_groups.HasDivisions() )
		f.fout << HTAG( HT_TH, sp_params->GetCodeFromGroupType(GT_DIV), AT_TITLE, "Division Groups volume" );
	if( g_groups.HasClassGroups() )
		f.fout << HTAG( HT_TH, sp_params->GetCodeFromGroupType(GT_CLASS), AT_TITLE, "Class Groups volume" );
	f.fout << HTAG( HT_TH, sp_params->GetCodeFromGroupType(GT_ATOMIC) );
	if( g_groups.HasClassGroups() )
			f.fout << HTAG( HT_TH, g_sum );

	HTAG th( f, HT_TH );
	th.AddAttrib( AT_TITLE, g_strings.GetTxt(S_EMPTY_SLOTS) );
	th.PrintWithContent( g_strings.GetTxt(S_EMPTY_SLOTS_ABBREV) );
}

//-----------------------------------------------------------------------------------
/// Generate "Group / Week" (OF_GW) page
void
UserData::PrintPage_GroupWeek( const GroupSets& groups ) const
{
	void OpenDataPlotFile( std::ofstream& f, const std::string& gr );
	void CloseDataPlotFile( std::ofstream& f );

	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( OF_GW );
	fp->SetStyleSheet( "style_table.css" );
	fp->Open();

	HTAG table( *fp, HT_TABLE  );
	table.OpenTag();

	PrintWeekUsage_line1( *fp );    // first line of the table

	PrintWeekUsage_line2( *fp );    // second line of the table

// main parsing: atomic groups
	for( size_t ag_idx=0; ag_idx<groups.GetNbGroups(GT_ATOMIC); ag_idx++ )
	{
		CERR << "- StudentGroup:"<< groups.GetGroup( GT_ATOMIC, ag_idx ).GetName() << " idx=" << ag_idx << ENDL;
		HTAG tr( *fp, HT_TR );
		tr.OpenTag();

		fp->fout << HTAG( HT_TH, groups.GetGroup( GT_ATOMIC, ag_idx ).GetName() ) << g_endl;
		Triplet row_sum;
		size_t tot_nb_es   = 0;
		size_t tot_nb_days = 0;
		for( const auto& week: _v_weeks )
		{
			FileHTML& f = *fp;
			Triplet t = week.GetVolumeT_w1( ag_idx );
			row_sum += t;
			HTAG td( *fp, HT_TD, AT_CLASS, "blw" );

			if( groups.HasDivisions() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_DIV );
				td.CloseTag();
			}
			if( groups.HasClassGroups() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_CLASS );
				td.CloseTag();
			}
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_ATOMIC );
				td.CloseTag();
			}

			if( groups.HasClassGroups() )
			{
				td.PrintWithContent( t.Sum() );
			}

			size_t nb_es = week.GetNbEmptySlots_w( ag_idx );
			tot_nb_es += nb_es;
			fp->fout << HTAG( HT_TD, (int)nb_es, AT_CLASS, "brw" ) << g_endl;

			size_t nb_days = week.GetNbDays_w( ag_idx );
			tot_nb_days += nb_days;
		}

// sums for all the weeks
		{
			HTAG td( *fp, HT_TD, AT_CLASS, "blw" );

			if( groups.HasDivisions() )
				td.PrintWithContent( row_sum.Get(GT_DIV) );
			if( groups.HasClassGroups() )
				td.PrintWithContent( row_sum.Get(GT_CLASS) );
			td.PrintWithContent( row_sum.Get(GT_ATOMIC) );
			if( groups.HasClassGroups() )
				td.PrintWithContent( row_sum.Sum() );

			td.PrintWithContent( tot_nb_es );

			g_stats.AddMetaInfo( row_sum, tot_nb_es, tot_nb_days );
		}
		tr.CloseTag();

//		CloseDataPlotFile( fout_plot_dat );
	}
	table.CloseTag();

	/// see UserData::generateVolPerWeekPlot()
	fp->fout << "<div><img alt='volume per week' src='vol_per_week.png'></div>\n";

	fp->Close();

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Prints the "planning" (subject/week) page
void
UserData::PrintPage_SubjectPerWeek() const
{
	DEBUG_IN;
	OutputFile_SP fp = g_files.GetFile( OF_SW );
	PrintPage_PerWeek( fp, IT_SUBJECT );
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Prints the "planning" (subject/week) page
void
UserData::PrintPage_InstructorPerWeek() const
{
	DEBUG_IN;
	OutputFile_SP fp = g_files.GetFile( OF_IW );
	PrintPage_PerWeek( fp, IT_INSTRUCTOR );
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
void
printLinksOnDivs( OutputFile_SP fp, bool doCumulatedSum )
{
	char prefix = doCumulatedSum ? 'c' : 's';
	if( g_groups.HasDivisions() ) // then, one table per division
	{
		HTAG ul( *fp, HT_UL );                           // print links at top of page
		ul.OpenTag();
		for( size_t i=0; i<g_groups.GetNbGroups(GT_DIV); i++ )
		{
			HTAG li( *fp, HT_LI );
			li.OpenTag();
			fp->fout << "<a href=\"#" << prefix << "_div_" << i << "\">Division " << i+1 << ": " << g_groups.GetGroup(GT_DIV, i).GetName() << "</a>" << ENDL;
		}
		{
			HTAG li( *fp, HT_LI );
			li.OpenTag();
			fp->fout << "<a href=\"#" << prefix << "_all\">" << g_strings.GetTxt( S_Table_SUMS ) << "</a>" << ENDL;
		}
	}
}

//-----------------------------------------------------------------------------------
/// Prints the "per week" page, used by PrintPage_InstructorPerWeek() and PrintPage_SubjectPerWeek()
void
UserData::PrintPage_PerWeek( OutputFile_SP fp, EN_INFOTYPE infotype ) const
{
	DEBUG_IN;

	fp->SetStyleSheet( "style_table.css" );
	fp->Open();
	fp->fout << HTAG( HT_H2, g_strings.GetTxt( S_VOL_PER_WEEK ) ) << ENDL;
	printLinksOnDivs( fp, false );
	fp->fout << HTAG( HT_H2, g_strings.GetTxt( S_VOL_PER_WEEK_C ) ) << ENDL;
	printLinksOnDivs( fp, true );
	PrintVolumePerWeek( fp, infotype, false );
	PrintVolumePerWeek( fp, infotype, true ); // cumulated volume

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Prints the "per week" page, used by PrintPage_InstructorPerWeek() and PrintPage_SubjectPerWeek()
void
UserData::PrintVolumePerWeek( OutputFile_SP fp, EN_INFOTYPE infotype, bool doCumulatedSum ) const
{
	DEBUG_IN;
	fp->fout << "<hr>\n" << HTAG( HT_H2, (doCumulatedSum ? g_strings.GetTxt( S_VOL_PER_WEEK_C ) : g_strings.GetTxt( S_VOL_PER_WEEK ) ) ) << ENDL;

	char prefix = (doCumulatedSum ? 'c' : 's');

	if( g_groups.HasDivisions() ) // then, one table per division
	{
		for( size_t i=0; i<g_groups.GetNbGroups(GT_DIV); i++ )
		{
			std::ostringstream oss;
			oss << prefix << "_div_" << i;
			HTAG sec_title( *fp, HT_H3, AT_ID, oss.str() );
			sec_title.OpenTag();
			fp->fout << "Division " << i+1 << ": " << g_groups.GetGroup(GT_DIV, i).GetName() << ENDL;
			sec_title.CloseTag();
			PrintTable_PlanningPerDiv( *fp, i, infotype, doCumulatedSum );
		}
		std::ostringstream oss2;
		oss2 << prefix << "_all";

		fp->fout << HTAG( HT_H3, g_strings.GetTxt( S_Table_SUMS ), AT_ID, oss2.str() ) << ENDL;
	}
	PrintTable_PlanningPerDiv( *fp, -1, infotype, doCumulatedSum ); // Print the sum of all divisions

 	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
#ifdef EXPERIMENTAL_CSV_DATA
/// Saves data per week as csv files
/**
Works fine, but not really useful at present (?)
*/
void
saveDataVolumePerWeek(
	const std::vector<Triplet>&    v_vol_per_week,
	const std::vector<WeekEvents>& v_weeks,
	int         div_idx,
	EN_INFOTYPE infotype
)
{
	std::ostringstream oss;
	oss << "data_per_week_" << (infotype == IT_SUBJECT ? "subj" : "instr" );
	if( div_idx == -1 )
		oss << "_ALL";
	else
		oss << "_d" << div_idx;
	FileCSV f_data( oss.str() );
	f_data.Open();

	if( f_data.isOpen() ) // else just don't do anything
	{
		f_data.fout << "# week_num, volume\n";
		auto it = std::begin( v_weeks );
		for( const auto& e: v_vol_per_week )
		{
			f_data.fout << it->GetWeekNum() << ',' << e.Sum() << '\n';
			it++;
		}
	}
	else
		std::cerr << "CANT OPEN FILE data_per_week.csv !!!\n";
}
#endif
//-----------------------------------------------------------------------------------
/// Print planning table for division \c div_idx (or sum of all divisions if -1)
/**
called by UserData::PrintPage_PerWeek()
*/
void
UserData::PrintTable_PlanningPerDiv(
	FileHTML&   f,                ///< output file
	int         div_idx,          ///< division, -1 if none or sum
	EN_INFOTYPE infotype,         ///< line item, subject or instructor
	bool        doCumulatedSum    ///< if true, will generated a cumulated sum
) const
{
	DEBUG_IN;
	assert( infotype == IT_SUBJECT || infotype == IT_INSTRUCTOR );

	HTAG table( f, HT_TABLE, AT_CLASS, "tsw" );
	table.OpenTag();

	{
		HTAG tr( f, HT_TR );                 // first line: number of weeks
		tr.OpenTag();

		bool HasClassGroups = (bool)g_groups.GetNbGroups( GT_CLASS );

		f.fout << HTAG( HT_TH, "", AT_COLSPAN, HasClassGroups?2:1 );
		for( int i=0; i<(int)_v_weeks.size(); i++ )
			f.fout << HTAG( HT_TH, i+1 );

		if( !doCumulatedSum )                            // if "cumulated sum", then no sum column (irrelevant)
		{
			HTAG last( f, HT_TH, AT_CLASS, "blw" );
			int nbcols = HasClassGroups ? 2 : 1;
			last.AddAttrib( AT_COLSPAN, _dataHasSpecialEvents ? nbcols+1 : nbcols );
			last.PrintWithContent( "Sum" );
		}
	}
	{
		std::string str_col_item = (infotype == IT_SUBJECT?g_strings.GetTxt(S_Subj) : g_strings.GetTxt(S_Instr) );
		HTAG tr( f, HT_TR );                 // second line: week numbers
		tr.OpenTag();
		std::string col_pt;
		if( g_groups.GetNbGroups( GT_CLASS ) )
		{
			f.fout << HTAG( HT_TH, str_col_item ) << HTAG( HT_TH, g_strings.GetTxt(S_WEEK), AT_STYLE, "text-align:right;" );
			col_pt = "per type";
		}
		else
			f.fout << HTAG( HT_TH, str_col_item + "/" + g_strings.GetTxt(S_WEEK) );

		bool doLinksForGlobalTable = true;
		if( div_idx == -1 && sp_params->bswitch.at(BS_ONE_PAGE_PER_DIV) )
			doLinksForGlobalTable = false;

		for( const auto& week: _v_weeks )
		{
			f.fout << "<th>";
			if( doLinksForGlobalTable )
				f.fout << "<a href='"
					<< GenerateOutputFilePath( OID_SCHED, div_idx, week.GetWeekNum(), *sp_params )
					<< "'>";
			f.fout << week.GetWeekNum();
			if( doLinksForGlobalTable )
				f.fout << "</a>";
			f.fout << "</th>";
		}

		if( !doCumulatedSum )                            // if "cumulated sum", then no sum column (irrelevant)
		{
			f.fout << HTAG( HT_TH, col_pt, AT_CLASS, "blw" );

			if( _dataHasSpecialEvents )
				f.fout << HTAG( HT_TH, "SE" );

			if( g_groups.GetNbGroups( GT_CLASS ) )
				f.fout << HTAG( HT_TH, "Total" );
		}
	}

	auto nb_lines = infotype==IT_INSTRUCTOR ? _v_instructors.size() : _v_subjects.size();

	std::vector<Triplet> v_colsum( GetNbWeeks() );
	for( size_t line=0; line<nb_lines; line++ )
	{
		std::vector<Triplet> v_vol_per_week( GetNbWeeks() );        // get volume of subject for each week
		std::vector<Triplet> v_cumsum_per_week( GetNbWeeks() );

// first, compute the volume for each week, and the sum for that line
		Triplet cumul_sum;
		for( size_t w=0; w<GetNbWeeks(); w++ )
		{
			v_vol_per_week[w] = GetWeek(w).GetVolPerDiv_w( div_idx, infotype, line );
			cumul_sum += v_vol_per_week[w];
			v_cumsum_per_week[w] = cumul_sum;
		}
		Triplet tsum = std::accumulate( v_vol_per_week.begin(), v_vol_per_week.end(), Triplet() );


// then, if that sum is not empty, print the line
		if( tsum.Sum() > 0.0f ) // if there is any volume for that subject, whatever the week
		{
			if( doCumulatedSum )
				sw_ProcessTripletLine( f, infotype, (int)line, doCumulatedSum, v_cumsum_per_week );
			else
				sw_ProcessTripletLine( f, infotype, (int)line, doCumulatedSum, v_vol_per_week );
		}
		for( size_t w=0; w < GetNbWeeks(); w++ )                  // compute colum sums and prints it
			v_colsum[w] += (doCumulatedSum ? v_cumsum_per_week[w] : v_vol_per_week[w]);
	}

#ifdef EXPERIMENTAL_CSV_DATA
	if( !doCumulatedSum )
		saveDataVolumePerWeek( v_colsum, _v_weeks, div_idx, infotype );
#endif

	sw_ProcessTripletLine( f, infotype, -1, doCumulatedSum, v_colsum );  // sum line per group type

	if( g_groups.GetNbGroups( GT_CLASS ) )   // last-last line (only if class/div groups)
	{
		HTAG tr( f, HT_TR, AT_CLASS, "sw_sumline" );
		tr.OpenTag();
		f.fout << HTAG( HT_TH, "Total volume" ) << HTAG( HT_TD );
		float s = 0.0f;
		for( size_t w=0; w < GetNbWeeks(); w++ )
		{
			f.fout <<  HTAG( HT_TD, v_colsum[w].Sum() );
			s += v_colsum[w].Sum();
		}
		if( !doCumulatedSum )
		{
			HTAG td( HT_TD, s, AT_CLASS, "blw" );
			f.fout << td;
		}
	}
	table.CloseTag();

 	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Prints out a set of three line for subject \c item_idx
void
UserData::sw_ProcessTripletLine(
	FileHTML&                   f,              ///< output file
	EN_INFOTYPE                 infotype,
	int                         item_idx,       ///< subject/instructor index, or -1 if sum line
	bool                        doCumulatedSum,
	const std::vector<Triplet>& v_vol_per_week  ///< the volume that will get printed
) const
{
	DEBUG_IN;

	std::string colclass;
	if( item_idx != -1 )
	{
		std::string s;                                  // set the html class (=cell color) from subject color
		TO_STRING_I( item_idx % sp_params->NbCSSColors, s );
		colclass = "c" + s;
	}

	int NbRows = 1;
	if( g_groups.GetNbGroups( GT_DIV ) )
		NbRows++;
	if( g_groups.GetNbGroups( GT_CLASS ) )
		NbRows++;

	{                                // first line
		HTAG tr( f, HT_TR );
		if( item_idx == -1 )
			tr.AddAttrib( AT_CLASS, "sw_sumline" );

		if( g_groups.GetNbGroups( GT_CLASS ) ) // if we have "class" groups, then no border below
			tr.AddAttrib( AT_CLASS, "nbb" );
		tr.OpenTag();

		if( item_idx == -1 )
			f.fout << HTAG( HT_TH, "Sum", AT_ROWSPAN, NbRows );
		else
		{
			HTAG th( f, HT_TH, AT_ROWSPAN, NbRows );   // print subjects as links on page "Group/subjects" page (gs.html)
			std::ostringstream oss2;
			if( infotype == IT_SUBJECT )
			{
				th.AddAttrib( AT_TITLE, GetInstructorString( item_idx ) );
				oss2 << "<a href=\"gs.html#sub_" << item_idx << "\">" << _v_subjects[item_idx] << "</a>";
			}
			else
			{
				oss2 << _v_instructors[item_idx];
			}
			th.PrintWithContent( oss2.str() );
		}
		sw_ProcessSubjectLine_2( f, doCumulatedSum, v_vol_per_week, GT_ATOMIC, colclass );
	}

	if( NbRows > 1 )                // second line
	{
		HTAG tr( f, HT_TR );
		if( item_idx == -1 )
			tr.AddAttrib( AT_CLASS, "sw_sumline" );

		if( g_groups.GetNbGroups( GT_DIV ) ) // if we have "division" groups, then no border below
			tr.AddAttrib( AT_CLASS, "nbb" );
		tr.OpenTag();
		sw_ProcessSubjectLine_2( f, doCumulatedSum, v_vol_per_week, GT_CLASS, colclass );
	}

	if( NbRows > 2 )               // third line
	{
		HTAG tr( f, HT_TR );
		if( item_idx == -1 )
			tr.AddAttrib( AT_CLASS, "sw_sumline" );

		tr.OpenTag();
		sw_ProcessSubjectLine_2( f, doCumulatedSum, v_vol_per_week, GT_DIV, colclass );
	}

 	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Prints out one of the three lines for a subject/instructor
void
UserData::sw_ProcessSubjectLine_2(
	FileHTML&                   f,               ///< output file
	bool                        doCumulatedSum,
	const std::vector<Triplet>& v_vol_per_week,  ///< volume per week
	EN_GROUP_TYPE               gt,              ///< group type
	const std::string&          colclass         ///< html class (for cell color)
) const
{
	DEBUG_IN;

	if( g_groups.GetNbGroups( GT_CLASS ) )                   // first cell: type of group
	{
		HTAG td( f, HT_TD );
		if( gt == GT_ATOMIC && g_groups.GetNbGroups( GT_CLASS ) )
			td.AddAttrib( AT_CLASS, "tal" );
		if( gt == GT_DIV )
			td.AddAttrib( AT_CLASS, "tar" );
		td.OpenTag();
		f.fout << sp_params->GetCodeFromGroupType( gt );
	}

	float line_sum = 0.0f;
	for( size_t w=0; w<GetNbWeeks(); w++ )
	{
		float vol = v_vol_per_week[w].Get( gt );
		if( v_vol_per_week[w].Sum() > 0.0f )
		{
			HTAG td( f, HT_TD, AT_CLASS, colclass );
			if( gt == GT_ATOMIC && g_groups.GetNbGroups( GT_CLASS ) )
				td.AddAttrib( AT_CLASS, "tal" );
			if( gt == GT_DIV )
				td.AddAttrib( AT_CLASS, "tar" );
			td.OpenTag();
			f.fout << vol;
			td.CloseTag(true);
		}
		else
			f.fout << HTAG( HT_TD );

		line_sum += vol ;
	}
	if( !doCumulatedSum )
	{                                        // last column (sum)
		HTAG td( f, HT_TD, AT_CLASS, colclass );
		td.AddAttrib( AT_CLASS, "blw" );  // Border Left Width
		td.AddAttrib( AT_CLASS, "sw_sumcol" );
		if( gt == GT_ATOMIC && g_groups.GetNbGroups( GT_CLASS ) )
			td.AddAttrib( AT_CLASS, "tal" );
		if( gt == GT_DIV )
			td.AddAttrib( AT_CLASS, "tar" );

		td.PrintWithContent( line_sum );
	}

	if( !colclass.empty() && gt == GT_ATOMIC && g_groups.GetNbGroups( GT_CLASS ) && !doCumulatedSum ) // if first line, then print sum
	{
		Triplet t = std::accumulate( v_vol_per_week.begin(), v_vol_per_week.end(), Triplet() );
		int nbRows = g_groups.GetNbGroups( GT_DIV ) ? 3 : 2;

		if( _dataHasSpecialEvents )
		{
			HTAG td( f, HT_TD, AT_ROWSPAN, nbRows );
			td.AddAttrib( AT_CLASS, colclass );
			if( t.Get(GT_NONE) > 0.0f )
			{
				td.PrintWithContent( t.Get(GT_NONE) );
			}
			else
				td.PrintWithContent("");
		}
		{
			HTAG td( f, HT_TD, AT_ROWSPAN, nbRows );
			td.AddAttrib( AT_CLASS, "sw_sumcol" );
			td.AddAttrib( AT_CLASS, colclass );
			td.PrintWithContent( t.Sum() );
		}
	}
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Helper function for PrintItemList()
/// \todo fix bug in PrintVolumeDetails() below: does no work with special events
void
UserData::PrintEventList(
	FileHTML&                  f,    ///< output file
	std::vector<const Event*>& v_ev, ///< the vector of events to print
	EventPrintDetails          what, ///< what do we want to be printed
	int                        c
) const
{
	DEBUG_IN;

	if( v_ev.empty() )
		f.fout << ": <small>(" << g_strings.GetTxt(S_NONE) << ")</small>" << g_endl;
	else
	{
		std::sort( v_ev.begin(), v_ev.end(), PointerCompare() );
		Triplet t = GetVolume( v_ev );
		f.fout << ": " << v_ev.size() << ' ' << g_strings.GetTxt( S_EVENT) << ", volume=" << t.Sum() << " h.";

		PrintVolumeDetails( f, t, *sp_params );

		HTAG ul( f, HT_OL );
		if( c != -1 )
			ul.AddAttrib( AT_START, c );
		ul.OpenTag();
		for( size_t j=0; j<v_ev.size(); j++ )
			v_ev[j]->Print( f.fout, what );
	}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Helper function for UserData::PrintPage_GroupsSubjects()
void
UserData::PrintItemList(
	FileHTML&          fp,      ///< output file
	int                sub_idx, ///< subject index
	const char*        htmlid,  ///< a string used for "id" element, so we can link to it
	const std::string& text,    ///< group type
	EN_GROUP_TYPE      gt       ///< the group type
) const
{
	DEBUG_IN;

	EventPrintDetails what;
	what.group = false;
	what.subject = false;

	if( GetVolumeT( IT_SUBJECT, sub_idx ).Get( gt ) > 0 )
	{
// nav bar between div, class, and atomic group courses
		fp.fout << "<p id=\"sub_" << sub_idx << "_" << htmlid << "\">" << g_endl;
		fp.fout << "<a href=\"#sub_" << sub_idx << "_CM\">" << g_strings.GetTxt(S_GR_DIV)    << "</a> - ";
		fp.fout << "<a href=\"#sub_" << sub_idx << "_TD\">" << g_strings.GetTxt(S_GR_CLASS)  << "</a> - ";
		fp.fout << "<a href=\"#sub_" << sub_idx << "_TP\">" << g_strings.GetTxt(S_GR_ATOMIC) << "</a> - ";
		fp.fout << "<a href=\"#top\"><img src=\"bU.png\" alt=\"top\"></a>\n</p>" << g_endl;

		const std::string& subject = GetItem( IT_SUBJECT, sub_idx );
		fp.fout << HTAG( HT_H4, subject + ": " + text ) << g_endl;
		{
			HTAG ulB( fp, HT_UL );
			ulB.OpenTag();
			for( size_t gr_idx=0; gr_idx<g_groups.GetNbGroups( gt ); gr_idx++ )
			{
				const StudentGroup& gr = g_groups.GetGroup( gt, gr_idx );
				std::vector<const Event*> vt = GetEventList_5( IT_SUBJECT, sub_idx, gr_idx, gt );

				HTAG liB( fp, HT_LI, AT_CLASS, "eventlist_B" );
				liB.OpenTag();
				fp.fout << g_strings.GetTxt( S_GROUP ) << ' ' << gr.GetName();
				PrintEventList( fp, vt, what );
				liB.CloseTag(true);
			}
		}
	}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Helper function for printing a column in a table in UserData::PrintPage_GroupsSubjects()
void
PrintTriplet( FileHTML& f, const Triplet& t, bool PrintSpecialEvent=false )
{
	HTAG td( f, HT_TD, AT_CLASS, "blw" );

	if( g_groups.HasDivisions() )
	{
		td.OpenTag();
		PRINT_T_IF_NOT_NULL( GT_DIV );
		td.CloseTag();
	}
	if( g_groups.HasClassGroups() )
	{
		td.OpenTag();
		PRINT_T_IF_NOT_NULL( GT_CLASS );
		td.CloseTag();
	}
	{
		td.OpenTag();
		PRINT_T_IF_NOT_NULL( GT_ATOMIC );
		td.CloseTag();
	}
	if( PrintSpecialEvent )
	{
		td.OpenTag();
		PRINT_T_IF_NOT_NULL( GT_NONE );
		td.CloseTag();
	}
	if( g_groups.HasClassGroups() )
		td.PrintWithContent( t.Sum() );
}
//-----------------------------------------------------------------------------------
/// Helper function for printing a sum column in a table in UserData::PrintPage_GroupsSubjects()
/**
This one differs from PrintTriplet() by the fact that it also prints the ratio between volume and reference volume
*/
void
printSumTriplet( FileHTML& f, const Triplet& t, bool PrintSpecialEvent=false, const Triplet* p_ref_volume=0 )
{
	HTAG td( f, HT_TD, AT_CLASS, "blw" );

	if( g_groups.HasDivisions() )
	{
		td.OpenTag();
		t.printIfNotNull( f, GT_DIV, true, p_ref_volume );
		td.CloseTag();
	}
	if( g_groups.HasClassGroups() )
	{
		td.OpenTag();
		t.printIfNotNull( f, GT_CLASS, true, p_ref_volume );
		td.CloseTag();
	}
	{
		td.OpenTag();
		t.printIfNotNull( f, GT_ATOMIC, true, p_ref_volume );
		td.CloseTag();
	}
	if( PrintSpecialEvent )
	{
		td.OpenTag();
		t.printIfNotNull( f, GT_NONE, false );
		td.CloseTag();
	}
	if( g_groups.HasClassGroups() )
		td.PrintWithContent( t.Sum() );
}

//-----------------------------------------------------------------------------------
void UserData::PrintFirstHeaderLine(
	FileHTML&                 f,
	const StringVector*       p_col,                   ///< pointer on column data
	const std::vector<bool>&  printSubjectColumn,      ///< holds true if column needs to be printed
	const std::vector<bool>&  columnHasSpecialEvent,   ///< holds true for each column holding a special event
	bool                      printFirstColumn,
	int                       rowhw,                   ///< row header width (1 or 2 columns usually)
	bool                      PrintSum,                ///< true if each column outta hold sum
	bool                      printSubjectLinks
) const
{
	assert( printSubjectColumn.size() == columnHasSpecialEvent.size() );

	HTAG tr( f, HT_TR );
	tr.OpenTag();

	size_t nbCols = p_col->size();

	{
		HTAG ht_first( HT_TH, "", AT_COLSPAN, rowhw );
		ht_first.AddAttrib( AT_CLASS, "firstcol_gs" );
		f.fout << ht_first;
	}

	HTAG::SetGlobalAttrib( HT_TH, AT_CLASS, "blw" );

	uchar nbColsPerSubject = g_groups.GetNbColumns() +  (g_groups.HasClassGroups() ? (uchar)PrintSum : 0 );
	bool hasSpecialEvent(false);
	for( size_t j=0; j<nbCols; j++ )
		if( j != 0 || printFirstColumn || printSubjectColumn.at(j) )
		{
			uchar trueNbColsPerSubject = nbColsPerSubject;
			if( columnHasSpecialEvent.at(j) )
			{
				trueNbColsPerSubject++;
				hasSpecialEvent = true;
			}
			{
				//f.fout << HTAG( HT_TH, p_col->at(j), AT_COLSPAN, trueNbColsPerSubject ) << g_endl;
				HTAG th( f, HT_TH, AT_COLSPAN, trueNbColsPerSubject );
				th.OpenTag();
				if( printSubjectLinks )
					f.fout << "<a href=\"gs.html#sub_" << j << "\">" << p_col->at(j) << "</a>";
				else
					f.fout << p_col->at(j);
			}
		}

	uchar trueNbColsPerSubject = (g_groups.HasClassGroups()?(nbColsPerSubject+1):nbColsPerSubject);
	f.fout << HTAG( HT_TH, g_strings.GetTxt(S_SUMS), AT_COLSPAN, hasSpecialEvent ? trueNbColsPerSubject+1 : trueNbColsPerSubject ) << g_endl; // last group of columns
	HTAG::ClearGlobalAttribs();
}
//-----------------------------------------------------------------------------------
/// Prints page "Group / Subject " (OF_GS)
void
UserData::PrintPage_GroupsSubjects() const
{
	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( OF_GS );
	fp->SetStyleSheet( "style_table.css" );
	fp->AssignScript( "show_subject.js" );
	fp->Open();

	HTAG table( *fp, HT_TABLE );
	table.OpenTag();

	std::vector<bool> v_PrintSubjColumn    = FindDataWithVolume( IT_SUBJECT );
	std::vector<bool> v_colHasSpecialEvent = FindDataWithSE( IT_SUBJECT );

	bool sumColHasSE = std::find( v_colHasSpecialEvent.cbegin(), v_colHasSpecialEvent.cend(), true ) != v_colHasSpecialEvent.cend();

	ColumnFlags flags;
	flags.printFirstCol = false;
	if( GetVolumeT( IT_SUBJECT, 0 ).Sum() > 0.0f ) // print subject of index 0 (unknown) only if there is some volume associated with it
		flags.printFirstCol = true;

	PrintFirstHeaderLine( *fp, &_v_subjects, v_PrintSubjColumn, v_colHasSpecialEvent, flags.printFirstCol, 1, true, true );
	PrintSecondHeaderLine( *fp, *sp_params,  v_PrintSubjColumn, v_colHasSpecialEvent, flags, 1 );

	Triplet ref_sum;
	if( sp_params->HasReferenceFile )                                 // print volume reference
	{
		HTAG tr( *fp, HT_TR, AT_CLASS, "reference" );
		tr.OpenTag();
		{
			fp->fout << HTAG( HT_TH, "Ref." );
			for( size_t i=0; i<GetNbItems( IT_SUBJECT ); i++ )
				if( i != 0 || v_PrintSubjColumn[0] )
				{
					Triplet t = _volref.Get( GetItem( IT_SUBJECT, i ) );
					ref_sum += t;
					PrintTriplet( *fp, t, v_colHasSpecialEvent.at(i) );
				}
			PrintTriplet( *fp, ref_sum, sumColHasSE );
		}
	}

// fill the table
	for( size_t ag_idx=0; ag_idx<g_groups.GetNbGroups(GT_ATOMIC); ag_idx++ )  // table lines
	{
		CERR << "- StudentGroup:"<< ag_idx << ENDL;

		HTAG tr( *fp, HT_TR );
		tr.OpenTag();
		{
			fp->fout << HTAG( HT_TH, g_groups.GetGroup( GT_ATOMIC, ag_idx ).GetName() ) << g_endl;
			Triplet row_sum;

			for( size_t i=0; i<GetNbItems( IT_SUBJECT ); i++ ) // table columns
			{
				if( i != 0 || v_PrintSubjColumn[0] )
				{
					Triplet t = GetVolume2Cond( IT_SUBJECT, i, ag_idx );
					row_sum += t;
					PrintTriplet( *fp, t, v_colHasSpecialEvent.at(i)  );
				}
			}
			if( sp_params->bswitch.at(BS_SHOW_SUM_RATIO) && sp_params->HasReferenceFile )
				printSumTriplet( *fp, row_sum, sumColHasSE, &ref_sum );
			else
				PrintTriplet( *fp, row_sum, sumColHasSE );  // last groups of columns, with total
		}
	}
	table.CloseTag();
	fp->fout << "<hr>\n";

// -----------------------------
// second part of page: print the list of all events
// -----------------------------
	fp->fout << HTAG( HT_H3, g_strings.GetTxt(S_LOE) )<< g_endl;
	for( size_t i=0; i<GetNbItems( IT_SUBJECT ); i++ )
	{
		CERR << "-subject: " << i << "=" << GetItem( IT_SUBJECT, i ) << ENDL;
		if( i != 0 || v_PrintSubjColumn[0] )
		{
			HTAG div( *fp, HT_DIV, AT_CLASS, "sub_list" );
			{
				std::ostringstream oss;
				oss << "sub_" << i;
				div.AddAttrib( AT_ID, oss.str() );
			}
			div.OpenTag();

			fp->fout << HTAG( HT_H3, g_strings.GetTxt(S_Subj) + ": " + GetItem( IT_SUBJECT, i ) ) << g_endl;

			PrintItemList( *fp, i, "CM", g_strings.GetTxt(S_GR_DIV),    GT_DIV    );
			PrintItemList( *fp, i, "TD", g_strings.GetTxt(S_GR_CLASS),  GT_CLASS  );
			PrintItemList( *fp, i, "TP", g_strings.GetTxt(S_GR_ATOMIC), GT_ATOMIC );
		}
	}
	fp->Close();

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Returns a string holding the list of instructors that are involved with teaching subject \c subj
/**
This is used to provide a html "title" attribute, so that hovering hover the cell holding a subject will show the instructors.
*/
std::string
UserData::GetInstructorString( int subj ) const
{
	ASSERT_2( subj < (int)_v_subjects.size(), subj, _v_subjects.size() );

	std::ostringstream oss;
	oss << "Instructors (";

	if( _m_sub_instr.find( _v_subjects[subj] ) == _m_sub_instr.end() )
		oss << "none)";
	else
	{
		oss << _m_sub_instr.at( _v_subjects[subj] ).size();
		oss << "):\n";
		size_t si = _m_sub_instr.at( _v_subjects[subj] ).size();
		for( size_t i=0; i<si; i++ )
		{
			oss << "- " << _m_sub_instr.at( _v_subjects[subj] ).at(i);
			if( i != si-1 )                                             // so we don't have a line feed after the last one
				oss << g_endl;
		}

	}
	return oss.str();
}

//-----------------------------------------------------------------------------------
void
UserData::FindIfSpecialEvents()
{
	_dataHasSpecialEvents = false;

	for( const auto& week: _v_weeks )
		for( const auto& day: week.GetDays() )
			if( day.GetNbSpecialEvents_d() > 0 )
			{
				_dataHasSpecialEvents = true;
				break;
			}
}

//-----------------------------------------------------------------------------------
