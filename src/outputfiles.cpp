/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "outputfiles.h"
#include "globals.h"
#include "strings.h"

//-----------------------------------------------------------------------------------
/// Assigns names and titles to the output files, language dependent
/**
\todo in several places in the code, the filenames are hardcoded, but they are stored here.
So need to parse the code and check where, and replace with something like
<code>g_files.GetFile(OF_XXX)->GetName()</code>
*/
void OutputFiles::AssignNames()
{
	_m_files[OF_GW]        = OutputFile_SP( new FileHTML( "gw",        g_strings.GetTxt( S_FN_GW        ) ) );
	_m_files[OF_GS]        = OutputFile_SP( new FileHTML( "gs",        g_strings.GetTxt( S_FN_GS        ) ) );

	_m_files[OF_SW]        = OutputFile_SP( new FileHTML( "sw",        g_strings.GetTxt( S_FN_SW        ) ) );
	_m_files[OF_IW]        = OutputFile_SP( new FileHTML( "iw",        g_strings.GetTxt( S_FN_IW        ) ) );

	_m_files[OF_WT]        = OutputFile_SP( new FileHTML( "wt",        g_strings.GetTxt( S_FN_I_WORKTIME) ) );
	_m_files[OF_SCHED]     = OutputFile_SP( new FileHTML( "sched",     g_strings.GetTxt( S_FN_SCHED     ) ) );
	_m_files[OF_STATS]     = OutputFile_SP( new FileHTML( "stats",     g_strings.GetTxt( S_FN_STATS     ) ) );
	_m_files[OF_WS]        = OutputFile_SP( new FileHTML( "ws",        g_strings.GetTxt( S_FN_WS        ) ) );
	_m_files[OF_TABLES]    = OutputFile_SP( new FileHTML( "tables",    g_strings.GetTxt( S_FN_TABLES    ) ) );
	_m_files[OF_ICAL]      = OutputFile_SP( new FileHTML( "ical",      g_strings.GetTxt( S_FN_ICAL      ) ) );
	_m_files[OF_ID]        = OutputFile_SP( new FileHTML( "id",        g_strings.GetTxt( S_FN_I_CALENDAR) ) );
	_m_files[OF_CONFLICTS] = OutputFile_SP( new FileHTML( "conflicts", g_strings.GetTxt( S_FN_CONFLICTS ) ) );
}

//-----------------------------------------------------------------------------------
