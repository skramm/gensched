/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// groups.cpp

#include <boost/property_tree/ini_parser.hpp>

#include "groups.h"
#include "params.h"
#include "globals.h"
#include "helper_functions.h"
#include "error.h"

namespace boost_pt = boost::property_tree;

#define READ_FROM_PT( a, b ) \
    std::string b = pt.get( a, "FAIL" ); \
    if( b == "FAIL" ) \
	{ \
		std::cerr << "Error: reading " << a << ", result=" << b << ENDL; \
		std::exit(1); \
	}


//-----------------------------------------------------------------------------------
/// Returns -1 if unable to find a group with that name
int GroupSets::GetGroupIndex( EN_GROUP_TYPE gt, const std::string& g ) const
{
	switch( gt )
	{
		case GT_ATOMIC:
			for( size_t i=0; i<_v_groups_at.size(); i++ )
				if( _v_groups_at[i].GetName() == g )
					return static_cast<int>(i);
		break;
		case GT_CLASS:
			for( size_t i=0; i<_v_groups_class.size(); i++ )
				if( _v_groups_class[i].GetName() == g )
					return static_cast<int>(i);
		break;
		case GT_DIV:
			for( size_t i=0; i<_v_groups_div.size(); i++ )
				if( _v_groups_div[i].GetName() == g )
					return static_cast<int>(i);
		break;
		default: assert(0);
	}
	return -1;
}

//-----------------------------------------------------------------------------------
/// Dumps all the groups
void GroupSets::Dump( std::ostream& f ) const
{
	f << "-------------------------------------\n";
	f << "GROUPS:\n";
	f << " - 1: Atomic groups: " << _v_groups_at.size() << ENDL;
	for( const auto& a: _v_groups_at )
		a.Dump( f );

	f << " - 2: class groups: " << _v_groups_class.size() << ENDL;
	for( const auto& b: _v_groups_class )
		b.Dump( f );

	f << " - 3: division groups: " << _v_groups_div.size() << ENDL;
	for( const auto& c: _v_groups_div )
		c.Dump( f );

	f << "-------------------------------------\n";
}

//-----------------------------------------------------------------------------------
/// Constructor
GroupSets::GroupSets()
{
	StudentGroup::sp_groups = this;
}

//-----------------------------------------------------------------------------------
/// Read from groups data file
bool GroupSets::ReadFromFile( const std::string& fn )
{
	DEBUG_IN;
	CERR << "Reading file " << fn << ENDL;

    boost_pt::ptree pt;                   // step 1: read ini file into property tree
    try
    {
    	boost_pt::read_ini( fn, pt );
    }
    catch(...)
    {
    	CERR << "exception in reading groups ini file: " << fn << ENDL;
 		DEBUG_OUT;
    	return false;
    }

	Clear();

// step 2: fetch values from property tree 'pt'

// 2.1 atomic groups
	StringVector v_at;
    READ_FROM_PT( "atomic.groups", agroups );
	Tokenize( agroups, v_at, ',' );

	if( v_at.empty() )     // if no atomic groups read, then we have to stop ...
	{
		std::cout << "Fatal error, see stderr\n";
		std::cerr << "No atomic groups read in groups file, tokens string: " << agroups << ", exiting...\n";
		throw ERROR( "No atomic groups read in groups file, tokens string: ", agroups );
	}

	CERR << " - read " << v_at.size() << " atomic groups\n";
	for( const auto& s: v_at )
	{
		AtomicGroup gr( s );
		AddGroup( gr );
	}

	FetchGroups( pt, "class", GT_CLASS );  // 2.2 class groups
	FetchGroups( pt, "division", GT_DIV );  //  2.3 division groups

	DEBUG_OUT;
	return true;
}

//-----------------------------------------------------------------------------------
/// Reads group of category \c type in property tree \c pt
void GroupSets::FetchGroups(
	const boost::property_tree::ptree& pt,
	std::string                        file_section,
	EN_GROUP_TYPE                      type )
{
	DEBUG_IN;
	assert( type != GT_NONE );

	std::string readkey1 = file_section + ".groups";
    std::string groups = pt.get( readkey1, "" );
    if( groups == "" )
	{
		DEBUG_OUT;
		return;      // no groups of that type
	}

	StringVector vs_groups;
	Tokenize( groups, vs_groups );

	CERR << "Adding " << vs_groups.size() << " groups of type " << type << ENDL;
	for( const auto& c: vs_groups ) // for each  group
	{
//		CERR << " c=" << c << ENDL;
		ClassGroup grc( c );
		DivGroup   grd( c );

		std::string readkey2 = file_section + "." + c;
		READ_FROM_PT( readkey2, s );
		StringVector vs;
		Tokenize( s, vs );
		CERR << " type " << type << ": adding " << vs.size() << " group(s)\n";
		for( const auto& gr: vs ) // each token is an atomic group string
		{
//			CERR << "   a= " << gr << ENDL;
			if( type == GT_CLASS )
				grc.AddSubGroup( gr );
			else
				grd.AddSubGroup( gr );
		}
		if( type == GT_CLASS )
			AddGroup( grc );
		else
			AddGroup( grd );
	}
	CERR << "Done\n";
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void GroupSets::AddGroup( const StudentGroup& gr )
{
	DEBUG_IN;
	switch( gr.GetType() )
	{
		case GT_ATOMIC: _v_groups_at.push_back( dynamic_cast<const AtomicGroup&>(gr) ); break;
		case GT_CLASS:
			if( gr.GetNbAtomicGroups() == 0 )
			{
				DEBUG_OUT;
				throw ERROR( "Cannot add an empty class group", "" );
			}
			_v_groups_class.push_back( dynamic_cast<const ClassGroup&>(gr) );
		break;
		case GT_DIV:
			if( gr.GetNbAtomicGroups() == 0 )
			{
				DEBUG_OUT;
				throw ERROR( "Cannot add a div group holding no atomic groups", "" );
			}
			_v_groups_div.push_back( dynamic_cast<const DivGroup&>(gr) );
		break;
		default: assert(0);
	}
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Returns index of groups, after searching in the list of groups. If "Day Off" special event, returns -1
/**
if Special Event, this function also determines the group type
*/
int GroupSets::GetGroupIndexFromString( std::string group, EN_EVENT_TYPE et, EN_GROUP_TYPE& gt ) const
{
	DEBUG_IN;

	int d = -1;
	switch( et )
	{
		case ET_DAY_OFF:
		break;

		case ET_SPECIAL:
			if( group.empty() )
				throw ERROR( "invalid empty group", "" );
			d = FindInGroups( _v_groups_at, group );
			if( d<0 )
			{
				d = FindInGroups( _v_groups_class, group );
				if( d<0 )
				{
					d = FindInGroups( _v_groups_div, group );
					if( d < 0 )
						throw ERROR( "Can't find group", group );
					gt = GT_DIV;
				}
				else
					gt = GT_CLASS;
			}
			else
				gt = GT_ATOMIC;
		break;

		case ET_TEACH:
			switch( gt )
			{
				case GT_ATOMIC:
					d = FindInGroups( _v_groups_at, group );
				break;

				case GT_CLASS:
					d = FindInGroups( _v_groups_class, group );
				break;

				case GT_DIV:
					d = FindInGroups( _v_groups_div, group );
				break;

				default: assert(0);
			}
		break;
		default: assert(0);
	}

	if( d < 0)
		CERR << "Warning: invalid group string and group type: group=" << group << " gt=" << gt << ENDL;

	DEBUG_OUT;
	return d;
}

//-----------------------------------------------------------------------------------
/// searches the set of divisions and returns the index on the division that holds \c group_idx
/**
Input group can be group of any type (atomic, class, or division)
*/
int GroupSets::GetDivisionIndex( EN_GROUP_TYPE gt, size_t group_idx ) const
{
	DEBUG_IN;

	if( 0 == GetNbGroups( GT_DIV ) )
	{
		DEBUG_OUT;
		return -1;
	}
	for( size_t i=0; i<_v_groups_div.size(); i++ )
	{
		const DivGroup& div = _v_groups_div[i];
		if( div.IncludesGroup( gt, group_idx ) )
		{
			DEBUG_OUT;
			return (int)i;
		}
	}
	CERR << "Can't find division holding group " << group_idx << " of type " << (int)gt << ENDL;
	throw ERROR( "Can't find division holding group", "" );
}

//-----------------------------------------------------------------------------------
#if 0
/// Returns false if discrepancy between event group type and event group string
bool GroupSets::GroupAndGroupTypeOK( const Event& e ) const
{
	switch( e.GetGroupType() )
	{
		case GT_ATOMIC:
			if( _v_groups_at.end() == std::find( _v_groups_at.begin(), _v_groups_at.end(), e.GetGroup() ) )
				return false;
			return true;
		break;

		case GT_CLASS:
			for( const auto& g: _v_groups_class )
				if( g.GetName() == e.GetGroup() )
					return true;
		break;

		case GT_DIV:
			for( const auto& g: _v_groups_div )
				if( g.GetName() == e.GetGroup() )
					return true;
		break;

		default: assert(0);
	}
	return false;
}
#endif
//-----------------------------------------------------------------------------------
const StudentGroup& GroupSets::GetGroup( EN_GROUP_TYPE gt, size_t k ) const
{
	DEBUG_IN;
	switch( gt )
	{
		case GT_ATOMIC:
			ASSERT_2( k< _v_groups_at.size(), k, _v_groups_at.size() );
			DEBUG_OUT;
			return _v_groups_at[k];
		break;
		case GT_CLASS:
			ASSERT_2( k< _v_groups_class.size(), k, _v_groups_class.size() );
			DEBUG_OUT;
			return _v_groups_class[k];
		break;
		case GT_DIV:
			ASSERT_2( k< _v_groups_div.size(), k, _v_groups_div.size() );
			DEBUG_OUT;
			return _v_groups_div[k];
		break;
		default:
			CERR << "gt=" << gt << " k=" << k << ENDL;
			assert(0);
	}
	assert(0); // shoudn't be reachable
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Returns nb of groups of the considered category of groups
/**
	if \c div_idx != -1 AND \c gt == GT_ATOMIC then the function will return the number
	of atomic groups that the considered division has.
*/
size_t GroupSets::GetNbGroups( EN_GROUP_TYPE gt, int div_idx ) const
{
	switch( gt )
	{
		case GT_ATOMIC:
			if( -1 == div_idx )
				return _v_groups_at.size(); // return the total number of atomic groups
			else
				return GetGroup( GT_DIV, div_idx ).GetNbAtomicGroups();
		break;

		case GT_CLASS:
			if( -1 == div_idx )
				return _v_groups_class.size(); // return the total number of class groups
			else
				return GetGroup( GT_DIV, div_idx ).GetNbSubGroups();
		break;

		case GT_DIV:
			assert( -1 == div_idx );
			return _v_groups_div.size();
		break;

		default: assert(0);
	}
}
//-----------------------------------------------------------------------------------
void GroupSets::CreateAtomicGroups( const StringVector& v_atomic_groups )
{
	for( const auto& at_gr: v_atomic_groups )
	{
		_v_groups_at.push_back( at_gr );
	}
}

//-----------------------------------------------------------------------------------
