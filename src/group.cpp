/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// group.cpp

#include "groups.h"
#include "error.h"

GroupSets* StudentGroup::sp_groups = 0;

//-----------------------------------------------------------------------------------
const StudentGroup& ClassGroup::GetFirstAtGroup() const
{
	assert( sp_groups );
	assert( GetNbSubGroups() > 0 );
	return GetSubGroup(0);
}
//-----------------------------------------------------------------------------------
const StudentGroup& AtomicGroup::GetFirstAtGroup() const
{
	return *this;
}

//-----------------------------------------------------------------------------------
const StudentGroup& GroupWSG::GetSubGroup( size_t i ) const
{
	assert( sp_groups );
	assert( i<GetNbSubGroups() );
	size_t idx = _v_idx_sg[i];
	switch( GetType() )
	{
		case GT_DIV:
			return sp_groups->GetGroup( GT_CLASS, idx );
		break;
		case GT_CLASS:
			return sp_groups->GetGroup( GT_ATOMIC, idx );
		break;
		default: assert(0);
	}
}

//-----------------------------------------------------------------------------------
size_t DivGroup::GetNbAtomicGroups() const
{
	size_t c=0;
	for( size_t i=0; i<GetNbSubGroups(); i++ )
	{
		const StudentGroup& grc = GetSubGroup(i);
		for( size_t j=0; j<grc.GetNbSubGroups(); j++ )
			c++;
	}
	return c;
}

//-----------------------------------------------------------------------------------
#if 0
void DivGroup::AddAtomicGroups( size_t nb )
{
	DEBUG_IN;
	for( size_t i=0; i<nb; i++ )
		_v_idx_at.push_back( i );

	DEBUG_OUT;
}
#endif
//-----------------------------------------------------------------------------------
const StudentGroup& DivGroup::GetFirstAtGroup() const
{
	assert( sp_groups );
	assert( GetNbSubGroups() );
	const StudentGroup& grc = GetSubGroup( 0 );

	assert( grc.GetNbSubGroups() );
	return grc.GetSubGroup( 0 );
}

//-----------------------------------------------------------------------------------
void GroupWSG::AddSubGroup( const std::string& s )
{
	assert( sp_groups );

	EN_GROUP_TYPE gt = GetType();
	if( gt == GT_DIV )
		gt = GT_CLASS;
	else
		gt = GT_ATOMIC;

	int idx = sp_groups->GetGroupIndex( gt, s );
	if( -1 == idx )
		throw ERROR( "Unable to add group, does not exist", s );

	size_t uidx = static_cast<size_t>(idx);
	if( std::find( _v_idx_sg.begin(), _v_idx_sg.end(), uidx ) != _v_idx_sg.end() )
		throw ERROR( "Atomic group already in class group", s );

	_v_idx_sg.push_back( uidx );
}

//-----------------------------------------------------------------------------------
void StudentGroup::PrintName( std::ostream& f, const std::string& type ) const
{
	f << "--------------------\n" << type << " name=" << GetName() << ENDL;
}
//-----------------------------------------------------------------------------------
void AtomicGroup::Dump( std::ostream& f ) const
{
	f << " - AtomicGroup : " << GetName() << ENDL;
}
//-----------------------------------------------------------------------------------
void ClassGroup::Dump( std::ostream& f ) const
{
	PrintName( f, "ClassGroup" );

	f << " -atomic groups: " << GetNbSubGroups() << ENDL;
	for( const auto& g: _v_idx_sg )
		f << g << "  - ";
	f << "\n--------------------\n";
}
//-----------------------------------------------------------------------------------
void DivGroup::Dump( std::ostream& f ) const
{
	PrintName( f, "DivGroup" );
	f << " -class groups: " << GetNbSubGroups() << ENDL;
	for( const auto& g: _v_idx_sg )
		f << g << "  - ";
	f << "\n--------------------\n";
}

//-----------------------------------------------------------------------------------
/// Returns the current index of the group in the list of atomic groups
size_t AtomicGroup::GetAtomicIdx() const
{
	DEBUG_IN;
//	CHECK_STATIC_POINTER;
	int res = sp_groups->GetGroupIndex( GT_ATOMIC, GetName() );
	assert( res >= 0 );
	DEBUG_OUT;
	return static_cast<size_t>(res);
}
//-----------------------------------------------------------------------------------
/// Fills \c v_out with the indexes of all the contained atomic groups
void GroupWSG::GetAtomicGroups( std::vector<size_t>& v_out ) const
{
	DEBUG_IN;
	v_out.clear();
	if( GetType() == GT_CLASS )
		v_out = _v_idx_sg;  // then, just copy indexes into the output vector
	else
	{
		for( size_t i=0; i<_v_idx_sg.size(); i++ )
		{
			std::vector<size_t> v2;
			const GroupWSG& gr = dynamic_cast<const GroupWSG&>( GetSubGroup( i ) );
			gr.GetAtomicGroups( v2 );
			v_out.insert( v_out.begin(), v2.begin(), v2.end() );
		}
	}
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
bool AtomicGroup::Overlaps( const StudentGroup& g2 ) const
{
	DEBUG_IN;
	if( g2.GetType() == GT_ATOMIC )  // if both groups are atomic, then the only
	{
		bool b = (GetName() == g2.GetName());    // situation where they are overlapping is when they are the same
		DEBUG_OUT;
		return b;
	}
	else
	{
		bool b = g2.Overlaps( *this );
		DEBUG_OUT;
		return b;
	}

}
//-----------------------------------------------------------------------------------
/// Returns true if one group overlaps with the other
/**
A group overlaps with another if it has at least one atomic group in common
*/
bool GroupWSG::Overlaps( const StudentGroup& g2 ) const
{
	DEBUG_IN;

// CASE 1: g2 is atomic:
	if( g2.GetType() == GT_ATOMIC )
	{
		std::vector<size_t> v;
		GetAtomicGroups( v );
		const AtomicGroup& ag = dynamic_cast<const AtomicGroup&>(g2);
		size_t idx = ag.GetAtomicIdx();
		bool b = ( std::find( v.begin(), v.end(), idx ) != v.end() );
		DEBUG_OUT;
		return b;
	}

// CASE 2: g2 is DIV: then if the current groups is DIV, no overlapping, else, try by reversing
	if( g2.GetType() == GT_DIV )
	{
		if( GetType() == GT_DIV )  // two divisions never overlap, unless they are the same
		{
			bool b = ( GetName() == g2.GetName() );
			DEBUG_OUT;
			return b;
		}
		else
		{
			bool b = g2.Overlaps( *this );
			DEBUG_OUT;
			return b;
		}

	}

	const GroupWSG& g = dynamic_cast<const GroupWSG&>(g2); // this is safe, because if it was an atomic group, then it has been detected just above

	std::vector<size_t> v1;
	std::vector<size_t> v2;
	this->GetAtomicGroups( v1 );
	g.GetAtomicGroups( v2 );

	for( const auto& i: v1 )
		for( const auto& j: v2 )
			if( i == j )
			{
				DEBUG_OUT;
				return true;
			}

	DEBUG_OUT;
	return false;
}

//-----------------------------------------------------------------------------------
bool DivGroup::IncludesGroup( EN_GROUP_TYPE gt, size_t idx ) const
{
	switch( gt )
	{
		case GT_ATOMIC:
			return IncludesAtomicGroup_a( idx );
		break;
		case GT_CLASS:
		{
			for( const auto& clgrp_idx: _v_idx_sg )
				if( clgrp_idx == idx )
					return true;
			return false;
		}
		break;

		case GT_DIV:
		{
			assert( sp_groups );
			int current_idx = sp_groups->GetGroupIndex( GT_DIV, GetName() );
			assert( current_idx >= 0 );
			return current_idx == static_cast<int>(idx);
		}
		break;

		default:
//			CERR << "ERROR: gt=" << (int)gt << " idx=" << idx << ENDL;
			assert(0);
	}
}

//-----------------------------------------------------------------------------------
bool AtomicGroup::IncludesAtomicGroup_a( size_t ) const
{
	assert(0);
}

//-----------------------------------------------------------------------------------
bool ClassGroup::IncludesAtomicGroup_a( size_t at_group_idx ) const
{
	for( const auto& i: _v_idx_sg )
		if( i == at_group_idx )
			return true;
	return false;
}

//-----------------------------------------------------------------------------------
bool DivGroup::IncludesAtomicGroup_a( size_t at_group_idx ) const
{
	for( size_t i=0; i<GetNbSubGroups(); i++ )
	{
		const StudentGroup& gr = GetSubGroup(i);
		for( size_t j=0; j<gr.GetNbSubGroups(); j++ )
		{
			if( gr.GetSubGroupIdx(j) == at_group_idx )
				return true;
		}
	}
	return false;
}

//-----------------------------------------------------------------------------------
/// Returns the k'th indexes of the atomic groups that are inside the division
size_t DivGroup::GetAtomicIdx( size_t k ) const
{
	DEBUG_IN;
	assert( k<GetNbAtomicGroups() );
	size_t c = 0;
	for( size_t i=0; i<GetNbSubGroups(); i++ )
	{
		const StudentGroup& gr_c = GetSubGroup( i );
		assert( gr_c.GetNbSubGroups()>0 );
		if( c+gr_c.GetNbSubGroups() > k )
		{
			const StudentGroup& gr_a = gr_c.GetSubGroup( k - c );
			size_t out_idx = sp_groups->GetGroupIndex( GT_ATOMIC, gr_a.GetName() );
			DEBUG_OUT;
			return out_idx;
		}

		c += gr_c.GetNbSubGroups();
	}
	assert(0);
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
