// triplet.cpp

#include "triplet.h"
#include "htmlfile.h"
#include <iomanip>

void Triplet::printIfNotNull( FileHTML& f, EN_GROUP_TYPE grt, bool doPrintRatio, const Triplet* ref_volume ) const
{
	if( Get(grt) > 0.0 )
	{
		f.fout << Get(grt);
		if( doPrintRatio )
		{
			f.fout << "<br><span class='sum_ref_ratio'>";
			auto r = 100.0 * Get(grt) / ref_volume->Get(grt);
			if( r >= 100.0 )
				f.fout << std::setprecision(3);
			else
				f.fout << std::setprecision(2);
			f.fout << r << "%</span>";
		}
	}
	f.fout << std::defaultfloat << std::setprecision(6);
}
