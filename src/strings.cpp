/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// strings.cpp

#include "strings.h"
#include "datatypes.h"

#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

using namespace boost;

static const char* g_textid_strs[]={ BOOST_PP_SEQ_FOR_EACH(TO_STR,~,STRING_SEQ) };
#undef SEQ

//-----------------------------------------------------------------------------------
/// Constructor, assigns default values to strings (English)
Strings::Strings()
{
	DEBUG_IN;
	_v_strings.resize( S_MAX_VALUE );

	Set( S_WEEK,       "week" );
	Set( S_WEEK_SHORT, "w" );
	Set( S_DAY,        "day" );
	Set( S_DAYS,       "days" );
	Set( S_TIME_SLOT,  "time-slot" );

	Set( S_LINKS_TOP,  "top" );
	Set( S_LINKS_PREV, "previous" );
	Set( S_LINKS_NEXT, "next" );
	Set( S_LINKS_INDEX, "index" );

	Set( S_LoC, "Conflicts summary" );
	Set( S_MI,  "Missing information" );

	Set( S_C_RTS, "Room and Time slot" );
	Set( S_C_ITS, "Instructor and Time slot" );
	Set( S_C_GTS, "Group and Time slot" );
	Set( S_C_DAY_OFF, "Day off" );
	Set( S_C_TOO_LONG, "Too long" );

	Set( S_GenOn, "Generated on" );

	Set( S_Table_IS, "Table instructors / subjects" );
	Set( S_Table_IR, "Table instructors / rooms" );
	Set( S_Table_SR, "Table subjects / rooms" );
	Set( S_Table_SUMS, "All divisions" );

	Set( S_Instr, "Instructor" );
	Set( S_Subj,  "Subject" );
	Set( S_Room,  "Room" );

	Set( S_MI_NI, "Missing instructor" );
	Set( S_MI_NR, "Missing room" );
	Set( S_MI_NS, "Missing subject" );

	Set( S_EVENT, "event(s)" );

	Set( S_NB_EVENTS_TOTAL,   "Total number of events" );
	Set( S_NB_EVENTS_VALID,   "Valid events" );
	Set( S_NB_EVENTS_SPECIAL, "Special events" );
	Set( S_NB_EVENTS_INVALID, "Invalid events" );
	Set( S_NB_EVENTS_DUPE,    "Duplicate events" );
	Set( S_NB_EVENTS_TEACH,   "Teaching events" );
	Set( S_NB_EVENTS_COMMENTED, "Events with a comment" );
	Set( S_NB_EVENTS_CONSIDER,  "Considered events" );
	Set( S_NB_EVENTS_IGNORED,   "Ignored events" );
	Set( S_IGNORED_WEEKS,       "Ignored weeks" );
	Set( S_SELECTED_WEEKS,      "Selected weeks" );

	Set( S_NB_CONFLICTS, "Total number of conflicts" );
	Set( S_NB_DAYS, "Nb days" );
	Set( S_CONFLICTS, "conflict(s)" );
	Set( S_CONFLICTS_MSG, "conflict(s) detected on this week, see page" );

	Set( S_LOE, "List of events" );

	Set( S_EVENT_TYPE, "Event type" );
	Set( S_GROUP_TYPE, "Class type" );

	Set( S_GROUP, "Group" );
	Set( S_GR_DIV, "Division" );
	Set( S_GR_CLASS, "Class group" );
	Set( S_GR_ATOMIC, "Lab Group" );

	Set( S_INDEX, "index" );
	Set( S_NONE, "none" );
	Set( S_PER_WEEK, "Per week" );

	Set( S_FN_GS,        "Group / Subject" );
	Set( S_FN_GW,        "Group / Week" );
	Set( S_FN_SW,        "Subject / Week" );
	Set( S_FN_IW,        "Instructor / Week" );
	Set( S_FN_WS,        "Week summary" );
	Set( S_FN_SCHED,     "Schedule" );
	Set( S_FN_I_WORKTIME, "Instructors worktime" );
	Set( S_FN_I_CALENDAR, "Instructors diary" );
	Set( S_FN_TABLES,    "Tables" );
	Set( S_FN_STATS,     "Stats" );
	Set( S_FN_ICAL,      "Ical files" );
	Set( S_FN_CONFLICTS, "Conflicts" );
	Set( S_MEAN,          "Mean" );
	Set( S_SUM,           "Sum" );
	Set( S_SUMS,          "Sums" );
	Set( S_SUM_PER_DIV,   "Sums per div." );
	Set( S_SUM_PER_SUBJ,  "Sum<br>per subject" );
	Set( S_SUM_PER_INSTR, "Sum<br>per instructor" );
	Set( S_STATS_PER_GROUP, "Stats per group" );
	Set( S_DAY_OFF,  "OFF" );
	Set( S_ERRORS, "Error" );

	Set( S_EMPTY_SLOTS, "Empty Slots" );
	Set( S_EMPTY_SLOTS_ABBREV, "ES" );

	Set( S_SPECIAL_EVENT, "Special Events" );
	Set( S_SPECIAL_EVENT_ABBREV, "SE" );

	Set( S_ICAL_TXT, "Import these ical files into your favorite calendar software, or link against them." );
	Set( S_ICAL_TRAINEES,    "Trainees calendar files" );
	Set( S_ICAL_INSTRUCTORS, "Instructors calendar files" );

	Set( S_CW_LINE, "Line" );

	Set( S_VOL_PER_WEEK, "Volume per week" );
	Set( S_VOL_PER_WEEK_C, "Cumulative volume per week" );



	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Reads strings from language file
bool Strings::Read( std::string fname )
{
	std::ostringstream oss;

#ifdef _WIN32
   oss << "c:\\program files\\gensched\\";
#else
	oss << "/etc/gensched/" << fname;
#endif // _WIN32

	property_tree::ptree pt;
	try
	{
		read_ini( oss.str(), pt );
	}
	catch(const std::exception& e )
	{
		std::cerr << "Error: exception in reading file: " << oss.str() << ", msg=" << e.what() << std::endl;
		return false;
	}

	for( size_t i=0; i<(size_t)S_MAX_VALUE; i++ )
	{
		std::ostringstream oss;
		oss << "strings." << g_textid_strs[i];
		std::string s = pt.get( oss.str(), "" );
		if( s.size() == 0 )
		{
			std::cerr << "Error: unable to read key \"" << oss.str() << "\" from file " << fname << std::endl;
			return false;
		}
		Set( (EN_STRING_ID)i, s.c_str() );
	}
	return true;
}
//-----------------------------------------------------------------------------------
void Strings::Dump( std::ostream& f ) const
{
	f << "-------------------------------------\n";
	f << "Strings:\n";
	for( size_t i=0; i<(size_t)S_MAX_VALUE; i++ )
	{
		f << i << ": " << g_textid_strs[i] << ": " << GetTxt( (EN_STRING_ID)i ) << ENDL;
	}
	f << ENDL;
}
//-----------------------------------------------------------------------------------

