/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// table.cpp

#include "table.h"
#include "htag.h"
#include "user_data.h"
#include "groups.h"
#include "globals.h"
#include "strings.h"
#include "params.h"
#include "helper_functions.h"

const GroupSets* TripletTable::sp_groups = 0;

//-----------------------------------------------------------------------------------
/// Helper function
bool RowHasVolume( const std::vector<Triplet>& v_trip )
{
	Triplet sum;
	for( const auto& t: v_trip )
		sum += t;
	if( sum.Sum() > 0.0f )
		return true;
	return false;
}

//-----------------------------------------------------------------------------------
/// helper function for TripletTable::Print2Html()
void TripletTable::PrintColumnGroupFooter( FileHTML& f, const Triplet& tri, bool hasSpecialEvents ) const
{
	HTAG td( f, HT_TD, AT_CLASS, "blw" );
	if( sp_groups->HasDivisions() )
		td.PrintWithContent( tri.Get(GT_DIV) );
	if( sp_groups->HasClassGroups() )
		td.PrintWithContent( tri.Get(GT_CLASS) );
	td.PrintWithContent( tri.Get(GT_ATOMIC) );
	if( hasSpecialEvents )
		td.PrintWithContent( tri.Get(GT_NONE) );
}

//-----------------------------------------------------------------------------------
/// Returns a vector of bool, size equal to the number of columns.
/** Each element holds the information wether the column holds a special event.

See \ref sss_intro_sevents
*/
std::vector<bool>
TripletTable::GetColumnsWithSE( size_t nbRows, const StringVector* p_col ) const
{
	size_t nbCols = p_col->size();

	std::vector<bool> columnHasSpecialEvent( nbCols, false );
	for( size_t row=0; row<nbRows; row++ )
	{
		const std::vector<Triplet>& v_trip = GetRow( row );
		for( size_t col=0; col<nbCols; col++ )
			if( v_trip.at(col).Get( GT_NONE ) > 0.0f )
				columnHasSpecialEvent.at( col ) = true;
	}
	return columnHasSpecialEvent;
}

//-----------------------------------------------------------------------------------
void
TripletTable::Print2Html(
	FileHTML&            f,
	EN_INFOTYPE          col_data,  ///< what's on columns
	const StringVector*  p_row,     ///< pointer on row data
	const StringVector*  p_col,     ///< pointer on column data
	const UserData&      data,
	const Params&        params
) const
{
	DEBUG_IN;

	size_t NbRows = p_row->size();
	size_t NbCols = p_col->size();

	assert( f.fout.is_open() );
	HTAG table( f, HT_TABLE );
	table.OpenTag();

	bool PrintFirstColumn = false;
	if( data.GetVolumeT( col_data, 0 ).Sum() != 0 ) // print subject of index 0 (unknown) only if there is some volume associated with it
		PrintFirstColumn = true;

// identify which columns need to have a "special event" column
	std::vector<bool> columnHasSpecialEvent = GetColumnsWithSE( p_row->size(), p_col );
	bool HasSpecialEvents = std::find( columnHasSpecialEvent.cbegin(), columnHasSpecialEvent.cend(), true ) != columnHasSpecialEvent.cend() ? true : false;

	std::vector<bool> PrintSubjectColumn = data.FindDataWithVolume( col_data );

	data.PrintFirstHeaderLine( f, p_col, PrintSubjectColumn, columnHasSpecialEvent, PrintFirstColumn, 2, false );

//	if( sp_groups->HasClassGroups() || HasSpecialEvents )  // second header line, only if groups defined, or if there are some special events
	{
		ColumnFlags flags;
		flags.printSumColumn = false;
		PrintSecondHeaderLine( f, params, PrintSubjectColumn, columnHasSpecialEvent, flags, 2 );
	}

// iterate over each row of the table, but only print it out if there is any content
	std::vector<Triplet> col_sum( NbCols+1 );  // column sums
	{
		size_t i=0;
		int row_nb=0;
		do
		{
			const std::vector<Triplet>& v_trip = GetRow( i );
//			if( RowHasVolume( v_trip ) )                                   // TEMP !
			{
//				Triplet row_sum;
				HTAG tr( f, HT_TR );
				if( row_nb%2 )
					tr.AddAttrib( AT_CLASS, "oddline" );
				tr.OpenTag();
				f.fout << HTAG(HT_TD, row_nb+1 );
				f.fout << HTAG(HT_TD, p_row->at(i) );
				PrintRow( f, v_trip, col_sum, columnHasSpecialEvent, PrintFirstColumn, HasSpecialEvents );
				tr.CloseTag();
				row_nb++;
			}
			i++;
		}
		while( i<NbRows );
	}
	{
		HTAG tr( f, HT_TR, AT_CLASS, "sums" );                   // last line with column sums
		tr.OpenTag();

		f.fout << HTAG( HT_TD, "", AT_COLSPAN, 2 ) << g_endl;
		for( size_t j=0; j<NbCols; j++ )
			if( j != 0 || PrintFirstColumn )
				PrintColumnGroupFooter( f, col_sum[j], columnHasSpecialEvent.at(j) );

		PrintColumnGroupFooter( f, col_sum[NbCols], HasSpecialEvents );

		if( sp_groups->HasClassGroups() )
		{
			HTAG td( f, HT_TD );
			td.PrintWithContent( col_sum[NbCols].Sum() );
		}
	}

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// Print one row of the TripletTable
/**
*/
void TripletTable::PrintRow(
	FileHTML&                   f,                         ///< Output file
	const std::vector<Triplet>& v_trip,                    ///< data
	std::vector<Triplet>&       col_sum,                   ///< sum for each column, gets incremented here
	const std::vector<bool>&    columnHasSpecialEvent,     ///< information about each column: does it have the "special event" column ?
	bool                        PrintFirstColumn,          ///<
	bool                        PrintSpecialEventsSummary  ///< true if the last group of columns needs to have the "special event" cell
) const
{
	size_t NbCols = v_trip.size(); /// \todo is this value always the same as the one in the caller function ?
	Triplet row_sum;

	for( size_t j=0; j<NbCols; j++ )
	{
		const Triplet& t = v_trip[j];
		if( j != 0 || PrintFirstColumn )
		{
			HTAG td( f, HT_TD, AT_CLASS, "blw" );
			if( sp_groups->HasDivisions() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_DIV );
				td.CloseTag();
			}
			if( sp_groups->HasClassGroups() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_CLASS );
				td.CloseTag();
			}
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_ATOMIC );
				td.CloseTag();
			}
			if( columnHasSpecialEvent.at(j) )
//			if( t.Get( GT_NONE ) > 0.0f )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL( GT_NONE );
				td.CloseTag();
			}
			f.fout << g_endl;
		}
		row_sum += t;
		col_sum[j] += t;
	}
	col_sum[NbCols] += row_sum;

	{                                                          // last group of columns
		HTAG td( f, HT_TD, AT_CLASS, "blw" );
		if( sp_groups->HasDivisions() )
			td.PrintWithContent( row_sum.Get(GT_DIV) );
		if( sp_groups->HasClassGroups() )
			td.PrintWithContent( row_sum.Get(GT_CLASS) );
		td.PrintWithContent( row_sum.Get(GT_ATOMIC) );

		if( PrintSpecialEventsSummary )
			td.PrintWithContent( row_sum.Get(GT_NONE) );

		if( sp_groups->HasClassGroups() )
			td.PrintWithContent( row_sum.Sum() );
	}
}

//-----------------------------------------------------------------------------------
void TripletTable::Dump( std::ostream& f ) const
{
	f << "TripletTable DUMP:\n";
	for( size_t i=0; i<_vv_table.size(); i++ )
	{
		f << "row" << i+1 << ": ";
		for( size_t j=0; j<_vv_table[i].size(); j++ )
		{
			f << _vv_table[i][j] << " ";
		}
		f << g_endl;
	}
	f << g_endl;
}
//-----------------------------------------------------------------------------------
