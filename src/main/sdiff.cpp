/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file sdiff.cpp
\brief A diff tool to compare 2 data file

!!! PRELIMINARY !!!

This is a companion program for gensched. It is designed to help compare data files as week events.
The main difference with a classic "diff" program is that it does NOT consider the order of data in the input files.
*/

#include "user_data.h"
#include "globals.h"
#include "helper_functions.h"
#include "params.h"
#include "groups.h"
#include "error.h"

using namespace std;

int ReadData( const std::string& csv_fn, const Params&, UserData& data, std::vector<size_t>& v_num_weeks );

//-----------------------------------------------------------------------------------
/// see sdiff.cpp
int main( int argc, char** argv )
{
	Params params;
	if( argc < 3 )
	{
		cout << "Error, need at least 2 arguments\n";
		return 1;
	}

   	cout << " - Reading configuration file '" << params.ConfigFileName << "': ";
    if( !params.ReadParamsFromFile( params.ConfigFileName ) ) {
		cout << "failure, using defaults\n";
	}
    else {
        cout << "success\n";
		params.ConfigFileRead = true;
	}


	string csv_fn_1 = argv[argc-2];
	string csv_fn_2 = argv[argc-1];


//	STEP( "read groups from config file" );
// read groups from config file, if any
	if( params.GroupsFileName.size() )
	{
		cout << " - Reading groups from file '" << params.GroupsFileName;
		if( !g_groups.ReadFromFile( params.GroupsFileName ) ) {
			cout << "': failure, reading groups from input file\n";
		}
		else
		{
			cout << "': success\n";
			params.HasGroups = true;
		}
	}

	Event::AssignGroups( g_groups );

//	STEP( "read input files" );
//	COUT << " - Read input file '" << csv_fn_1;

	int r;
	UserData data_1;
	vector<size_t> v_num_weeks_1;
	if( 0 != ( r=ReadData( csv_fn_1, params, data_1, v_num_weeks_1 ) ) )
	{
		cout << "Error on reading data from " << csv_fn_1 << endl;
		return r;
	}

	UserData data_2;
	vector<size_t> v_num_weeks_2;
	if( 0 != ( r=ReadData( csv_fn_2, params, data_2, v_num_weeks_2 ) ) )
	{
		cout << "Error on reading data from " << csv_fn_2 << endl;
		return r;
	}

// first step: get common weeks and weeks only in data1 or only in data2

	vector<size_t> v_weeks_only_1;
	vector<size_t> v_weeks_only_2;
	vector<size_t> v_common;
	CompareVectors( v_num_weeks_1, v_num_weeks_2, v_weeks_only_1, v_weeks_only_2, v_common );

	cout << "Weeks only in file " << csv_fn_1 << ": ";
	if( v_weeks_only_1.empty() )
		cout << "none";
	else
		for( auto i: v_weeks_only_1 )
			cout << i << "-";
	cout << endl;

	cout << "Weeks only in file " << csv_fn_2 << ": ";
	if( v_weeks_only_2.empty() )
		cout << "none";
	else
		for( auto i: v_weeks_only_2 )
			cout << i << "-";
	cout << endl;

	return 0;
}

//-----------------------------------------------------------------------------------
int ReadData( const std::string& csv_fn, const Params& params, UserData& data, std::vector<size_t>& v_num_weeks )
{
	vector<LineContent> in_table;
	if( !ReadCSV( csv_fn, in_table, params.CSV_delim, params.nb_tokens ) )
	{
		cout << "': failure, exiting...\n";
		exit(1);
	}
	else
		cout << "': success, read " << in_table.size() << " lines\n";

	data.Init();
    data.BuildLists( in_table );

	cout << "BuildLists(): done\n";
	try
	{
		BuildWeekNumList( in_table, v_num_weeks, params );
	}
	catch( const ErrorClass& e )
	{
		cout << "Error, unable to build week list\n";
		cerr << "Error: " << e.what() << endl;
		return 4;
	}
	data.SetWeeks( v_num_weeks );


// step 2 : transfert information into weeks and days
	for( size_t k=0; k<in_table.size(); k++ )                         // parse ALL lines of input data
	{
		try
		{
			Event event( in_table.at(k) );                //   - build the event

			size_t widx = data.GetWeekIndex( event.GetIndex( IT_WEEK ) );
			WeekEvents& week = data.GetWeek(widx);
			week.AddEventToWeek( event );            //   - add it to the current week
		}
		catch( const ErrorClass& e )
		{
			cerr << __FUNCTION__ << ": Error: invalid event, error=" << e.what() << ENDL;
		}
    }

    return 0; // success
}

//-----------------------------------------------------------------------------------
