/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file readical.cpp

http://tools.ietf.org/search/rfc5545

!!! preliminar !!!
*/

#include <boost/preprocessor.hpp>
#include "helper_functions.h"
#include <fstream>


//-----------------------------------------------------------------------------------
#define ICAL_PROP_STRING_SEQ \
	(DTSTAMP) \
	(DTSTART) \
	(DTEND) \
	(BEGIN) \
	(END) \
	(METHOD) \
	(VERSION) \
	(PROGID) \
	(SUMMARY) \
	(LOCATION) \
	(DESCRIPTION) \
	(UID) \
	(CREATED) \
	(LASTMODIFIED) \
	(COMMENT) \
	(SEQUENCE)

enum EN_ICAL_PROP { BOOST_PP_SEQ_ENUM(ICAL_PROP_STRING_SEQ) };


bool ReadIcal( std::string, size_t n=10 );

//-----------------------------------------------------------------------------------
/// preliminary  see readical.cpp
int main( int argc, char** argv )
{
	if( argc < 1 )
	{
		std::cout << "Error, need at least 1 argument\n";
		return 1;
	}

	ReadIcal( argv[1] );

	return 0;
}

//-----------------------------------------------------------------------------------
EN_ICAL_PROP GetPropFromText( std::string val )
{

}
//-----------------------------------------------------------------------------------
struct ICALEVENT
{
	public:
		void ProcessData( EN_ICAL_PROP prop, std::string val );
		void Add2Previous( std::string line );
};

//-----------------------------------------------------------------------------------
void ICALEVENT::ProcessData( EN_ICAL_PROP prop, std::string val )
{

}

//-----------------------------------------------------------------------------------
void ICALEVENT::Add2Previous( std::string line )
{
}
//-----------------------------------------------------------------------------------
/// Read ical data file \c fn
bool ReadIcal(
	std::string                fn,            ///< file name
	size_t                     min_nb_tokens  ///< minimum nb of tokens to be read
)
{
	DEBUG_IN;

	std::cout << " - reading ical file: " << fn << std::endl;
	std::ifstream icalfile;
	icalfile.open( fn, std::ios_base::in );
	if( !icalfile.is_open() )
	{
		std::cout << " -unable to open file\n";
		return false;
	}

	char buf[BUF_SIZE];
	bool go_on = true;
	size_t linecount=0;
	ICALEVENT event;
	do
	{
		linecount++;
		icalfile.getline( buf, BUF_SIZE );
		std::string line(buf);
		if( icalfile.eof() )
		{
			std::cout << " - Reached EOF in file\n";
			go_on = false;
		}
		else
		{
			if( !icalfile.good() )
			{
				std::cout << " - stop: bad datafile, failure\n";
				go_on = false;
				DEBUG_OUT;
				return false;
			}
			else
			{
//				cerr << "line " << l << " : ok\n";
				if( line.size() > 0 )
				{
					if( line.at(0) != ' ' )
					{
						StringVector tokens;
						Tokenize( line, tokens, ':', min_nb_tokens ); // tokenize the whole line
						if( tokens.size() != 2 )
							std::cout << "Error, line hasn't 2 tokens:\n" << line << std::endl;
						else
							event.ProcessData( GetPropFromText( tokens[0] ), tokens[1] );
					}
					else
					{
						std::cout << "space !!!\n";
						event.Add2Previous( line );
					}
				}
			}
		}
	}
	while( go_on );
	std::cout << " - success\n";
	DEBUG_OUT;
	return true;
}

//-----------------------------------------------------------------------------------

