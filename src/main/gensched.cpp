/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file main.cpp
\brief Holds the main() function

\todo clean up user error handling

*/

#include <ctime>

#include "helper_functions.h"
#include "cal_data.h"
#include "user_data.h"
#include "summary.h"
#include "groups.h"
#include "macros.h"
#include "globals.h"
#include "strings.h"
#include "stats.h"
#include "params.h"
#include "error.h"


using namespace std;

// valid languages, add codes here when adding new language files
static std::vector< std::string> gv_LangCodes = { "en", "fr" };


extern UserData* gp_data;
extern Params*   gp_params;

void Help();
void ShowDoc();
void TransferDataToEvents( const std::vector<LineContent>& in_table, UserData& data, Summary& summary, Stats& stats );


/// used only in main(), to show sequence of operation in debug mode
#define STEP( a ) \
		{ \
			CERR << "\n*** STEP " << __COUNTER__ << ": " << a << ENDL; \
		}

//-----------------------------------------------------------------------------------
/// Global data wrapper (WIP), to remove globals
struct DataWrapper
{
	DataWrapper( const UserData& userData, const Params& params )
		: _rUserData( userData)
		, _rParams( params )
	{}
	const UserData& _rUserData;
	const Params&   _rParams;
};
//-----------------------------------------------------------------------------------
/// see main.cpp
int main( int argc, const char** argv )
{
/*	int j = std::system( "gnuplot test.plt" );
	std::cout << "j=" << j << "\n";
	return 0;
*/
    g_caldata.SetCurrentDate();

   	UserData userdata;
	gp_data = &userdata;

	Params params;
	gp_params = &params;

    if( argc < 2 )
    {
        cout << " - needs 1 arguments (input filename), see gensched -h for help\n";
        return 1;
    }
// first step: check some "special" command-line switches, before the "real" parsing
    if( HasSingleOption( "-h", argc, argv ) )
    {
		Help();
		return 0;
    }
	if( HasSingleOption( "-H", argc, argv ) )
	{
		ShowDoc();
		return 0;
	}
	if( HasSingleOption( "-f", argc, argv ) )
	{
		params.PrintParams( cout );
		return 0;
	}

// see if user requests a configuration file different than the default one
	if( HasStringOption( "-p", argc, argv, params.ConfigFileName ) )
		COUT << "Requesting configuration file " << params.ConfigFileName << g_endl;

// get input data file name
    std::string csv_fn = argv[argc-1];
	if( csv_fn[0] == '-' )
	{
		cout << "Invalid option detected: " << csv_fn << ", exiting...\n";
		return 3;
	}

    cout << " - gensched: processing input file: \"" << csv_fn << "\"\n";
//    cerr << " - gensched: processing input file: \"" << csv_fn << "\"\n";


#ifdef DEBUG
	CERR << "- Compiled in DEBUG mode\n";
	params.DebugMode = true;
	params.VerboseMode = true;
#endif
	CERR << "- Running in DEBUG mode\n";

//	params.PrintParams( cerr );

// read configuration file
   	CERR << " - Reading configuration file '" << params.ConfigFileName << "':\n";
	COUT << " - Reading configuration file '" << params.ConfigFileName << "': ";
    if( !params.ReadParamsFromFile( params.ConfigFileName ) ) {
		COUT << "failure, using defaults\n";
	}
    else {
        COUT << "success\n";
		params.ConfigFileRead = true;
	}

//	params.PrintParams( cerr );


////////////////////////////////
// TEMP, WIP !!!!!!!!!!!!!!
//	DataWrapper dataWrapper( g_data, params );

	DayEvents::sp_params_d  = &params;
	WeekEvents::sp_params_w = &params;
	UserData::sp_params     = &params;
	Event::sp_params_e      = &params;
////////////////////////////////



	STEP( "Command-line analysis" );
    params.CommandLineAnalysis( argc, argv );

    if( params.OverrideYear != -1 )
	{
		CERR << "-Current year (" << g_caldata._current_year << ") overriden by  command-line switch, now " << params.OverrideYear << ENDL;
		g_caldata._current_year = params.OverrideYear;
	}

    try
    {
		FileHTML::AssignProjectName( csv_fn );
	}
	catch( const std::exception& err )
	{
		std::cerr << "main(): error=" << err.what() << g_endl;
		return 1;
	}


	STEP( "read groups from config file" );
// read groups from config file, if any
	if( params.GroupsFileName.size() )
	{
		COUT << " - Reading groups from file '" << params.GroupsFileName;
		if( !g_groups.ReadFromFile( params.GroupsFileName ) ) {
			COUT << "': failure, reading groups from input file\n";
		}
		else
		{
			COUT << "': success\n";
			params.HasGroups = true;
		}
	}

	Event::AssignGroups( g_groups );

	if( params.DebugMode )
		g_groups.Dump( cerr );

// if no groups read from file, then don't show course type, whatever request option
	if( !params.HasGroups )
		params.bswitch[BS_PRINT_EVENT_TYPE] = false;

//////////////////////////////////////////////////////////////////////////////
// read reference volume, if requested, and if available
	STEP( "read reference volume file" );
    if( params.HasReferenceFile )
    {
    	COUT << " - Reading volume reference from file '" << params.ReferenceFileName;
        if( !userdata.ReadRefVol( params.ReferenceFileName, params.CSV_delim ) )
        {
            COUT << "': failure\n";
            params.HasReferenceFile = false;
        }
        else
            COUT << "': success\n";
    }

//////////////////////////////////////////////////////////////////////////////
// read language file, if any
	STEP( "read language file" );
    if( params.LanguageFileName.size() )
    {
    	COUT << " - Reading language file '" << params.LanguageFileName;
        if( !g_strings.Read( params.LanguageFileName ) ) {
            COUT << "': failure\n";
		}
        else
            COUT << "': success\n";
    }
	g_files.AssignNames();

//////////////////////////////////////////////////////////////////////////////
// read instructor file, if requested
#if 0
    if( params.InstructorFileName.size() )
    {
    	COUT << " - Reading instructor file '" << params.InstructorFileName;
        if( !g_instr.Read( params.InstructorFileName ) ) {
			COUT << "': failure\n";
		}
        else
            COUT << "': success\n";
    }
#endif

    if( !CreateOutFolders( params ) )
	{
        COUT << "failure, exiting...\n";
        return 1;
	}

//////////////////////////////////////////////////////////////////////////////
// step 1 : read input data and store it in memory
	STEP( "read input file" );
	COUT << " - Read input file '" << csv_fn;
    std::vector<LineContent> in_table;
    if( !ReadCSV( csv_fn, in_table, params.CSV_delim, params.nb_tokens ) )
    {
        COUT << "': failure, exiting...\n";
        return 1;
    }
    else
		COUT << "': success, read " << in_table.size() << " lines\n";

	CERR << "- input file: read " << in_table.size() << " lines\n";

    if( in_table.size() < 1 )
    {
        COUT << " - input csv file is empty, exiting...\n";
        return 2;
    }

//	DumpTable(in_table);

//////////////////////////////////////////////////////////////////////////////
// step 2 : store the data in lists in a relational form
	STEP( "build lists from input data" );
	userdata.Init();
    userdata.BuildLists( in_table );

// check if reference file does defines additional courses
	if( userdata.AddSubjectsIfNeeded() )
		COUT << " - Warning: found unreferenced subjects in reference file '" << params.ReferenceFileName << "', see log file for details\n";

	if( params.DebugMode )
    {
		g_strings.Dump( cerr );
		params.Dump( cerr );
		userdata.PrintLists();
		g_groups.Dump( cerr );
	}

//////////////////////////////////////////////////////////////////////////////
// step 3 : If a week string is given on command-line, then parse it, else build week list from input data

	STEP( "build week string" );

	vector<size_t> v_num_weeks;
	if( false == params.WeekClString.empty() )                            // IF a week string was given
	{                                                                  // then, attempt the parsing
		CERR << "Building week num vector\n";
		try
		{
			BuildWeekNumVector( params.WeekClString, v_num_weeks );
		}
		catch( const ErrorClass& e )
		{
			cout << "Error, incorrect week option string: *" << params.WeekClString << "*\n";
			cerr << "main(): error message=" << e.what() << g_endl;
			return 3;
		}
	}
	else
	{
		try
		{
			BuildWeekNumList( in_table, v_num_weeks, params );              // ELSE, just build it from data
		}
		catch( const ErrorClass& e )
		{
			cout << "Error, unable to build week list\n";
			cerr << "main(): error message=" << e.what() << endl;
			return 4;
		}
	}

	PrintVector( std::cerr, v_num_weeks, "v_num_weeks: " );
	userdata.SetWeeks( v_num_weeks );

//////////////////////////////////////////////////////////////////////////////
// step 4 : parse input data and add events to weeks/days

	STEP( "parse input data" );
    Summary summary( userdata, g_groups, params );

// step 2 : transfert information into weeks and days
	TransferDataToEvents( in_table, userdata, summary, g_stats );

#ifdef DEBUG
	userdata.DumpData( cerr );
#endif

//////////////////////////////////////////////////////////////////////////////
// step 3: process output table
	STEP( "print schedule" );
    userdata.PrintPage_Week( params, OID_SCHED );

	STEP( "print week summary" );
    userdata.PrintPage_Week( params, OID_SUMMARY );

// print summary tables
//	summary.Dump( cerr );
	STEP( "print summary tables" );
    summary.PrintPage_Tables( userdata );

	STEP( "print instructor wortime" );
    summary.PrintPage_InstrWorktime( userdata );

	STEP( "print week usage" );
    userdata.PrintPage_GroupWeek( g_groups );

	STEP( "print groups per subject" );
    userdata.PrintPage_GroupsSubjects();


    STEP( "print conflicts" );
    userdata.PrintPage_Conflicts();

	STEP( "print ical files" );
	userdata.GenerateCalendars();

	STEP( "print instructors diaries" );
	userdata.PrintPage_InstrDiary();

	STEP( "print SW page" );
	userdata.PrintPage_SubjectPerWeek();

	STEP( "print IW page" );
	userdata.PrintPage_InstructorPerWeek();

	STEP( "print stats" );
    g_stats.PrintStatsHtml( userdata, params, g_groups );

	STEP( "Create index" );
    CreateIndex( userdata, g_groups, params );
	CreateHelpFile( userdata, g_groups, params );

	cout << " - nb of events processed: " << g_stats.GetNbEvents(EC_ALL) << ", invalid ones: " << g_stats.GetNbEvents( EC_INVALID );
	cout << "\n - nb of conflicts: " << g_stats.GetTotNbConflicts() << g_endl;
	COUT << " - nb of events with missing information: " << g_stats.NbMissingInfo;
    COUT << "\n - Done!\n";

	CERR << " - Finished!\n\n";
    return 0;
}

//-----------------------------------------------------------------------------------
void
TransferDataToEvents(
	const std::vector<LineContent>& in_table,
	UserData&   data,
	Summary&    summary,
	Stats&      stats
)
{
	DEBUG_IN;
	for( size_t k=0; k<in_table.size(); k++ )                         // parse ALL lines of input data
	{
		try
		{
			Event event( in_table.at(k) );                //   - build the event

			int week_no = event.GetIndex( IT_WEEK );
			if( data.HoldsWeek( week_no ) )
			{
				size_t widx = data.GetWeekIndex( week_no );
				WeekEvents& week = data.GetWeek(widx);

				if( week.AddEventToWeek( event ) )            //   - add it to the current week
				{
					summary.AddEvent( event );                //   - and add it to the summary (only if it's not a dupe)
					data.AddToSI_Map( event );
				}
			}
			else
			{
				stats.AddIgnoredEvent( event );
			}
		}
		catch( const ErrorClass& e )
		{
			cerr << __FUNCTION__ << ": Error: invalid event, error=" << e.what() << ENDL;
		}
    }
    data.FindIfSpecialEvents();
    DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void ShowDoc()
{
	std::string cmd;
	#ifdef _WIN32
		cmd = "c:\\program files\\gensched\\html\\index.html";   // NEEDS TO BE TESTED !!!
	#else
		cmd = "xdg-open /etc/gensched/html/index.html";
		// if problem, try also sensible-browser, see http://stackoverflow.com/questions/1950127/
	#endif
	int s = system( cmd.c_str() );
	if( s )
		std::cerr << " Something went wrong, cmd \"" << cmd << "\" failed...\n";
}


//-----------------------------------------------------------------------------------
void Help()
{
	cout << "gensched, a html schedule layout engine\n";
	cout << " - version: " << GENSCHED_VERSION << ", build on " << BUILD_DATE << ", ";

#ifdef _WIN32
	 cout << ", Windows build\n";
#endif
	cout << ENDL;

	cout << " - project page and manual: https://gitlab.com/skramm/gensched/\n";

	cout << " - usage: gensched [options] input_file\n - options:\n"
		<< "   -h: print this short Help and quit\n"
		<< "   -H: shows full (local) documentation in default browser and quit\n"
		<< "   -f: print deFault runtime values and quit\n"
		<< "   -t: add Time stamp to output folder\n"
		<< "   -v: verbose mode\n"
		<< "   -w <week_string>: specify which Weeks to extract\n"
		<< "   -p <file>: use 'file' as parameter file instead of 'gensched.ini'\n"
		<< "   -g <file>: use Groups file\n"
		<< "   -n: do Not copy style sheets in output folder\n"
		<< "   -l <language_code>: produce output using specified Language\n"
		<< "   -d: output some Debug information in stderr\n"
		<< "   -c: don't print course Codes in schedule page table cells\n"
		<< "   -i: colors per instructor (default is per subject)\n"
		<< "   -y <year>: override Year value\n"
		;

	cout << " Other settings may be given through configuration file (gensched.ini), see full manual\n";
	cout << " - Available language files: " << gv_LangCodes.size() << " (";
	for( size_t i=0; i<gv_LangCodes.size(); i++ )
	{
		cout << gv_LangCodes[i];
		if( i+1 < gv_LangCodes.size() )
			cout << ',';
	}
	cout << ")\n";
}


//-----------------------------------------------------------------------------------
