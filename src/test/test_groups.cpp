// test_groups.cpp


#include "groups.h"
#include "event.h"

using namespace std;

KUT_DEF_TEST_METHOD( GroupSets )
{
	KUT_CTM_START( GroupSets ); // Class Test Method - start

	KUT_EQ( GetNbGroups( GT_ATOMIC ),  0 );
	KUT_EQ( GetNbGroups( GT_CLASS ),  0 );
	KUT_EQ( GetNbGroups( GT_DIV ),  0 );

// -------------------- Adding two atomic groups: A, B
	{
		AtomicGroup gra ("A");
		AddGroup( gra );
	}
	KUT_EQ( GetNbGroups( GT_ATOMIC ),  1 );
	{
		AtomicGroup gra ("B");
		AddGroup( gra );
	}
	KUT_EQ( GetNbGroups( GT_ATOMIC ),  2 );

	KUT_EQ( GetGroupIndex( GT_ATOMIC, "A" ),  0 );
	KUT_EQ( GetGroupIndex( GT_ATOMIC, "B" ),  1 );
	KUT_EQ( GetGroupIndex( GT_ATOMIC, "C" ), -1 );    // C is not present

// -------------------- Adding a class group: AB
	{
		ClassGroup gra ("AB");
		KUT_TRY_NOTHROW( gra.AddSubGroup( "A" ) );
		KUT_TRY_NOTHROW( gra.AddSubGroup( "B" ) );
		KUT_TRY_THROW(   gra.AddSubGroup( "A" ) );   // this throws an exception: group A already added, can not be added twice.
		KUT_TRY_THROW(   gra.AddSubGroup( "C" ) );   // this throws an exception: group C does not exist.

		KUT_EQ( gra.GetFirstAtGroup().GetName(), "A" );
		KUT_TRY_NOTHROW( AddGroup( gra ) );
	}
	KUT_EQ( GetNbGroups( GT_CLASS ),  1 );

// -------------------- Adding a class group: CD
	{
		ClassGroup grc ("CD");
		KUT_TRY_THROW( AddGroup( grc ) );        // unable, empty

		KUT_TRY_THROW( grc.AddSubGroup( "C" ) );         // atomic group C does not exist

		AtomicGroup gr1 ("C");
		AtomicGroup gr2 ("D");
		AddGroup( gr1 );
		AddGroup( gr2 );

		KUT_TRY_NOTHROW( grc.AddSubGroup( "C" ) );
		KUT_TRY_NOTHROW( grc.AddSubGroup( "D" ) );
		KUT_EQ( grc.GetNbSubGroups(), 2 );
		KUT_TRY_NOTHROW( AddGroup( grc ) );

		KUT_EQ( grc.GetFirstAtGroup().GetName(), "C" );

		KUT_FALSE( grc.IncludesAtomicGroup_a( 0 ) );
		KUT_FALSE( grc.IncludesAtomicGroup_a( 1 ) );
		KUT_TRUE(  grc.IncludesAtomicGroup_a( 2 ) );
		KUT_TRUE(  grc.IncludesAtomicGroup_a( 3 ) );

	}
	KUT_EQ( GetNbGroups( GT_CLASS ),  2 );

// -------------------- Adding a class group: EFG
	{
		ClassGroup grc ("EFG");
		KUT_TRY_THROW( AddGroup( grc ) );
		KUT_TRY_THROW( grc.AddSubGroup( "E" ) );
		KUT_TRY_THROW( grc.AddSubGroup( "F" ) );
		KUT_TRY_THROW( grc.AddSubGroup( "G" ) );
		KUT_TRY_THROW( AddGroup( grc ) );               // group is empty

		AtomicGroup gr1 ("E"); AtomicGroup gr2 ("F"); AtomicGroup gr3 ("G");
		AddGroup( gr1 );        AddGroup( gr2 );        AddGroup( gr3 );

		KUT_TRY_NOTHROW( grc.AddSubGroup( "E" ) );
		KUT_TRY_NOTHROW( grc.AddSubGroup( "F" ) );
		KUT_TRY_NOTHROW( grc.AddSubGroup( "G" ) );

		KUT_EQ( grc.GetFirstAtGroup().GetName(), "E" );

		KUT_TRY_NOTHROW( AddGroup( grc ) );

		KUT_EQ( grc.GetFirstAtGroup().GetName(), "E" );
	}
	KUT_EQ( GetNbGroups( GT_CLASS ), 3 );

	KUT_EQ( GetDivisionIndex( GT_ATOMIC, 0 ), -1 );  // no divisions added
	KUT_EQ( GetDivisionIndex( GT_CLASS,  0 ), -1 );  // no divisions added
	KUT_EQ( GetDivisionIndex( GT_DIV,    0 ), -1 );  // no divisions added


// -------------------- Adding two division groups: D1, D2
	{
		DivGroup d1( "D1" );
		DivGroup d2( "D2" );
		KUT_TRY_THROW( AddGroup( d1 ) );     // group is empty
		KUT_TRY_THROW( d1.AddSubGroup( "aaa" ) );     // unable, no such group
		KUT_TRY_NOTHROW( d1.AddSubGroup( "AB" ) );
		KUT_TRY_NOTHROW( d1.AddSubGroup( "CD" ) );
		KUT_TRY_THROW(   d1.AddSubGroup( "CD" ) );      // "CD" already in group d1

		KUT_EQ( GetNbGroups( GT_DIV ), 0 );
		KUT_TRY_NOTHROW( AddGroup( d1 ) );
		KUT_TRY_THROW(   AddGroup( d2 ) ); // unable, empty

		KUT_TRY_THROW( d2.AddSubGroup( "E" ) );   // no such class group
		KUT_TRY_NOTHROW( d2.AddSubGroup( "EFG" ) );
		KUT_TRY_THROW( d2.AddSubGroup( "EFG" ) );  // "EFG" already in group

		KUT_TRY_NOTHROW( AddGroup( d2 ) );

		KUT_TRUE( d1.IncludesAtomicGroup_a( 0 ) );
		KUT_TRUE( d1.IncludesAtomicGroup_a( 1 ) );
		KUT_TRUE( d1.IncludesAtomicGroup_a( 2 ) );
		KUT_TRUE( d1.IncludesAtomicGroup_a( 3 ) );
		KUT_TRUE( d2.IncludesAtomicGroup_a( 4 ) );
		KUT_TRUE( d2.IncludesAtomicGroup_a( 5 ) );
		KUT_TRUE( d2.IncludesAtomicGroup_a( 6 ) );
		KUT_FALSE( d2.IncludesAtomicGroup_a( 7 ) ); // no atomic group with index 7

		KUT_EQ( d1.GetFirstAtGroup().GetName(), "A" );
		KUT_EQ( d2.GetFirstAtGroup().GetName(), "E" );

		KUT_EQ( GetNbGroups( GT_DIV ), 2 );

		KUT_EQ( GetDivisionIndex( GT_ATOMIC, 0 ), 0 );
		KUT_EQ( GetDivisionIndex( GT_ATOMIC, 1 ), 0 );
		KUT_TRY_THROW( GetDivisionIndex( GT_ATOMIC, 9 ) );  // we don't have 9 atomic groups

		KUT_EQ( GetDivisionIndex( GT_CLASS, 0 ), 0 );
		KUT_EQ( GetDivisionIndex( GT_CLASS, 1 ), 0 );
		KUT_EQ( GetDivisionIndex( GT_CLASS, 2 ), 1 );
		KUT_TRY_THROW( GetDivisionIndex( GT_CLASS, 3 ) ); // we only have 3 class groups

		KUT_MSG( "test of DivGroup::IncludesGroup()" );
		KUT_TRUE(  d1.IncludesGroup( GT_ATOMIC, 0 ) );   // atomic group A (idx:0) is in d1, not in d2
		KUT_FALSE( d2.IncludesGroup( GT_ATOMIC, 0 ) );
		KUT_FALSE( d1.IncludesGroup( GT_ATOMIC, 4 ) );   // atomic group E (idx:4) is in d2, not in d1
		KUT_TRUE(  d2.IncludesGroup( GT_ATOMIC, 4 ) );

		KUT_TRUE(  d1.IncludesGroup( GT_CLASS, 0 ) );  // class group AB (idx:0) is in d1 not in d2
		KUT_FALSE( d2.IncludesGroup( GT_CLASS, 0 ) );

		KUT_FALSE( d1.IncludesGroup( GT_CLASS, 2 ) );  // class group EFG (idx:2) is in d2 not in d1
		KUT_TRUE(  d2.IncludesGroup( GT_CLASS, 2 ) );

		KUT_TRUE(  d1.IncludesGroup( GT_DIV, 0 ) );   // division d1 includes division d1 (idx:0)
		KUT_FALSE( d2.IncludesGroup( GT_DIV, 0 ) );
		KUT_FALSE( d1.IncludesGroup( GT_DIV, 1 ) );
		KUT_TRUE(  d2.IncludesGroup( GT_DIV, 1 ) );

		KUT_EQ( d1.GetAtomicIdx(0), 0 );
		KUT_EQ( d1.GetAtomicIdx(1), 1 );
		KUT_EQ( d1.GetAtomicIdx(2), 2 );
		KUT_EQ( d1.GetAtomicIdx(3), 3 );
		KUT_EQ( d2.GetAtomicIdx(0), 4 ); // E
		KUT_EQ( d2.GetAtomicIdx(1), 5 ); // F
		KUT_EQ( d2.GetAtomicIdx(2), 6 ); // G

		std::vector<size_t> vd1={0,1,2,3};
		std::vector<size_t> vd2={4,5,6};
		std::vector<size_t> v;
/*
		d1.GetAtomicGroups( v );
		KUT_EQ( v, vd1 );
		d2.GetAtomicGroups( v );
		KUT_EQ( v, vd2 );
*/

	}
	Dump( cerr );

	KUT_MSG( "test of DivGroup::Overlaps()" );
	const StudentGroup& gd1 = GetGroup( GT_DIV, 0 );
	const StudentGroup& gd2 = GetGroup( GT_DIV, 1 );
	KUT_FALSE( gd1.Overlaps(gd2) );
	KUT_FALSE( gd2.Overlaps(gd1) );
	KUT_TRUE(  gd1.Overlaps(gd1) );
	KUT_TRUE(  gd2.Overlaps(gd2) );

	const StudentGroup& gc1 = GetGroup( GT_CLASS, 0 );  // group "AB"
	const StudentGroup& gc2 = GetGroup( GT_CLASS, 1 );  // group "CD"
	KUT_TRUE( gc1.Overlaps(gc1) );
	KUT_TRUE( gc2.Overlaps(gc2) );

	KUT_FALSE( gc1.Overlaps(gc2) );
	KUT_FALSE( gc2.Overlaps(gc1) );

	KUT_FALSE( gd2.Overlaps(gc1) );
	KUT_FALSE( gc1.Overlaps(gd2) );

	KUT_TRUE( gd1.Overlaps(gc1) );
	KUT_TRUE( gc1.Overlaps(gd1) );


	const StudentGroup& ga1 = GetGroup( GT_ATOMIC, 0 );  // group "A"
	const StudentGroup& ga2 = GetGroup( GT_ATOMIC, 1 );  // group "B"
	KUT_TRUE( ga1.Overlaps(ga1) );
	KUT_TRUE( ga2.Overlaps(ga2) );

	KUT_TRUE( ga1.Overlaps(gc1) );
	KUT_TRUE( gc1.Overlaps(ga1) );

	KUT_TRUE( ga1.Overlaps(gd1) );
	KUT_TRUE( gd1.Overlaps(ga1) );

	KUT_FALSE( gd2.Overlaps(ga1) );
	KUT_FALSE( ga1.Overlaps(gd2) );

	const StudentGroup& gc3 = GetGroup( GT_CLASS, 2 );  // group "EFG"

	KUT_FALSE( gc3.Overlaps(ga1) );
	KUT_FALSE( ga1.Overlaps(gc3) );

	KUT_FALSE( gc3.Overlaps(gc1) );
	KUT_FALSE( gc1.Overlaps(gc3) );

	KUT_FALSE( gd1.Overlaps(gc3) );
	KUT_FALSE( gc3.Overlaps(gd1) );

	KUT_TRUE( gc3.Overlaps(gd2) );
	KUT_TRUE( gd2.Overlaps(gc3) );


	KUT_MSG( "test of reading demo group files" );
	Clear();

	KUT_TRUE( ReadFromFile( "demo/groups_demoB_4b.ini" ) );
	Dump( cerr );

	KUT_TRUE( ReadFromFile( "demo/groups_demoB_4c.ini" ) );
	Dump( cerr );

	KUT_TRUE( ReadFromFile( "demo/groups_demoB_4d.ini" ) );
	Dump( cerr );

	KUT_TRUE( ReadFromFile( "demo/groups_demoD_full.ini" ) );
	Dump( cerr );

/*
	KUT_MSG( "test of GroupAndGroupTypeOK()" );
	Event e;

	e._group = "AB";
	e._GroupType = GT_ATOMIC; KUT_FALSE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_CLASS;  KUT_TRUE(  g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_DIV;    KUT_FALSE( g.GroupAndGroupTypeOK( e ) );

	e._group = "A";
	e._GroupType = GT_ATOMIC; KUT_TRUE(  g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_CLASS;  KUT_FALSE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_DIV;    KUT_FALSE( g.GroupAndGroupTypeOK( e ) );

	e._group = "FA";
	e._GroupType = GT_ATOMIC; KUT_TRUE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_CLASS;  KUT_TRUE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_DIV;    KUT_TRUE( g.GroupAndGroupTypeOK( e ) );

	e._group = "FI";
	e._GroupType = GT_ATOMIC; KUT_FALSE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_CLASS;  KUT_FALSE( g.GroupAndGroupTypeOK( e ) );
	e._GroupType = GT_DIV;    KUT_TRUE(  g.GroupAndGroupTypeOK( e ) );


*/
	KUT_CTM_END;	// Class Test Method - end
}
