
// test_helper_functions.cpp


#include "helper_functions.h"
#include "kut.h"
#include "htag.h"
#include <sstream>

using namespace std;

//-----------------------------------------------------------------------------------
KUT_TYPE Test_GetTime()
{
	KUT_FT_START; //(( getTime );

	KUT_EQ( GetTime("8h30"),   "083000" );
	KUT_EQ( GetTime("8H30"),   "083000" );
	KUT_EQ( GetTime("8:30"),   "083000" );
//	KUT_EQ( GetTime("8:30am"), "083000Z" );
//	KUT_EQ( GetTime("8:30AM"), "083000Z" );

	KUT_EQ( GetTime("9am"), "090000" );
	KUT_EQ( GetTime("9AM"), "090000" );
	KUT_EQ( GetTime("9pm"), "210000" );
	KUT_EQ( GetTime("9PM"), "210000" );

//	KUT_EQ( GetTime("0900"), "090000Z" );


	KUT_FT_END;
}

//-----------------------------------------------------------------------------------
KUT_TYPE Test_tokenizer()
{
	KUT_FT_START; //(( tokenizer );
	std::vector<std::string> v_output;
	{
		Tokenize( "a,b,c", v_output );

		KUT_EQ( v_output.size(), 3 );
		KUT_EQ( v_output[0], "a" );
		KUT_EQ( v_output[1], "b" );
		KUT_EQ( v_output[2], "c" );
	}
	{
		Tokenize( "a, ,c", v_output );

		KUT_EQ( v_output.size(), 3 );
		KUT_EQ( v_output[0], "a" );
		KUT_EQ( v_output[1], " " );
		KUT_EQ( v_output[2], "c" );
	}
	{
		Tokenize( "a,,c,", v_output );

		KUT_EQ( v_output.size(), 4 );
		KUT_EQ( v_output[0], "a" );
		KUT_EQ( v_output[1], "" );
		KUT_EQ( v_output[2], "c" );
		KUT_EQ( v_output[3], "" );
	}
	KUT_FT_END;
}

//-----------------------------------------------------------------------------------
KUT_TYPE Test_BuildWeekNumVector()
{
	KUT_FT_START; //(( BuildWeekNumVector );
	std::vector<size_t> v_num_weeks;
	{
		string s = "34";
		BuildWeekNumVector( s, v_num_weeks );
		KUT_EQ( v_num_weeks.size(), 1 );
		KUT_EQ( v_num_weeks[0], 34 );
	}
	{
		string s = "34,35,36";
		BuildWeekNumVector( s, v_num_weeks );
		KUT_EQ( v_num_weeks.size(), 3 );
		KUT_EQ( v_num_weeks[0], 34 );
		KUT_EQ( v_num_weeks[1], 35 );
		KUT_EQ( v_num_weeks[2], 36 );
	}
	{
		string s = "34-35";
		BuildWeekNumVector( s, v_num_weeks );
		KUT_EQ( v_num_weeks.size(), 2 );
		KUT_EQ( v_num_weeks[0], 34 );
		KUT_EQ( v_num_weeks[1], 35 );
	}
	{
		string s = "10,34-36,40";
		BuildWeekNumVector( s, v_num_weeks );
		KUT_EQ( v_num_weeks.size(), 5 );
		KUT_EQ( v_num_weeks[0], 10 );
		KUT_EQ( v_num_weeks[1], 34 );
		KUT_EQ( v_num_weeks[2], 35 );
		KUT_EQ( v_num_weeks[3], 36 );
		KUT_EQ( v_num_weeks[4], 40 );
	}

	KUT_MSG( "erroneous week strings" );
	{
		string s = "36-35";
		KUT_TRY_THROW( BuildWeekNumVector( s, v_num_weeks ) );
	}

	KUT_FT_END;
}

//-----------------------------------------------------------------------------------
KUT_TYPE Test_DayFromWeek()
{
	KUT_FT_START; //(( test_DayFromWeek );
	string format( "%Y%m%d" );

	int year = 2014;
	KUT_EQ( GetDate( 2, 0, year, format ), "20140106" ); // week 2, monday, 2014 is 6/01/2014
	KUT_EQ( GetDate( 2, 1, year, format ), "20140107" ); // week 2, tuesday, 2014 is 7/01/2014
	KUT_EQ( GetDate( 3, 0, year, format ), "20140113" ); // week 3, monday, 2014 is 13/01/2014

	year = 2015;
	KUT_EQ( GetDate( 2, 0, year, format ), "20150105" ); // week 2, monday, 2015 is 5/01/2015
	KUT_EQ( GetDate( 3, 0, year, format ), "20150112" ); // week 3, monday, 2015 is 12/01/2015

	year = 2016;
	KUT_EQ( GetDate( 1, 0, year, format ), "20160104" ); // week 1, monday, 2016 is 4-01-2016
	KUT_EQ( GetDate( 1, 1, year, format ), "20160105" ); // week 1, tuesday, 2016 is 5-01-2016
	KUT_EQ( GetDate( 2, 0, year, format ), "20160111" ); // week 2, monday, 2016 is 11-01-2016

	year = 2017;
	KUT_EQ( GetDate( 1, 0, year, format ), "20170102" ); // week 1, monday, 2017 is 2-01-2016

	KUT_FT_END;
}

//-----------------------------------------------------------------------------------
KUT_TYPE Test_HTAG()
{
	KUT_FT_START; //( test_HTAG );

	{                                      // string content
		std::ostringstream s;
		s << HTAG( HT_TD, "aaa" );
		KUT_EQ( s.str(), string("<td>aaa</td>") );
	}
	{                                      // int content
		std::ostringstream s;
		s << HTAG( HT_TD, 1 );
		KUT_EQ( s.str(), string("<td>1</td>") );
	}
	{                                      // float content
		std::ostringstream s;
		s << HTAG( HT_TD, 1.5f );
		KUT_EQ( s.str(), string("<td>1.5</td>") );
	}

/// \todo This test fails, as the constructor seems to take the default (std::string) version of templated constructor. Investigate! (when some time...)
	{                                      // double content
		std::ostringstream s;
		s << HTAG( HT_TD, 1.5f );                      // ok for floats<
		KUT_EQ( s.str(), "<td>1.5</td>" );
	}
	{                                      // double content
		std::ostringstream s;
		s << HTAG( HT_TD, 1.5 );                      // fail for double
		KUT_EQ( s.str(), "<td>1.5</td>" );
	}

	KUT_MSG( "test of multiple classes" );

	HTAG td(HT_TD, "aaa", AT_CLASS, "c1" );
	{
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td class=\"c1\">aaa</td>" );
	}
	KUT_TRY_NOTHROW( td.AddAttrib( AT_ID, "id" ) );
	{
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td class=\"c1\" id=\"id\">aaa</td>" );
	}
	KUT_TRY_THROW(   td.AddAttrib( AT_ID, "id2" ) );  // can't add a second 'id' attribute
	KUT_TRY_NOTHROW( td.AddAttrib( AT_CLASS, "c2" ) );
	{
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td class=\"c1 c2\" id=\"id\">aaa</td>" );
	}
	{
		HTAG td( HT_TD );
		td << "blahblahblah";
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td>blahblahblah</td>" );
	}
	{
		HTAG td( HT_TD, "iii-" );
		td << "ooo";
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td>iii-ooo</td>" );
	}
	{
		HTAG td( HT_TD, "iii-", AT_CLASS, "ccc" );
		td << "ooo";
		ostringstream oss;
		oss << td;
		KUT_EQ( oss.str(), "<td class=\"ccc\">iii-ooo</td>" );
	}

	KUT_FT_END;
}

//-----------------------------------------------------------------------------------
KUT_TYPE Test_CompareVectors()
{
	KUT_FT_START; //( CompareVectors );

	const vector<size_t> v1 = { 1, 2, 3, 4, 5 };
	const vector<size_t> v2 = { 3, 4, 5, 6, 7, 8 };
	vector<size_t> v_only_1;
	vector<size_t> v_only_2;
	vector<size_t> v_common;
	KUT_EQ( 5, CompareVectors( v1, v2, v_only_1, v_only_2, v_common ) );

	const vector<size_t> v_gt_only_1 = { 1, 2 };
	const vector<size_t> v_gt_only_2 = { 6, 7, 8 };
	const vector<size_t> v_gt_common = { 3, 4, 5 };

	KUT_EQ( v_only_1.size(), 2 );
	KUT_EQ( v_only_2.size(), 3 );
	KUT_EQ( v_common.size(), 3 );
	KUT_EQ_NS( v_only_1, v_gt_only_1 );
	KUT_EQ_NS( v_only_2, v_gt_only_2 );
	KUT_EQ_NS( v_common, v_gt_common );

	KUT_FT_END;
}
//-----------------------------------------------------------------------------------
