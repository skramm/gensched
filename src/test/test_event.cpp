// test_event.cpp

#include "event.h"
#include "globals.h"
#include "user_data.h"
#include "groups.h"
#include "params.h"
#include "helper_functions.h"

extern Params        g_params;
extern UserData g_data;

KUT_DEF_TEST_METHOD( Event )
{
	KUT_CTM_START( Event ); // Class Test Method - start

	g_data.Init(); // this is needed to initialise the user data with the default subject, room, teacher

// create some groups
	GroupSets groups;
	Event::AssignGroups( groups );
	{
		groups.AddGroup( AtomicGroup( "A" ) );
		groups.AddGroup( AtomicGroup( "B" ) );
	}

	StringVector line((int)IT_MAXVALUE_INPUT_INDEX);

	CERR << " - line.size=" << line.size() << ENDL;
	line[g_params.input_pos[IT_SUBJECT]] = g_params.unknown_subject_code;

	KUT_TRY_THROW( Event e( std::make_pair(line, 0) ) );   // some fields are empty => invalid

	line[g_params.input_pos[IT_ROOM]]       = g_params.unknown_room_code;
	line[g_params.input_pos[IT_INSTRUCTOR]] = g_params.unknown_instructor_code;

	KUT_TRY_THROW( Event e( std::make_pair(line, 0) ) );   // some fields are still empty => invalid

	line[g_params.input_pos[IT_DURATION]] = "1";
	line[g_params.input_pos[IT_WEEK]] = "30";
	line[g_params.input_pos[IT_TIMESLOT]] = "M1";
	line[g_params.input_pos[IT_DAY]] = "M";   // Monday
	line[g_params.input_pos[IT_TYPE]] = "AG";
	line[g_params.input_pos[IT_GROUP]] = "A";

	KUT_TRY_NOTHROW( Event e( std::make_pair(line, 0) ) );   // valid line

// create two events with gr A and B and check that the second one does not hold the first one
	line[g_params.input_pos[IT_GROUP]] = "A";
	Event e1( std::make_pair(line, 0) );
	line[g_params.input_pos[IT_GROUP]] = "B";
	Event e2( std::make_pair(line, 0) );
	KUT_FALSE( e2.HoldsGroup( GT_ATOMIC, 0 ));

	KUT_CTM_END;	// Class Test Method - end
}

KUT_TYPE InvalidLines()
{
	KUT_FT_START;

// create some groups
	GroupSets groups;
	Event::AssignGroups( groups );
	groups.AddGroup( AtomicGroup( "A" ) );

	{
		KUT_MSG( "test of INvalid input data" );
		std::vector<LineContent> in_table;
		KUT_TRUE( ReadCSV( "src/test/test_invalid_data.csv", in_table, g_params.nb_tokens ) );

		g_data.Init();
		g_data.BuildLists( in_table );
		g_data.PrintLists();

		for( size_t k=0; k<in_table.size(); k++ )                         // parse lines of input data
		{
			CERR << "k=" << k << ", ";
			PrintLine(in_table[k]);
			KUT_TRY_THROW_2( Event e( in_table[k] ), k );                //   - build the event and check that it throws an exception
		}
	}

	{
		KUT_MSG( "test of valid input data" );
		std::vector<LineContent> in_table;
		KUT_TRUE( ReadCSV( "src/test/test_valid_data.csv", in_table, g_params.nb_tokens ) );

		g_data.Init();
		g_data.BuildLists( in_table );
		g_data.PrintLists();

		for( size_t k=0; k<in_table.size(); k++ )                         // parse lines of input data
		{
			CERR << "k=" << k << ", ";
			PrintLine(in_table[k]);
			KUT_TRY_NOTHROW_2( Event e( in_table[k] ), k );                //   - build the event and check that it throws an exception
		}
	}

	KUT_FT_END;
}
