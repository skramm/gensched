
#include <kut.h>
// test file
#include "event.h"
#include "groups.h"
#include "params.h"
#include "globals.h"
#include "user_data.h"

using namespace std;

Params   g_params;
UserData g_data;

KUT_ALLOC;

int main()
{
	KUT_MAIN_START;

g_params.DebugMode = true;

	KUT_TEST_CLASS( Event );	// call of unit-test function of ClassA
	KUT_TEST_CLASS( GroupSets );	// call of unit-test function of GroupSets

	KUT_TEST_FUNC( Test_BuildWeekNumVector );	// call of unit-test function
	KUT_TEST_FUNC( Test_GetTime );	// call of unit-test function

	KUT_TEST_FUNC( Test_HTAG );	// call of unit-test function

	KUT_TEST_FUNC( Test_tokenizer );
	KUT_TEST_FUNC( Test_CompareVectors );
	KUT_TEST_FUNC( Test_GetCLA );

	KUT_TEST_FUNC( Test_DayFromWeek );
	KUT_TEST_FUNC( InvalidLines );

	KUT_MAIN_END;
}
