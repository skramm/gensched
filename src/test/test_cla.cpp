
/**
\name test_cla.cpp
\brief test of Command Line Arguments parsing
*/

#include "kut.h"
#include "helper_functions.h"

using namespace std;

//-----------------------------------------------------------------------------------
KUT_TYPE Test_GetCLA()
{
	KUT_FT_START; //( hasSingleOption );
	{
		const char* argv[] = { "zero", "one", "two", "three" };
		int argc = 4;

		KUT_FALSE( HasSingleOption( "aaa", argc, argv ) );
		KUT_TRUE( HasSingleOption( "one", argc, argv ) );
	}

	{
		const char* argv[] = { "zero", "one", "two", "three" };
		int argc = 4;
		string val;
		KUT_FALSE( HasStringOption( "aaa", argc, argv, val ) );
		KUT_TRUE( HasStringOption( "one", argc, argv, val ) );
		KUT_EQ( val, "two" );

		KUT_TRY_THROW( HasStringOption( "three", argc, argv, val ) );
		KUT_TRUE( HasStringOption( "two", argc, argv, val ) );
		KUT_EQ( val, "three" );
	}

	KUT_FT_END;
}

//-----------------------------------------------------------------------------------

