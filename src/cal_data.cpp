/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "cal_data.h"
#include <algorithm>
#ifdef _WIN32
	#include <ctime> // needed on windows build
#endif


//-----------------------------------------------------------------------------------
/// Stores the current running time, so it can be used for generating file names and file content (timestamp)
/**
For ical timestamp format, see https://tools.ietf.org/html/rfc5545.html#section-3.3.5

Here we use the UTC form, ad this is required for the DTSTAMP attribute
*/
void CalData::SetCurrentDate()
{
	time_t result = time(NULL);
    tm* t = localtime(&result);
    char buf[128];

    strftime( buf, 128, "%Y%m%d_%H%M", t );
	_now = buf;

    strftime( buf, 128, "%Y%m%dT%H%M%SZ", t );
	_now_rfc_ical = buf;

	_current_year = t->tm_year +1900;
}
//-----------------------------------------------------------------------------------
