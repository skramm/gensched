/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// summary.cpp

#include <iomanip>      // std::setprecision

#include "globals.h"

#include "helper_functions.h"
#include "summary.h"
#include "htag.h"
#include "user_data.h"
#include "cal_data.h"
#include "strings.h"
#include "params.h"
#include "groups.h"


//-----------------------------------------------------------------------------------
/// Constructor
Summary::Summary( const UserData& data, const GroupSets& groups, const Params& params )
	: _rv_teachers(data._v_instructors)
	, _rv_subjects(data._v_subjects)
	, _rv_rooms(data._v_rooms)
	, _r_groups(groups)
	, _rParams(params)
{
	DEBUG_IN;

	TripletTable::sp_groups = &groups;

	size_t n = _r_groups.GetNbGroups( GT_DIV );
	if( 0 == n )
		n++;

	_v_table_is.resize( n );
	_v_table_ir.resize( n );
	_v_table_sr.resize( n );

	for( size_t i=0; i<n; i++ )
	{
		_v_table_is[i].Alloc( _rv_teachers.size(), _rv_subjects.size() );
		_v_table_ir[i].Alloc( _rv_teachers.size(), _rv_rooms.size() );
		_v_table_sr[i].Alloc( _rv_subjects.size(), _rv_rooms.size() );
	}
	_sum_table_is.Alloc( _rv_teachers.size(), _rv_subjects.size() );
	_sum_table_ir.Alloc( _rv_teachers.size(), _rv_rooms.size() );
	_sum_table_sr.Alloc( _rv_subjects.size(), _rv_rooms.size() );

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void Summary::AddEvent( const Event& e )
{
	DEBUG_IN;
//	CERR << "ADD Event TO SUMMARY, event=" << e << ENDL;

	if( e.GetEventType() == ET_DAY_OFF )
//	if( e.IsSpecialEvent() )   // special events are not added in summary
	{
		DEBUG_OUT;
		return;
	}

	size_t index_i = e.GetIndex( IT_INSTRUCTOR );
	size_t index_s = e.GetIndex( IT_SUBJECT );
	size_t index_r = e.GetIndex( IT_ROOM );
	float duration = e.GetIndex( IT_DURATION ) * _rParams.atomic_duration;

	Triplet t;
	if( e.GetEventType() == ET_TEACH )
		t.Add( e.GetGroupType(), duration );
	else
		t.Add( GT_NONE, duration );

//	CERR << "ADD Event TO SUMMARY, triplet=" << t << ENDL;

	int div_idx = e.GetIndex( IT_DIVISION );
	if( -1 == div_idx )           // if no division, then div_idx = 0;
		div_idx = 0;

	_v_table_is.at(div_idx).Add2Cell( index_i, index_s, t );
	_v_table_ir.at(div_idx).Add2Cell( index_i, index_r, t );
	_v_table_sr.at(div_idx).Add2Cell( index_s, index_r, t );

	_sum_table_is.Add2Cell( index_i, index_s, t );
	_sum_table_ir.Add2Cell( index_i, index_r, t );
	_sum_table_sr.Add2Cell( index_s, index_r, t );

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
#if 0
void OpenDataPlotFile( std::ofstream& f, const std::string& gr )
{
	assert( !f.is_open() );
	std::ostringstream oss;
	oss << g_params.OutputDirFullName << "/week_volume_" << gr << ".dat";
	f.open( oss.str() );
	assert( f.is_open() );
	f << "# group " <<  gr << g_endl;
	f << "# date; CM; TD; TP\n";
}
//-----------------------------------------------------------------------------------
void CloseDataPlotFile( ofstream& f )
{
	assert( f.is_open() );
	f.close();
}
#endif

//-----------------------------------------------------------------------------------
/// Helper function for Summary::PrintPage_InstrWorktime()
void
Summary::PrintWTHeader( FileHTML& f ) const
{
	HTAG tr( f, HT_TR );
	tr.OpenTag();

	f.fout << HTAG( HT_TH, g_strings.GetTxt(S_Instr) ) << g_endl;  // column "instructor"
	f.fout << HTAG( HT_TH, g_strings.GetTxt(S_Subj) ) << g_endl; // column "subject"

	if( _r_groups.HasDivisions() )
		f.fout << HTAG( HT_TH, "Division" ) << HTAG( HT_TH, _rParams.GetCodeFromGroupType( GT_DIV ) ) << g_endl;

	if( _r_groups.HasClassGroups() )
		f.fout << HTAG( HT_TH, _rParams.GetCodeFromGroupType( GT_CLASS ) ) << g_endl;

	if( _r_groups.HasDivisions() )
		f.fout << HTAG( HT_TH, _rParams.GetCodeFromGroupType( GT_ATOMIC ) ) << g_endl;
	else
		f.fout << HTAG( HT_TH, "Vol." ) << g_endl;

	f.fout << HTAG( HT_TH, g_strings.GetTxt(S_SUM_PER_SUBJ) ) << HTAG( HT_TH, g_strings.GetTxt(S_SUM_PER_INSTR) ) << g_endl;
}

//-----------------------------------------------------------------------------------
/// Helper function for Summary::PrintPage_InstrWorktime()
void
CountUsefulStuff(
	const std::vector<TripletTable>& v_table_is,     ///< input data
	size_t                    i,                     ///< input: instructor index
	size_t&                   NbSumLinesPerDiv,      ///< output:
	size_t&                   nb_subjects_notnull )  ///< output:
{
	for( const auto& table: v_table_is ) // table for each division
	{
		size_t count_subj_per_div = 0;
		const std::vector<Triplet>& vt = table.GetRow(i);
		for( const auto& t : vt ) // t: a Triplet
		{
			if( t.Sum() != 0 )
			{
				nb_subjects_notnull++;
				count_subj_per_div++;
			}
		}
		if( count_subj_per_div > 1 )
			NbSumLinesPerDiv++;
	}
}

//-----------------------------------------------------------------------------------
/// Generates the "worktime" output page
void
Summary::PrintPage_InstrWorktime( const UserData& data ) const
{
	DEBUG_IN;

	OutputFile_SP fp = g_files.GetFile( OF_WT );

	std::string csv_fn = "gensched_worktime"; // + g_caldata.GetNow();
	FileCSV fpcsv( csv_fn );

	fp->SetStyleSheet( "style_table.css" );
	fp->Open();
	fpcsv.Open();
	fpcsv.fout << "# instructor, subject, division, Lecture volume, class volume, lab volume\n";
	fpcsv.fout << "# generated on " << g_caldata.GetNow() << g_endl;

	fp->fout << "<p><a href=\"" << csv_fn << ".csv\" target=\"_blank\">Raw csv data</a></p>\n";
	fp->fout << "<table>\n";
	PrintWTHeader( *fp );

	for( size_t i=0; i<_rv_teachers.size(); i++ ) // enumerate all the instructors
	{
		CERR << "INSTRUCTOR INDEX: " << i << g_endl;

// step 1: count how many subjects this teacher has, and how many divisions this teacher has (needed for the "rowspan" attribute)
		size_t NbSumLinesPerDiv = 0;
		size_t nb_subjects_notnull = 0;
		CountUsefulStuff( _v_table_is, i, NbSumLinesPerDiv, nb_subjects_notnull );


// step 2: proceed this instructor (only if it is not the "dummy" one (i=0) or if it indeed has some work)
//		if( i != 0 && nb_subjects_notnull != 0 )
		if( nb_subjects_notnull != 0 )
		{
			size_t  div_count = 0;
			Triplet t_all;
			size_t  line_count = 0;
			bool    firstline_interdiv = true;
			for( const auto& table: _v_table_is ) // process each division separately
			{
				bool firstline_intradiv = true;
				const std::vector<Triplet>& vt = table.GetRow(i); // for that given division, get the list of subjects where the instructor 'i' teaches
				size_t subject_count = 0;
				Triplet div_sum;
				size_t NbSubjectsNotNull = 0;
				for( const auto& t : vt ) // t: a Triplet, one for each column of table (one for each subject)
				{
					if( t.Sum() != 0 )
					{
						line_count++;      // this is needed only to get oddlines gray
						div_sum += t;

						HTAG tr( *fp, HT_TR );
						if( !firstline_intradiv || !firstline_interdiv ) // if it is NOT the first line, we need to check for odd lines
						{
							if( !(line_count%2) )
								tr.AddAttrib( AT_CLASS, "oddline" );
						}
						tr.OpenTag();

						if( firstline_intradiv && firstline_interdiv )  // we print the first left cell holding instructor name
						{
							HTAG td( *fp, HT_TD, AT_ROWSPAN, std::to_string(nb_subjects_notnull + NbSumLinesPerDiv + 1) );
							td.AddAttrib( AT_STYLE, "border-bottom-width:2px;" );
							td.OpenTag();
							{
								HTAG a( *fp, HT_A, AT_HREF, "id.html#instr_" + std::to_string(i) );
								a.OpenTag();
								fp->fout << i << ": " << _rv_teachers.at( i );
							}
							td.CloseTag(true);
						}
						{
							fp->fout << HTAG( HT_TD, _rv_subjects.at( subject_count ) ) << g_endl;
							if( _r_groups.HasDivisions() )
							{
								fp->fout << HTAG( HT_TD, _r_groups.GetGroup(GT_DIV, div_count).GetName() ) << g_endl;
								fp->fout << HTAG( HT_TD, t.Get( GT_DIV ) ) << g_endl;
							}
							if( _r_groups.HasClassGroups() )
								fp->fout << HTAG( HT_TD, t.Get( GT_CLASS ) ) << g_endl;
							fp->fout << HTAG( HT_TD, t.Get( GT_ATOMIC ) ) << g_endl;
							{
								HTAG td( HT_TD );
								td << t.Sum_wt() << " h.";
								*fp << td;
							}
							fp->fout << HTAG( HT_TD ) << g_endl;
						}
						tr.CloseTag();

						t_all += t;
						firstline_intradiv = false;
						firstline_interdiv = false;

						fpcsv.fout << _rv_teachers.at( i ) << ',' << _rv_subjects.at( subject_count ) << ',';
						if( _r_groups.HasDivisions() )
							fpcsv.fout << _r_groups.GetGroup(GT_DIV, div_count).GetName();
						fpcsv.fout << ',' << t.Get( GT_DIV );
						fpcsv.fout << ',' << t.Get( GT_CLASS );
						fpcsv.fout << ',' << t.Get( GT_ATOMIC ) << g_endl;

						NbSubjectsNotNull++;
					}
					subject_count++;
				}

				if( div_sum.Sum() > 0 && NbSubjectsNotNull > 1 )
				{
					HTAG tr( *fp, HT_TR, AT_CLASS, "sum-line_div" );
					tr.OpenTag();

					fp->fout << HTAG( HT_TD, g_strings.GetTxt(S_SUM_PER_DIV) ) << g_endl;

					if( _r_groups.HasDivisions() )
					{
						fp->fout << HTAG( HT_TD, _r_groups.GetGroup( GT_DIV, div_count).GetName() ) << g_endl;
						fp->fout << HTAG( HT_TD, div_sum.Get( GT_DIV )                           ) << g_endl;
					}
					if( _r_groups.HasClassGroups() )
						fp->fout << HTAG( HT_TD, div_sum.Get( GT_CLASS )  ) << g_endl;
					fp->fout << HTAG( HT_TD, div_sum.Get( GT_ATOMIC )     ) << g_endl;
					fp->fout << HTAG( HT_TD ) << HTAG( HT_TD ) << g_endl;
				}
				div_count++;
			}

			{
				HTAG tr( *fp, HT_TR, AT_CLASS, "sum_line" );     // LAST LINE OF INSTRUCTOR
				tr.OpenTag();
				fp->fout << HTAG( HT_TD, g_strings.GetTxt(S_SUMS), AT_STYLE, "text-align:right;" ) << g_endl;
				if( _r_groups.HasDivisions() )
				{
					fp->fout << HTAG(HT_TD);
					HTAG td( HT_TD );
					td << t_all.Get( GT_DIV );
					*fp << td;
				}
				if( _r_groups.HasClassGroups() )
				{
					HTAG td( HT_TD );
					td << t_all.Get( GT_CLASS );
					*fp << td;
				}
				{
					HTAG td( HT_TD );
					td << t_all.Get( GT_ATOMIC );
					*fp << td;
				}
				fp->fout << HTAG( HT_TD ) << g_endl;
				{
					HTAG td( *fp, HT_TD );
					td.OpenTag();
					fp->fout << t_all.Sum_wt() << " h. / " << data.GetNbDays( IT_INSTRUCTOR, i )
					   << " " << g_strings.GetTxt(S_DAYS) << g_br
					   << "<span style=\"font-weight: normal;\">"
					   << g_strings.GetTxt(S_MEAN) << " = "
					   << std::setprecision(2)
					   << t_all.Sum_wt() / data.GetNbDays( IT_INSTRUCTOR, i ) << " h./" << g_strings.GetTxt(S_DAY)
					   << std::setprecision(6)
					   << "</span>";
				}
			}
		}
	}

	fp->fout << "</table>\n";
	fp->Close();
	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------
/// helper function for Summary::PrintPage_Tables()
void
Summary::PrintLinks( FileHTML& f, const std::string& tabletype ) const
{
	if( _r_groups.HasDivisions() )
	{
		f.fout << " : ";
		for( size_t d=0; d<_r_groups.GetNbGroups(GT_DIV); d++ )
		{
			std::ostringstream oss;
			oss << '#' << tabletype << d;
			f.fout << HTAG( HT_A, _r_groups.GetGroup(GT_DIV, d).GetName(), AT_HREF, oss.str() ) << g_endl;
		}

		f.fout << HTAG( HT_A, "Sum", AT_HREF, std::string("#") + tabletype + std::string("_sum") ) << g_endl;
	}
}
//-----------------------------------------------------------------------------------
/// Generate page OF_TABLES
void
Summary::PrintPage_Tables( const UserData& data ) const
{
	DEBUG_IN;
	OutputFile_SP fp = g_files.GetFile( OF_TABLES );

	FileHTML& f = *fp;
	f.SetStyleSheet( "style_table.css" );
	f.Open();

	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();
		{
			HTAG li( f, HT_LI );

			li.OpenTag();
			f.fout << HTAG( HT_A, "Instructors vs. Subjects", AT_HREF, "#is" );
			PrintLinks( f, "is" );
			li.CloseTag();

			li.OpenTag();
			f.fout << HTAG( HT_A, "Instructors vs. Rooms", AT_HREF, "#ir" );
			PrintLinks( f, "ir" );
			li.CloseTag();

			li.OpenTag();
			f.fout << HTAG( HT_A, "Subjects vs. Rooms", AT_HREF, "#sr" );
			PrintLinks( f, "sr" );
		}
	}
	{
		HTAG div( f, HT_DIV, AT_ID, "content" );
		div.OpenTag();
		PrintTables( f, IT_INSTRUCTOR, IT_SUBJECT, data );
		PrintTables( f, IT_INSTRUCTOR, IT_ROOM,    data );
		PrintTables( f, IT_SUBJECT,    IT_ROOM,    data );
	}
	f.Close();

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Prints html worktime tables (one per division, plus one holding the sums)
/**
*/
void
Summary::PrintTables(
	FileHTML&        f,            ///< output file
	EN_INFOTYPE      row_data,     ///< what's on rows
	EN_INFOTYPE      col_data,     ///< what's on columns
	const UserData&  data          ///< the data
) const
{
	DEBUG_IN;

	assert( f.fout.is_open() );
	f.fout << "<hr>\n";

	const StringVector* p_row = &_rv_teachers;
	const StringVector* p_col = 0;

	const std::vector<TripletTable>* p_table = 0;
	const TripletTable*              p_table_sums = 0;
	std::string tabletype("is");

	if( col_data == IT_SUBJECT )
	{
		assert( row_data == IT_INSTRUCTOR );
		f.fout << "<h3 id=\"is\">" << g_strings.GetTxt( S_Table_IS ) << "</h3>" << g_endl;
		p_col        = &_rv_subjects;
		p_table      = &_v_table_is;
		p_table_sums = &_sum_table_is;
	}
	else
	{
		assert( col_data == IT_ROOM );
		p_col   = &_rv_rooms;
		if( row_data == IT_INSTRUCTOR )
		{
			f.fout << "<h3 id=\"ir\">" << g_strings.GetTxt( S_Table_IR ) << "</h3>" << g_endl;
			tabletype    = "ir";
			p_table      = &_v_table_ir;
			p_table_sums = &_sum_table_ir;
		}
		else
		{
			assert( row_data == IT_SUBJECT );
			f.fout << "<h3 id=\"sr\">" << g_strings.GetTxt( S_Table_SR ) << "</h3>" << g_endl;
			tabletype    = "sr";
			p_row        = &_rv_subjects;
			p_table      = &_v_table_sr;
			p_table_sums = &_sum_table_sr;
		}
	}

// one table for each division
	size_t nb = _r_groups.GetNbGroups(GT_DIV);
	if( nb == 0 )
		nb++;
	for( size_t div=0; div<nb; div++ )
	{
		std::ostringstream oss;
		oss << tabletype << div;
		if( _r_groups.HasDivisions() )
		{
			HTAG h4( f, HT_H4, AT_ID, oss.str() );
			h4 << "Division " << div+1 << " : " << _r_groups.GetGroup(GT_DIV, div).GetName();
			f << h4;
		}
		p_table->at(div).Print2Html( f, col_data, p_row, p_col, data, _rParams );
	}

// if more than one division, then print the sums
	if( _r_groups.HasDivisions() )
	{
		f.fout << "<h4 id=\"" << tabletype << "_sum" << "\">" << g_strings.GetTxt( S_Table_SUMS ) << "</h4>\n";
		p_table_sums->Print2Html( f, col_data, p_row, p_col, data, _rParams );
	}
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void
Summary::Dump( std::ostream& f ) const
{
	for( size_t i=0; i<_v_table_is.size(); i++ )
	{
		f << " *** table " << i << g_endl;
		_v_table_is[i].Dump( f );
	}
}
//-----------------------------------------------------------------------------------
