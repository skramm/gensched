/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define USE_BOOST_FILESYSTEM

#include <ctime>

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

#define BOOST_NO_CXX11_SCOPED_ENUMS
#define BOOST_NO_SCOPED_ENUMS
#include <boost/filesystem.hpp>

#include "helper_functions.h"
#include "htag.h"
#include "globals.h"
#include "groups.h"
#include "stats.h"
#include "strings.h"
#include "params.h"
#include "error.h"
#include "user_data.h"

extern Params g_params;

/// local macro, used to create line entry in index
#define ADD_LINK( a ) \
	{ \
		HTAG li( f, HT_LI ); \
		li.OpenTag(); \
		OutputFile_SP p = g_files.GetFile( a ); \
		f.fout << HTAG( HT_A, p->GetTitle(), AT_HREF, p->GetFileName() + htmlext ) << g_endl; \
		li.CloseTag(true); \
	}

//-----------------------------------------------------------------------------------
/// returns a string from a duration in hours, used for Ical files
/**
 input: 1.5 => output 1H30M
*/
std::string
GetDuration( float hour )
{
	double intpart;
	double fractpart = std::modf( hour, &intpart );

	std::ostringstream oss;
	oss << intpart << "H" << fractpart * 60. << "M";
	return oss.str();
}

//-----------------------------------------------------------------------------------
/// Convert time slot code into a normalized time format (HHMMSS)
/**
It is used to generate the ical file time data.

The \c zulu_offset argument holds the offset between zulu time and local time.
See \ref p_temp


\todo Problem: input format might not always be the same.

From: http://www.cplusplus.com/reference/string/string/npos/:
\quote
"npos is a static member constant value with the greatest possible value for an element of type size_t."
\endquote
*/
std::string
GetTime( std::string time_slot, int /*zulu_offset*/ )
{
	StringVector v_sep = { "AM", "am", "PM", "pm", "h", "H", ":" };
	const char* sep = 0;
	bool found = false;
	for( const auto& sep_elem: v_sep )
	{
		if( time_slot.find( sep_elem ) != std::string::npos )
		{
			sep = sep_elem.c_str();
			found = true;
			break;
		}
	}
	if( !found )
	{
		std::cerr << __FUNCTION__ << ": unable to identify token in time string: " << time_slot << ENDL;
		throw ERROR( "unable to identify token in time string", time_slot );
	}

	int offset = 0;
	if( std::string(sep) == "PM" || std::string(sep) == "pm" )
		offset = 12;

	StringVector v_tokens;
	Tokenize3( time_slot, v_tokens, sep );
	assert( v_tokens.size() < 3 );

	int hours = 0;
	TRY_STRING2INT( v_tokens[0], hours, "time slot" );
	hours += offset;
//	hours -= zulu_offset;

	std::ostringstream oss;
	oss <<  std::setfill ('0') << std::setw(2) << hours;
	if( v_tokens.size() == 2 )
		oss << v_tokens[1];
	else
		oss << "00";

//	oss << "00Z"; // seconds, and adds 'Z' to mean UTC/zulu time
	oss << "00"; // seconds (local time, no time zone)
	return oss.str();
}

//-----------------------------------------------------------------------------------
/// Local helper function to get the date for a given combination of year, week and weekday (0-6)
/**
- Adapted from http://stackoverflow.com/questions/15691579/
- uses boost::gregorian, see http://www.boost.org/doc/libs/1_59_0/doc/html/date_time/gregorian.html
- see https://en.wikipedia.org/wiki/ISO_week_date#Calculation (as of 2015/10)
*/
void
GetDateFromWeekNumber(
	int     year,        ///< input
	int     week,        ///< input
	int     dayOfWeek,   ///< input
	time_t& current      ///< output
)
{
    namespace b_greg = boost::gregorian;
	assert( dayOfWeek>=0 && dayOfWeek<7 );
	assert( week>=0 && week<53 );

#ifdef TESTMODE
	CERR << "\n* Input data: year=" << year << " week=" << week << " wday=" << dayOfWeek << ENDL;
#endif

	dayOfWeek++;
	if(dayOfWeek == 7)  // algorithm considers sunday as day 0. For us, monday is day 0
	{
        dayOfWeek = 0;
        week++;
    }

/* Check if 1th of january is a friday. If yes, then means the year before is a "leap year", and we need to increment the week */
	{
		b_greg::date d01( year, b_greg::Jan, 1 ); // build date: 4th january of considered year
		int wd = d01.day_of_week();
//		CERR << "d01 of "<< year <<  " is " << wd << ENDL;
		if( wd == 5 ) // (0=sunday, 1=monday, )
		{
//			CERR << "incrementing week !\n";
			week++;
		}
	}

    b_greg::date d( year, b_greg::Jan, 1 ); // build date: 1st january of considered year
    int curWeekDay = d.day_of_week(); // get week day of 01/01

    d += b_greg::date_duration((week - 1) * 7) + b_greg::date_duration(dayOfWeek - curWeekDay);
    std::tm tmp = b_greg::to_tm(d);
    current = std::mktime( &tmp );
}


//-----------------------------------------------------------------------------------
/// Returns a string holding day, month and year from given input arguments (week number and day index)
/**
see http://en.cppreference.com/w/cpp/chrono/c/strftime

\todo replace ugly C string handling with some neat C++
*/
std::string
GetDate( int week_no, size_t day_idx, size_t year, std::string format )
{
	time_t t;
	GetDateFromWeekNumber( year, week_no, day_idx, t );
	std::tm* time = localtime( &t );
	char buf[64];
	std::strftime( buf, 64, format.c_str(), time );
	return buf;
}

//-----------------------------------------------------------------------------------
/// Command-line week string parser (see \ref sec_clioptions)
/**
- This function will build vector \c v_num_weeks from input string \c week_string
- This function can parse string such as "30-34,37,40-50"
*/
void BuildWeekNumVector(
	const std::string&   week_string,  ///< input string
	std::vector<size_t>& v_num_weeks   ///< output vector of week numbers
)
{
	v_num_weeks.clear();
	StringVector first_level;
	Tokenize( week_string, first_level, ',' );
	for( const auto& tok1: first_level )
	{
		StringVector second_level;
		Tokenize( tok1, second_level, '-' );
		if( second_level.size() > 2 )
			throw ERROR( "too much tokens level 2", tok1 );

		size_t num1 = 0;
		TRY_STRING2INT( second_level.at(0), num1, "week" );
		v_num_weeks.push_back( num1 );

		if( second_level.size() == 2 )
		{
			size_t num2 = 0;
			TRY_STRING2INT( second_level.at(1), num2, "week" );
			if( num2<=num1 )
				throw ERROR( "invalid string, is less than first week", week_string );

			for( size_t i=0; i<num2-num1; i++ )
				v_num_weeks.push_back( num1+i+1 );
		}
	}
}

//-----------------------------------------------------------------------------------
/// local function, called from CreateOutFolders()
void
CopyToOutputFolder( std::string s, const Params& params )
{
#ifdef _WIN32
	boost::filesystem::path p_from( "c:/program files/gensched" );
#else
	boost::filesystem::path p_from( "/etc/gensched" );
#endif // _WIN32

	p_from /= s;
//	cerr << "copy from =" << p_from << ENDL;
	boost::filesystem::path p_to( params.OutputDirFullName );
	p_to /= s;
//	cerr << "copy to =" << p_to << ENDL;

	boost::filesystem::copy_file( p_from, p_to, boost::filesystem::copy_option::overwrite_if_exists );
}

//-----------------------------------------------------------------------------------
/// local function, called from CreateOutFolders()
void
CreateOneFolder( const boost::filesystem::path& p )
{
	if( !boost::filesystem::is_directory( p ) )
		if( !boost::filesystem::create_directory(p) )
		{
			std::cerr << "Error, unable to create output folder " << p.filename() << ENDL;
			throw ERROR( "unable to create output folder", p.string() );
		}
}
//-----------------------------------------------------------------------------------
/// Create the output folders
bool
CreateOutFolders( const Params& params )
{
	CERR << " -current working path: " << boost::filesystem::current_path() << g_endl;

	boost::filesystem::path p( params.OutputDirFullName );
//	cerr << "path1=" << p << ENDL;
	try
	{
		CreateOneFolder( p );
		boost::filesystem::path p1( p );
		p1 /=  "ical"; // add "ical" to the previous path
		CreateOneFolder( p1 );

		boost::filesystem::path p2( p );
		p2 /= "img";
		CreateOneFolder( p2 );

		boost::filesystem::path p3( p );
		p3 /= "input_data";
		CreateOneFolder( p3 );

		if( params.bswitch.at(BS_COPY_STYLE_SHEETS) )                 // copy style sheets to ouput folder
		{
			CopyToOutputFolder( "common.css", params );
			CopyToOutputFolder( "style_table.css", params );
			CopyToOutputFolder( "print.css", params );
		}
		CopyToOutputFolder( "show_subject.js", params ); // always copy this one
		CopyToOutputFolder( "bU.png", params );
		CopyToOutputFolder( "bL.png", params );
		CopyToOutputFolder( "bR.png", params );
		CopyToOutputFolder( "bH.png", params );
		CopyToOutputFolder( "bS.png", params );
	}
	catch( const ErrorClass& err )
	{
		std::cerr << "Error: " << err.what() << g_endl;
		return false;
	}
	catch( ... )
	{
		std::cerr << "Unhandled exception in " << __FUNCTION__ << ", path is " << params.OutputDirFullName << ENDL;
		return false;
	}
	return true;
}

//-----------------------------------------------------------------------------------
/// Generates the output file name for \c OF_SCHED and \c OF_WS, without extension .html
std::string
GenerateOutputFileName( EN_OUTPUTFILE_ID id, int div_idx, int week_num )
{
	std::ostringstream oss;
	if( id == OID_SCHED )
	   oss << "sched";
	else
		oss << "ws";

	if( div_idx != -1 )
		oss << "_d" << div_idx+1;
	if( week_num != -1 )
		oss << "_w" << week_num;

	return oss.str();
}
//-----------------------------------------------------------------------------------
/// Generates the output file full path, to produce links for \c OF_SCHED and \c OF_WS,
std::string
GenerateOutputFilePath(
	EN_OUTPUTFILE_ID id,        ///< \c OF_SCHED or \c OF_WS
	int              div_idx,
	int              week_num,
	const Params&    params
)
{
	int d = params.bswitch.at(BS_ONE_PAGE_PER_DIV)  ? div_idx  : -1;
	int w = params.bswitch.at(BS_ONE_PAGE_PER_WEEK) ? week_num : -1;

	std::string out = GenerateOutputFileName( id, d, w ) + ".html";
	if( !params.bswitch.at(BS_ONE_PAGE_PER_WEEK) )
		out += "#s" + std::to_string(week_num);
	return out;
}

//-----------------------------------------------------------------------------------
/// A helper function for CreateHelpFile()
void
PrintAsHtmlList( FileHTML& f, std::string title, const StringVector& v_elem )
{
	HTAG li_a( f, HT_LI );
	li_a.OpenTag();
	f.fout << "List of " << v_elem.size() << ' ' << title << g_endl;
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();

		size_t count=0;
		for( const auto& elem : v_elem )
		{
			HTAG li_b( f, HT_LI );
			li_b.OpenTag();
			f.fout << ++count << " : " << elem;
		}
	}
}

//-----------------------------------------------------------------------------------
void
CreateHelpFile( const UserData& data, const GroupSets& groups, const Params& params )
{
	DEBUG_IN;

	FileHTML f( "help" );
	f.SetTitle( "Contextual help page" );
	f.Open();

	f.fout << HTAG( HT_P, "This page references the valid codes for the input file, considering the current configuration file and input data." ) << g_endl;
	f.fout << HTAG( HT_H3, "1 - General configuration" ) << g_endl;
	params.PrintToHtml( f );

	f.fout << HTAG( HT_H3, "2 - User data" ) << ENDL;
	f.fout << HTAG( HT_H4, "2.1 - Groups" ) << ENDL;
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			f.fout << "Group file name: <tt>" << params.GroupsFileName << "</tt>";
			f.fout << ", read status: " << ( params.HasGroups ? "Yes" : "no, using groups from input data file" );
		}
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			f.fout << groups.GetNbGroups( GT_ATOMIC) << " atomic groups: ";
			for( size_t i=0; i<groups.GetNbGroups( GT_ATOMIC); i++ )
			{
				f.fout << groups.GetGroup( GT_ATOMIC, i ).GetName();
				if( i < groups.GetNbGroups( GT_ATOMIC)-1 )
					f.fout << " - ";
			}
		}
		if( groups.GetNbGroups( GT_CLASS ) )
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			f.fout << groups.GetNbGroups( GT_CLASS ) << " class groups:";
			{
				HTAG ul2( f, HT_UL );
				ul2.OpenTag();
				for( size_t i=0; i<groups.GetNbGroups( GT_CLASS); i++ )
				{
					const StudentGroup& gr = groups.GetGroup( GT_CLASS, i );
					f.fout << g_lio << "group " << gr.GetName() << ", holds " << gr.GetNbSubGroups() << " atomic groups: ";
					for( size_t j=0; j<gr.GetNbSubGroups(); j++ )
						f.fout << gr.GetSubGroup(j).GetName() << " ";
					f.fout << g_lic;
				}
			}
		}
		if( groups.GetNbGroups( GT_DIV ) )
		{
			HTAG li( f, HT_LI );
			li.OpenTag();
			f.fout << groups.GetNbGroups( GT_DIV ) << " division groups: ";
			{
				HTAG ul2( f, HT_UL );
				ul2.OpenTag();
				for( size_t i=0; i<groups.GetNbGroups( GT_DIV); i++ )
				{
					const StudentGroup& gr = groups.GetGroup( GT_DIV, i );
					f.fout << g_lio << "group " << gr.GetName() << ", holds " << gr.GetNbSubGroups() << " class groups: ";
					for( size_t j=0; j<gr.GetNbSubGroups(); j++ )
						f.fout << gr.GetSubGroup(j).GetName() << " ";
					f.fout << g_lic;
				}
			}
		}
	}

	f.fout << HTAG( HT_H4, "2.2 - Volume reference input file" ) << ENDL;
	{
		f.fout << "- file name: <tt>" << params.ReferenceFileName << "</tt>\n";
	}

	f.fout << HTAG( HT_H4, "2.3 - Field values" ) << ENDL;
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();
		{
			PrintAsHtmlList( f, "instructors", data._v_instructors );
			PrintAsHtmlList( f, "subjects",    data._v_subjects );
			PrintAsHtmlList( f, "rooms",       data._v_rooms );
		}
	}
}

//-----------------------------------------------------------------------------------
/// Helper function for CreateIndex()
/**
Prints the two links on schedule AND week summary
*/
void
PrintLinkSchedSummary( FileHTML& f, const UserData& data, int div_idx, bool onePagePerWeek )
{
	std::string htmlext( ".html" );

	HTAG ul_C( f, HT_UL );
	if( div_idx != -1 )
		ul_C.OpenTag();

	{
		HTAG li_C( f, HT_LI );
		li_C.OpenTag();

		HTAG a1( f, HT_A, AT_HREF, GenerateOutputFileName( OID_SCHED, div_idx, onePagePerWeek?data.GetWeek(0).GetWeekNum():-1 ) + htmlext );
		a1.PrintWithContent( g_strings.GetTxt(S_FN_SCHED) );
		li_C.CloseTag();
		li_C.OpenTag();
		HTAG a2( f, HT_A, AT_HREF, GenerateOutputFileName( OID_SUMMARY, div_idx, onePagePerWeek?data.GetWeek(0).GetWeekNum():-1 ) + htmlext );
		a2.PrintWithContent( g_strings.GetTxt(S_FN_WS) );
	}
}

//-----------------------------------------------------------------------------------
/// Creates the main index file
void
CreateIndex( const UserData& data, const GroupSets& groups, const Params& params )
{
	DEBUG_IN;

	FileHTML f( "index" );
	f.SetTitle( "schedule main page" );
	f.Open();

	std::string htmlext( ".html" );

	HTAG ul_A( f, HT_UL );
	ul_A.OpenTag();
	{
		HTAG li_A( f, HT_LI );
		li_A.OpenTag();
		f.fout << g_strings.GetTxt( S_PER_WEEK ) << g_endl;
		{
			HTAG ul_B( f, HT_UL );
			ul_B.OpenTag();

			if( params.bswitch.at(BS_ONE_PAGE_PER_DIV) )
			{
				for( size_t div_idx=0; div_idx<groups.GetNbGroups( GT_DIV ); div_idx++ )
				{
					const StudentGroup& div = groups.GetGroup( GT_DIV, div_idx );

					HTAG li_B( f, HT_LI );
					li_B.OpenTag();
					std::ostringstream oss;
					oss << "Division " << div_idx+1 << ": " << div.GetName();
					f.fout << oss.str() << ": ";
					PrintLinkSchedSummary( f, data, div_idx, params.bswitch.at(BS_ONE_PAGE_PER_WEEK) );
				}
			}
			else
				PrintLinkSchedSummary( f, data, -1, params.bswitch.at(BS_ONE_PAGE_PER_WEEK) );

			ADD_LINK( OF_SW );
			ADD_LINK( OF_IW );
		}
	}
	{
		HTAG li( f, HT_LI );
		li.OpenTag();
		OutputFile_SP p = g_files.GetFile( OF_CONFLICTS );
		if( g_stats.GetTotNbConflicts() )
		{
			f.fout << HTAG( HT_A, p->GetTitle(), AT_HREF, p->GetFileName() + htmlext );
			f.fout << ": " << g_stats.GetTotNbConflicts();
		}
		else
			f.fout << p->GetTitle() << ": " << g_strings.GetTxt( S_NONE );
	}

	ADD_LINK( OF_GW );
	ADD_LINK( OF_GS );
//	ADD_LINK( OF_SW );
//	ADD_LINK( OF_IW );
	ADD_LINK( OF_WT );
	ADD_LINK( OF_ID );
	ADD_LINK( OF_TABLES );

	{                               // add link on stats page
		HTAG li( f, HT_LI );
		li.OpenTag();
		OutputFile_SP p = g_files.GetFile( OF_STATS );
		f.fout << HTAG( HT_A, p->GetTitle(), AT_HREF, p->GetFileName() + htmlext );
		if( g_stats.GetNbEvents( EC_INVALID ) )
			f.fout << " (" << g_strings.GetTxt( S_NB_EVENTS_INVALID ) << ": " << g_stats.GetNbEvents( EC_INVALID ) << ")\n";
	}
	ADD_LINK( OF_ICAL )

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Create vector of week numbers directly from input data
void
BuildWeekNumList(
	const std::vector<LineContent>& vv_intable, ///< input table
	std::vector<size_t>&            v_weeknum,  ///< out: vector of week numbers
	const Params&                   params      ///< parameters
)
{
	DEBUG_IN;

	v_weeknum.clear();
	for( size_t k=0; k<vv_intable.size(); k++ )
	{
		const StringVector& v_string = vv_intable[k].first;
		const std::string& item = v_string.at(params.input_pos[IT_WEEK]);
		size_t num(0);
		bool Error(false);
#if 0
		TRY_STRING2INT( item, num, "week" );

#else
		try
		{
			TRY_STRING2INT( item, num, "week" );
		}
		catch( ... )
		{
			std::cerr << "\nERROR: invalid week string: \"" << item << "\" at line " << k+1 << g_endl;
			for( size_t i=0; i<v_string.size(); ++i )
				std::cerr << " -field " << i << ": length=" << v_string[i].size() << ", value=" << v_string[i] << ENDL;
			Error = true;
		}
#endif
		if( !Error )
			push_back_ifnotpresent( v_weeknum, num );
	}
	if( v_weeknum.size() > 1 )                               // else, nothing to sort
		std::sort( v_weeknum.begin(), v_weeknum.end() );

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Searches vector \c v_event and returns a pointer on the first event that is of group type \c gtype AND that holds \c atomic_group_idx.
/**
- Return nullptr if none found
*/
const Event*
GetMatchedEvent( const std::vector<const Event*>& v_event, size_t atomic_group_idx, EN_GROUP_TYPE gtype )
{
	for( size_t i=0; i<v_event.size(); i++ ) // parse the set of events and check if they are related
	{
		if( v_event.at(i)->GetGroupType() == gtype )        // if type of event is the one searched,
		{
			if( gtype == GT_ATOMIC )                        // if atomic group, then
			{                                               //  just check if event is about that group
				if( v_event.at(i)->GetIndex( IT_GROUP ) == static_cast<int>(atomic_group_idx) )
					return v_event.at(i);
			}
			else                                             // else, searches the vector
			{
				const StudentGroup& cg = g_groups.GetGroup( gtype, v_event.at(i)->GetIndex( IT_GROUP ) );
				if( cg.IncludesAtomicGroup_a( atomic_group_idx ) )
					return v_event.at(i);
			}
		}
	}
	return nullptr; // none found
}
//-----------------------------------------------------------------------------------
/// Read csv data file \c fn and fills \c table
bool
ReadCSV(
	std::string               fn,            ///< file name
	std::vector<LineContent>& table,         ///< output: the data read from input file
	char                      delim,
	size_t                    min_nb_tokens  ///< minimum nb of tokens to be read
)
{
	DEBUG_IN;

	table.clear();
	CERR << " - reading csv file: " << fn << g_endl;
	std::ifstream datafile;
	datafile.open( fn, std::ios_base::in );
	if( !datafile.is_open() )
	{
		CERR << " -unable to open file\n";
		return false;
	}

	char buf[BUF_SIZE];
	bool go_on = true;
	size_t linecount=0;
	do
	{
		linecount++;
		datafile.getline( buf, BUF_SIZE );
		std::string line(buf);
		CERR << "LINE=" << line << ENDL;
		if( datafile.eof() )
		{
			CERR << " - Reached EOF in file\n";
			go_on = false;
		}
		else
		{
			if( !datafile.good() )
			{
				CERR << " - stop: bad datafile, failure\n";
				go_on = false;
				DEBUG_OUT;
				return false;
			}
			else
			{
//				cerr << "line " << l << " : ok\n";
				if( line.size() > 0 )
				{
					if( line.at(0) != ';' && line.at(0) != '#' && line.at(0) != delim ) // if line is not a comment AND first field not empty
					{
						StringVector tokens;
						Tokenize( line, tokens, delim, min_nb_tokens ); // tokenize the whole line
						table.push_back( std::make_pair( tokens, linecount ) );
					}
//					else
//						cerr << " - line: \"" << line << "\" is a comment\n";
				}
			}
		}
	}
	while( go_on );
	CERR << " - success\n";
	DEBUG_OUT;
	return true;
}

//-----------------------------------------------------------------------------------
/// Tokenizing input string into vector of strings,
/// using \c delim as delimitor (needs BOOST)
/**
This function also trims leading spaces <= NO, DOES NOT, BUT COULD (SEE CODE)

20150312: now works with empty fields, see http://stackoverflow.com/a/22332077/193789
*/
void
Tokenize(
	const std::string&  input,           ///< The input string
	StringVector&       v_output,        ///< output
	char                delim,           ///< the delimiter character
	size_t              min_nb_tokens )  ///< if not 0, output vector will be filled with empty fields until nb of fields reaches this value
{
	v_output.clear();
	std::string d;
	d += delim;
	boost::char_separator<char> sep( d.c_str(), "", boost::keep_empty_tokens );
	boost::tokenizer< boost::char_separator<char> > tokens( input, sep );

	BOOST_FOREACH(std::string t, tokens)
	{
//		TrimString( t, " #" );
		v_output.push_back( t );
	}
	if( min_nb_tokens )
		while( v_output.size() < min_nb_tokens )  // if line has not enough tokens, fill with empty strings
		{
			v_output.push_back( "" );
		}
}

//-----------------------------------------------------------------------------------
/// Tokenizing input string into vector of strings,
/// using \c delim as delimitor (needs BOOST)
void
Tokenize3(
	const std::string&  input,    ///< The input string
	StringVector&       v_output, ///< output
	const char*         delims )  ///< the delimiter characters
{
	v_output.clear();
	boost::char_separator<char> sep( delims );
	boost::tokenizer< boost::char_separator<char> > tokens( input, sep );

	BOOST_FOREACH(std::string t, tokens)
	{
		v_output.push_back( t );
	}
}

//-----------------------------------------------------------------------------------
/// Returns the volume contained in vector
Triplet
GetVolume( std::vector<const Event*>& ve )
{
	Triplet res;
	for( const auto& e: ve )
		res += e->GetEventVolume();
	return res;
}

//-----------------------------------------------------------------------------------
void
PrintErrorMessage( FileHTML& f, int NbConflicts )
{
	HTAG p( f, HT_P );
	p.OpenTag();
	f.fout << "<strong>" << g_strings.GetTxt(S_ERRORS) << "</strong>: " << NbConflicts << ' ';
	f.fout << g_strings.GetTxt(S_CONFLICTS_MSG) << " <a href=\"conflicts.html\">conflicts</a>";
}

//-----------------------------------------------------------------------------------
void
PrintLine( const LineContent& line )
{
	CERR << "line " << line.second << ": ";
	for( const auto& s: line.first )
		CERR << s << '-';
	CERR << ENDL;
}

void
DumpTable( const std::vector<LineContent>& in_table )
{
	CERR << "Dump data table:\n";
	for( const auto& line: in_table )
		PrintLine( line );
}

//-----------------------------------------------------------------------------------
bool
HasSingleOption( std::string opt, int argc, const char** argv )
{
	for( int i=1; i<argc; i++ )
		if( argv[i] == opt )
			return true;
	return false;
}

//-----------------------------------------------------------------------------------
bool
HasStringOption( std::string opt, int argc, const char** argv, std::string& value )
{
	for( int pos=1; pos<argc; pos++ )
		if( argv[pos] == opt )
		{
			if( argc < pos+2 )
			{
				std::cerr << " - missing argument, option " << opt << " needs a string\n";
				throw ERROR( "missing argument", opt );
			}
			value = argv[pos+1]; // add checking that it is not another option (it muss not start with '-')
			assert( !value.empty() );
			return true;
		}
	return false;
}

//-----------------------------------------------------------------------------------
/// helper function for TripletTable::Print2Html()
void
PrintColumnGroupHeader(
	FileHTML&      f,
	const Params&  params,
	bool           printSpecialEventColumn,
	bool           printSumColumn
)
{
	if( g_groups.HasDivisions() )
	{
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_DIV), AT_CLASS, "blw" );
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_CLASS) );
	}
	else
		f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_CLASS), AT_CLASS, "blw" );
	f.fout << HTAG( HT_TH, params.GetCodeFromGroupType(GT_ATOMIC) ) << g_endl;

	if( printSpecialEventColumn )
		f.fout << HTAG( HT_TH, g_strings.GetTxt(S_SPECIAL_EVENT_ABBREV) ) << g_endl;
	if( printSumColumn )
		f.fout << HTAG( HT_TH, g_sum ) << g_endl;
}

//-----------------------------------------------------------------------------------
/// Prints the table second header line on pages OF_GS and tables
void
PrintSecondHeaderLine(
	FileHTML&                f,
	const Params&            params,
	const std::vector<bool>& printSubjectColumn,
	const std::vector<bool>& columnHasSpecialEvent,
	ColumnFlags              flags,
	int                      rowhw                    ///< row header width, in cells
)
{
	if( !g_groups.HasClassGroups() )
		return;
	assert( printSubjectColumn.size() == columnHasSpecialEvent.size() );

	size_t nbCols = printSubjectColumn.size();
	HTAG tr( f, HT_TR );
	tr.OpenTag();

	f.fout << HTAG( HT_TH, "", AT_COLSPAN, rowhw ) << g_endl;

//	std::cerr << "flags.printFirstColumn=" << flags.printFirstCol << "\n";
	bool hasSpecialEvent(false);
	for( size_t j=0; j<nbCols; j++ )
	{
//		std::cerr << "j=" << j << " printSubjectColumn.at(j)=" << printSubjectColumn.at(j) << "\n";
		if( j != 0 || printSubjectColumn.at(j) )
		{
			PrintColumnGroupHeader( f, params, columnHasSpecialEvent[j], flags.printSumColumn );
			if( columnHasSpecialEvent[j] )
				hasSpecialEvent = true;
		}
	}
	PrintColumnGroupHeader( f, params, hasSpecialEvent, g_groups.HasClassGroups() );                     // last group of columns
}

//-----------------------------------------------------------------------------------

