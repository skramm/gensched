/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// vol_ref.cpp

#include "vol_ref.h"

#include <iostream>
#include "helper_functions.h"
#include "macros.h"
#include "error.h"

//-----------------------------------------------------------------------------------
/// Read reference course volumes from csv data file
/**
\todo What happens if course volume is described with only 1 or 2 course types, and 3 are required ???
*/
bool RefVolume::readInFile( const std::string& fname, char delim )
{
	std::vector<LineContent> table;
	if( !ReadCSV( fname, table, delim ) )
	{
		std::cerr << "* unable to read file " << fname << std::endl;
		return false;
	}

	size_t count = 0;
	for( const auto& v: table )
	{
		const StringVector& v_string = v.first;
		if( v_string.size() == 4 )
		{
			float v1, v2, v3;
			TRY_STRING2FLOAT( v_string[1], v1, "invalid float value" );
			TRY_STRING2FLOAT( v_string[2], v2, "invalid float value" );
			TRY_STRING2FLOAT( v_string[3], v3, "invalid float value" );
			Triplet t( v1, v2, v3 );
			_m_trip[v_string[0]] = t;
			count++;
		}
		else
		{
			std::cerr << "Error, line should have 4 fields.\n";
			return false;
		}
/*		{
			if( v_string.size() != 0 ) // in order to allow empty lines
			{
				std::cerr << " * Error: elem " << count << " has " << v.size() << " fields, should have 4\n";
				for( size_t i=0; i<v.size(); i++ )
					std::cerr << "field " << i+1 << ": -" << v[i] << "-\n";
				return false;
			}
		}*/
	}
	return true;
}
//-----------------------------------------------------------------------------------


