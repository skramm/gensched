/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>

#include "week.h"

#include "user_data.h"
#include "helper_functions.h"
#include "htag.h"
#include "globals.h"
#include "groups.h"
#include "stats.h"
#include "strings.h"
#include "params.h"
#include "error.h"
#include "cal_data.h"

/// global used to rememeber when a particular group is used by an event, when iterating over time slots
static std::vector<size_t> gv_columns_memory;


/// Allocation of static variable
Params* WeekEvents::sp_params_w = 0;

//-----------------------------------------------------------------------------------
/// Constructor
WeekEvents::WeekEvents() : _num(0), _yearLeap(false)
{
	for( size_t i=0; i<sp_params_w->GetNbDays(); i++ )
		_v_days.push_back( DayEvents(i) );
}

//-----------------------------------------------------------------------------------
Triplet
WeekEvents::GetVolumeT_w2( EN_INFOTYPE it, size_t idx ) const
{
	Triplet sum;
	for( const auto& day: _v_days )
		sum += day.GetVolumeT_d2( it, idx );
	return sum;
}

//-----------------------------------------------------------------------------------
/// Used in output file "gw"
Triplet
WeekEvents::GetVolumeT_w1( size_t ag_idx ) const
{
	DEBUG_IN;
	Triplet sum;
	for( const auto& day: _v_days )
		sum += day.GetVolumeT_d1( ag_idx );
	DEBUG_OUT;
	return sum;
}

//-----------------------------------------------------------------------------------
size_t
WeekEvents::GetNbEventsPerSubjectPerDiv( int div_idx, size_t sub_idx ) const
{
	if( div_idx >= 0 )
	{
		ASSERT_2( (size_t)div_idx < _vv_nbevents.size(),          div_idx, _vv_nbevents.size() );
		ASSERT_2( (size_t)sub_idx < _vv_nbevents[div_idx].size(), sub_idx, _vv_nbevents[div_idx].size() );
		return _vv_nbevents[div_idx][sub_idx];
	}
	else  // if div_idx == -1, we return the sum for all divs
	{
		size_t sum = 0;
		std::for_each(
			_vv_nbevents.begin(),
			_vv_nbevents.end(),
			[&](const std::vector<size_t>& v){ sum += v[sub_idx]; }         // lambda
		);
		return sum;
	}
}

//-----------------------------------------------------------------------------------
void
WeekEvents::GetEventList_w5( EN_INFOTYPE it, size_t idx, size_t group_idx, EN_GROUP_TYPE gt, std::vector<const Event*>& v_out ) const
{
	DEBUG_IN;
	for( const auto& day: _v_days )
		day.GetEventList_d5( it, idx, group_idx, gt, v_out );
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Adds to \c v_out the adress of events that match the criterion: index \c it has value \c idx
void
WeekEvents::GetEventList_w3( EN_INFOTYPE it, size_t idx, std::vector<const Event*>& v_out ) const
{
	DEBUG_IN;
	for( const auto& day: _v_days )
		day.GetEventList_d3( it, idx, v_out );
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
const Event*
WeekEvents::GetEventFromId_w( size_t Id ) const
{
	for( const auto& d: _v_days )
	{
		const Event* e = d.GetEventFromId_d( Id );
		if( e )
			return e;
	}
	return 0; // the event is not in this week
}

//-----------------------------------------------------------------------------------
/// Return a descriptive string about the (first found) conflict involving event \c e
/**
Steps:
- search in the vector of conflicts for the first one involving \c e
- get the second event from conflict
- build up a descriptive string and return it
*/
std::string
WeekEvents::GetConflictString( const Event& e1 ) const
{
	DEBUG_IN;

	assert( e1.IsConflicted() );
	assert( p_user_data );

// 1 - search for corresponding conflict
	size_t e2_id = 0;
	bool found = false;
	Conflict conf;
	for( const auto& c: _v_wconflicts )
	{
		if( true == c.UsesEventId( e1.GetId() ) )
		{
			found = true;
			conf = c;
			break;
		}
	}
	assert( found );
	const Event* pe2 = 0;
	if( conf.GetType() != CT_TOO_LONG )      // 2 - get second event, if there is any
	{
		e2_id = conf.GetOtherEventId( e1.GetId() );
		pe2 = p_user_data->GetEventFromId( e2_id );
		assert( pe2 );
	}

// 3 - build string
	std::ostringstream oss;
	oss << "Conflict: ";

	std::string group1, group2;
	if( conf.GetType() != CT_DAY_OFF )
	{
		size_t idx_group_1 = e1.GetIndex( IT_GROUP );
		group1 = (e1.GetGroupType() == GT_ATOMIC
			? g_groups.GetGroup( GT_ATOMIC, idx_group_1 ).GetName()
			: g_groups.GetGroup( e1.GetGroupType(), idx_group_1 ).GetName() );
		if( conf.GetType() != CT_TOO_LONG )
		{
			size_t idx_group_2 = pe2->GetIndex( IT_GROUP );
			group2 = (pe2->GetGroupType() == GT_ATOMIC
				? g_groups.GetGroup( GT_ATOMIC, idx_group_2 ).GetName()
				: g_groups.GetGroup( pe2->GetGroupType(), idx_group_2 ).GetName() );
		}
	}

	switch( conf.GetType() )
	{
		case CT_ITS:
			oss << "Instructor " << p_user_data->GetItem( IT_INSTRUCTOR, e1.GetIndex(IT_INSTRUCTOR) );
			oss << "\nis with two groups at the same time:\n";
			oss << "- group " << group1 << " (" << p_user_data->GetItem( IT_SUBJECT, e1.GetIndex(IT_SUBJECT) ) << ")\n";
			oss << "- group " << group2 << " (" << p_user_data->GetItem( IT_SUBJECT, pe2->GetIndex(IT_SUBJECT) ) << ")\n";

		break;
		case CT_RTS:
			oss << "Room " << p_user_data->GetItem( IT_ROOM, e1.GetIndex(IT_ROOM) ) << "\nholds two groups at the same time:\n";
			oss << "- gr. " << group1 << " with Instr. " << p_user_data->GetItem( IT_INSTRUCTOR, e1.GetIndex(IT_INSTRUCTOR) ) << g_endl;
			oss << "- gr. " << group2 << " with Instr. " << p_user_data->GetItem( IT_INSTRUCTOR, pe2->GetIndex(IT_INSTRUCTOR) ) << g_endl;
		break;
		case CT_GTS:
			oss << "Group " << group1 << " is in two rooms at the same time:\n";
			oss << "- room " << p_user_data->GetItem( IT_ROOM, e1.GetIndex(IT_ROOM) ) << " with Instr. "
				<< p_user_data->GetItem( IT_INSTRUCTOR, e1.GetIndex(IT_INSTRUCTOR) )
				<< " (" << p_user_data->GetItem( IT_SUBJECT, e1.GetIndex(IT_SUBJECT) ) << ")\n";
			oss << "- room " << p_user_data->GetItem( IT_ROOM, pe2->GetIndex(IT_ROOM) ) << " with Instr. "
				<< p_user_data->GetItem( IT_INSTRUCTOR, pe2->GetIndex(IT_INSTRUCTOR) )
				<< " (" << p_user_data->GetItem( IT_SUBJECT, pe2->GetIndex(IT_SUBJECT) ) << ")\n";

		break;
		case CT_DAY_OFF:
			oss << "Day off, conflicts with other event, see conflicts page";
		break;

		case CT_TOO_LONG:
			oss << "Too long";
		break;

		default: assert(0);
	}

	DEBUG_OUT;
	return oss.str();
}

//-----------------------------------------------------------------------------------
void
WeekEvents::PrintWeekConflicts( FileHTML& f, const UserData& data ) const
{
	if( !_v_wconflicts.empty() )
	{
		HTAG ul( f, HT_UL );
		ul.OpenTag();

		size_t counter = 0;
		for( auto c: _v_wconflicts )
		{
			f.fout << g_lio << ++counter << ": ";
			c.PrintHtml( f, data );
			f.fout << g_lic << g_endl;
		}
	}
}

//-----------------------------------------------------------------------------------
/// Adds event to week (event is NOT const, because it might conflicts with another event,
/**
and if so, it will be tagged as so).

Returns true if event is to be added to the summary (not a duplicate, and not a special event)
*/
bool
WeekEvents::AddEventToWeek( Event& e )
{
	DEBUG_IN;

	for( size_t i=0; i<_v_days.size(); i++ )   // parses the days of the week,
	{
		int day_idx = e.GetIndex( IT_DAY );
		assert( day_idx >= 0 );
		if( static_cast<size_t>(day_idx) == i )        // and if the event matches,
		{
			bool b = _v_days.at(i).AddEventToDay( e, _v_wconflicts );   // then, add it to the day
			int div_idx = e.GetIndex( IT_DIVISION );
			if( div_idx == -1 )
				div_idx = 0;
			_vv_nbevents.at( div_idx ).at( e.GetIndex( IT_SUBJECT ) )++;              // and increment the counter of events of that subject for this week
			DEBUG_OUT;
			return b; // no need to continue
		}
	}
	CERR << "Error: bad day code: " << e;
	throw ERROR( "Invalid day code",  e.GetIndex( IT_DAY ) );
}

//-----------------------------------------------------------------------------------
/// Parses all the events of the week and returns in \c v_sub a sorted list of the indexes that are used in these events,
/**
related to either subjects, instructors or rooms, depending on \c it
*/
std::vector<size_t>
WeekEvents::P_GetIndexList( EN_INFOTYPE it ) const
{
	std::vector<size_t> v_sub;
	for( const auto& day: _v_days )
		day.AddToIndexList( it, v_sub );
	std::sort( v_sub.begin(), v_sub.end() );
	return v_sub;
}
//-----------------------------------------------------------------------------------
size_t
WeekEvents::GetNbSpecialEvents() const
{
	size_t n = 0;
	for( const auto& day: _v_days )
		n += day.GetNbSpecialEvents_d();
	return n;
}

//-----------------------------------------------------------------------------------
/// helper function for WeekEvents::PrintSummary(), prints first header line of table
void
WeekEvents::PrintSummary_header_1( FileHTML& f, const UserData& data, const std::vector<size_t>& v_sub ) const
{
	size_t NbCols=1;                   // nb of columns per subject
	if( g_groups.HasClassGroups() )
	{
		NbCols += 1;
		if( g_groups.HasDivisions() )
			NbCols += 1;
	}
	{                                  // first line
		HTAG tr( f, HT_TR );
		tr.OpenTag();

		{
			HTAG ht_first( HT_TH, "", AT_CLASS, "firstcol_ws" );
			f.fout << ht_first;
		}

		for( const auto& sub: v_sub )
		{
			HTAG th( f, HT_TH );

			th.AddAttrib( AT_CLASS, "blw" );
			th.AddAttrib( AT_CLASS, "brw" );
			th.AddAttrib( AT_COLSPAN, NbCols );
			th.SetLineBreak( true );
			std::ostringstream oss;
			oss << "<a href=\"gs.html#sub_" << sub << "\" title=\"" << data.GetInstructorString(  sub ) << "\">";
			oss << data.GetItem( IT_SUBJECT, sub ) << "</a>";

			th.PrintWithContent( oss.str() );
		}


		if( NbCols==1 )
			NbCols--;
		HTAG th( f, HT_TH );
		th.AddAttrib( AT_COLSPAN, NbCols + 2 + (GetNbSpecialEvents() ? 1 : 0)  );
		th.PrintWithContent( g_strings.GetTxt(S_SUMS) );
	}
}

//-----------------------------------------------------------------------------------
/// helper function for WeekEvents::PrintSummary(), prints second header line of table
void
WeekEvents::PrintSummary_header_2(
	FileHTML& f,      ///< output file
	size_t    nb_sub  ///< nb of subject columns (groups of columns, actually)
) const
{
	HTAG tr( f, HT_TR );
	tr.OpenTag();

	f.fout << HTAG( HT_TH ) << g_endl;
	for( size_t i=0; i<nb_sub; i++ )
	{
		HTAG th( f, HT_TH, AT_CLASS, "blw" );

//		th.SetLineBreak( true );
		if( g_groups.HasDivisions() )
			th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_DIV ) );
		if( g_groups.HasClassGroups() )
			th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_CLASS ) );
		th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_ATOMIC ) );
	}

	{                                           // last column group
		HTAG th( f, HT_TH );
		th.AddAttrib( AT_CLASS, "blw" );
		if( g_groups.HasDivisions() )
			th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_DIV ) );
		if( g_groups.HasClassGroups() )
			th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_CLASS ) );
		th.PrintWithContent( sp_params_w->GetCodeFromGroupType( GT_ATOMIC ) );

		if( g_groups.HasClassGroups() )
			th.PrintWithContent( g_sum );
		if( GetNbSpecialEvents() )
		{
			th.AddAttrib( AT_TITLE, g_strings.GetTxt(S_SPECIAL_EVENT) );
//			std::cout << th << g_endl;
			th.PrintWithContent( g_strings.GetTxt(S_SPECIAL_EVENT_ABBREV) );
//			th.RemoveAttrib( AT_TITLE );
		}
		th.PrintWithContent( g_strings.GetTxt(S_EMPTY_SLOTS) );
	}
}
//-----------------------------------------------------------------------------------
/// Print week summary, for current week
/**
\todo Add printing of events with missing information
*/
void
WeekEvents::PrintSummary( FileHTML& f, const UserData& data, int div_idx ) const
{
	DEBUG_IN;
	assert( f.isOpen() );

	std::vector<size_t> v_sub = P_GetIndexList( IT_SUBJECT );

	HTAG table( f, HT_TABLE, AT_CLASS, "table_ws" );
	table.OpenTag();

//	std::cout << "nb sub=" << v_sub.size() << "\n";
	PrintSummary_header_1( f, data, v_sub );
	PrintSummary_header_2( f, v_sub.size() );

// table content
	size_t Nbag = g_groups.GetNbGroups( GT_ATOMIC );
	if( -1 != div_idx )
		Nbag = g_groups.GetGroup( GT_DIV, div_idx ).GetNbAtomicGroups();

#ifdef EXPERIMENTAL_VOL_PER_WEEK
	std::ostringstream oss;
	oss << "data_per_week_";
	if( div_idx == -1 )
		oss << "_ALL";
	else
		oss << "_d" << div_idx;
	FileCSV f_csv( oss.str() );
	f_csv.Open( FAP_APPEND );
	f_csv.fout << GetWeekNum() << ',';

#endif

	for( size_t i=0; i<Nbag; i++ )   // for each atomic group
	{
		size_t ag_idx = i;
		if( -1 != div_idx )
			ag_idx = g_groups.GetGroup( GT_DIV, div_idx ).GetAtomicIdx( i );

//		CERR << "-StudentGroup: " << ag_idx << ENDL;
		const std::string& grpe = g_groups.GetGroup( GT_ATOMIC, ag_idx ).GetName();
		HTAG tr( f, HT_TR );
		tr.OpenTag();
		{
			HTAG th( f, HT_TH );
			th.PrintWithContent( grpe );
		}

		Triplet sum;
		for( size_t i=0; i<v_sub.size(); i++ )                // for each subject
		{
			Triplet t = GetVolume2Cond_w( IT_SUBJECT, v_sub[i], ag_idx );
			sum += t;
//			CERR << "  -SUBJECT:" << i << " triplet=" << t << ENDL;

			HTAG td( f, HT_TD, AT_CLASS, "blw" );
			if( g_groups.HasDivisions() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL(GT_DIV);
				td.CloseTag();
			}
			if( g_groups.HasClassGroups() )
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL(GT_CLASS);
				td.CloseTag();
			}
			{
				td.OpenTag();
				PRINT_T_IF_NOT_NULL(GT_ATOMIC);
				td.CloseTag();
			}
		}
#ifdef EXPERIMENTAL_VOL_PER_WEEK
		f_csv.fout << sum.Sum();
		if( i != Nbag-1 )
			f_csv.fout << ',';
		else
			f_csv.fout << '\n';
#endif

		HTAG td( f, HT_TD, AT_CLASS, "blw" );

		if( g_groups.HasDivisions() )
			td.PrintWithContent( sum.Get(GT_DIV) );
		if( g_groups.HasClassGroups() )
			td.PrintWithContent( sum.Get(GT_CLASS) );
		td.PrintWithContent( sum.Get(GT_ATOMIC) );
		if( g_groups.HasClassGroups() )
			td.PrintWithContent( sum.Sum() );
		if( GetNbSpecialEvents() )
			f << HTAG( HT_TD, GetNbSpecialEvents() );
		td.PrintWithContent( GetNbEmptySlots_w( ag_idx ) );
	}

#ifdef EXPERIMENTAL_VOL_PER_WEEK
	f_csv.Close();
#endif

	table.CloseTag();

	if( GetNbConflicts() )
	{
		PrintErrorMessage( f, GetNbConflicts() );
		PrintWeekConflicts( f, data );
	}

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
void
WeekEvents::DumpWeek( std::ostream& f ) const
{
	f << "\n *** dump of week " << _num << g_endl;
	for( const auto& d: _v_days )
		d.DumpDay( f );
}

//-----------------------------------------------------------------------------------
/// Returns the number of days that the group \c ag_idx has to travel to school
size_t
WeekEvents::GetNbDays_w( size_t ag_idx ) const
{
	size_t nb = 0;
	for( const auto& d: _v_days )
		if( d.GetVolumeT_d1( ag_idx ).Sum() > 0.0f )
			nb++;
	return nb;
}

//-----------------------------------------------------------------------------------
/// Returns the number of days
size_t
WeekEvents::GetNbDays_w2( EN_INFOTYPE it, size_t idx ) const
{
	size_t nb = 0;
	for( const auto& d: _v_days )
		if( d.GetVolumeT_d2( it, idx ).Sum() > 0.0f )
			nb++;
	return nb;
}

//-----------------------------------------------------------------------------------
size_t
WeekEvents::GetNbEmptySlots_w( size_t ag_idx ) const
{
	size_t nb = 0;
	for( const auto& d: _v_days )
		nb += d.GetNbEmptySlots_d( ag_idx );
	return nb;
}

//-----------------------------------------------------------------------------------
/// Returns sum of duration (volume) of all events in week that :
/**
- involve either instructor, room or subject (depending on \c it),
- with index \c idx,
- are related to a group type \c gt,
- and that involves the atomic group \c group.
*/
Triplet
WeekEvents::GetVolume2Cond_w( EN_INFOTYPE it, size_t idx, size_t ag_idx ) const
{
	DEBUG_IN;
	Triplet sum;
	for( const auto& d: _v_days )
		sum += d.GetVolume2Cond_d( it, idx, ag_idx );
	DEBUG_OUT;
	return sum;
}
//-----------------------------------------------------------------------------------
Triplet
WeekEvents::GetVolPerDiv_w( int div_idx, EN_INFOTYPE infotype, size_t idx ) const
{
	DEBUG_IN;
	Triplet sum;
	for( const auto& d: _v_days )
		sum += d.GetVolPerDiv_d( div_idx, infotype, idx );
	DEBUG_OUT;
	return sum;
}
//-----------------------------------------------------------------------------------
/// helper function for WeekEvents::GenerateTableHeader()
void
PrintCell_th( const StudentGroup& gr, int k, FileHTML& f, int nb_col_pd )
{
	assert( gr.GetNbAtomicGroups() );
	HTAG th( f, HT_TH, AT_COLSPAN, gr.GetNbAtomicGroups() );
	if( k == 0 && nb_col_pd>2 )
		th.AddAttrib( AT_CLASS, "blw" );
	th.PrintWithContent( gr.GetName() );
	f.fout << g_endl;
}
//-----------------------------------------------------------------------------------
/// Helper function, generates the header of table on the schedule page of a week
void
WeekEvents::GenerateTableHeader( FileHTML& f, int div_idx ) const
{
	DEBUG_IN;

	size_t nb_col_pd = g_groups.GetNbGroups( GT_ATOMIC ); // nb of columns per day (= nb of atomic groups)
	if( -1 != div_idx )
	{
		const DivGroup& grdiv = dynamic_cast<const DivGroup&>(g_groups.GetGroup( GT_DIV, div_idx ) );
		nb_col_pd = grdiv.GetNbAtomicGroups();
	}
	CERR << "table header: div_idx=" << div_idx << " nb_col_pd=" << nb_col_pd << g_endl;

// weekdays line: START ------------------
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();

		HTAG ht_heure( HT_TH, "", AT_CLASS, "firstcol" );                         // colonne des heures
		f << ht_heure;

		for( size_t j=0; j<sp_params_w->GetNbDays(); j++ )
		{
			HTAG th( f, HT_TH, AT_COLSPAN, nb_col_pd );
			if( nb_col_pd > 1 )
				th.AddAttrib( AT_CLASS, "blw" );
			th << sp_params_w->GetWeekday(j).value;
			if( true == sp_params_w->bswitch[BS_PRINT_DAY_DATE] )
			{
				size_t year = g_caldata._current_year;
				if( _yearLeap )
					year++;
				th << " " << GetDate( GetWeekNum(), j, year, "%d/%m" );
			}
			f << th;
		}
	}
// weekdays line: END ------------------

// class groups line: START ----------------------
	if( g_groups.GetNbGroups( GT_CLASS ) )                     // print this header only if we have class groups
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();

		f << HTAG( HT_TH, sp_params_w->GetCodeFromGroupType(GT_CLASS) );
		for( size_t j=0; j<sp_params_w->GetNbDays(); j++ )
		{
			if( -1 == div_idx )      // one page for all divisions
			{
				for( size_t k=0; k<g_groups.GetNbGroups( GT_CLASS ); k++ )
					PrintCell_th( g_groups.GetGroup( GT_CLASS, k ), k, f, nb_col_pd );
			}
			else                      // different pages for divisions
			{
				const StudentGroup& gr_div = g_groups.GetGroup( GT_DIV, div_idx );
				for( size_t k=0; k<gr_div.GetNbSubGroups(); k++ )
					PrintCell_th( gr_div.GetSubGroup(k), k, f, nb_col_pd );
			}
		}
	}
// class groups line: END ----------------------

// atomic groups line: START ----------------------
	bool PrintAGLine = false;
	if( -1 == div_idx )                                // if "all fivisions on same page",
	{                                                  // then, print that line only
		if( g_groups.GetNbGroups( GT_ATOMIC ) > 1 )    // if we DO have more than 1 atomic group
			PrintAGLine = true;
	}
	else                                               // else, only print line if divisions has several atomic groups
	{
		const DivGroup& grdiv = dynamic_cast<const DivGroup&>(g_groups.GetGroup( GT_DIV, div_idx ) );
		size_t n = grdiv.GetNbAtomicGroups();
		if( n > 1 )
			PrintAGLine = true;
	}

	if( PrintAGLine )                    // print this header line only if we have more than 1 group
	{
		HTAG tr( f, HT_TR );
		tr.OpenTag();

		f << HTAG( HT_TH, sp_params_w->GetCodeFromGroupType(GT_ATOMIC) ); // time-slot column
		for( size_t j=0; j<sp_params_w->GetNbDays(); j++ )
		{
			if( -1 == div_idx )      // print all divisions
			{
				for( size_t k=0; k<g_groups.GetNbGroups( GT_ATOMIC ); k++ )
				{
					HTAG th( f, HT_TH );
					if( k == 0 )
						th.AddAttrib( AT_CLASS, "blw" );
					th.PrintWithContent( g_groups.GetGroup( GT_ATOMIC, k ).GetName() );
				}
			}
			else                     // only print the current division
			{
				const StudentGroup& gr_div = g_groups.GetGroup( GT_DIV, div_idx );
				for( size_t k=0; k<gr_div.GetNbSubGroups(); k++ )
				{
					const StudentGroup& gr_class = gr_div.GetSubGroup( k );
//					CERR << "class sub group: " << k << " name="<< gr_class.GetName() << g_endl;
					for( size_t i=0; i<gr_class.GetNbSubGroups(); i++ )
					{
						HTAG th( f, HT_TH );
//						CERR << "atomic sub group: " << i << " name=" << gr_class.GetSubGroup( i ).GetName() << g_endl;
						if( k==0 && i==0 && gr_div.GetNbAtomicGroups()>2 )
							th.AddAttrib( AT_CLASS, "blw" );
						th.PrintWithContent( gr_class.GetSubGroup( i ).GetName() );
					}
				}
			}
		}
	}
// atomic groups line: END ----------------------

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Helper function for WeekEvents::PrintCalendar()
void
PrintNoonLine(
	FileHTML& f,
	size_t    NbDaysOff,
	int       nagpd,      ///< nb of atomic groups per div
	size_t    nbDays      ///< Total nb of days
 )
{
	HTAG tr( f, HT_TR );
	tr.OpenTag();
	f.fout << HTAG( HT_TD );
	assert( NbDaysOff < nbDays );
	for( size_t i=0; i<nbDays - NbDaysOff; i++ )
	{
		HTAG td( f, HT_TD, AT_COLSPAN, nagpd );
		if( nagpd > 1 )
			td.AddAttrib( AT_CLASS, "blw" );
		td.OpenTag();
		td.CloseTag();
	}
}

//-----------------------------------------------------------------------------------
size_t
WeekEvents::GetNbDaysOff() const
{
	size_t c = 0;
	for( const auto& day: _v_days )
		if( day.IsOff() )
			c++;
	return c;
}

//-----------------------------------------------------------------------------------
/// Prints the calendar into the html file
/**
     if \c div_idx=-1, all divisions are printed on the page
*/
void
WeekEvents::PrintCalendar( FileHTML& f, const UserData& data, bool cps, int div_idx ) const
{
	DEBUG_IN;

	p_user_data = &data;

	HTAG table( f, HT_TABLE, AT_CLASS, "cal" );
	table.OpenTag();

	GenerateTableHeader( f, div_idx );

	std::vector<bool> DayOffNotTaggedYet( sp_params_w->GetNbDays(), true );

	size_t nagpd = g_groups.GetNbGroups( GT_ATOMIC );                   // nagpd: Nb of Atomic Groups Per Division
	if( div_idx >= 0 )
		nagpd = g_groups.GetGroup( GT_DIV, div_idx ).GetNbAtomicGroups();

//	CERR << __FUNCTION__ << ": div_idx=" << div_idx << " nagpd=" << nagpd << ENDL;

	_v_columns_memory.resize( sp_params_w->GetNbDays() * nagpd, 0 );

	for( size_t ts=0; ts<sp_params_w->GetNbTimeSlots(); ts++ )                                  // one line for each Time Slot (ts)
	{
		CERR << " - time slot: " << ts << " : "<< sp_params_w->GetTimeSlot(ts).code << g_endl;
		if( ts == sp_params_w->NbTimeSlotsAM && sp_params_w->bswitch[BS_PRINT_NOON_LINE] )  // print empty line to separate AM from PM (if requested)
			PrintNoonLine( f, GetNbDaysOff(), nagpd, sp_params_w->GetNbDays() );

		HTAG tr( f, HT_TR );
		tr.OpenTag();
		{
			HTAG td( f, HT_TD, AT_CLASS, "timeslot" );
			td.OpenTag();
			f << HTAG( HT_DIV, sp_params_w->GetTimeSlot(ts).value, AT_CLASS, "minh" );
		}
		for( size_t k=0; k<sp_params_w->GetNbDays(); k++ )       // parsing days
		{
			CERR << "  - day: " << k << " : " << sp_params_w->GetWeekday(k).code << g_endl;
			const DayEvents& day = GetDay(k);
			const Event* doe = day.IsOff();                      // fetch Day Off Event (doe), if any
			if( doe )                             // if there is one, process it
			{
				if( DayOffNotTaggedYet[k] )
				{
					DayOffNotTaggedYet[k] = false;
					int nb_ts = sp_params_w->GetNbTimeSlots() + (sp_params_w->bswitch[BS_PRINT_NOON_LINE]?1:0);
					std::string se_text = doe->GetComment();
					if( se_text.size() == 0 )
						se_text = g_strings.GetTxt(S_DAY_OFF);

					HTAG td( f, HT_TD,  AT_CLASS, "specialevent" );
					if( doe->IsConflicted() )
					{
						td.AddAttrib( AT_CLASS, "conflicted" );
						td.AddAttrib( AT_TITLE, GetConflictString( *doe ) );
					}

					td.AddAttrib( AT_ROWSPAN, nb_ts );

					if( g_groups.GetNbGroups( GT_ATOMIC, div_idx ) )
						td.AddAttrib( AT_COLSPAN, nagpd );

					if( nagpd>1 )
						td.AddAttrib( AT_STYLE, "border-left-width:2px;" );

					td.PrintWithContent( se_text );
				}
			}
			else               // day not off so print all the columns
			{
				std::vector<const Event*> v_event = day.GetEventsOnTS( ts ); // list of events on this time slot
				size_t nb = g_groups.GetNbGroups( GT_ATOMIC, div_idx );

				const StudentGroup* div_gr = 0;
				if( div_idx > 0 )
					div_gr = &g_groups.GetGroup( GT_DIV, div_idx );

//				CERR << "DIVISION " << div_idx << " : " << nb << " ATOMIC GROUPS\n";
				for( size_t gr=0; gr<nb; gr++ )            // parsing atomic groups (columns)
				{
					size_t idx_col = k * nb + gr;
					size_t agr_idx = gr;
					if( div_idx > 0 )
						agr_idx = div_gr->GetAtomicIdx(gr);

					CERR << "    - group: " << gr << ", idx=" << agr_idx << ", col=" << idx_col << g_endl;
					if( _v_columns_memory.at( idx_col ) )  // if event above had a duration higher than 1, then ignore that column
						_v_columns_memory.at( idx_col )--;
					else
					{
						const Event* event = GetMatchedEvent( v_event, agr_idx, GT_ATOMIC );       // 3 - check if atomic group has something here
						if( event )                                                    // if yes, then print it out
							P_ComputeCell( f, event, idx_col, cps, agr_idx, data );
						else                                                          // else, maybe a class group or a division ?
						{
							int n = 0;
							const Event* event2 = GetMatchedEvent( v_event, agr_idx, GT_CLASS );
							if( event2 )
								n = P_ComputeCell( f, event2, idx_col, cps, agr_idx, data );
							else
							{
								const Event* event3 = GetMatchedEvent( v_event, agr_idx, GT_DIV );
								if( event3 )
									n = P_ComputeCell( f, event3, idx_col, cps, agr_idx, data );
							}
							if( n )         // means that some content has been put into the cell
								gr += n-1;
							else            // else, we need to close the 'td' tag
							{
								HTAG td( f, HT_TD );
								if( gr == 0 && nagpd>1 ) // first column of an empty cell
									td.AddAttrib( AT_CLASS, "blw" );
								td.PrintWithContent( "" );
								f.fout << g_endl;
							}
						}
					}
				}
			}
		}
	}

	p_user_data = 0;
	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// helper function for WeekEvents::PrintCalendar()
int
WeekEvents::P_ComputeCell(
	FileHTML&      f,         ///< the output file
	const Event*   event,     ///< the considered event
	size_t         idx_col,   ///< current index of column in the table, used to remember events more than 1 timeslot long
	bool           cps,       ///< color per subject
	size_t         at_gr_idx,  ///< current atomic group index
	const UserData& userdata
) const
{
	DEBUG_IN;

// first check that current atomic group is the first atomic group of the group, to prevent horizontal overlap
	if( event->GetGroupType() != GT_ATOMIC )
	{
		const StudentGroup& gr = g_groups.GetGroup( event->GetGroupType(), event->GetIndex( IT_GROUP ) ); // fetch the class group to whom the events belongs to
		CERR << " group=" << gr.GetName()<< ENDL;
		if( g_groups.GetGroup( GT_ATOMIC, at_gr_idx ) != gr.GetFirstAtGroup() )
		{
			CERR << " CONFLICT, horiz overlap detected: at_gr_idx=" << at_gr_idx << g_endl;
			CERR << " idx_col=" << idx_col << g_endl;
			CERR << *event;
			DEBUG_OUT;
			return 0;
		}
	}

	HTAG td( f, HT_TD );

	size_t duration = event->GetIndex( IT_DURATION );
	if ( duration > 1 )      // if event has duration of more than 1 time slot,
	{                                              // then mark it for the next line
		td.AddAttrib( AT_ROWSPAN, duration );

		size_t NbAtomicGroups = 1;
		if( event->GetGroupType() != GT_ATOMIC )
		{
			const StudentGroup& gr = g_groups.GetGroup( event->GetGroupType(), event->GetIndex( IT_GROUP ) );
			NbAtomicGroups = gr.GetNbAtomicGroups();
		}
		for( size_t i=0; i<NbAtomicGroups; i++ )
			_v_columns_memory.at(idx_col+i) = duration-1;
	}

	int n = event->FindGroupSize();
	td.AddAttrib( AT_COLSPAN, n );

	if( !event->IsConflicted() )
	{
		std::ostringstream oss;
		oss << "c";
		if( cps )
			oss << event->GetIndex( IT_SUBJECT )%sp_params_w->NbCSSColors;
		else
			oss << event->GetIndex( IT_INSTRUCTOR )%sp_params_w->NbCSSColors;
		if( oss.str().empty() )
			CERR << "Warning, string empty\n";
		td.AddAttrib( AT_CLASS, oss.str() );

		if( event->HasComment() )
		{
			td.AddAttrib( AT_TITLE, event->GetComment() );
			if( true == sp_params_w->bswitch[BS_COMMENT_FORMATTING] )
				td.AddAttrib( AT_CLASS, "comment" );
			g_stats.AddCommentedEvent( *event );
		}
	}
	else
	{
		td.AddAttrib( AT_CLASS, "conflicted" );
		td.AddAttrib( AT_TITLE, GetConflictString( *event ) );
	}

	if( at_gr_idx == 0 && g_groups.GetNbGroups(GT_ATOMIC)>1 )
		td.AddAttrib( AT_CLASS, "blw" );

	td.AddAttrib( AT_CLASS, "cne" ); // Cell Not Empty
	td.OpenTag();
	event->PrintInCell( f.fout, userdata );
	td.CloseTag(true);

	DEBUG_OUT;
	return n;
}

//-----------------------------------------------------------------------------------
