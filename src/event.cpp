/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// event.cpp

#include "event.h"
#include "globals.h"
#include "cal_data.h"
#include "user_data.h"
#include "helper_functions.h"
#include "error.h"
#include "htag.h"
#include "params.h"
#include "groups.h"
#include "stats.h"
#include "strings.h"

/// This macro makes sure that the pointer on the groups is valid.
#define CHECK_STATIC_POINTER assert( sp_st_groups )

extern UserData* gp_data;

// Allocation of static variables
size_t           Event::s_IdCounter  = 0;
const GroupSets* Event::sp_st_groups = 0;
Params*          Event::sp_params_e  = 0;

//-----------------------------------------------------------------------------------
/// Macro used for checking input values validity
#define CHECK_INDEX_MAXVALUE( a, b, c ) \
	{ \
		if( a > b ) \
		{ \
			std::cerr << "Error on input file: invalid " << c << ", value=" << (int)a << ", max value is " << (int)b << "\n"; \
			std::cout << "Input file error, check stderr.txt\n"; \
			throw ERROR( "invalid value for ", c ); \
		} \
	}

//-----------------------------------------------------------------------------------
Triplet
Event::GetEventVolume() const
{
	if( GetEventType() == ET_TEACH )
		return Triplet( GetGroupType(), _duration * sp_params_e->atomic_duration );
	return Triplet( GT_NONE, _duration * sp_params_e->atomic_duration );
}

//-----------------------------------------------------------------------------------
/// Constructor, builds the event from \c input_line
/**
All the data is converted into indexes, see class declaration
*/
Event::Event( const LineContent& in_line ) :
	_groupType(GT_NONE),
	_week_no(-1),
	_timeslot_idx(0),
	_duration(0),
	_div_idx(-1),
	_evConflictId(-1),
	_conflictType(CT_DUMMY)
{
	DEBUG_IN;
	CHECK_STATIC_POINTER;

	_eventId = s_IdCounter++;

	const StringVector& input_line = in_line.first; // just an alias
	try {
		TRY_STRING2INT( input_line.at(sp_params_e->input_pos[IT_WEEK]), _week_no, "week num" );

		_subject_idx    = gp_data->GetIndexFromString( IT_SUBJECT,    input_line.at(sp_params_e->input_pos[IT_SUBJECT]) );
		_room_idx       = gp_data->GetIndexFromString( IT_ROOM,       input_line.at(sp_params_e->input_pos[IT_ROOM]) );
		_instructor_idx = gp_data->GetIndexFromString( IT_INSTRUCTOR, input_line.at(sp_params_e->input_pos[IT_INSTRUCTOR]) );

		std::string event_type  = input_line.at(sp_params_e->input_pos[IT_TYPE]);
		sp_params_e->ComputeType( event_type, _eventType, _groupType );

		std::string event_group = input_line.at(sp_params_e->input_pos[IT_GROUP]);
		CERR << "event_type=" << event_type << " group=" << event_group << " computed: _eventType=" << _eventType << " _groupType=" << _groupType << ENDL;

		_groupIdx = sp_st_groups->GetGroupIndexFromString( event_group, _eventType, _groupType );
		CERR << "_groupIdx=" << _groupIdx << " _groupType=" << _groupType << ENDL;
		if( _groupIdx == -1 && _eventType != ET_DAY_OFF )
		{
			throw ERROR( "unrecognized group", event_group );
		}

		if( _eventType != ET_DAY_OFF )
			_div_idx = sp_st_groups->GetDivisionIndex( _groupType, _groupIdx );

		_day_idx = sp_params_e->GetDayIndexFromCode( input_line.at(sp_params_e->input_pos[IT_DAY]) );

		if( _eventType != ET_DAY_OFF ) // if day off, no need to have a valid time slot
		{
			_timeslot_idx = sp_params_e->GetTimeSlotIndexFromCode( input_line.at(sp_params_e->input_pos[IT_TIMESLOT]) );
			TRY_STRING2INT( input_line.at(sp_params_e->input_pos[IT_DURATION]), _duration, "duration" );
		}
		_comment = input_line.at(sp_params_e->input_pos[IT_COMMENT]);

// validity post checking
		CHECK_INDEX_MAXVALUE( _day_idx,      sp_params_e->GetNbDays(),      "day index" );
		CHECK_INDEX_MAXVALUE( _timeslot_idx, sp_params_e->GetNbTimeSlots(), "time slot index" );
		CHECK_INDEX_MAXVALUE( _duration,     sp_params_e->MaxDuration,      "duration" );

		if( _duration == 0 && !IsSpecialEvent() ) // can't be...
		{
			std::cerr << "ERROR: invalid null duration\n";
			throw ERROR( "invalid null duration value", "" );
		}

// checking that duration does not extend the day period (or the half day)
		auto RefLength( sp_params_e->GetNbTimeSlots() );
		if( sp_params_e->bswitch[BS_PRINT_NOON_LINE] )
		{
			if( _timeslot_idx < sp_params_e->NbTimeSlotsAM )  // morning event
			{
				if( _timeslot_idx + _duration > sp_params_e->NbTimeSlotsAM )
				{
					RefLength = sp_params_e->NbTimeSlotsAM;
					_conflictType = CT_TOO_LONG;
				}
			}
		}
		if( _timeslot_idx + _duration > sp_params_e->GetNbTimeSlots() )
			_conflictType = CT_TOO_LONG;

		if( _conflictType == CT_TOO_LONG )
		{
			assert( (int)RefLength - (int)_timeslot_idx > 0 );
			std::cerr << "Event too long, duration was " << (int)_duration;
			_duration = RefLength - _timeslot_idx;
			std::cerr << ", now truncated to " << (int)_duration << g_endl;
		}
	}
	catch( const ErrorClass& e )
	{
		g_stats.AddInvalidEvent( in_line, e.what() );
		std::cerr << "Error with input line " << in_line.second << " (" << input_line.size() << " elements), ";
		std::cerr << "msg="<< e.what() << g_endl;

/*		size_t count=0;
		for( const auto& s: input_line )
			std::cerr << " - " << count++ << ":-" << s << "-\n"; */
		DEBUG_OUT;
		throw e;
	}
	catch( ... )
	{
		std::cerr << "UNHANDLED EXCEPTION !!!\n"; // shouldn't happen, but who knows...
		DEBUG_OUT;
		throw;
	}

	DEBUG_OUT;
}

//-----------------------------------------------------------------------------------
/// Returns information as an index, depending on \c it
/**
Returns an int because if some information is not available, it is implemented as -1
(groups for example, if no group specified)
*/
int
Event::GetIndex( EN_INFOTYPE it ) const
{
	switch( it )
	{
		case IT_SUBJECT    : return _subject_idx;    break;
		case IT_ROOM       : return _room_idx;       break;
		case IT_INSTRUCTOR : return _instructor_idx; break;
		case IT_TIMESLOT   : return static_cast<int>(_timeslot_idx);  break;
		case IT_DURATION   : return static_cast<int>(_duration);      break;
		case IT_DAY        : return static_cast<int>(_day_idx);       break;
		case IT_DIVISION   : return _div_idx;       break;
		case IT_WEEK       : return _week_no;       break;
		case IT_GROUP      : return _groupIdx;      break;
		default:
			assert(0);
	}
}

//-----------------------------------------------------------------------------------
bool
Event::HoldsGroup( EN_GROUP_TYPE gt, size_t idx ) const
{
	CHECK_STATIC_POINTER;

	if( _groupIdx<0 ) // if a special event, then no group
		return false;

/*	switch( et )                 // this is not really needed:
	{                            // if the event is not one of these 3 types,
		case ET_ATOMIC:          // then it should been detected by the test just above
		case ET_CLASS:
		case ET_DIV:
		break;
		default:
			return false;
	}

	if( _eventType == ET_ATOMIC )
	{
		if( et == ET_ATOMIC )
			return static_cast<int>(idx) == _groupIdx;   // just check if the groups are the same
		else
			return false;            // else, an atomic group cannot hold another type of group
	}
*/
	const StudentGroup& cg1 = sp_st_groups->GetGroup( _groupType, _groupIdx ); // group of the event ( GT_CLASS or GT_DIV )
	const StudentGroup& cg2 = sp_st_groups->GetGroup( gt,         idx       ); // other group ( GT_CLASS or GT_DIV )
	return cg1.Overlaps( cg2 );
}
//-----------------------------------------------------------------------------------
/// Returns the number of atomic groups that the event covers
int
Event::FindGroupSize() const
{
	CHECK_STATIC_POINTER;

	if( _groupType == GT_ATOMIC )
		return 1;
	const StudentGroup& cg = sp_st_groups->GetGroup( _groupType, _groupIdx );
	return cg.GetNbAtomicGroups();
}

//-----------------------------------------------------------------------------------
/// Prints the data into the html cell (assumes <td> and </td> will be printed by the caller)
void
Event::PrintInCell( std::ofstream& fout, const UserData& userdata ) const
{
	if( sp_params_e->bswitch[BS_PRINT_EVENT_TYPE] && _eventType != ET_SPECIAL )
		fout << sp_params_e->GetCodeFromGroupType( GetGroupType() ) << g_br;

	if( true == sp_params_e->bswitch[BS_PRINT_UNKNOWN] || GetIndex( IT_SUBJECT ) != 0 )
	{
		auto s = userdata.GetItem( IT_SUBJECT, GetIndex( IT_SUBJECT ) );
		if( !s.empty() )
			fout << s << g_br;
	}

	if( true == sp_params_e->bswitch[BS_PRINT_UNKNOWN] || GetIndex( IT_ROOM ) != 0 )
	{
		auto s = userdata.GetItem( IT_ROOM, GetIndex( IT_ROOM ) );
		if( !s.empty() )
			fout << s << g_br;
	}

	if( true == sp_params_e->bswitch[BS_PRINT_UNKNOWN] || GetIndex( IT_INSTRUCTOR ) != 0 )
		fout << userdata.GetItem( IT_INSTRUCTOR, GetIndex( IT_INSTRUCTOR ) );

#ifdef EXPERIMENTAL_SESSION_ORDER
		fout << g_br << '(' << _orderIndex << ')';
#endif
}

//-----------------------------------------------------------------------------------
/// Used to print the events having a comment
void
Event::PrintAsHtmlLine( FileHTML& f, const UserData& userdata ) const
{
	{
		HTAG td( f, HT_TD );
		td.OpenTag();
		f.fout << sp_params_e->GetLinkOnWeekSchedule( _week_no, _div_idx, LT_WEEK );
	}

	f.fout << HTAG( HT_TD, sp_params_e->GetWeekday( _day_idx ).value );
	f.fout << HTAG( HT_TD, userdata.GetItem( IT_INSTRUCTOR, GetIndex( IT_INSTRUCTOR ) ) );
	f.fout << HTAG( HT_TD, _comment );
}

//-----------------------------------------------------------------------------------
/// Used to print events as list items
/**
Used on
 - the conflicts page
 - the instructor's diary page
 - the subjects page
*/
void
Event::Print( std::ofstream& fout, EventPrintDetails what ) const
{
	CHECK_STATIC_POINTER;

	fout << g_lio;
	if( what.week_no )
		fout << g_strings.GetTxt( S_WEEK ) << " " << GetIndex( IT_WEEK ) << ", ";

	fout << sp_params_e->GetWeekday( GetIndex( IT_DAY ) ).value << ' '
		<< GetDate( GetIndex( IT_WEEK ), GetIndex( IT_DAY ), g_caldata._current_year, "%d/%m" ) << ", "
		<< sp_params_e->GetTimeSlot( GetIndex( IT_TIMESLOT ) ).value << ", "
		<< GetIndex(IT_DURATION) * sp_params_e->atomic_duration << "h.";
	if( _eventType != ET_SPECIAL )
		fout << ", " << sp_params_e->GetCodeFromGroupType( _groupType );

	if( what.group && _eventType != ET_DAY_OFF )
		fout << ", " << g_strings.GetTxt( S_GROUP ) << ' ' << sp_st_groups->GetGroup( _groupType, _groupIdx ).GetName();

	if( what.subject )
		fout  << ", " << gp_data->GetItem( IT_SUBJECT, GetIndex( IT_SUBJECT ) );
	if( what.instructor )
		fout  << ", " << gp_data->GetItem( IT_INSTRUCTOR, GetIndex( IT_INSTRUCTOR ) );
	if( what.room )
		fout  << ", " << gp_data->GetItem( IT_ROOM, GetIndex( IT_ROOM ) );

	fout  << g_lic << g_endl;
}

//-----------------------------------------------------------------------------------
/// used to sort events
bool
Event::operator < ( const Event& e ) const
{
	if( _week_no < e._week_no )
			return true;
	if( _week_no == e._week_no )
	{
		if( _day_idx < e._day_idx )
			return true;
		if( _day_idx == e._day_idx && _timeslot_idx < e._timeslot_idx )
			return true;
	}

	return false;
}
//-----------------------------------------------------------------------------------
bool
Event::operator == ( const Event& e ) const
{
	if( _groupIdx != e._groupIdx )
		return false;
	if( _eventType != e._eventType )
		return false;
	if( _day_idx != e._day_idx )
		return false;
	if( _timeslot_idx != e._timeslot_idx )
		return false;
	if( _duration != e._duration )
		return false;

	if( _subject_idx != e._subject_idx )
		return false;
	if( _instructor_idx != e._instructor_idx )
		return false;
	if( _room_idx != e._room_idx )
		return false;
	if( _week_no != e._week_no )
		return false;

	return true;
}

//-----------------------------------------------------------------------------------
/// streamin operator
/**
\todo 20181116: Find a way to reenable access to Params (see code)
*/
std::ostream&
operator << ( std::ostream& stream, const Event& e )
{
	stream << "Event:"
		<< "\n# Id: " << e._eventId << " conflict:" << e._evConflictId << " week: " << e._week_no
		<< "\n# event type: " << (size_t)e._eventType
		<< "# group type: " << (size_t)e._groupType // << " code: " << sp_params_e->GetCodeFromGroupType(e._groupType)
		<< "\n# group index: "    << e._groupIdx
		<< "\n# subject idx: "    << e._subject_idx       << " (" << gp_data->GetItem( IT_SUBJECT,    e.GetIndex(IT_SUBJECT) ) << ")"
		<< "\n# room idx: "       << e._room_idx          << " (" << gp_data->GetItem( IT_ROOM,       e.GetIndex(IT_ROOM) ) << ")"
		<< "\n# instructor idx: " << e._instructor_idx    << " (" << gp_data->GetItem( IT_INSTRUCTOR, e.GetIndex(IT_INSTRUCTOR) ) << ")"

//	stream << "# day idx: "        << (int)e._day_idx      << " (" << sp_params_e->GetWeekday( e._day_idx ).value << ")\n";
//	stream << "# time slot: "      << (int)e._timeslot_idx << " (" << sp_params_e->GetTimeSlot(e._timeslot_idx).code   << ")"
//		<< " duration: "  <<  (int)e._duration   << ENDL << ENDL;

		<< ENDL << ENDL;

	return stream;
}

//-----------------------------------------------------------------------------------
/// Streams event as Ical's VEVENT objet into \c f.
/**
For Ical specs, see http://tools.ietf.org/search/rfc5545
*/
void
Event::PrintAsIcal(
	FileICAL&        f,
	EN_CALENDAR_TYPE agt,
	const UserData&  userdata
) const
{
	CHECK_STATIC_POINTER;

	static size_t ical_uid_index=0;

	if( IsSpecialEvent() )   // at present, special events are NOT exported in ical files.
		return;

	DEBUG_IN;

	int week_nb = GetIndex( IT_WEEK );
	size_t widx = userdata._week_indexes.at(week_nb);
	const WeekEvents& we = userdata.GetWeek( widx );

	size_t year = g_caldata._current_year;
	if( we._yearLeap )
		year++;

	std::stringstream oss;
	oss << GetDate( GetIndex( IT_WEEK ), GetIndex( IT_DAY ), year, "%Y%m%d" );
	oss << "T";
	oss << GetTime( sp_params_e->GetTimeSlot(_timeslot_idx).value, g_caldata._zoulou_offset );

	f.fout << "BEGIN:VEVENT" << FileICAL::CRLF;
	f.fout << "DTSTART;TZID=France/Paris:" << oss.str() << FileICAL::CRLF;
//	f.fout << "DTSTART:" << oss.str() << FileICAL::CRLF;
	f.fout << "UID:gensched@" << ical_uid_index++ << FileICAL::CRLF;
	f.fout << "DTSTAMP:" << g_caldata.GetNow_ical() << FileICAL::CRLF;
	f.fout << "DURATION:PT" << GetDuration( _duration*sp_params_e->atomic_duration ) << FileICAL::CRLF;
	f.fout << "SUMMARY:" << sp_params_e->GetCodeFromGroupType( _groupType ) << " " << userdata.GetItem( IT_SUBJECT, GetIndex(IT_SUBJECT) );
	if( agt == CAL_INSTRUCTOR )
	{
		f.fout << " - Group " << sp_st_groups->GetGroup( _groupType, _groupIdx ).GetName();
	}
	else
	{
		f.fout << " - Instr: " << userdata.GetItem( IT_INSTRUCTOR, GetIndex(IT_INSTRUCTOR) );
	}
	f.fout << FileICAL::CRLF;
	f.fout << "DESCRIPTION: instr.: " << userdata.GetItem( IT_INSTRUCTOR, GetIndex(IT_INSTRUCTOR) ) << FileICAL::CRLF;
	f.fout << "LOCATION: " << userdata.GetItem( IT_ROOM, GetIndex(IT_ROOM) ) << FileICAL::CRLF;
	f.fout << "END:VEVENT" << FileICAL::CRLF; // << FileICAL::CRLF;

	DEBUG_OUT;
}
//-----------------------------------------------------------------------------------

