/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
\file globals.cpp
\brief The file holds all the global data that needs to be accessed from different parts of the program
*/

#include "globals.h"

#include "params.h"
#include "stats.h"
#include "cal_data.h"
#include "user_data.h"
#include "groups.h"
#include "strings.h"

#ifdef DEBUG
	#warning "Compiling in DEBUG mode"
	unsigned char GlobalIndent=0;
#endif

Params*     gp_params=0;
Stats       g_stats;
CalData     g_caldata;
const UserData*    gp_data = 0;
GroupSets   g_groups;
Strings     g_strings;
OutputFiles g_files;

//INSTRUCTORS g_instr;

const std::string g_lio = "<li>";
const std::string g_lic = "</li>";

const std::string g_sum = "&#931;";      ///< sigma html entity
const std::string g_hour = " (h.)";

extern const std::string g_br = "<br>";

const char g_endl       = '\n';   ///< endline

