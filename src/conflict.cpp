/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/**
\file conflict.cpp
\brief implementation of class Conflict
*/

#include "conflict.h"
#include "user_data.h"
#include "helper_functions.h"
#include "htag.h"
#include "stats.h"

#include "globals.h"

size_t Conflict::s_ConfIdCounter = 0;

//-----------------------------------------------------------------------------------
/// Constructor, assigns a new id to conflict, and assigns this id to the two events
Conflict::Conflict( EN_CONFLICT_TYPE t, Event& e1, Event& e2 ) : _type(t)
{
	_conflictId = s_ConfIdCounter++;
	_event1_id = e1.GetId();
	_event2_id = e2.GetId();
	e1.SetConflict( _conflictId );
	e2.SetConflict( _conflictId );
	g_stats.AddConflict(t);
}
//-----------------------------------------------------------------------------------
/// Constructor for a conflict involving a single event
Conflict::Conflict( EN_CONFLICT_TYPE t, Event& e1 ) : _event2_id(-1), _type(t)
{
	_conflictId = s_ConfIdCounter++;
	_event1_id = static_cast<int>( e1.GetId() );

	if( e1.IsConflicted() == false )
		e1.SetConflict( _conflictId );
	g_stats.AddConflict(t);
}

//-----------------------------------------------------------------------------------
size_t
Conflict::GetOtherEventId( size_t ev_id ) const
{
	assert( _event1_id >= 0 );
	assert( _event2_id >= 0 );
	if( _event1_id == static_cast<int>(ev_id) )
		return _event2_id;
	else
		return _event1_id;
}
//-----------------------------------------------------------------------------------
/// Returns true if conflict uses \c id1 for either \c _event1_id or \c _event1_id
/**
if yes, then \c id2 will hold upon return the index of the other event
*/
bool
Conflict::UsesEventId( size_t id ) const
{
	assert( _event1_id != -1 );

	int idi = static_cast<int>(id);
	if( idi == _event1_id || idi == _event2_id )
		return true;
	return false;
}

//-----------------------------------------------------------------------------------
const char*
Conflict::GetTypeString() const
{
	const char* n = 0;
	switch( _type )
	{
		case CT_GTS:      n = "group";      break;
		case CT_ITS:      n = "instructor"; break;
		case CT_RTS:      n = "room";       break;
		case CT_DAY_OFF:  n = "day-off";    break;
		case CT_TOO_LONG: n = "Event duration too long";   break;
		default: assert(0);
	}
	return n;
}

//-----------------------------------------------------------------------------------
/// Used to print conflict on the "conflicts" page
void
Conflict::PrintHtml( FileHTML& f, const UserData& data ) const
{
	EventPrintDetails what;
	what.week_no = false;

	f.fout << "Type: " << GetTypeString() << ENDL;

	assert( _event1_id != -1 );
	const Event* e1 = data.GetEventFromId( _event1_id );
	assert( e1 );

	const Event* e2 = 0;
	if( _type != CT_TOO_LONG )
	{
		assert( _event2_id != -1 );
		e2 = data.GetEventFromId( _event2_id );
		assert( e2 );
	}
	HTAG ul( f, HT_UL );
	ul.OpenTag();
	if( _type == CT_DAY_OFF )               // then, find the one related and print only the other one
	{
		if( e1->GetEventType() == ET_DAY_OFF )
			e2->Print( f.fout, what );
		else
			e1->Print( f.fout, what );
	}
	else
	{
		e1->Print( f.fout, what );
		if( _type != CT_TOO_LONG )
			e2->Print( f.fout, what );
	}
}

//-----------------------------------------------------------------------------------
#if 0
std::ostream& operator << ( std::ostream& stream, const Conflict& c )
{
	assert(0);
	stream << "Conflict:\n -type=";
	switch( c._type )
	{
		case CT_GTS: stream << "Group"; break;
		case CT_ITS: stream << "Instructor"; break;
		case CT_RTS: stream << "Room"; break;
//		case CT_HOLIDAY: stream << "holiday"; break;
		default: assert(0);
	}
	stream << "\n -event 1:";
	const Event* e1 = gp_data->GetEventFromId( c._event1_id );
	assert( e1 );
	stream << *e1;
//	if( c._type != CT_HOLIDAY )
	{
		stream << " -event 2:";
		const Event* e2 = gp_data->GetEventFromId( c._event2_id );
		assert( e2 );
		stream << *e2;
	}
	return stream;
}
#endif
//-----------------------------------------------------------------------------------

