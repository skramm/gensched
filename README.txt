This is file README.txt for software package "gensched"

Please refer to included documentation or to
https://gitlab.com/skramm/gensched/
for further information.

For quick build, see "make help"
or try in a linux shell:
   make
   sudo make install-bin

Licence: GPL v3


