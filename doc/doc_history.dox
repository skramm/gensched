/**
\page p_history Release log

-20180825:
 - switched to gitlab.com
 - bug fix on \ref OF_GS page

- 20160316: 0.6.2
 - added new page holding a sorted list of events per instructor, and added link on it from worktime page
 (see <a href="demo/demoD_full-00/id.html">example</a>.)
 - fixed bug on leap weeks on some years
 - dropped Windows support (temporarly ?)
 - many bug fixes

- 20150418: 0.6
 - added man page (Linux)
 - extended help page
 - extended/fixed subject/week (planning) page
 - bug fixes
 - removed dependency to GetPot
 - regorganized header files dependencies, now mostly forward declarations
 - reorganized command-line and configuration reading
 - added switch to have one page per week and/or one page per division
 - added "CommentFormatting" switch
 - added feature: when partial extraction (switch -w), stats page now shows number of ignored events and ignored weeks


- 20150208: 0.5.0
 - Added "planning" view (see page \ref p_features)
 - Added "help" page, showing valid codes for input file
 - extended max duration of events

- 20141219: 0.4.2 (rev 165)
  - fixed bug on reference volume on page gs.html: removed unneeded cell
  - fixed bug on leap year in ical files
  - lots of code cleaning, replaced usage of global strings for html tags with calls to a dedicated class (HTAG)
  - first Windows release

- 20141121: 0.4.0 (rev. 147)
 - added detection of events that are too long
 - added Tidy to the demo build process, to catch html compliance violations
 - re-enabled pdf demos in makefile
 - added bold line between days on schedule
 - added Verbose run-time command-line option
 - added class CELL
 - added in navbar of each week a link pointing on dual page (schedule/summary)
 - added csv export of instructor worktime, and added nb of days
 - fixed build process issues

- 20141012: 0.3
 - Windows build in progress
 - complete refactoring of how groups are handled: now, division groups hold class groups (and not atomic groups as before)
 - replaced text links with some icons from http://flaticons.net/
 - makefile now performs an out-of-source build, into the \c build folder
 - added feature: option to render the schedule of the different divisions on different pages, see Params::OnePagePerDiv
 - ical files: removed time stamp on file names (wasn't relevant)
 - added "Day off" event type, see \ref sss_intro_sevents


- 20140715: 0.2.3 (rev 97)
 - cleaned packaging of releases

- 20140714: 0.2.2 (rev 93)
 - fixed bug: duplicate events were counted in summary
 - demos now open directly on "main" schedule page
 - added flag PrintNoonSepLine
 - added pdf generation of demos through makefile and wkhtmltopdf
 - cleaned some html generation issues
 - implemented color in schedule as command-line option
 - misc.

- 20140323: 0.2.1 (rev 74)
 - all output file information is now centralized in class OutputFiles, instanciated in global \c g_files

- 20140322 : 0.2 (rev 72)
 - changed specs of groups.ini file: now key is the same ("groups") for the three sections
 - code: Removed groups string field in class EVENT, now stored as an index
 - fixed issue with horizontal line between AM and PM
 - Added Ical (.ics) file generation
 - Added color demo
 - fixed exception handling on invalid events, now correctly appear on "stats" page, and this is shown on index page.
 - duplicate events are now stored as objects (instead of pointers, because this caused issues due to std::vector memory handling)
 - added table formatting for duplicate events
 - code: added globals html tags strings to reduce app memory footprint

- 20131122 : 0.1 - first release (alpha)
*/

