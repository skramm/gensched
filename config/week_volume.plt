# this file is part of gensched, a GPL HTML schedule generator
# see http://sourceforge.net/projects/gensched/

# common header for gnuplot command file

set datafile separator ";"

set terminal pngcairo size 640,480
set output "week_volume.png"
set xlabel "week number"
set ylabel "volume"

set style data linespoints
set timefmt "%d/%m/%Y"
set xdata time
set format x "%U"


