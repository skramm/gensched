#!/bin/bash
# this script is used to install software when downloading binaries only
# - needs sudo
# - follows linux FHS

set +x

cp build/gensched /usr/local/bin

mkdir -p /etc/gensched
cp config/*.js /etc/gensched
cp config/*.css /etc/gensched
cp config/*.ini /etc/gensched
cp config/*.png /etc/gensched

chmod a+rx /usr/local/bin/gensched
chmod a+r /etc/gensched/*

# copying doc
mkdir -p /etc/gensched/html
cp -r build/html/* /etc/gensched/html

chmod -R a+r /etc/gensched/html

# copying man page
# ref: http://www.pathname.com/fhs/pub/fhs-2.3.html#USRSHAREMANMANUALPAGES
mkdir -p /usr/local/man/man1
cp doc/gensched.1 /usr/local/man/man1

echo "install done."

