#!/bin/bash

# remove program
rm /usr/local/bin/gensched

# remove configuration files
rm /etc/gensched/*
rmdir /etc/gensched

# remove manual
rm -r /etc/gensched/html/*
rmdir /etc/gensched/html/

