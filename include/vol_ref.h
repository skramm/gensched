/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// vol_ref.h

#ifndef HG_VOL_REF_H
#define HG_VOL_REF_H

#include <map>
#include "triplet.h"

//-----------------------------------------------------------------------------------
/// holds reference volumes for each course, that will be printed out in summary
class RefVolume
{
	friend class UserData;
	public:
		bool readInFile( const std::string& fname, char delim );

		Triplet Get( const std::string& subject ) const
		{
			if( _m_trip.find(subject) == _m_trip.cend() ) // if not found, then
				return Triplet();                        //  return empy value
			return _m_trip.at(subject);
		}

	private:
		std::map< std::string,Triplet > _m_trip;
};
//-----------------------------------------------------------------------------------

#endif
