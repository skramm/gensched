/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// params.h

#ifndef HG_PARAMS_H
#define HG_PARAMS_H

#include <ostream>
#include <numeric>
#include <map>

#include "datatypes.h"

class FileHTML;
class Event;

//-----------------------------------------------------------------------------------
/// A generic functor for sorting vector of pointers
/**
Usage: assuming the vector is declared as:
\code
	std::vector<Class*> myvect;
\endcode
then, the sorting is done with:
\code
	std::sort( myvect.begin(), myvect.end(), PointerCompare );
\endcode
Of course, you \b NEED to implement the operator < in the given class.
*/
struct PointerCompare
{
    template< typename T >
    bool operator()( T const *p1, T const *p2 ) const
        { return *p1 < *p2; }
};

//-----------------------------------------------------------------------------------
/// boolean run-time parameters, see related GetBoolSwitchString( EN_BOOL_SWITCH ) and Params::bswitch
enum EN_BOOL_SWITCH
{
	BS_ONE_PAGE_PER_DIV = 0
	,BS_ONE_PAGE_PER_WEEK
	,BS_COLOR_PER_SUBJECT
	,BS_PRINT_DAY_DATE         ///< true: print the date of day in the header of each week
	,BS_PRINT_UNKNOWN          ///< true: if a value of one of the fields {subject, instructor, room} is "unknown", then it will be printed in the schedule
	,BS_OUTPUT_TIME_STAMP
	,BS_PRINT_EVENT_TYPE
	,BS_PRINT_NOON_LINE
	,BS_COPY_STYLE_SHEETS
	,BS_COMMENT_FORMATTING
	,BS_SHOW_SUM_RATIO    ///< if true, prints in the sum column the ratio between volume and reference volume. See printSumTriplet()

	,BS_DUMMY
};

std::string GetBoolSwitchString( EN_BOOL_SWITCH );

//-----------------------------------------------------------------------------------
/// global run-time parameters
/**
\todo Add a command-line switch to have another csv file delimiter
*/
struct Params
{
	std::string ConfigFileName =  "gensched.ini";
	bool ConfigFileRead        = false;
	std::string OutputDirRootName; ///< \todo check if these two are really needed
	std::string OutputDirFullName; ///< holds the output folder, with the time stamp (if it is requested, see \ref sec_clioptions)

	std::string GroupsFileName    = "groups.ini";
	bool        HasGroups         = false;
	bool        HasReferenceFile  = false;
	std::string ReferenceFileName;
	std::string LanguageFileName;     ///< The language file name
	std::string InstructorFileName;   ///< The instructor file name

	int  OverrideYear = -1;

	std::string WeekClString;

	bool DebugMode   = false;
	bool VerboseMode = false;

/// \name input indexes related data
/**
Consider the following situation: the input file has 20 columns of data, where only 10 are useful.
Say week value is in column 1, day index is in column 5, and room index is in column 20. (and so on)
Then: \c input_pos[IT_WEEK] will hold 0, input_pos[IT_DAY] will hold 4, and input_pos[IT_ROOM] will hold 19

The fields name are stored in \c v_index_name, that has 10 values (indexes 0 to 9)

To map position of fields to names, we use \c m_input_to_names
*/
///@{
	std::vector<size_t>      input_pos;         ///< position of fields in input file, size will be IT_MAXVALUE_INPUT_INDEX
	StringVector             v_index_name;      ///< names of indexes in input file (related to input_pos), \warning It MUST match the order of EN_INFOTYPE, and has the same size
	std::map<size_t,size_t>  m_input_to_names;  ///< gives the correspondence between input index (key) and name index (value)
///@}

	std::string unknown_subject_code;
	std::string unknown_instructor_code;
	std::string unknown_room_code;

	char CSV_delim;

	size_t nb_tokens;    ///< nb of tokens in a line of the CSV input file

// --- section: general ---
	float  atomic_duration;  ///< This is the duration, expressed in hours, of the root time slot unit. For example, if equal to 1.5, this means that each time slot is 90 minutes
	uchar  MaxDuration;

	size_t NbTimeSlotsAM;   ///< holds the number of time slots in the morning. Is used to add a separation line in the output schedule for lunchtime

// --- /section: general ---

	size_t NbCSSColors; ///< nb of colors in CSS file

	int graphVolumePerWeek_width = 800;

/// boolean switches, can be set either through command-line, either through configuration file, either both.
/// Are initialised in Params::Params()
	std::map<EN_BOOL_SWITCH,bool> bswitch;

	private:
		std::vector<std::pair<std::string,EN_GROUP_TYPE> >  _v_gtypes;   ///< vector holding pairs of (code of the course type, course type enum)
		std::vector<std::pair<std::string,EN_EVENT_TYPE> >  _v_etypes;   ///< vector holding pairs of (code of the course type, course type enum)
		std::vector<TimeSlot>  _v_timeslots; ///< vector of valid time slots
		std::vector<WeekDay>   _v_weekdays;  ///< vector of valid weekdays

	public:
		Params();
		bool ReadParamsFromFile( const std::string& fname );
		void Dump( std::ostream& ) const;
		void PrintParams( std::ostream& ) const;
		void SetInputFileName( const char* );
		void ComputeType( std::string type, EN_EVENT_TYPE& eventType, EN_GROUP_TYPE& eventGroup );

//		EN_EVENT_TYPE GetTypeFromCode( const std::string& type ) const;

		std::string   GetCodeFromEventType( EN_EVENT_TYPE type ) const;
		std::string   GetCodeFromGroupType( EN_GROUP_TYPE type ) const;

		TimeSlot GetTimeSlot( size_t idx ) const;
		WeekDay  GetWeekday(  size_t idx ) const;

		size_t GetNbDays()      const { return _v_weekdays.size(); }
		size_t GetNbTimeSlots() const { return _v_timeslots.size(); }

		uchar  GetTimeSlotIndexFromCode( const std::string& slot_code ) const;
		size_t GetDayIndexFromCode( const std::string& day_code ) const;
		std::string GetLinkOnWeekSchedule( int week_no, int div_idx, EN_LINK_TEXT lt, std::string=std::string() ) const;

		void CommandLineAnalysis( int argc, const char** argv );
		void PrintToHtml( FileHTML& f ) const;

	private:
		bool InputFieldsCheck();
};

//-----------------------------------------------------------------------------------
inline
TimeSlot Params::GetTimeSlot( size_t idx ) const
{
	assert( idx<GetNbTimeSlots() );
	return _v_timeslots[idx];
}
inline
WeekDay Params::GetWeekday( size_t idx ) const
{
	assert( idx<GetNbDays() );
	return _v_weekdays[idx];
}

//-----------------------------------------------------------------------------------

#endif // HG_PARAMS_H

