/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// cal_data.h

#ifndef HG_CAL_DATA_H
#define HG_CAL_DATA_H

#include <string>

void GetDateFromWeekNumber( int year, int week, int dayOfWeek, time_t& current );

//-----------------------------------------------------------------------------------
struct CalData
{
	size_t _current_year;
	int    _zoulou_offset;  ///< when ical files are saved with Zulu time, this is the offset between Zulu and local

	std::string _now;           ///< timestamp
	std::string _now_rfc_ical;  ///< timestamp for ical files

	public:
		CalData() : _zoulou_offset(2) {}

		void SetCurrentDate();
		std::string GetNow() const { return _now; }
		std::string GetNow_ical() const { return _now_rfc_ical; }
};
//-----------------------------------------------------------------------------------
#endif // HG_CAL_DATA_H
