/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// week.h

#ifndef HG_WEEK_H
#define HG_WEEK_H

#include "day.h"
#include "conflict.h"

class UserData;

//-----------------------------------------------------------------------------------
struct WeekEvents
{
	private:
		int  _num;                           ///< N° of week, as defined by https://en.wikipedia.org/wiki/ISO_week_date
		std::vector<DayEvents>      _v_days;
		std::vector<Conflict> _v_wconflicts; ///< set of conflicts for this week
		std::vector<std::vector<size_t>>   _vv_nbevents;   ///< holds the number (per div) of events of a given subject on that week (indexes refer to UserData::_v_subjects)

		mutable const UserData* p_user_data = 0;

/// used to remember when a particular group is used by an event, when iterating over time slots
/** mutable, because it is not directly related to contained data, and it is used in a \c const function */
		mutable std::vector<size_t> _v_columns_memory;

	public:
		bool _yearLeap;                ///< true to tell that we switched from december to january, so this week needs to increment the year

		static Params* sp_params_w;  ///< pointer on parameters

	public:
		WeekEvents();
		int    GetWeekNum() const { return _num; }
		size_t GetNbEventsPerSubjectPerDiv( int div_idx, size_t sub_idx ) const;
		void   SetWeekNumber( size_t n ) { _num=n; }
		bool   AddEventToWeek( Event& );
		const DayEvents& GetDay( size_t idx ) const { return _v_days.at(idx); }
		const std::vector<DayEvents>& GetDays() const { return _v_days; }

		size_t GetNbSpecialEvents() const;

		void DumpWeek( std::ostream& ) const;

		size_t GetNbConflicts() const { return _v_wconflicts.size(); }
		void   PrintWeekConflicts( FileHTML&, const UserData& data ) const;

		const Event* GetEventFromId_w( size_t Id ) const;

		Triplet GetVolumeT_w2( EN_INFOTYPE, size_t idx ) const;
		Triplet GetVolumeT_w1( size_t ag_idx ) const;

		Triplet GetVolume2Cond_w( EN_INFOTYPE, size_t idx, size_t ag_idx ) const;

		Triplet GetVolPerDiv_w( int div_idx, EN_INFOTYPE, size_t idx ) const;

		void GetEventList_w5( EN_INFOTYPE, size_t idx, size_t group_idx, EN_GROUP_TYPE gt, std::vector<const Event*>& v_out ) const;
		void GetEventList_w3( EN_INFOTYPE, size_t idx, std::vector<const Event*>& v_out ) const;

		size_t  GetNbEmptySlots_w( size_t ag_idx ) const;
		size_t  GetNbDays_w( size_t ag_idx ) const;
		size_t  GetNbDays_w2( EN_INFOTYPE it, size_t idx ) const;

		size_t  GetNbDaysOff() const;

		void PrintSummary(  FileHTML&, const UserData&, int div_idx ) const;
		void PrintCalendar( FileHTML&, const UserData&, bool ColorPerSubject, int div_idx=-1 ) const;

		std::string GetConflictString( const Event& ) const;

		void AllocateForSubjects( size_t nb_div, size_t nb_sub );

	private:
		void GenerateTableHeader( FileHTML&, int div_idx=-1 ) const;
		std::vector<size_t> P_GetIndexList( EN_INFOTYPE ) const;
		int  P_ComputeCell( FileHTML&, const Event* event, size_t idx_col, bool cps, size_t gr, const UserData& ) const;
		void PrintSummary_header_1( FileHTML&, const UserData&, const std::vector<size_t>& ) const;
		void PrintSummary_header_2( FileHTML&, size_t nb_sub ) const;
};

//-----------------------------------------------------------------------------------
inline
void WeekEvents::AllocateForSubjects( size_t nb_div, size_t nb_sub )
{
	assert( nb_sub>0 );
	if( 0 == nb_div )
		nb_div++;
	_vv_nbevents.resize( nb_div );
	for( auto& v: _vv_nbevents )
		v.resize( nb_sub, 0 );
}

//-----------------------------------------------------------------------------------

#endif // WEEK_H
