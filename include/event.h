/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// event.h

#ifndef HG_EVENT_H
#define HG_EVENT_H

#include "triplet.h"

class FileICAL;
class GroupSets;
class FileHTML;
class UserData;
class Params;

//-----------------------------------------------------------------------------------
class Event
{
	friend std::ostream& operator << ( std::ostream& stream, const Event& e );

#ifdef TESTMODE
	friend class GroupSets;
#endif

////////////////////
// Static data
////////////////////
	private:
		static size_t s_IdCounter;
		static const GroupSets* sp_st_groups;

	public:
		static Params* sp_params_e;  ///< pointer on parameters

////////////////////
// Regular data
////////////////////
	private:
		size_t        _eventId; ///< unique identifier, created through s_IdCounter
		EN_EVENT_TYPE _eventType;
		EN_GROUP_TYPE _groupType;
		int           _groupIdx;  ///< Index of group, -1 if no group (special event)

		std::string   _comment;  ///< optional comment, shown as title attribute in html output

		size_t _subject_idx;     ///< index on subject listed in UserData::_v_subjects, 0 for "unknown".
		size_t _instructor_idx;  ///< index on subject listed in UserData::_v_instructors, 0 for "unknown".
		size_t _room_idx;        ///< index on subject listed in UserData::_v_rooms, 0 for "unknown".
		int    _week_no;
		uchar  _day_idx;
		uchar  _timeslot_idx;
		uchar  _duration;
		int    _div_idx; ///< index of the division involved, used for building worktime per divisions. -1 if not applyable

		int    _evConflictId; ///< if event is conflicted with another, this will hold the conflict ID, -1 if no conflict
		EN_CONFLICT_TYPE _conflictType;

#ifdef EXPERIMENTAL_SESSION_ORDER
		size_t _orderIndex = 0;
#endif

////////////////////
// Interface
////////////////////
	public:                     // temp
		bool IsConflicted() const
		{
			return _evConflictId != -1 || _conflictType != CT_DUMMY;
		}

		EN_CONFLICT_TYPE GetConflictType() const
		{
			return _conflictType;
		}
		Event( const LineContent& input_line );

#ifdef TESTMODE
		Event() {};
#endif

		bool operator == ( const Event& e ) const;
		bool operator < ( const Event& e ) const;

		int     FindGroupSize() const;
		Triplet GetEventVolume() const;

		bool HoldsGroup( EN_GROUP_TYPE gt, size_t group_idx ) const;

		void Print( std::ofstream& fout, EventPrintDetails ) const;
		void PrintInCell( std::ofstream&, const UserData& ) const;
		void PrintAsHtmlLine( FileHTML&, const UserData& ) const;
		size_t             GetId()        const { return _eventId; }
		EN_GROUP_TYPE      GetGroupType() const { return _groupType; }
		EN_EVENT_TYPE      GetEventType() const { return _eventType; }
		int                GetIndex( EN_INFOTYPE it ) const;
		bool               HasComment() const { return _comment.size() != 0; }
		std::string        GetComment() const { return _comment; }
		void               SetConflict( size_t conf_id ) { _evConflictId = conf_id; }
		void               PrintAsIcal( FileICAL&, EN_CALENDAR_TYPE agt, const UserData& ) const;

		bool IsSpecialEvent() const
		{
			if( _eventType == ET_DAY_OFF )
				return true;
			if( _eventType == ET_SPECIAL )
				return true;
			return false;
		}
		static void AssignGroups( const GroupSets& g )
		{
			sp_st_groups = &g;
		}
#ifdef TESTMODE
	KUT_CLASS_DECLARE
#endif
};
//-----------------------------------------------------------------------------------

#endif // EVENT_H
