/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// triplet.h

#ifndef HG_TRIPLET_H
#define HG_TRIPLET_H

#include <ostream>

#include "datatypes.h"

// Forward declaration
class FileHTML;

//-----------------------------------------------------------------------------------
/// Holds a set of 3 durations, one for each type of course/group (GT_DIV/GT_CLASS/GT_ATOMIC) , plus a fourth value for special events
class Triplet
{
	friend std::ostream& operator << ( std::ostream& stream, const Triplet& t );
	public:
		Triplet() : d_atomic(0.0f), d_class(0.0f), d_div(0.0f), d_spe(0.0f) {}
		Triplet( float cm, float td, float tp ) : d_atomic(tp), d_class(td), d_div(cm), d_spe(0.0f) {}

		Triplet( EN_GROUP_TYPE gt, float value ) : Triplet()
		{
			Add( gt, value );
		}

		float Sum() const { return d_atomic+d_class+d_div+d_spe; }
		float Sum_wt() const { return d_atomic+d_class+d_div; }
		int GetNbValues() const;
		void printIfNotNull( FileHTML&, EN_GROUP_TYPE, bool doPrintRatio, const Triplet* t=0 ) const;

/// Adds \c value to \c gt part of the triplet
		void Add( EN_GROUP_TYPE gt, float value )
		{
			switch( gt )
			{
				case GT_DIV:    d_div    += value; break;
				case GT_CLASS:  d_class  += value; break;
				case GT_ATOMIC: d_atomic += value; break;
				case GT_NONE:   d_spe    += value; break;
				default: assert(0);
			}
		}
		float Get( EN_GROUP_TYPE infotype ) const
		{
			switch( infotype )
			{
				case GT_DIV:    return d_div;    break;
				case GT_CLASS:  return d_class;  break;
				case GT_ATOMIC: return d_atomic; break;
				case GT_NONE:   return d_spe;    break;
				default: assert(0);
			}
		}

		Triplet& operator += ( const Triplet& t )
		{
			d_atomic += t.d_atomic;
			d_class  += t.d_class;
			d_div    += t.d_div;
			d_spe    += t.d_spe;
			return *this;
		}

		Triplet operator + ( const Triplet& other ) const
		{
			Triplet result( *this );
			result += other;
			return result;
		}

	private:
		float d_atomic, d_class, d_div, d_spe;
};

//-----------------------------------------------------------------------------------
inline std::ostream& operator << ( std::ostream& stream, const Triplet& t )
{
	stream << '(' << t.d_div << ',' << t.d_class << ',' << t.d_atomic << ":" << t.d_spe << ')';
	return stream;
}
//-----------------------------------------------------------------------------------
inline
int Triplet::GetNbValues() const
{
	int c = 0;
	if( Get(GT_DIV) > 0.0f )
		c++;
	if( Get(GT_CLASS) > 0.0f )
		c++;
	if( Get(GT_ATOMIC) > 0.0f )
		c++;
	if( Get(GT_NONE) > 0.0f )
		c++;

	return c;
}

//-----------------------------------------------------------------------------------
#endif
