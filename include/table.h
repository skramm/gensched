/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// table.h

#ifndef HG_TABLE_H
#define HG_TABLE_H

#include "triplet.h"

class FileHTML;
class UserData;
class GroupSets;
class Params;

//-----------------------------------------------------------------------------------
/// Container for a table (2-dim vector) holding objects of type Triplet, used only in Summary
class TripletTable
{
	private:
		std::vector<std::vector<Triplet>> _vv_table;
	public:
		static const GroupSets* sp_groups; ///< static pointer on groups, to avoid using the global. Initialized by Summary constructor

	public:
		void Dump( std::ostream& f ) const;
		size_t Rows() const { return _vv_table.size(); }
		const std::vector<Triplet>& GetRow( size_t j ) const
		{
			assert( j<Rows() );
			return _vv_table[j];
		}
		const Triplet& GetCell( size_t i, size_t j ) const
		{
			assert( i<Rows() );
			assert( j<_vv_table[i].size() );
			return _vv_table[i].at(j);
		}

		void Alloc( size_t rows, size_t cols )
		{
			assert( rows>0 && cols>0 );
			_vv_table.resize( rows );
			for( auto& line: _vv_table )
				line.resize( cols );
		}
		void Add2Cell( size_t row, size_t col, const Triplet& t )
		{
			assert( row < _vv_table.size() );
			assert( col < _vv_table[row].size() );
			_vv_table[row].at(col) += t;
		}
		void Print2Html( FileHTML&, EN_INFOTYPE col_data, const StringVector*, const StringVector*, const UserData&, const Params& ) const;

	private:
		void PrintRow( FileHTML&, const std::vector<Triplet>& vt, std::vector<Triplet>& col_sum, const std::vector<bool>& columnHasSpecialEvent, bool PrintFirstColumnbool, bool PrintSpecialEventsSummary ) const;
//		void PrintColumnGroupHeader( FileHTML&, bool specialEventColumn=false ) const;
		void PrintColumnGroupFooter( FileHTML&, const Triplet& tri, bool HasSpecialEvents ) const;
		std::vector<bool> GetColumnsWithSE( size_t nbRows, const StringVector* p_col ) const;

};
//-----------------------------------------------------------------------------------

#endif // HG_TABLE_H
