/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// htmlfile.h

#ifndef HG_HTMLFILE_H
#define HG_HTMLFILE_H

#include <fstream>
#include <string>
#include <cassert>

//-----------------------------------------------------------------------------------
/// A free function that takes a string and returns it with:
/**
- all spaces replaces by underscores
- all dots removed

Example
- input: "abc d.e f.g"
- output:"abc_de_fg"
*/
inline
std::string ReplaceSpaces( const std::string& in )
{
	std::string out;
	for( const auto& c: in )
	{
		if( c == 32 )
			out.push_back( '_' );
		else
			if( c != '.' )
				out.push_back( c );
	}
	return out;
}
//-----------------------------------------------------------------------------------
enum EN_AppendMode
{ FAP_NO_APPEND, FAP_APPEND };
//-----------------------------------------------------------------------------------
/// Root class output file wrapper (virtual), holds the stream and filename
class OutputFile
{
	public:
		std::ofstream fout;
		OutputFile( std::string n )
		{
			SetFileName(n);
		}
		void SetFileName( const std::string& n )
		{
			assert( !fout.is_open() );
			_filename = ReplaceSpaces(n);
		}
		bool isOpen() const
		{
			return fout.is_open();
		}
		const std::string& GetFileName() const { return _filename; }
		void Open( EN_AppendMode=FAP_NO_APPEND );
		void Close();
		void PrintInfo( std::ostream& f ) const;

		static std::string s_OutputFolder;

	protected:
		std::string _filename;   ///< file name
		std::string _fullname;   ///< full file name (with path added)
	protected:
		virtual void ComputeFullName() = 0;
};

//-----------------------------------------------------------------------------------
/// Ical file
class FileICAL : public OutputFile
{
	public:
		FileICAL( std::string n ) : OutputFile( n ) {}
		static const std::string CRLF;

	protected:
		void ComputeFullName();
};
//-----------------------------------------------------------------------------------
/// Csv file
class FileCSV : public OutputFile
{
	public:
		FileCSV( std::string n ) : OutputFile( n ) {}

	protected:
		void ComputeFullName();
};

//-----------------------------------------------------------------------------------
/// Html file
class FileHTML : public OutputFile
{
	public:
		FileHTML( const std::string& n=std::string(), const std::string& title=std::string() );
		void SetTitle( std::string n );
		std::string GetTitle() const { return _title_page; }
		void SetStyleSheet( std::string st ) { _stylesheet=st; }
		void AssignScript(std::string s ) { _scriptfile=s; }
		void Open();
		void Close();

		static void AssignProjectName( std::string pn );
		static std::string GetProjectName() { return s_project_name; }

	protected:
		void ComputeFullName();

	private:
		static std::string s_project_name;

		std::string _title_html;  ///< html title (whats inside <title></title> ): "project: page-title"
		std::string _title_page;  ///< title printed on top of page  (whats inside <h1></h1> )
		std::string _stylesheet;  ///< style sheet to add
		std::string _scriptfile;  ///< javascript file to add

};
//-----------------------------------------------------------------------------------

#endif // HG_HTMLFILE_H
