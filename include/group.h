/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// group.h

#ifndef HG_GROUP_H
#define HG_GROUP_H

#include <ostream>
#include "datatypes.h"

class GroupSets;

//-----------------------------------------------------------------------------------
/// An abstract student group, holds only a name
class StudentGroup
{
	friend class GroupSets; // so it can initialise the static pointer

	protected:
		std::string _name;
		static GroupSets* sp_groups;

	protected:
		StudentGroup( const std::string& n ) : _name(n) { }

	public:
		std::string GetName() const { return _name; }
		void PrintName( std::ostream& f, const std::string& type ) const;

// pure virtual functions
		virtual void          Dump( std::ostream& f )                          const = 0;
		virtual EN_GROUP_TYPE GetType()                                        const = 0;
		virtual bool          Overlaps( const StudentGroup& g2 )               const = 0;
		virtual size_t        GetNbAtomicGroups()                              const = 0;
		virtual bool          IncludesAtomicGroup_a( size_t at_group_idx )     const = 0;
		virtual const StudentGroup&  GetFirstAtGroup()                         const = 0;
		virtual size_t        GetNbSubGroups()	                               const = 0;

// virtual functions used only with inherited classes
		virtual const StudentGroup&  GetSubGroup( size_t )    const { assert(0); }
		virtual size_t        GetSubGroupIdx( size_t ) const { assert(0); }
		virtual size_t        GetAtomicIdx( size_t )   const { assert(0); }

		bool operator == ( const StudentGroup& other ) const { return other._name == _name; }
		bool operator != ( const StudentGroup& other ) const { return other._name != _name; }
};

//-----------------------------------------------------------------------------------
/// A "lab" student group
class AtomicGroup : public StudentGroup
{
	public:
		AtomicGroup( const std::string& n ) : StudentGroup( n ) {}
		EN_GROUP_TYPE GetType() const { return GT_ATOMIC; }
		bool operator == ( const AtomicGroup& other ) const { return StudentGroup::operator ==(other); }
		void Dump( std::ostream& f ) const;

		bool   Overlaps( const StudentGroup& g2 ) const;
		size_t GetNbAtomicGroups() const { return 1; }
		size_t GetAtomicIdx() const;
		bool   IncludesAtomicGroup_a( size_t at_group_idx ) const;
		const StudentGroup& GetFirstAtGroup() const;
		size_t GetNbSubGroups()	const { return 0; }
};

//-----------------------------------------------------------------------------------
/// A "Group With Sub Groups". It holds indexes on contained atomic groups
class GroupWSG : public StudentGroup
{
	protected:
		std::vector<size_t> _v_idx_sg;
		GroupWSG( const std::string& n ) : StudentGroup( n ) {}

	public:
		size_t           GetNbSubGroups() const { return _v_idx_sg.size(); }
		const StudentGroup&     GetSubGroup( size_t i ) const;
		size_t           GetSubGroupIdx( size_t i ) const { assert( i<GetNbSubGroups() ); return _v_idx_sg[i]; }
		void             AddSubGroup( const std::string& s );
		bool             Overlaps( const StudentGroup& g2 ) const;

//	private:
		void GetAtomicGroups( std::vector<size_t>& v_at_idx ) const;
};

//-----------------------------------------------------------------------------------
/// A regular class group, holds a set of atomic groups
class ClassGroup : public GroupWSG
{
	public:
		ClassGroup( const std::string& n ) : GroupWSG( n ) {}
		virtual EN_GROUP_TYPE GetType() const { return GT_CLASS; }
		bool operator == ( const ClassGroup& other ) const { return StudentGroup::operator ==(other); }
		void Dump( std::ostream& f ) const;

		size_t GetNbAtomicGroups() const { return GetNbSubGroups(); }
		bool   IncludesAtomicGroup_a( size_t at_group_idx ) const;
		const StudentGroup& GetFirstAtGroup() const;
};

//-----------------------------------------------------------------------------------
/// A division group, holds a name and a set of included groups
class DivGroup : public GroupWSG
{
	public:
		DivGroup( const std::string& n ) : GroupWSG( n ) {}
		EN_GROUP_TYPE GetType() const { return GT_DIV; }
		bool operator == ( const DivGroup& other ) const { return StudentGroup::operator ==(other); }
		void Dump( std::ostream& f ) const;

		size_t GetNbAtomicGroups() const;
		bool   IncludesAtomicGroup_a( size_t at_group_idx ) const;
		const StudentGroup& GetFirstAtGroup() const;

		size_t GetAtomicIdx( size_t ) const;
		bool   IncludesGroup( EN_GROUP_TYPE gt, size_t idx ) const;
};

//-----------------------------------------------------------------------------------

#endif

