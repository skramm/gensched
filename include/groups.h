/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// groups.h

#ifndef HG_GROUPS_H
#define HG_GROUPS_H

#include "group.h"

#include <iostream>

#include <boost/property_tree/ptree.hpp>

class Event;


//-----------------------------------------------------------------------------------
/// Holds the different groups sets. Instanciated globally in g_groups
class GroupSets
{
	private:
		std::vector<AtomicGroup> _v_groups_at;      ///< atomic groups (EN_GROUP_TYPE=GT_ATOMIC)
		std::vector<ClassGroup>  _v_groups_class;   ///< class group
		std::vector<DivGroup>    _v_groups_div;     ///< division groups

	public:
		GroupSets();
		bool ReadFromFile( const std::string& fname );
		const StudentGroup&     GetGroup( EN_GROUP_TYPE gt, size_t idx ) const;

		int GetGroupIndex( EN_GROUP_TYPE gt, const std::string& g ) const;

		void AddGroup( const StudentGroup& gr );
		void CreateAtomicGroups( const StringVector& v_atomic_groups );

		uchar  GetNbColumns() const;
		size_t GetNbGroups( EN_GROUP_TYPE gt, int div_idx=-1 ) const;
		int    GetDivisionIndex( EN_GROUP_TYPE gt, size_t group_idx ) const;

		int    GetGroupIndexFromString( std::string group, EN_EVENT_TYPE, EN_GROUP_TYPE& ) const;
		bool   GroupAndGroupTypeOK( const Event& e ) const;
		void   Dump( std::ostream& f ) const;
		void   Clear();
		bool HasClassGroups() const;
		bool HasDivisions() const;

	private:
		void FetchGroups( const boost::property_tree::ptree& pt, std::string file_section, EN_GROUP_TYPE type );

#ifdef TESTMODE
	KUT_CLASS_DECLARE
#endif
};

//-----------------------------------------------------------------------------------
/// Does the search in the given vector of groups, return -1 if not found
template<typename T>
int FindInGroups( const std::vector<T>& v_gr, const std::string& group )
{
	const auto& f = std::find( v_gr.begin(), v_gr.end(), group );

	if( f == v_gr.end() )
	{
		std::cerr << "Warning: unable to find group: " << group << ENDL;
//		CERR << "Warning: invalid group string and group type: group=" << group << " gt=" << gt << ENDL;
		return -1;
	}

	return (int)std::distance( v_gr.begin(), f );
}

//-----------------------------------------------------------------------------------
inline
void GroupSets::Clear()
{
	_v_groups_at.clear();
	_v_groups_class.clear();
	_v_groups_div.clear();
}

//-----------------------------------------------------------------------------------
inline
bool GroupSets::HasClassGroups() const
{
	return _v_groups_class.size();
}

//-----------------------------------------------------------------------------------
inline
bool GroupSets::HasDivisions() const
{
	return _v_groups_div.size() > 1;
}

//-----------------------------------------------------------------------------------
inline
uchar GroupSets::GetNbColumns() const
{
	uchar NbColsPerSubject=1;
	if( HasClassGroups() )
	{
		NbColsPerSubject++;                 // one for the class groups, one for the "sum" column
		if( HasDivisions() )
			NbColsPerSubject++;
	}
	return NbColsPerSubject;
}

//-----------------------------------------------------------------------------------

#endif // GROUPS_H
