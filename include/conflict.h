/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// conflict.h

#ifndef HG_CONFLICT_H
#define HG_CONFLICT_H

#include "datatypes.h"

class FileHTML;
class UserData;
class Event;

//-----------------------------------------------------------------------------------
/// Holds a reference (Id) on two events that are in conflict one with the other
/**
For example, an instructor that is with two student groups at the same time slot.
*/
class Conflict
{
	friend std::ostream& operator << ( std::ostream& stream, const Conflict& c );

	private:
		static size_t s_ConfIdCounter;  ///< static counter, used to generate a unique conflict Id

		size_t _conflictId;
		int _event1_id         = -1;
		int _event2_id         = -1;
		EN_CONFLICT_TYPE _type = CT_DUMMY;

	public:
		Conflict(): _conflictId(0), _event1_id(-1),_event2_id(-1)
		{
		}
		Conflict( EN_CONFLICT_TYPE t, Event& e1, Event& e2 );
		Conflict( EN_CONFLICT_TYPE t, Event& e1 );

		const char* GetTypeString() const;
		EN_CONFLICT_TYPE GetType() const { return _type; }

		void PrintHtml( FileHTML& f, const UserData& data ) const;
		bool UsesEventId( size_t id1 ) const;
		size_t GetOtherEventId( size_t ) const;
};
//-----------------------------------------------------------------------------------


#endif // CONFLICT_H
