/**
\file html_table.h
\brief this is unused at present and only preliminar. See HTMLTABLE
*/

#include <vector>
#include "htmlfile.h"

//-----------------------------------------------------------------------------------
template<typename T>
class DiagonalTable
{
	private:
		std::vector<T> _v_diag_cell;
	public:
		DiagonalTable( size_t nb )
		{
			_v_diag_cell.resize( nb );
		}
};
//-----------------------------------------------------------------------------------
enum EN_HtmlTableType { HTT_Type1, HTT_Type2 };
struct HTMLTABLE_CONFIG
{
		EN_HtmlTableType htt = HTT_Type1;

		bool hasHeader_1       = false;   ///< table has first line header
		bool hasHeader_2       = false;   ///< table has second line header (with possibly sub-columns)
		bool hasFooter         = false;   ///< table has footer
		bool hasHeaderColumn   = false;
		bool hasFinalColumn    = false;
		int  finalColumnNbCols = -1;
};
//-----------------------------------------------------------------------------------
/// Preliminar: attempt to generalize html tables so we can abstract the final html rendering
/**
T is the type of "inner" cells.
Allowed types for \c T:
- std::tuple<Q>

Type 1:
\verbatim
               head   head                              final
               col1   col2     col[0]      col[1]        col
              +-----+------+-----------+-----------+-----------+
header1 line  |     |      |           |           |           |
              +-----+------+---+---+---+---+---+---+---+---+---+
header2 line  |     |      | X | Y | Z | X | Y | Z | A | B | C |
              +-----+------+---+---+---+---+---+---+-----------+
line 0        |     |      | 1 | 21| 5 | 6 | 5 | 0 | 4 | 4 | 2 |
              +-----+------+---+---+---+---+---+---+---+---+---+
line 1        |     |      |   |   |   |   |   |   |   |   |   |
              +-----+------+---+---+---+---+---+---+---+---+---+
footer        |     |      |   |   |   |   |   |   |   |   |   |
              +-----+------+---+---+---+---+---+---+---+---+---+
\endverbatim

Type 2:
\verbatim
               head   head                              final
               col1   col2     col[0]      col[1]        col
              +-----+-------+-----------+-----------+-----------+
header1 line  |     |       |   Howdy   |  John     |  Final    |
              +-----+-------+---+---+---+---+---+---+---+---+---+
header2 line  |     |       | X | Y | Z | X | Y | Z | A | B | C |
              +-----+-------+---+---+---+---+---+---+-----------+
              |     | A     | 1 |   |   | 4 |   |   | 5 |   |   |
line 0        |     |   B   |   | 4 |   |   | 5 |   |   | 9 |   |
              |     |     C |   |   | 2 |   |   | 6 |   |   | 8 |
              +-----+-------+---+---+---+---+---+---+---+---+---+
line 1        |     |       |   |   |   |   |   |   |   |   |   |
              +-----+-------+---+---+---+---+---+---+---+---+---+
footer        |     |       |   |   |   |   |   |   |   |   |   |
              +-----+-------+---+---+---+---+---+---+---+---+---+

\endverbatim
*/
template<typename T1,typename T2>
class HTMLTABLE
{
	private:
		HTMLTABLE_CONFIG _config;
		std::vector<std::vector<T1>> _vv_cell;             ///< cells
		std::vector<std::string>     _v_FirstHeaderLine;   ///< first header line
		std::vector<std::array<std::string,>     _v_SecondHeaderLine;  ///< first header line

		std::vector<T2>              _v_finalColumn;       ///< final column
	public:
		HTMLTABLE( HTMLTABLE_CONFIG config, size_t nbrows, size_t nbcols ) : _config(config)
		{
			assert( nbrows > 0 && nbcols > 0 );

			_vv_cell.resize( nbrows );
			for( auto& row: _vv_cell )
				row.resize( nbcols );

			if( _config.hasHeader_1 )
				_v_FirstHeaderLine.resize( nbcols );

			Alloc( nbrows, nbcols );
//			 + _config.hasHeader_1 + _config.hasHeader_2,
//				nbcols + _config.hasFinalColumn + _config.hasHeaderColumn

			if( _config.hasFinalColumn )
				_v_finalColumn.resize( _config.finalColumnNbCols );
		}
		void RenderTable( HTMLFILE& f ) const;
		void FillHeaderCell( size_t col, std::string value )
		{
			size_t NbCols = _vv_cell[0].size();
			assert( col > _config.hasHeaderColumn?1:0 );
			assert( col < NbCols+_config.hasFinalColumn?1:0 );

		}
		void FillCellWith( size_t row, size_t col, const T& value )
		{
		}
};
//-----------------------------------------------------------------------------------
template<typename T>
void
HTMLTABLE<T>::RenderTable( HTMLFILE& f ) const
{
	f.fout << "<table>\n";
	if( _config.hasHeader_1 )
	{
		f.fout << "<tr>\n";
		f.fout << "</tr>\n";
	}
	if( _config.hasHeader_2 )
	{
		f.fout << "<tr>\n";
		f.fout << "</tr>\n";
	}
	f.fout << "</table>\n";
}
//-----------------------------------------------------------------------------------

