/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HG_SUMMARY_H
#define HG_SUMMARY_H

#include "table.h"

class Event;
class GroupSets;

//-----------------------------------------------------------------------------------
/// Holds "summary" information as tables. Used to generate the "table" page
class Summary
{
	public:
		Summary( const UserData&, const GroupSets&, const Params& );
		void PrintPage_Tables( const UserData& data ) const;
		void PrintTables( FileHTML& f, EN_INFOTYPE row_data, EN_INFOTYPE col_data, const UserData& data ) const;
		void PrintPage_InstrWorktime( const UserData& data ) const;
		void AddEvent( const Event& e );
		void Dump( std::ostream& f ) const;

	private:
		void PrintLinks( FileHTML& f, const std::string& tabletype ) const;
		void PrintWTHeader( FileHTML& f ) const;

	private:
		std::vector<TripletTable> _v_table_is;   ///< table of durations instructors/subjects, one per division
		std::vector<TripletTable> _v_table_ir;   ///< table of durations instructors/rooms, one per division
		std::vector<TripletTable> _v_table_sr;   ///< table of durations subjects/rooms, one per division
		TripletTable _sum_table_is;              ///< table of sums of durations instructors/subjects, for all divisions
		TripletTable _sum_table_ir;              ///< table of sums of durations instructors/rooms, for all divisions
		TripletTable _sum_table_sr;              ///< table of sums of durations subjects/rooms, for all divisions

		const StringVector& _rv_teachers;        ///< reference on source data (instructors)
		const StringVector& _rv_subjects;        ///< reference on source data (subjects)
		const StringVector& _rv_rooms;           ///< reference on source data (rooms)
		const GroupSets&    _r_groups;           ///< reference on source data (groups)
		const Params&       _rParams;            ///< reference on source data (parameters)
};
//-----------------------------------------------------------------------------------

#endif // SUMMARY_H
