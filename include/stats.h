/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// stats.h

#ifndef HG_STATS_H
#define HG_STATS_H

#include <map>
#include <iostream>

#include "event.h"

class UserData;
class Params;

//-----------------------------------------------------------------------------------
/// A type holding invalid events:
/**
Fields:
 -# input data file line number
 -# the vector of strings holding the input fields value
 -# the related error message.
*/
typedef std::tuple< size_t, std::vector< std::string >, std::string > InvalidEvent;


//-----------------------------------------------------------------------------------
/// Holds some statistics after processing data
struct Stats
{
	size_t NbEvents_valid    = 0;
	size_t NbMissingInfo     = 0;
	size_t NbIgnoredEvents   = 0;
	size_t NbIgnoredWeeks    = 0;

	std::vector< std::tuple<Triplet,size_t,size_t> > TotalPerGroup; ///< one element per group, holds volume, nb of empty slots, nb of days

	private:
		std::vector<size_t>               _v_conflicts;
		std::map<EN_MISSING_INFO,size_t>  _m_MissingInfo;
		std::vector<InvalidEvent>         _v_invalid; ///< vector holding invalid events, expressed as a tuple of (line_number,vector of strings, describing text)
		std::vector<Event>                _v_dupe;            ///< vector holding duplicate events
		std::vector<Event>                _v_commented;       ///< vector holding events that hold a comment
		std::vector<Event>                _v_special;         ///< vector holding events that hold a comment
		std::vector<int>                  _v_IgnoredWeeks;    ///< vector holding the weeks numbers that were ignored in input file (due to request of user through otpion -w)

	public:
		Stats();
		size_t GetTotNbConflicts() const;
		size_t GetNbEvents( EN_EVENT_CAT cat ) const;

		void AddEvent2Stats( const Event&, EN_EVENT_CAT );

		void AddMissingInformation( EN_MISSING_INFO mi )
		{
			NbMissingInfo++;
			_m_MissingInfo.at( mi )++;
		}

		void AddConflict( EN_CONFLICT_TYPE ct )
		{
			_v_conflicts.at(ct)++;
		}
		void PrintStatsHtml( const UserData&, const Params&, const GroupSets& ) const;

		void AddInvalidEvent( const LineContent& line, const char* msg );
		void AddIgnoredEvent( const Event& );

		void AddMetaInfo( Triplet sum, size_t tot_nb_es, size_t tot_nb_days )
		{
			TotalPerGroup.push_back( std::make_tuple( sum, tot_nb_es, tot_nb_days ) );
		}
		void AddCommentedEvent( const Event& e );
};

//-----------------------------------------------------------------------------------

#endif // HG_STATS_H
