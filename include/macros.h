/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// macros.h

#ifndef HG_MACROS_H
#define HG_MACROS_H

#include "error.h"

#define COUT \
	if( gp_params->VerboseMode ) \
		std::cout

/*#define ERROR( err_msg, err_value ) \
	Error( __FUNCTION__, __FILE__, __LINE__, err_msg, err_value )
*/

//----------------------------------------------------
/// this is due to some bug in mingw 4.8.1, not recognizing std::stoi
/** see http://stackoverflow.com/questions/8542221/ and http://stackoverflow.com/questions/16132176/
*/

#ifdef _WIN32
	#define STRING2INT(a)   std::atoi( a.c_str() )
	#define STRING2FLOAT(a) std::atof( a.c_str() )
	#define BUF_SIZE 256
	#define TO_STRING_F( in, out ) \
		{ \
			char buf[BUF_SIZE]; \
			std::sprintf( buf, "%f", in ); \
			out = buf; \
		}
	#define TO_STRING_I( in, out ) \
		{ \
			char buf[BUF_SIZE]; \
			std::sprintf( buf, "%d", in ); \
			out = buf; \
		}
#else
	#define STRING2INT(a)   std::stoi(a)
	#define STRING2FLOAT(a) std::stof(a)

/// here, we DON'T use <tt>out = std::to_string( in )</tt> because you can't fix precision...
/// Needs <sstream> in the user file
	#define TO_STRING_F( in, out ) \
	{ \
		std::ostringstream oss; \
		oss << in; \
		out = oss.str(); \
	}
	#define TO_STRING_I( in, out )  out = std::to_string( (int)in )
#endif


//-----------------------------------------------------------------------------------
/// a macro that tries atoi/stoi and generates a proper error signal in case of failure
/**
args:
-# : the string holding the numerical value
-# : the integer variable that will hold the value
-# : an associated text the will be printed in case of failure
*/
#define TRY_STRING2INT( a, b, txt ) \
	try \
	{ \
		b = STRING2INT( a ); \
	} \
	catch( const std::exception& err ) \
	{ \
		std::cerr << "catch error in " << __FUNCTION__ << "() file:" << __FILE__ << " line:" << __LINE__ << ", msg=" << err.what(); \
		std::string msg = std::string("unable to convert string to int, item:-") + txt + std::string("-, value=-") + a + std::string("-"); \
		throw ERROR( msg, a ); \
	}


//-----------------------------------------------------------------------------------
#define TRY_STRING2FLOAT( a, b, txt ) \
	try \
	{ \
		b = STRING2FLOAT( a ); \
	} \
	catch( const std::exception& err ) \
	{ \
		std::cerr << "catch error in " << __FUNCTION__ << "() file:" << __FILE__ << " line:" << __LINE__ << ", msg=" << err.what(); \
		std::string msg = std::string("unable to convert to float, item:-") + txt + std::string("-, value=-") + a + std::string("-"); \
		throw ERROR( msg, a ); \
	}


//-----------------------------------------------------------------------------------
#endif // HG_MACROS_H
