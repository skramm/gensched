/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file helper_functions.h
*/

#ifndef HG_HELPER_FUNCTIONS_H
#define HG_HELPER_FUNCTIONS_H

#include <algorithm> // for std::find()
#include <iostream>

#include "datatypes.h"

struct ColumnFlags
{
	bool printFirstCol = true;
	bool printSumColumn = true;
	bool printSEColumn = true;
};

class Event;
class Triplet;
class FileHTML;
class UserData;
class GroupSets;
class Params;

void DumpTable( const std::vector<LineContent>& in_table );
void PrintLine( const LineContent& in_line );

const Event* GetMatchedEvent( const std::vector<const Event*>& v_event, size_t agroup, EN_GROUP_TYPE etype );

bool ReadCSV( std::string fn, std::vector<LineContent>& table, char delim, size_t min_nb_tokens=0 );

void Tokenize(  const std::string& input, StringVector& v_out, char delim=',', size_t min_nb_tokens=0 );
void Tokenize3( const std::string& input, StringVector& v_out, const char* delims );

void BuildWeekNumVector( const std::string& week_string, std::vector<size_t>& v_num_weeks );

void CreateHelpFile( const UserData&, const GroupSets&, const Params& );
void CreateIndex(    const UserData&, const GroupSets&, const Params& );
bool CreateOutFolders( const Params& );

void BuildWeekNumList( const std::vector<LineContent>& in_table, std::vector<size_t>& v_weeknum, const Params& );

std::string GenerateOutputFileName( EN_OUTPUTFILE_ID, int div_idx=-1, int week_num=-1 );
std::string GenerateOutputFilePath( EN_OUTPUTFILE_ID, int div_idx,    int week_num, const Params& );

std::string GetDate( int week_no, size_t day_idx, size_t year, std::string format="%d/%m/%Y" );
std::string GetTime( std::string, int zulu_offset=0 );
std::string GetDuration( float hour );

Triplet GetVolume( std::vector<const Event*>& );

void PrintErrorMessage( FileHTML& f, int NbConflicts );

bool HasSingleOption( std::string opt, int argc, const char** argv );
bool HasStringOption( std::string opt, int argc, const char** argv, std::string& value );

void PrintSecondHeaderLine( FileHTML& f, const Params&, const std::vector<bool>& PrintSubjectColumn, const std::vector<bool>& columnHasSpecialEvent, ColumnFlags, int rowhw );

// templated functions
//-----------------------------------------------------------------------------------
template<typename T>
void PrintVector( std::ostream& f, const std::vector<T>& vec, const char* msg = 0 )
{
	f << "Print std::vector ";
	if( msg )
		f << msg;
	f << "\n";
	for( const auto& e: vec )
		f << e << "-";
	f << "\n";
}

//-----------------------------------------------------------------------------------
/// Returns true is \c elem was added to \c v, false otherwise
template<typename T>
bool
push_back_ifnotpresent( std::vector<T>& v, const T& elem )
{
	if( std::find( v.cbegin(), v.cend(), elem ) == v.cend() )
	{
		v.push_back( elem );
		return true;
	}
	return false;
}
//-----------------------------------------------------------------------------------
/// Build a vector \c v_diff holding elements that are not in both vectors
template<typename T>
void
Separate( const std::vector<T>& v_1, const std::vector<T>& v_2, std::vector<T>& v_diff )
{
	for( const auto& w: v_1 )
			if( std::find( v_2.cbegin(), v_2.cend(), w ) == v_2.cend() )
				v_diff.push_back( w );

}

//-----------------------------------------------------------------------------------
/// Compares two vector, and gets the elements that are only in one of them, and that are common
/**
Returns 0 if no differences
*/
template<typename T>
int CompareVectors(
	const std::vector<T>& v_1,
	const std::vector<T>& v_2,
	std::vector<T>&       v_only_1,
	std::vector<T>&       v_only_2,
	std::vector<T>&       v_common
)
{
	Separate( v_1, v_2, v_only_1 );
	Separate( v_2, v_1, v_only_2 );

	for( const auto& w: v_1 )
	{
		if( std::find( v_2.begin(), v_2.end(), w ) != v_2.end() )
			v_common.push_back( w );
	}
	return v_only_1.size() + v_only_2.size();
}

//-----------------------------------------------------------------------------------
#endif
