/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file globals.h
\brief Holds external declaration of globals
*/

#ifndef HG_GLOBALS_H
#define HG_GLOBALS_H

#include <string>

class Params;
class Stats;
class CalData;
class UserData;
class GroupSets;
class Strings;
class OutputFiles;

extern Params*       gp_params;
extern Stats         g_stats;
extern CalData       g_caldata;
//extern UserData    g_data;
extern GroupSets     g_groups;
extern Strings       g_strings;
extern OutputFiles   g_files;
//extern INSTRUCTORS g_instr;

extern const std::string g_lic;
extern const std::string g_lio;

extern const std::string g_sum;
extern const std::string g_hour;

extern const std::string g_br;

extern const char g_endl;

#endif
