/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// error.h

#ifndef HG_ERROR_H
#define HG_ERROR_H

#include <exception>
#include <sstream>

/// A macro used to build a \c ErrorClass object but with some added information. Second arg can be a string or an integer value
#define ERROR( err_msg, err_value ) \
	ErrorClass( __FUNCTION__, __FILE__, __LINE__, err_msg, err_value )


//-----------------------------------------------------------------------------------
/// General error class, inherits from std::exception
class ErrorClass: public std::runtime_error
{
	public:
    explicit ErrorClass(
		std::string func,
		std::string file,
		size_t      line,
		std::string msg,
		std::string value
	) throw()
		: std::runtime_error(
			"\n-file:" + file
			+ "\n-func:" + func
			+ "\n-line:" + std::to_string(line)
			+ "\n-error:" + msg
			+ "\n-value:" + value
		)
    {}

    explicit ErrorClass(
		std::string func,
		std::string file,
		size_t      line,
		std::string msg,
		size_t      value
	) throw()
		: std::runtime_error(
			"\n-file:" + file
			+ "\n-func:" + func
			+ "\n-line:" + std::to_string(line)
			+ "\n-error:" + msg
			+ "\n-value:" + std::to_string(value)
		)
    {}

    virtual ~ErrorClass() throw()
    {}

/*private:
    std::string _msg;
    std::string _val_str;
    size_t      _val_int;
//    size_t      _level;
*/
};
//-----------------------------------------------------------------------------------

#endif // HG_ERROR_H
