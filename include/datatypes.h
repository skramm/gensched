/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// datatypes.h

#ifndef HG_DATATYPES_H
#define HG_DATATYPES_H

#include <string>
#include <vector>
#include <cassert>
#include <functional>

//#ifdef DEBUG
//	#include "params.h"
//#endif

#ifdef TESTMODE
	#include <kut.h>
#endif

#ifndef ENDL
	#define ENDL '\n'
#endif

/// Buffer size for reading input CSV data files
#define BUF_SIZE 256


typedef unsigned char uchar;

/// this macro is used in summary.cpp and table.cpp, hence the reason it is defined here
#define PRINT_T_IF_NOT_NULL( div ) \
	if( t.Get(div) > 0.0 ) \
		f.fout << t.Get(div);


//----------------------------------------------------
#ifdef DEBUG

	extern unsigned char GlobalIndent;
	#define DEBUG_INDENT( a ) \
		{ for(int i=0; i<(a); i++ ) fprintf( stderr, "|  "); }

	#define DEBUG_IN \
		DEBUG_INDENT( GlobalIndent ); \
		fprintf( stderr, "IN : %s\n", __PRETTY_FUNCTION__ ); \
		GlobalIndent++;

	#define DEBUG_OUT \
		DEBUG_INDENT( GlobalIndent-1 ); \
		fprintf( stderr, "OUT: %s\n", __PRETTY_FUNCTION__ ); \
		GlobalIndent--;

	#define CERR \
		if( 1 ) std::cerr

	#define CERR_LOG \
		if( 1 ) std::cerr << __PRETTY_FUNCTION__ << ": "

#else
	#define DEBUG_IN ;
	#define DEBUG_OUT ;
	#define CERR \
		if( gp_params->DebugMode ) std::cerr

	#define CERR_LOG \
		if( gp_params->DebugMode ) std::cerr << __PRETTY_FUNCTION__ << ": "

#endif

#define ASSERT_2( a, b, c ) \
	if( false==(a) ) { \
		std::cerr << "* assert triggered:\n"; \
		std::cerr << "  - FILE=" << __FILE__ << ENDL; \
		std::cerr << "  - FUNCTION=" << __FUNCTION__ << "()\n"; \
		std::cerr << "  - LINE=" << __LINE__ << ENDL; \
		std::cerr << "  - expression=" << (a) << ENDL; \
		std::cerr << "  - value 1=" << b << ENDL; \
		std::cerr << "  - value 2=" << c << ENDL; \
		exit(1); \
	}

//-----------------------------------------------------------------------------------
/// used only to print week headers (link) both in schedule pages and in week summary pages
#if 1
typedef bool EN_OUTPUTFILE_ID;
#define	OID_SCHED true
#define	OID_SUMMARY false
#else
enum EN_OUTPUTFILE_ID
{
	OID_SCHED,
	OID_SUMMARY
};
#endif

//-----------------------------------------------------------------------------------
enum EN_OUTPUT_FILES : char
{
	OF_SCHED=0,
	OF_GW,           ///< groups / week
	OF_GS,           ///< groups / subject
	OF_SW,           ///< Subject / Week
	OF_IW,           ///< Instructor / Week
	OF_WT,           ///< worktime of instructors
	OF_WS,           ///< week summary
	OF_TABLES,
	OF_ICAL,
	OF_ID,           ///< Instructor Diary
	OF_STATS,        ///< stats
	OF_CONFLICTS,
	OF_DUMMY
};
//-----------------------------------------------------------------------------------
/// Type of event. See Params::GetCodeFromEventType()
enum EN_EVENT_TYPE : char
{
	ET_TEACH = 0
	, ET_DAY_OFF         ///< special event 1: day off
	, ET_SPECIAL
};
//-----------------------------------------------------------------------------------
/// Group type. See Params::GetCodeFromGroupType()
enum EN_GROUP_TYPE : char
{
	GT_ATOMIC=0,  ///< the smallest student group, can not be separated into subgroups.
	GT_CLASS,     ///< first level class group, can hold several atomic groups
	GT_DIV,       ///< division level class group, can hold several class groups

	GT_NONE       ///< this is used for Special Events
};

//-----------------------------------------------------------------------------------
/// Conflict Type
enum EN_CONFLICT_TYPE : char
{
	CT_GTS     ///< Group and Time Slot are the same
	,CT_ITS     ///< Instructor and Time Slot are the same
	,CT_RTS     ///< Room and Time Slot are the same
	,CT_DAY_OFF  ///< event is happening on a day off
	,CT_TOO_LONG
//	,CT_IUD      ///< Instructor Unavailable on this Day

	,CT_DUMMY
};

//-----------------------------------------------------------------------------------
/// Event category, used to classify what has been read in input file
enum EN_EVENT_CAT : char
{
	EC_VALID=0,
	EC_INVALID,
	EC_DUPE,
	EC_SPECIAL,
	EC_IGNORED,
	EC_ALL
};

//-----------------------------------------------------------------------------------
enum EN_MISSING_INFO : char
{
	MI_NOINSTR,
	MI_NOROOM,
	MI_NOSUBJ
};

//-----------------------------------------------------------------------------------
/// see Params::GetLinkOnWeekSchedule()
enum EN_LINK_TEXT : char
{
	LT_WEEK=0,
	LT_DIV,
	LT_PROVIDED
};

//-----------------------------------------------------------------------------------
/// Calendar type (for ical file generation)
enum EN_CALENDAR_TYPE { CAL_INSTRUCTOR, CAL_TRAINEES };

/// Just for clarity, as it is used lots of times
typedef std::vector<std::string> StringVector;

//-----------------------------------------------------------------------------------
/// Used to specify what is some index related to, in an object of type Event. Also used to
/**
specify indexes in input file, and the position corresponds to default position, i.e.
"week" will be assumed to be in first field and "day" in second.

This is used in Event::GetIndex(), but also in Params for input file column indexes
*/
enum EN_INFOTYPE : char
{
	IT_WEEK = 0,
	IT_DAY,
	IT_TIMESLOT,
	IT_GROUP,
	IT_TYPE,
	IT_SUBJECT,
	IT_ROOM,
	IT_INSTRUCTOR,
	IT_DURATION,
	IT_COMMENT,

	IT_MAXVALUE_INPUT_INDEX,

	IT_DIVISION,  ///< this one is only used in Event, not as input index
	IT_IGNORE     ///< this one is used only to tell that we request all events of a given group, see void DayEvents::GetEventList_d5()

};

inline
std::string
getString( EN_INFOTYPE it )
{
	std::string str;
	switch( it )
	{
		case IT_WEEK       : str = "IT_WEEK"; break;
		case IT_DAY        : str = "IT_DAY"; break;
		case IT_TIMESLOT   : str = "IT_TIMESLOT"; break;
		case IT_GROUP      : str = "IT_GROUP"; break;
		case IT_TYPE       : str = "IT_TYPE"; break;
		case IT_SUBJECT    : str = "IT_SUBJECT"; break;
		case IT_ROOM       : str = "IT_ROOM"; break;
		case IT_INSTRUCTOR : str = "IT_INSTRUCTOR"; break;
		case IT_DURATION   : str = "IT_DURATION"; break;
		case IT_COMMENT    : str = "IT_COMMENT"; break;
		case IT_MAXVALUE_INPUT_INDEX: str = "IT_MAXVALUE_INPUT_INDEX"; break;
		case IT_DIVISION   : str = "IT_DIVISION"; break;
		case IT_IGNORE     : str = "IT_IGNORE"; break;
		default:
			assert(0);
	}
	return str;
}
//-----------------------------------------------------------------------------------
/// Holds a pair of strings, that will be used to store:
/**
- time slots, expressed as the code used in input file and a human-readable value ("8h30")
- week days, expressed as a code used in input file ("M", for instance) and a human-readable value ("Monday")
*/
struct PairCodeValue
{
	std::string code;
	std::string value;
	PairCodeValue( const std::string& Code, const std::string& Value ) :code(Code),value(Value) {}
};

typedef PairCodeValue TimeSlot;
typedef PairCodeValue WeekDay;


/// holds one line of the input file, as a vector of strings and an line number (useful for reporting errors)
/**
Second element is the original line number, needed to report errors, because of potential empty lines.
*/
typedef std::pair<std::vector<std::string>,size_t> LineContent;


/// Used only in UserData::PrintEventList() to describe what part needs to get printed
struct EventPrintDetails
{
	bool instructor = true;
	bool subject = true;
	bool room = true;
	bool group = true;
	bool week_no = true;
};

/*
#define LOG_ERROR( a, b, c ) \
	{ \
		std::cerr << "ERROR in file: " << __FILE__ << ", line: " << __LINE__ \
		 << ", message: " << a \
		 << ", command string: " << b \
		 << ", value: " << c << std::endl; \
		throw "ERROR, see stderr file\n"; \
	}
*/

//-----------------------------------------------------------------------------------


#endif
