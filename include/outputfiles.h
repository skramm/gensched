/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HG_OUTPUTFILES_H
#define HG_OUTPUTFILES_H

#include <map>
#include <memory>         // for shared_ptr

#include "datatypes.h"
#include "htmlfile.h"

/// Output file smart pointer
/**
This is needed because you can not store std::ofstream in a container, because it is not copyable.
So instead, we store (smart) pointers.

ref: https://en.cppreference.com/w/cpp/memory/shared_ptr
*/
typedef std::shared_ptr<FileHTML> OutputFile_SP;


//-----------------------------------------------------------------------------------
/// Holds all the output html files (as smart pointers) in a std::map
class OutputFiles
{
	public:
		void AssignNames();
		/// Returns a (shared) pointer on file (actually a \c OutputFile_SP)
		OutputFile_SP GetFile( EN_OUTPUT_FILES of )
		{
			return _m_files[of];
		}
	private:
		std::map< EN_OUTPUT_FILES, OutputFile_SP > _m_files;
};
//-----------------------------------------------------------------------------------
#endif // OUTPUTFILES_H
