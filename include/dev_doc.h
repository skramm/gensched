/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// dev_doc.h


/**
\page p_codeconv Coding conventions

- uses TABS, not spaces (much more consistent) for indentation
- all C++ standard library components are prefixed with std:: (no "<tt>using namespace std;</tt>"), EXCEPT in main.cpp and in test code
- all classes/types have a Uppercase first letter, all variables are camelCase
- all enum types start with EN_, all enum values start with a common prefix
- all attributes (class members) start with a '_'
- all std::vector variables are prefixed with v_ (consequently, class-member vectors start with _v_)
- all globals are prefixed with g_, and are located in globals.cpp
- all header files have include guards in the form HG_{filename}_H, all caps
*/

/**
\page p_param_handling Parameter handling

At start, first the program checks if the -p option is given on command line, to get the name of the configuration file.
If a file is given, it is read first, and parameters are set accordingly to its content.
Finally, the rest of the command-line is parsed, so any option given on command-line will \b override any option given in the configuration file.

<hr>

Below is a flowchart of the runtime (quite outdated, however...):
\image html build/img/ditaa_3.png



*/
