/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// strings.h

#ifndef HG_STRINGS_H
#define HG_STRINGS_H

#include "datatypes.h"
#include <cctype> // for std::toupper

#include <boost/preprocessor.hpp>

/**
The following is a hack to build a set of strings from enum values. Taken from
http://www.gamedev.net/topic/437852-c-enum-names-as-strings/

It allows having a language file with these identifiers as keys, regardless of the order in which they are given
*/
#define TO_STR(unused,data,elem) BOOST_PP_STRINGIZE(elem) ,

#define STRING_SEQ \
	(S_WEEK) \
	(S_WEEK_SHORT) \
	(S_DAY) \
	(S_DAYS) \
	(S_TIME_SLOT) \
	(S_LINKS_TOP) \
	(S_LINKS_NEXT) \
	(S_LINKS_PREV) \
	(S_LINKS_INDEX) \
	(S_LoC) \
	(S_MI) \
	(S_C_RTS) \
	(S_C_ITS) \
	(S_C_GTS) \
	(S_C_DAY_OFF) \
	(S_C_TOO_LONG) \
	(S_GenOn) \
	(S_Table_IS) \
	(S_Table_IR) \
	(S_Table_SR) \
	(S_Table_SUMS) \
	(S_Instr) \
	(S_Subj) \
	(S_Room) \
	(S_MI_NI) \
	(S_MI_NR) \
	(S_MI_NS) \
	(S_EVENT) \
	(S_NB_EVENTS_TOTAL) \
	(S_NB_EVENTS_VALID) \
	(S_NB_EVENTS_INVALID) \
	(S_NB_EVENTS_DUPE) \
	(S_NB_EVENTS_IGNORED) \
	(S_NB_EVENTS_CONSIDER) \
	(S_NB_EVENTS_TEACH) \
	(S_NB_EVENTS_SPECIAL) \
	(S_NB_EVENTS_COMMENTED) \
	(S_IGNORED_WEEKS) \
	(S_SELECTED_WEEKS) \
	(S_NB_CONFLICTS) \
	(S_NB_DAYS) \
	(S_CONFLICTS) \
	(S_CONFLICTS_MSG) \
	(S_LOE) \
	(S_EVENT_TYPE) \
	(S_GROUP_TYPE) \
	(S_GROUP) \
	(S_GR_DIV) \
	(S_GR_CLASS) \
	(S_GR_ATOMIC) \
	(S_INDEX) \
	(S_NONE) \
	(S_PER_WEEK) \
	(S_FN_GS) \
	(S_FN_GW) \
	(S_FN_SW) \
	(S_FN_IW) \
	(S_FN_WS) \
	(S_FN_SCHED) \
	(S_FN_I_WORKTIME) \
	(S_FN_I_CALENDAR) \
	(S_FN_TABLES) \
	(S_FN_STATS) \
	(S_FN_ICAL) \
	(S_FN_CONFLICTS) \
	(S_FN_CUMSUM) \
	(S_MEAN) \
	(S_SUM) \
	(S_SUMS) \
	(S_SUM_PER_DIV) \
	(S_SUM_PER_SUBJ) \
	(S_SUM_PER_INSTR) \
	(S_STATS_PER_GROUP) \
	(S_DAY_OFF) \
	(S_ERRORS) \
	(S_EMPTY_SLOTS) \
	(S_EMPTY_SLOTS_ABBREV) \
	(S_SPECIAL_EVENT) \
	(S_SPECIAL_EVENT_ABBREV) \
	(S_ICAL_TXT) \
	(S_ICAL_TRAINEES) \
	(S_ICAL_INSTRUCTORS) \
	(S_CW_LINE) \
	(S_VOL_PER_WEEK) \
	(S_VOL_PER_WEEK_C) \
	(S_MAX_VALUE)        // this value MUST be the last one (used for enumerating)

enum EN_STRING_ID { BOOST_PP_SEQ_ENUM(STRING_SEQ) };

//-----------------------------------------------------------------------------------
/// Holds strings that appear in output files. They are initialized with english, and then read in a language file if requested
struct Strings
{
	Strings();
	bool Read( std::string fname );
	void Set( EN_STRING_ID id, const char* s )
	{
		_v_strings.at(id) = s;
	}

	std::string GetTxt( EN_STRING_ID id, bool CapitalizeFirstLetter=false ) const
	{
		if( !CapitalizeFirstLetter )
			return _v_strings[id];
		std::string temp = _v_strings[id];
		temp[0] = std::toupper( temp[0] );
		return temp;

	}
	void Dump( std::ostream& f ) const;
	private:
		StringVector _v_strings;

};
//-----------------------------------------------------------------------------------

#endif

