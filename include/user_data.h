/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// user_data.h

#ifndef HG_USER_DATA_H
#define HG_USER_DATA_H

#include "week.h"
#include "vol_ref.h"
#include "outputfiles.h"

#if 0
#include <tuple>

//-----------------------------------------------------------------------------------
/// a container for instructor unavailability
struct INSTR_UNAVAIL
{
	enum En_DayPeriod {
		DP_AM, ///< Day Period AM (morning)
		DP_PM, ///< Day Period PM (afternoon)
		DP_ALL ///< Day Period ALL day
	};
	std::string _instr; ///< instructor code
	std::vector<size_t> _v_days_unav; ///< holds the days indexes where the instructor is unavailable, for all the weeks
	std::vector< std::tuple<size_t,size_t,En_DayPeriod> > _v_single_days; ///< holds the days where the instructor is unavailable, under the form (day_index, week_num, Day period)

/*	INSTR_UNAVAIL( const std::string& s ) : _instr(s)
	{
	}*/
};

//-----------------------------------------------------------------------------------
class INSTRUCTOR_AVAILABILITY
{
	private:
		std::vector<INSTR_UNAVAIL> _v_unav;
	public:

		bool InstructorIsAvailable( const Event&  ) const;

};

#endif

class FileHTML;
class Params;

//-----------------------------------------------------------------------------------
#ifdef EXPERIMENTAL_SESSION_ORDER
struct SessionCounters
{
	typedef std::map<std::string,size_t> SCPG; ///< Session Counter Per Group (key: group name)
	typedef std::map<std::string,SCPG>   SCPS; ///< Session Counter Per subject (key, subject)

	private:
		SCPS counters;
//		std::array<3,std::vector<SCPS>> v_counters; ///< for each group. 3, one for each group type
	public:
		size_t getCounter( std::string subject, EN_GROUP_TYPE gt )
		{
			assert( gt != GT_NONE );
//			return v_counters[gt]
		}
};
#endif
//-----------------------------------------------------------------------------------
/// Holds all the data read in the input CSV file, in a relational form (indexes)
/**
- holds three sets that are holding strings for instructors, rooms and subjects. Read and build from input data file.
- holds a set of weeks, each of them holding the events.
*/
struct UserData
{
	friend class Summary;
	friend void CreateHelpFile( const UserData&, const GroupSets&, const Params& );

	private:
		StringVector            _v_instructors;     ///< instructors. Build from data
		StringVector            _v_rooms;
		StringVector            _v_subjects;
		std::vector<WeekEvents> _v_weeks;           ///< holds weeks, that hold the days, that hold the events.
		RefVolume               _volref;            ///< reference volume for the courses

/// a map storing for a given subject (map key) the list of instructors that are involved (map value).
/**
	Used to show in cells holding a subject that list as a html-title element (for mouse hovering)
*/
		std::map<std::string, StringVector> _m_sub_instr;

		bool _dataHasSpecialEvents = false;

	public:
/// holds for a given week number (map key), the corresponding index in vector _v_weeks (map value).
/**
	This is needed when generating ical files from events. As these hold only week numbers, we need a way to get the week from that value.
*/
		std::map<int,size_t>     _week_indexes;

		static Params* sp_params;

//	INSTRUCTOR_AVAILABILITY  _ia;

	public:
		void Init();
		size_t GetNbWeeks() const { return _v_weeks.size(); }
		WeekEvents&       GetWeek( size_t idx ) { assert( idx<GetNbWeeks() ); return _v_weeks[idx]; }
		const WeekEvents& GetWeek( size_t idx ) const { assert( idx<GetNbWeeks() ); return _v_weeks[idx]; }
		size_t GetWeekIndex( int week_nb ) const;
		void SetWeeks( const std::vector<size_t>& v_num_weeks );
		bool HoldsWeek( int week_no ) const;

		void DumpData( std::ostream& ) const;

		bool ReadRefVol( const std::string& fname, char delim )
		{
			return _volref.readInFile( fname, delim );
		}

		const Event* GetEventFromId( size_t Id ) const;

//		void PrintPage_WeekSummary( bool OnePagePerDiv ) const;

//		void PrintPage_WeekSummary( const Params& params ) const;
//		void PrintPage_Schedule( const Params& params ) const;
		void PrintPage_Week( const Params&, EN_OUTPUTFILE_ID ) const;

		void PrintPage_GroupsSubjects() const;

		void PrintPage_Conflicts() const;

		size_t GetNbItems( EN_INFOTYPE ) const;
		size_t GetNbDays( EN_INFOTYPE it, size_t idx ) const;

		void BuildLists( const std::vector<LineContent>& in_table );

		Triplet GetVolumeT( EN_INFOTYPE it, size_t idx ) const;
		void PrintLists() const;

		std::string GetItem( EN_INFOTYPE it, size_t idx ) const;
		size_t GetIndexFromString( EN_INFOTYPE it, const std::string& value ) const;

		Triplet GetVolume2Cond( EN_INFOTYPE it, size_t idx, size_t ag_idx ) const;

		std::vector<const Event*> GetEventList_5( EN_INFOTYPE it, size_t idx, size_t group_idx, EN_GROUP_TYPE ) const;
		std::vector<const Event*> GetEventList_2( EN_INFOTYPE it, size_t idx ) const;
		std::vector<const Event*> GetEventListHoldingWeek( std::vector<const Event*> v_in, int week_num ) const;

		void GenerateCalendars() const;
		void PrintPage_InstrDiary() const;
		bool AddSubjectsIfNeeded();

		void PrintPage_GroupWeek( const GroupSets& ) const;
		void PrintPage_SubjectPerWeek() const;
		void PrintPage_InstructorPerWeek() const;

		void AddToSI_Map( const Event& );
		std::string GetInstructorString( int subj ) const;

		std::vector<bool> FindDataWithVolume( EN_INFOTYPE ) const;
		std::vector<bool> FindDataWithSE( EN_INFOTYPE ) const;

		void PrintFirstHeaderLine( FileHTML&, const StringVector* p_col, const std::vector<bool>& PrintSubjectColumn, const std::vector<bool>& columnHasSpecialEvent, bool printFirstColumn, int rowhw, bool PrintSum, bool printSubjectLinks=false ) const;
		void FindIfSpecialEvents();

	private:
		const StringVector* GetPointer( EN_INFOTYPE it ) const;

//		void GenerateWeek_Schedule( OutputFile_SP&, const Params&, int div_idx=-1 ) const;
//		void GenerateWeek_Summary(  OutputFile_SP&, const Params&, int div_idx=-1 ) const;
		void GenerateWeek(  OutputFile_SP&, EN_OUTPUTFILE_ID, const Params&, int div_idx=-1 ) const;

		void PrintHeaderWeekLinks( FileHTML&, int current_week, EN_OUTPUTFILE_ID, bool onePagePerWeek=false, int div_idx=-1 ) const;
		void PrintHeader( FileHTML&, size_t widx, EN_OUTPUTFILE_ID, bool IsNextYear=false, int div_idx=-1 ) const;
		void GenerateStringVector( const std::vector<LineContent>& in_table, StringVector& v_outset, size_t idx, const std::string& unknown );
		void GenerateIcal( const std::string& name, const std::vector<const Event*>& v_out, EN_CALENDAR_TYPE agt ) const;
		void SaveCalendar( FileHTML&, EN_CALENDAR_TYPE agt ) const;
		void PrintTable_PlanningPerDiv( FileHTML&, int div_idx, EN_INFOTYPE, bool doCumulatedSum ) const;
		void PrintItemList(  FileHTML&, int sub_idx, const char* htmlid, const std::string& text, EN_GROUP_TYPE gt ) const;
		void PrintEventList( FileHTML&, std::vector<const Event*>& vt, EventPrintDetails, int c=-1 ) const;

		void PrintPage_PerWeek( OutputFile_SP, EN_INFOTYPE ) const;
		void PrintVolumePerWeek( OutputFile_SP fp, EN_INFOTYPE infotype, bool doCumulatedSum ) const;

		void sw_ProcessTripletLine(   FileHTML&, EN_INFOTYPE, int subj, bool doCumulatedSum, const std::vector<Triplet>& vol ) const;
		void sw_ProcessSubjectLine_2( FileHTML&, bool doCumulatedSum, const std::vector<Triplet>& vvpw, EN_GROUP_TYPE gt, const std::string& cl ) const;

		void PrintWeekUsage_line1( FileHTML& ) const;
		void PrintWeekUsage_line2( FileHTML& ) const;
		void generateVolPerWeekPlot() const;
};

//-----------------------------------------------------------------------------------

#endif // USER_DATA_H
