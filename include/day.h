/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// day.h

#ifndef HG_DAY_H
#define HG_DAY_H

#include "event.h"

class Conflict;
class Params;

//-----------------------------------------------------------------------------------
/// Holds a list of events in the same day
class DayEvents
{
	private:
		uchar              _day_index; ///< index of day: 0 for monday, 5 for friday
		std::vector<Event> _v_events;  ///< holds the events on that day (NOT ORDERED)
		bool               _is_off;    ///< true if day is off
		std::string        _se_text;   ///< special event text \todo actually, this is used ONLY for special events of type "Day off"
		size_t             _nbSpecialEvents = 0;

	public:
		static Params* sp_params_d;  ///< pointer on parameters

	public:
		DayEvents( size_t idx ) : _is_off(false) { assert( idx<256); _day_index = static_cast<uchar>(idx); }

		std::vector<const Event*> GetEventsOnTS( size_t time_slot_idx ) const;

		const Event* IsOff() const;

		void DumpDay( std::ostream& ) const;
		size_t GetNbSpecialEvents_d() const { return _nbSpecialEvents; }

		Event GetEvent( size_t idx ) const;
		bool  AddEventToDay( Event& e, std::vector<Conflict>& v_week_conflicts );
		const Event* GetEventFromId_d( size_t Id ) const;

		Triplet GetVolumeT_d1( size_t ag_idx ) const;
		Triplet GetVolumeT_d2( EN_INFOTYPE it, size_t idx ) const;

		Triplet GetVolume2Cond_d( EN_INFOTYPE it, size_t idx, size_t ag_idx ) const;

		Triplet GetVolPerDiv_d( int div_idx, EN_INFOTYPE, size_t idx ) const;

		size_t  GetNbEmptySlots_d( const std::string& group ) const;
		size_t  GetNbEmptySlots_d( size_t ag_idx ) const;
		void AddToIndexList( EN_INFOTYPE it, std::vector<size_t>& v_out ) const;

		void GetEventList_d5( EN_INFOTYPE it, size_t idx, size_t group_idx, EN_GROUP_TYPE gt, std::vector<const Event*>& v_out ) const;
		void GetEventList_d3( EN_INFOTYPE it, size_t idx, std::vector<const Event*>& v_out ) const;

};
//-----------------------------------------------------------------------------------
inline
Event DayEvents::GetEvent( size_t idx ) const
{
	assert( idx < _v_events.size() );
	return _v_events.at(idx);
}

//-----------------------------------------------------------------------------------

#endif // DAY_H
