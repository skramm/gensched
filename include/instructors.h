/*
	This file is part of gensched,
	a command-line utility that generates html schedule pages from input data file holding a list of events
    Copyright (C) 2013-2018 Sebastien Kramm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file
\brief Declares class INSTRUCTOR that holds instructor names and category (UNUSED AT PRESENT)

*/

#ifndef HG_INSTRUCTORS_H
#define HG_INSTRUCTORS_H

#include "helper_functions.h"

//-----------------------------------------------------------------------------------
class INSTRUCTORS
{
	private:
		std::string _code; ///< name, code, whatever string you want to identify the instructor
		std::string _real; ///< real name
		char        _cat;  ///< arbitrary category key code

	public:
		bool Read( std::string fname );
};
//-----------------------------------------------------------------------------------
inline
bool INSTRUCTORS::Read( std::string fname )
{
	std::vector<StringVector> table;
	if( !ReadCSV( fname, table ) )
	{
		std::cerr << "* unable to read file " << fname << std::endl;
		return false;
	}
	size_t count = 0;
	for( const auto& v: table )
	{
		if( v.size() != 2 )
		{
			std::cerr << " * Error: elem " << count << " has " << v.size() << " fields, should have 2\n";
			for( const auto& s: v )
				std::cerr << "field=" << s << std::endl;
			return false;
		}
		_code = v[0];
		_cat  = v[1][0];
		count++;
	}
	return true;
}
//-----------------------------------------------------------------------------------

#endif // HG_INSTRUCTORS_H
